<?php


Route::get('/', 'HomeController@index')->name("home");
Route::get('/test', 'TestController@test')->name("test");
Route::get('dashboard', 'HomeController@index')->name("dashboard");

Route::get('api/arrivals', 'HomeController@arrivals');

Route::prefix('customer')->group(function () {
    Route::get('create', "CustomerController@create")->name("createCustomer");
    Route::get('list', "CustomerController@list")->name("listCustomer");
    Route::get('edit/{id}', "CustomerController@edit")->name("editCustomer");
    Route::post('update', "CustomerController@update")->name("updateCustomer");
    Route::post("file/update-upload", "CustomerController@updateFile")->name("updateCustomerFile");
    //eliminar registro de forma lógica
    Route::post("delete", "CustomerController@delete")->name("customerDelete");
    //api
    Route::get("api/customer", "CustomerController@apiCustomer")->name("api.customer");
    //crear cliente: datos del cliente
    Route::post("file/upload", "CustomerController@storeFiles")->name("storeCustomerFile");
    Route::post("create", "CustomerController@store")->name("storeCustomer");
    //loads 
    Route::get("get-note-client-for-customer", "CustomerController@getNoteClient")->name("getNoteClient");
    Route::get("get-note-client-for-customer/{id}", "CustomerController@getNoteClientEdit")->name("getNoteClientEdit");
    //formatos
    Route::get("formato-nq/{id}", "CustomerController@formatoNq")->name("formatoNq");
    Route::get("formato-qa/{id}", "CustomerController@formatoQa")->name("formatoQa");
    Route::get("formato-qb/{id}", "CustomerController@formatoQb")->name("formatoQb");

    //confirmations
    Route::get("confirmation-package-date/{id}", "CustomerController@confirmacionPackageWithDate")->name("confirmationPackageWithDate");
    //confirmation open date
    Route::get('confirmation-open-date/{id}', "CustomerController@confirmationOpenDate")->name("confirmationOpenDate");
    Route::get('confirmation-with-date-hotel/{id}', "CustomerController@confirmationWithDateHotel")->name("confirmationWithDateHotel");
    Route::get('get-notifications', "CustomerController@getNotificationsToday")->name("getNotificationsToday");
    Route::post('change-notification-status/{move_id}', "CustomerController@changeNotificationStatus")->name("changeNotificationStatus");

    Route::get("get-states-from-country/{country_id}", 'CustomerController@getStates')->name("getStatesFromCountry");
});

/* Rutas para proyectos */
Route::prefix('project')->group(function () {
    Route::get('list/{origen?}', 'ProjectController@list')->name('proyectList');
    Route::get('api-project-list/{origen?}', 'ProjectController@apiProjectList')->name('api.projectList');
    //pasar proyecto a ejecutivo
    Route::post("make-project-to-customer/{email_id?}", "ProjectController@assignCustomerToExecutive")->name("assignCustomerToExecutive");
    Route::get("call", "ProjectController@call")->name("call");
    Route::get("answer-call", "ProjectController@answerCall")->name("answerCall");

    Route::get('get-email-tracings/{email_id}', 'ProjectController@getEmailTracing')->name('getEmailTracing');
    Route::post('proyect-history/{email_id}/create', 'ProjectController@storeEmailTracing')->name('storeEmailTracing');

    //metodo para subir datos a contactos del crm
    Route::get('upload', 'ProjectController@upload')->name('proyectUpload');
    Route::post('upload', 'ProjectController@postUpload');

    //obtener datos
    Route::get("get-email-info/{email_id}", "ProjectController@getEmailInfo")->name("getEmailInfo");
    Route::post("get-email-info/{email_id}", "ProjectController@getEmailInfo")->name("getEmailInfo");
    //eliminar dato de contactos
    Route::post("delete/{email_id}", "ProjectController@delete")->name("proyectDelete");
});

//login routes
Route::group(['prefix' => 'auth'],function(){
    Route::get("login", "AuthController@getLogin")->name("login");
    Route::post("login", "AuthController@postLogin")->name("postLogin");
    Route::get("logout", "AuthController@logout")->name("logout");
    //resetear contraseña
    //Route::get("password/reset", "Auth\ResetPasswordController@showResetForm")->name('getPasswordReset');

    //Route::get("get-web-page", "TestController@getWebPage")->name("getWebPage");
});


Route::prefix("admin")->group(function () {
    //permisos
    Route::get("permission-list", "AdminController@permissionList")->name("admin.permissions");
    Route::put("permission-update/{id}", "AdminController@updatePermission")->name("admin.updatePermission");
    //tipos de usuario
    Route::get("role-list", "AdminController@roleList")->name("admin.roleList");
    Route::put("role-update/{group_id}", "AdminController@updateRole")->name("admin.updateRole");
    Route::get("role-create", "AdminController@createRole")->name("admin.createRole");
    Route::post("role-create", "AdminController@storeRole")->name("admin.storeRole");
    //*Administracion de ejecutivos**/
    Route::get('executive-list', "AdminController@executiveList")->name("admin.listExecutive");
    Route::get('executive/{id}', 'AdminController@executiveEdit')->name('admin.editExecutive');
    Route::put('executive/{id}', 'AdminController@executiveUpdate');
    Route::post('executive/change-status/{id}', 'AdminController@executiveChangeStatus')->name('admin.changeStatusExecutive');
});
//usuarios
Route::prefix("user")->group(function() {
    Route::get("create", "UserController@create")->name("createUser");
    Route::post("create", "UserController@store")->name("storeUser");
    Route::get("list", "UserController@list")->name("listUser");
    Route::post("status/{login}", "UserController@changeStatus")->name("changeStatusUser");
    Route::post("role/{login}", "UserController@changeRole")->name("changeRoleUser");
    Route::delete("delete/{login}", "UserController@delete")->name("deleteUser");
    Route::get("edit/{login}", "UserController@edit")->name("editUser");
    Route::put("edit/{login}", "UserController@update")->name("updateUser");
});

/*sección de survey*/
Route::prefix('survey')->group(function(){
    Route::get('/', 'SurveyController@list')->name('listSurvey');
    Route::get('create', 'SurveyController@create')->name('createSurvey');
    Route::post('create', 'SurveyController@store');
    Route::get('edit/{id}', 'SurveyController@edit')->name('editSurvey');
    Route::put('edit/{id}', 'SurveyController@update');

    Route::get("api-survey-list", "SurveyController@apiSurveyList")->name("api.surveyList");

    Route::post("delete/{id}", "SurveyController@delete")->name("deleteSurvey");
});

/*seccion de reportes*/
Route::prefix('reports')->group(function(){
    //aqui se muestran los reportes
    Route::get('/', 'ReportController@index')->name('reportCustomer');
    Route::get('statics', 'ReportController@statics')->name('reportStatics');
    Route::get('print-statics', 'ReportController@printStatics')->name("sendReportStatic");

    //aqui se va la info de los reportes
});

Route::prefix('origen')->group(function(){
   Route::get('origen', 'OrigenController@listOrigen')->name('origenList');
   Route::get('origen/edit/{id}', 'OrigenController@editOrigen')->name('origenEdit');
   Route::post('origen/edit/{id}', 'OrigenController@updateOrigen');
   Route::get('origen/create', 'OrigenController@createOrigen')->name("origenCreate");
   Route::post('origen/create', 'OrigenController@storeOrigen');
   Route::get("api/list", "OrigenController@apiListOrigen")->name("api.origenList");

   Route::get('suborigen/list/{origen_id?}', 'OrigenController@listSuborigen')->name('subOrigenList');
   Route::get('suborigen/edit/{id}', 'OrigenController@editSubOrigen')->name('subOrigenEdit');
   Route::post('suborigen/edit/{id}', 'OrigenController@updateSubOrigen');
   Route::get('suborigen/create/{origen_id?}', 'OrigenController@createSubOrigen')->name('subOrigenCreate');
   Route::post('suborigen/create/{origen_id?}', 'OrigenController@storeSubOrigen');
   Route::get('api/list/suborigen/{origen_id?}', 'OrigenController@apiListSubOrigen')->name('api.subOrigenList');
   Route::post('chane-status/{id}', 'OrigenController@changeStatusSubOrigen')->name('subOrigenChangeStatus');

   Route::get("load-suborigen/{origen_id}", "OrigenController@loadSubOrigen")->name("subOrigenLoad");

});