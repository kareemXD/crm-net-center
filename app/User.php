<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class User extends Authenticatable
{
    use Notifiable;

    protected $connection = "intra";
    protected $table = "users";
    protected $fillable = [
       'full_name', 'username', 'active', 'email', 'password', 'remember_token', 'access_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['isSuperAdmin'];

    public function roles()
    {
        return $this->belongsToMany("App\l", "role_users");
    }

    public function roleUsers()
    {
        return $this->hasMany("App\RoleUser");
    }

    public function applications()
    {
        return $this->belongsToMany("App\Application", "application_users");
    }

    //mutator
    public function getIsSuperAdminAttribute()
    {
        $role = $this->roles()->first();
        if (!is_null($role))
        {
            return ($role->name == "super")? true : false;
        }

        return false;
        
    }


    public function hasAccess($application_id)
    {
        if (!is_null($this->applicationUsers()->where("application_id", $application_id)->first()))
        {
            return true;
        }else{
            return false;
        }
    }

    public function applicationUsers()
    {
        return $this->hasMany("App\ApplicationUser");
    }

    public function applicationUser()
    {
        return $this->hasOne("App\ApplicationUser");
    }

    public function hasAccessApp($access)
    {
        switch ($access) {
            case "delete":
                    if(Auth::user()->isSuperAdmin){
                        return true;
                    }else{
                        return false;
                    }
                break;
            
            default:
                    return false;
                break;
        }
    }
}
