<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Executive extends Model
{
    protected $table = "ejecutivos";
    protected $fillable = ["nombre", "login", "fecha_alta", "estatus", "nivel", "padre"];

    public $timestamps = false;
    protected $appends = ["email"];

    public function user()
    {
    	return $this->belongsTo(UserSafety::class, "login", "login");
    }

    public function getEmailAttribute()
    {
    	return !is_null($this->user)? $this->user->email : "";
    }
}
