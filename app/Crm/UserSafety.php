<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Notifications\Notifiable;
use App\Crm\ADMIN\Group;
use App\Crm\ADMIN\UserGroup;
use App\Crm\ADMIN\Permission;

class UserSafety extends Authenticatable
{
    use Notifiable;
    protected $table = "sec_seguridadusers";
    protected $fillable = ["login","pswd", "name", "email", "active","activation_code", "remember_token", "priv_admin"];
    public $incrementing=false;
    protected $primaryKey = "login";

    protected $appends = ["level_user"];
    protected $hidden = [
        'pswd', 'remember_token',
    ];
    //relationship
    public function executive()
    {
    	return $this->belongsTo("App\Crm\Executive", "login", "login");
    }

    public function roles()
    {
        return $this->belongsToMany(Group::class, "user_groups", "login", "group_id");
    }

    public function userRole()
    {
        return $this->hasOne(UserGroup::class, "login", "login");
    }

    public function userRoles()
    {
        return $this->hasMany(UserGroup::class, "login", "login");
    }
    

    public function hasAccessApp($access = null)
    {
        if(strtolower($this->login) == "kareem.lorenzana" || strtolower($this->login) == "david.nunez" ||  strtolower($this->login) == "david.nunez.grupo"){
            return true;
        }
        $permission = Permission::where('permission', $access)->first();
        if(is_null($permission)){
            return false;
        }

        if($this->userRoles()->whereIn('group_id',$permission->groups()->pluck('sec_seguridadgroups.group_id'))->count() > 0 ){
            return true;
        }

        return false;

    }

    public function getLevelUserAttribute(){
        if($this->login == "david.nunez" || $this->login == "david.nunez.grupo" || $this->login == "kareem.lorenzana")
        {
            return 100;
        }

        $level = (count($this->roles) > 0)? $this->roles[0]->level : 0;
        return $level;
    }


}
