<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = "actividades";
    protected $fillable = ["nombre"];
    public $timestamps = false;
}
