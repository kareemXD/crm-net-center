<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class SecGroup extends Model
{
    protected $table = "sec_seguridadgroups";
    protected $fillable = ["description"];
    protected $primaryKey = "group_id";

    public $timestamps = false;
}
