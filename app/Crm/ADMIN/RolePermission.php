<?php

namespace App\Crm\Admin;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = "group_permissions";
    protected $fillable = ["permission_id", "group_id"];

    //relations 
    public function permissions()
    {
    	return $this->hasMany("App\Crm\ADMIN\Permission", "permission_id");
    }

    public function permission() 
    {
    	return $this->belongsTo("App\Crm\ADMIN\Permission", "permission_id");
    }

    public function groups()
    {
    	return $this->hasMany("App\Crm\ADMIN\UserGroup", "group_id", "group_id");
    }

    public function group()
    {
    	return $this->belongsTo("App\Crm\ADMIN\UserGroup", "group_id", "group_id");
    }
}
