<?php

namespace App\Crm\ADMIN;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = "permissions";
    protected $fillable = ["permission", "description"];

    public function groups()
    {
    	return $this->belongsToMany("App\Crm\ADMIN\Group", "group_permissions","permission_id", "group_id");
    }

    public function groupPermissions(){
    	return $this->hasMany(RolePermission::class, 'permission_id');
    }

}
