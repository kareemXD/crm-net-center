<?php

namespace App\Crm\ADMIN;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = "user_groups";
    protected $fillable = ["login","group_id", "level"];

    public function users()
    {
    	return $this->belongsTo("App\Crm\UserSafety", "login");
    }
}
