<?php

namespace App\Crm\ADMIN;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "sec_seguridadgroups";
    protected $fillable = ["description"];
    protected $primaryKey = "group_id";

    public $timestamps = false;


    public function permissions()
    {
    	return $this->belongsToMany("App\Crm\ADMIN\Permission", "group_permissions", "group_id", "permission_id");
    }

    public function rolePermissions()
    {
    	return $this->hasMany("App\Crm\ADMIN\RolePermission", "group_id", "group_id");
    }
}
