<?php

namespace App\Crm\ADMIN;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = "user_permission";
    protected $fillable = ["login", "permission_id"];
}
