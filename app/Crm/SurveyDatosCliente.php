<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;
use App\Services\Encrypt;

class SurveyDatosCliente extends Model
{
	private $encrypt;
    public function __construct( array $attributes = array())
    {
        $this->encrypt = new Encrypt();
        parent::__construct($attributes);
    }

    protected $table = "survey_datos_clientes";
    protected $guarded = [];

    protected $appends = ["email_decrypt", "telefono_celular_decrypt", "telefono_casa_decrypt"];
    //relations
    public function survey(){
    	return $this->belongsTo(Survey::class, 'survey_id');
    }

    //obtener email desencryptado
    public function getEmailDecryptAttribute()
    {
    	return $this->encrypt->decrypt($this->email);
    }

    //telefono celular
    public function getTelefonoCelularDecryptAttribute()
    {
    	return $this->encrypt->decrypt($this->telefono_celular);
    }

    //Tlefono de casa
    public function getTelefonoCasaDecryptAttribute()
    {
    	return $this->encrypt->decrypt($this->telefono_casa);
    }

}
