<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class SecGroupApp extends Model
{
    protected $table = "sec_seguridadgroups_apps";
    protected $fillable = [
    	"group_id",
    	"app_name",
    	"priv_access",
    	"priv_delete",
    	"prov_update",
    ];
}
