<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "estatus";
    protected $fillable = ["nombre"];
    public $timestamps = false;
}
