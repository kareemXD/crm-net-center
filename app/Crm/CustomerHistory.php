<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class CustomerHistory extends Model
{
    protected $table = "customer_histories";
    protected $fillable = ["title", "old_value", "new_value", "username", "login", "customer_id"];
}
