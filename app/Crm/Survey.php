<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;
use StdClass;
use Auth;
class Survey extends Model
{
    protected $table = "surveys";
    protected $appends = ["survey_operation", "assign", "tarjeta_pago", "tarjeta_garantia"];


    protected $fillable =     [ 
        "comprobante_qa",
        "comentario_validacion",
        "fecha_primer_contacto" ,
        "executive_id",
        "origen_id",
        "vendedor",
        "folio",
        "a_visitado" ,
        "tomado_presentacion",
        "hace_cuanto",
        "se_hizo_socio",
        "numero_aprobacion",
        "requiere_factura",
        "ingreso_mensual" ,
        "estado_civil" ,
        "numero_tarjetas",
        "precio_total",
        "pagado",
        "saldo_pendiente",
        "comentarios",
        "desglose_paquete",
        "desglose_hoteleria",
        "comentarios_verificacion"
    ];



    public function surveyDatosClientes($params = null)
    {
    	if($params == "P" || $params== "A"){
            $surveyDatosClientes = SurveyDatosCliente::where('survey_id',$this->id)->where('tipo', $params)->first();
    		if(is_null($surveyDatosClientes)){
                return "";
            }
            return $surveyDatosClientes;

    	}
    	return $this->hasMany(SurveyDatosCliente::class, 'survey_id');
    }

    public function origen(){
        return $this->belongsTo(Origen::class);
    }
    public function surveyDatosCliente(){
        return $this->hasOne(SurveyDatosCliente::class);
    }

    public function executive()
    {
    	return $this->belongsTo(Executive::class, 'executive_id', 'id');
    }

    public function surveyDatosTarjetas($params = null)
    {
        if(strtoupper($params) == "G" || strtoupper($params)=="P")
        {
            $surveyDatosTarjeta = SurveyDatosTarjeta::where("survey_id", $this->id)->where("pago_garantia", strtoupper($params))->first();

            if(!is_null($surveyDatosTarjeta)){
                return $surveyDatosTarjeta;
            }
            $surveyDatosTarjeta = new StdClass();
            $surveyDatosTarjeta->numero_tarjeta_decrypt= "";
            $surveyDatosTarjeta->banco= "";
            $surveyDatosTarjeta->vencimiento_format = "00 / 0000";
            $surveyDatosTarjeta->tipo_tarjeta = "";
            $surveyDatosTarjeta->codigo = "";
            $surveyDatosTarjeta->survey_id = $this->id;
            return $surveyDatosTarjeta;
        }

        return $this->hasMany(SurveyDatosTarjeta::class, "survey_id", "id");
    }

    public function surveyDatosReservas()
    {
        $surveyDatosReservas =  $this->hasMany(SurveyDatosReserva::class, "survey_id", "id");
    }

    public function surveyDatosReserva()
    {
        return $this->hasOne(SurveyDatosReserva::class, "survey_id", "id");
    }

    //appends
    public function getSurveyOperationAttribute()
    {
        $surveyOperation = '';
        if(Auth::user()->hasAccessApp('survey_delete')){
            $surveyOperation .= '<a href="#" data-delete-route="'.route("deleteSurvey", $this->id).'" class="btn btn-danger" data-target="#modalDelete" data-toggle="modal"><i class="fa fa-trash"></i></a>';
        }

        $surveyOperation .= '<a class="btn btn-info" href="'. route('editSurvey', $this->id).'"><i class="fa fa-edit"></i></a>';

        return $surveyOperation;
    }

    public function getAssignAttribute()
    {
        return '';
    }

    //retornar los datos de la tarjeta completos o solo los 4 ultimos digitos
    public function getTarjetaPagoAttribute()
    {
        if($this->surveyDatosTarjetas("P") != null){
            $tarjeta = $this->surveyDatosTarjetas("P")->numero_tarjeta_decrypt;
            if(Auth::user()->hasAccessApp("survey_decrypt_card"))
            {
                return $tarjeta;
            }else{
                if(strlen($tarjeta) > 4){


                    $replaceString = substr($tarjeta, 0, (strlen($tarjeta) - 4 ));

                    $replaceString = strtr($replaceString, '0123456789', str_repeat('#', 10));
                    return $replaceString.substr($tarjeta, -4);
                }else{
                    return $tarjeta;
                }
            }
        }
    }

    //retornar los datos de la tarjeta completos o solo los 4 ultimos digitos
    public function getTarjetaGarantiaAttribute()
    {
        if($this->surveyDatosTarjetas("G") != null){
            $tarjeta = $this->surveyDatosTarjetas("G")->numero_tarjeta_decrypt;
            if(Auth::user()->hasAccessApp("survey_decrypt_card"))
            {
                return $tarjeta;
            }else{
                if(strlen($tarjeta) > 4){

                    $replaceString = substr($tarjeta, 0, (strlen($tarjeta) - 4 ));

                    $replaceString = strtr($replaceString, '0123456789', str_repeat('#', 10));
                    return $replaceString.substr($tarjeta, -4);
                }else{
                    return $tarjeta;
                }
            }
        }
    }

}
