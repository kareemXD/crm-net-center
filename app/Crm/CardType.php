<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    protected $table = "card_types";
    protected $fillable = ["code", "name"];

}
