<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class SurveyDatosReserva extends Model
{
    protected $table = "survey_datos_reservas";
    protected $guarded = [];

    public function survey(){
    	return $this->belongsTo(Survey::class);
    }
}
