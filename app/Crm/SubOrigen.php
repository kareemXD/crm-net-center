<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class SubOrigen extends Model
{
    protected $table ="sub_origenes";

    protected $fillable = ["name", "code", "origen_id", "status"];

    public function origen(){
    	return $this->belongsTo(Origen::class, "origen_id");
    }
}
