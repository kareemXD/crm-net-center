<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class EmailTracing extends Model
{
    protected $table = "email_tracings";
    protected $fillable = ["title", "comment", "executive_name", "login", "email_id"];

    public function email()
    {
    	return $this->belongsTo(Email::class, "email_id");
    }
}

