<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class CustomerSource extends Model
{
		protected $table = "customer_sources";
    protected $fillable = ["email_id", "customer_id", "login", "executive_id"];

    /*relations*/
    public function email()
    {
    	return $this->belongsTo(Email::class, "email_id");
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class, "customer_id", "id");
    }

    public function user()
    {
    	return $this->belongsTo(UserSafety::class, "login", "login");
    }

    public function executive()
    {
    	return $this->belongsTo(Executive::class, "executive_id", "id");
    }
}
