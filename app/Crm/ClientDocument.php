<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class ClientDocument extends Model
{
    protected $table = "documentos_clientes";
    protected $fillable = ["cmp_uploadfile", "iduno","file_status"];
    protected $primaryKey = "idmuchos";

    public $timestamps = false;

    public function customer()
    {
    	return $this->belongsTo("App\Crm\Customer", "iduno");
    }
}
