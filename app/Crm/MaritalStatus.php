<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class MaritalStatus extends Model
{
    public $timestamps = false;
    protected $table = "edo_civil";
    protected $fillable = ["nombre"];
}
