<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "pais";

    public $timestamps = false;

    protected $fillable = ["paisnombre"];

    //relation
    public function states()
    {
    	return $this->hasMany(State::class, "ubicacionpaisid", "id");
    }
}
