<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "estado";

    public $timestamps = false;

    protected $fillable = ["ubicacionpaisid", "estadonombre"];

    //relation
    public function country()
    {
    	return $this->belongsTo(Country::class, "ubicacionpaisid");
    }
}
