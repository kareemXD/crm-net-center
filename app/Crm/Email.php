<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
	  use SoftDeletes;
		//de esta tabla se obtienen los proyectos
    protected $table="correos";

    protected $fillable = ["ejecutivo", "correo","nombre" ,"fechaalta", "fechabaja", "estatus", "cliente", "lista_correos", "padre", "lista_externa", "telefono", "ip", "origen", "fecha_contacto", "comentario", "horacontacto"];
    
    protected $dates = ['deleted_at'];

    protected $appends = ["email_link"];
    public $timestamps = false;
    public function emailTracings()
    {
    	return $this->hasMany(EmailTracing::class, "email_id");
    }

    public function emailTracing()
    {
        return $this->hasOne(EmailTracing::class, "email_id");
    }

    public function getEmailLinkAttribute()
    {
        $emails = explode(',', $this->correo);
        $html = '';
        foreach($emails as $email)
        {
            $html .= '<a href = "mailto: '.strtolower(trim($email)).'">'.trim($email).'</a>';
        }
        return $html ;
    }
}
