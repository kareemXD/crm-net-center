<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;
use App\Services\Encrypt;

class SurveyDatosTarjeta extends Model
{
	private $encrypt;
    public function __construct( array $attributes = array())
    {
        $this->encrypt = new Encrypt();
        parent::__construct($attributes);
    }


    protected $table = "survey_datos_tarjetas";
    protected $guarded = [];
    protected $appends = ["vencimiento_format", "numero_tarjeta_decrypt"];

    public function getVencimientoFormatAttribute(){
    	$date = \DateTime::createFromFormat('Y-m-d', $this->vencimiento);
    	if($date->format('Y') > 2000){
    		return $date->format('m')." / ".$date->format('Y');
    	}
        return "";
    }

    public function getNumeroTarjetaDecryptAttribute(){
    	return $this->encrypt->decrypt($this->numero_tarjeta);
    }
}
