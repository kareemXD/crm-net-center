<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
    protected $table = "movimientos";
    protected $fillable = ["cliente", "actividad", "fecha", "estatus", "observaciones", "ejecutivo", "titulo", "padre"];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function executive()
    {
    	return $this->belongsTo("App\Crm\Executive", "ejecutivo");
    }

    public function customer()
    {
    	return $this->belongsTo("App\Crm\Customer", "cliente");
    }

    public function activity()
    {
    	return $this->belongsTo("App\Crm\Activity", "actividad");
    }

}
