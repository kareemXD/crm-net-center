<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Origen extends Model
{
    protected $table = "origen";

    protected $fillable = ["nombre", "code"];

    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo(UserSafety::class, "login", "login");
    }
}
