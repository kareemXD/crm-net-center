<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Services\Encrypt;
use DB;

class Customer extends Model
{
    private $encrypt;
    public function __construct( array $attributes = array())
    {
        $this->encrypt = new Encrypt();
        parent::__construct($attributes);
    }
	//nunca quitar el softDeletes 
	use SoftDeletes;
    protected $table = "clientes";
    protected $fillable = [
        //datos del cliente
        "ejecutivo", "origen", "vendedor", "nombre", "edad", "ocupacion", "nombre_co", "edad_co", "ocupacion_co", "tel_cel", "tel_casa", "tel_oficina", "extencion","estado", "email",
        //reservation data
         "checkin", "checkout", "plan", "adultos", "menores", "edadmenores", "folioreserva", "noches_en_certificado", "noches_en_hotel", "certi_hotel", "precio_total", "total_certificado", "total_hotel", "pagado","pendiente",
        //datos de tarjeta
         "four_digits","tarjeta","banco", "vencimiento1", "vencimiento2", "codigo", "tipotarjeta", "num_tarjetas", "visa", "mc", "amex", "ingreso_mensual", "estado_civil",
        //membership data
         "numero_contrato", "total_membresia", "solicita_factura", "habitacion", "comentario", "observaciones", "tipo_q", "fecha_venta", "tienefirmados", 'categoria',
         //defaults
         "fecha_alta", //se hace al crear
         "tipo",//se checa si es prospecto o cliente
         "fecha_modificacion",// se crea en el momento que se modifica
         "modificado_por", //id del ejecutivo
         "interesados",//por default poner "S"
         "padre", //id del ejecutivo padre
         //adicional
         'prefix_name',
        'prefix_name2',
        'has_vacation_ownership',
        'address',
        'take_presentation',
        'date_presentation',
        'homeowner',
        'many_times',
        'suborigen',
        'country',
        'state'
     ];

     protected $appends = ['tel_cel_decrypt', 'tel_casa_decrypt', 'tel_oficina_decrypt', 'email_decrypt', 'tarjeta_decrypt', 'plan_name', 'suborigenName', 'telefonos', 'emails'];

    //relations
    public function maritalStatus()
    {
        return $this->belongsTo("App\Crm\MaritalStatus", "estado_civil");
    }

    public function executive()
    {
    	return $this->belongsTo("App\Crm\Executive", "ejecutivo");
    }

    public function category()
    {
    	return $this->belongsTo("App\Crm\Category", "categoria", "code");
    }

    public function cardType()
    {
        return $this->belongsTo("App\Crm\CardType", "tipotarjeta", "code");
    }

    public function clientDocuments()
    {
        return $this->hasMany("App\Crm\ClientDocument", "iduno");
    }

    public function moves()
    {
        return $this->hasMany("App\Crm\Move", "cliente", "id");
    }

    public function customerHistories()
    {
        return $this->hasMany(CustomerHistory::class, "customer_id", "id");
    }

    public function subOrigenCliente(){
         return $this->belongsTo(SubOrigen::class, "suborigen","id");
    }
    //fin relations

    //scoope
    public function scopeList($query)
    {
        

        if(request()->input("filter-conexion") =="old"){
            $databaseName = DB::connection()->getDatabaseName();

            if(Auth::user()->hasAccessApp('customer_list_all'))
                return DB::connection("crm_netcenter")->table("clientes")->where('clientes.nombre', '!=', "")->leftJoin('ejecutivos', 'ejecutivos.id','=','clientes.ejecutivo')->leftJoin("".$databaseName.".categories as categories", "categories.code", "=", "clientes.categoria")->select('clientes.*', 'ejecutivos.nombre as executiveName', 'categories.name as categoryName');
            else if(Auth::user()->hasAccessApp('customer_list')){

                return DB::connection("crm_netcenter")->table("clientes")->where('clientes.nombre', '!=', "")->where('clientes.ejecutivo', Auth::user()->executive->id)->leftJoin('ejecutivos', 'ejecutivos.id','=','clientes.ejecutivo')->leftJoin("".$databaseName.".categories as categories", "categories.code", "=", "clientes.categoria")->select('clientes.*', 'ejecutivos.nombre as executiveName', 'categories.name as categoryName');
            }
        }else{
            if(Auth::user()->hasAccessApp('customer_list_all'))
                return $query->where('clientes.nombre', '!=', "")->leftJoin('ejecutivos', 'ejecutivos.id','=','clientes.ejecutivo')->leftJoin("categories", "categories.code", "=", "clientes.categoria")->select('clientes.*', 'ejecutivos.nombre as executiveName', 'categories.name as categoryName');
            else if(Auth::user()->hasAccessApp('customer_list')){

                return $query->where('clientes.nombre', '!=', "")->where('clientes.ejecutivo', Auth::user()->executive->id)->leftJoin('ejecutivos', 'ejecutivos.id','=','clientes.ejecutivo')->leftJoin("categories", "categories.code", "=", "clientes.categoria")->select('clientes.*', 'ejecutivos.nombre as executiveName', 'categories.name as categoryName');
            }
        }
    }

    //fin scoopes

    //Appends o modificadores (getters)
    public function getSuborigenNameAttribute(){
        if($this->suborigen == 0){
            return 'Sin Sub Origen';
        }

        return @$this->subOrigen->name;

    }
    public function getNumeroTarjetaAttribute()
    {
        if(Auth::user()->hasAccessApp('customer_card'))
        {
            $numero_tarjeta = $this->tarjeta_decrypt.$this->four_digits;
            $count = 0;
            $temporal = ""; 
            foreach(str_split($numero_tarjeta) as $ndex => $char){
                $temporal .= $char;
                if($count == 3){
                    $temporal .= " ";
                    $count = -1;
                }
                $count ++;
            }
            $numero_tarjeta = $temporal;

        }else{
            $numero_tarjeta = (strlen($this->four_digits) > 3 )? "#### #### #### ".$this->four_digits : (strlen($this->tarjeta_decrypt) > 4? substr($this->tarjeta_decrypt,  -4) : "");
        }

        return $numero_tarjeta;
    }

    public function getExpirationAttribute()
    {
        if(strlen($this->vencimiento2) >= 2 && strlen($this->vencimiento1) >= 2)
        {
            return $this->vencimiento1." / 20".$this->vencimiento2;
        }elseif(strlen($this->vencimiento2) >= 2)
        {
            return "00 / 20".$this->vencimiento2;
        }elseif(strlen($this->vencimiento1) >= 2)
        {
            return $this->vencimiento1." / 0000";
        }else{
            return "";
        }

    }

    //return decrypted email attribute 
    public function getEmailDecryptAttribute() {

        return $this->encrypt->decrypt($this->email);
    }

    //return decrypted tarjeta attribute 
    public function getTarjetaDecryptAttribute() {
        return $this->encrypt->decrypt($this->tarjeta);
    }

    //return decrypted Celular Attributte
    public function getTelCelDecryptAttribute() {
        return $this->encrypt->decrypt($this->tel_cel);
    }


    //return decrypted telefono casa attribute
    public function getTelCasaDecryptAttribute() {
        return $this->encrypt->decrypt($this->tel_casa);
    }

    //return decrypted telefono oficina attribute
    public function getTelOficinaDecryptAttribute() {
        return $this->encrypt->decrypt($this->tel_oficina);
    }

    public function getTelefonosAttribute() {
        $phones = [
            $this->tel_cel_decrypt,
            $this->tel_casa_decrypt,
            $this->tel_oficina_decrypt
        ];

        $whatsap_phones = [];

        foreach($phones as $index => $tel_phone)
        {
            $tel_cel = "";
            $tel_cel = str_replace(" ", "", $tel_phone);
            $tel_cel =  preg_replace("/[^0-9]/", "", $tel_cel );

            $country_code = "";
            if($this->country == "42"){
                $country_code = "521";
            }else{
                $country_code = "001";
            }
            if(strlen($tel_cel) == 10){
                $whatsap_phones[] = '<a target="_blank" href="https://wa.me/'.$country_code.$tel_cel. '" >'.$tel_phone.'</a>';
            }elseif(strlen($tel_cel) == 13 && strpos("521", substr($tel_cel,0, 3)) !== false){
                $whatsap_phones[] = '<a target="_blank" href="https://wa.me/'.$tel_cel. '" >'.$tel_phone.'</a>';
            }elseif(strlen($tel_cel) == 13 && strpos("001", substr($tel_cel,0, 3)) !== false){
                $whatsap_phones[] = '<a target="_blank" href="https://wa.me/'.$tel_cel. '" >'.$tel_phone.'</a>';
            }elseif(strlen($tel_cel) == 12 && strpos("52", substr($tel_cel,0, 2)) !== false ){
                $whatsap_phones[] = '<a target="_blank" href="https://wa.me/'.$country_code.substr($tel_cel, -10). '" >'.$tel_phone.'</a>';
            }elseif(strlen($tel_cel) == 13 && strpos("+52", substr($tel_cel,0, 3)) !== false){
                $whatsap_phones[] = '<a target="_blank" href="https://wa.me/'.$country_code.substr($tel_cel, -10). '" >'.$tel_phone.'</a>';
            }
            else{
                $whatsap_phones[] = " ".$tel_phone;
            }
           
        }

        $return_string = "";
        foreach($whatsap_phones as $whatsapp){
            $return_string .=  $whatsapp. " <br>";
        }

        return $return_string;
    }

    public function getEmailsAttribute() {
        $email = $this->email_decrypt;
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            return '<a target="_blank" href="mailto:'.$email.'" >'.$email.'</a>';
        }else{
            return $email;
        }
    }

    public function getDocumentCardAttribute() {
        if($this->tarjeta_decrypt == ""){
            return "";
        }

        $card =  $this->tarjeta_decrypt;
        $card = preg_replace('/\p{N}/', '*',$card);
        if(strlen($card) > 10) {
            $four_digits = (strlen($this->four_digits) === 4)? $this->four_digits : substr($this->tarjeta_decrypt, -4);
            return substr($card, 0, 12)." ".$four_digits;
        }
        return "";
    }

    public function getPlanNameAttribute()
    {
        return ($this->plan=="E")? "EUROPEO": "TODO INCLUIDO";
    }

    //setters 
    
    // set encrypt email data
    public function setEmailAttribute($value) {
        $this->attributes['email'] = $this->encrypt->encrypt($value);
    }

    //set encrypt tarjeta attributte
    public function setTarjetaAttribute($value) {
        $this->attributes['tarjeta'] = $this->encrypt->encrypt($value);
    }

    //set encrypt tel_casa attribute
    public function setTelCasaAttribute($value) {
        $this->attributes['tel_casa'] = $this->encrypt->encrypt($value);
    }

    //set encrypt tel_cel Attribute
    public function setTelCelAttribute($value) {
        $this->attributes['tel_cel'] = $this->encrypt->encrypt($value);
    }

    //set encrypt tel_oficina attribute
    public function setTelOficinaAttribute($value) {
        $this->attributes['tel_oficina'] = $this->encrypt->encrypt($value);
    }    
    
    //fin Appends o modificadores
    //funcion del modelo
    public function formatDate($date)
    {
        setlocale(LC_TIME, "es_MX");
        $daterw=date("d-m-Y", strtotime($date));
        return strftime("%d %b %Y",strtotime($daterw));
    }

    //fin
}
