<?php

namespace App\CRM;

use Illuminate\Database\Eloquent\Model;

class SecLogged extends Model
{
    protected $table = "sec_seguridadlogged";
    protected $fillable = ["login", "date_login", "sc_session", "ip"];
}
