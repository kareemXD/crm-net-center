<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ReportMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasAccessApp('report')) {

            return redirect()->route('home');
        }
        return $next($request);
    }
}
