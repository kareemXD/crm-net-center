<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class StoreEmailTracingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:60',
            'comment' => 'required|min:3|max:300',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El titulo es un campo obligatorio',
            'title.min' => 'El titulo debe contener al menos 3 caracteres',
            'title.max' => 'El titulo no debe pasar los  60 caracteres',
            'comment.required' => 'El seguimiento es un campo requerido',
            'comment.min' => 'El seguimiento debe contener al menos 3 caracteres',
            'comment.max' => 'El  seguimiento no debe pasar de los 300 caracteres',
        ];
    }
}
