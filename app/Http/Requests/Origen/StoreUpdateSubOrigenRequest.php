<?php

namespace App\Http\Requests\Origen;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreUpdateSubOrigenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if(is_null($this->route('id'))){
            return [
                'suborigen-name' => 'required|min:3|max:60',
                'suborigen-code' => 'max:1|unique:sub_origenes,code',
                'suborigen-origen-id' => 'required'
            ];
        }else{
            return [
                'suborigen-name' => 'required|min:3|max:60',
                'suborigen-code' => 'max:1|unique:sub_origenes,code,'.$this->route('id').',id',
                'suborigen-origen-id' => 'required'
            ];
        }


    }

    public function messages()
    {
        return [
            'suborigen-name.required' => 'El nombre es un campo obligatorio.',
            'suborigen-name.min' => 'El nombre debe contener al menos 3 caracteres.',
            'suborigen-name.max' => 'El nombre no debe pasar de los 60 caracteres',
            'suborigen-code.max' => 'El código solo puede tener un caracter',
            'suborigen-code.unique' => 'El código no debe repetirse.',
            'suborigen-origen-id.required' => 'Debe elegir un origen padre.'
        ];
    }
}
