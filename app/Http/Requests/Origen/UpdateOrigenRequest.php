<?php

namespace App\Http\Requests\Origen;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Validation\Rule;

class UpdateOrigenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(is_null($this->route('id'))){
            return [
                'origen-nombre' => 'required|min:3|max:60',
                'origen-code' => 'unique:origen,code|max:1',
            ];
        }else{
            return [
                'origen-nombre' => 'required|min:3|max:60',
                'origen-code' => 'unique:origen,code,'.$this->route('id').',id|max:1',
            ];
        }

    }

    public function messages()
    {
        return [
            'origen-nombre.required' => 'El nombre es un campo obligatorio',
            'origen-nombre.min' => 'El nombre debe contener al menos 3 caracteres',
            'origen-nombre.max' => 'El nombre no debe pasar los  60 caracteres',
            'origen-code.max' => 'El codigo solo puede tener un digito',
            'origen-code.unique' => 'El Código del origen debe ser único'
        ];
    }
}
