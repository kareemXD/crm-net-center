<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:sec_seguridadusers,login',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'active' => 'required',
            'user-group' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'El nombre de Usuario es obligatorio',
            'username.unique' => 'El nombre de usuario ya existe',
            'password.required' => 'El campo Contraseña es obligatorio',
            'password.min' => 'La contraseña debe ser mayor a 3 digitos',
            'password.confirmed' => 'La contraseña debe ser confirmada',
            'name.required' => 'El campo nombre es obligatorio',
            'name.min' => 'El nombre debe tener al menos 3 digitos',
            'email.required' => 'El correo es un campo obligatorio',
            'email.email' => 'El campo debe ser un correo válido',
            'active.required' => 'Debe definirse si el campo esta activo o no',
            'user-group.required' => 'Debe especificar un rol de usuario',
        ];
    }
}
