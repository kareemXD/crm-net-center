<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->request->get("password") != "" && !is_null($this->request->get("password"))) 
        {
            $validations = [
                'password' => 'required|min:3|confirmed',
                'password_confirmation' => 'required',
                'name' => 'required|min:3',
                'email' => 'required|email',
                'active' => 'required',
                'user-group' => 'required',
            ];
        }else{
            $validations = [
                'name' => 'required|min:3',
                'email' => 'required|email',
                'active' => 'required',
                'user-group' => 'required',
            ];
        }
        return $validations;
    }

    public function messages()
    {
        return [
            'password.required' => 'El campo Contraseña es obligatorio',
            'password.min' => 'La contraseña debe ser mayor a 3 digitos',
            'password.confirmed' => 'Las contraseñas deben ser iguales.',
            'name.required' => 'El campo nombre es obligatorio',
            'name.min' => 'El nombre debe tener al menos 3 digitos',
            'email.required' => 'El correo es un campo obligatorio',
            'email.email' => 'El campo debe ser un correo válido',
            'active.required' => 'Debe definirse si el campo esta activo o no',
            'user-group.required' => 'Debe especificar un rol de usuario',
        ];
    }
}
