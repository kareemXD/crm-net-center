<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Crm\ADMIN\Permission;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $permission_id = Permission::pluck("id")->toArray();

        return [
            'description' => 'required|min:3|max:50|unique:sec_seguridadgroups,description|',
            'permission' => ['required', 'array', Rule::in($permission_id)]
        ];
    }

    public function messages()
    {
        return [
            'description.min' => 'El grupo debe tener al menos 3 caracteres',
            'description.required' => 'El grupo es un campo obligatorio',
            'description.max' => 'El grupo no puede pasar de 50 caracteres.',
            'description.unique' => 'Ya hay un Grupo con el mismo nombre',
            'permission.required' => 'Elija al menos un permiso para el Grupo',
            'permission.array' => 'Ups! hay algo mal con los permisos',
            'permission.min' => 'Elija al menos un permiso para el Grupo',
            'permission.in' => 'Alguno de los permisos no es válido'
        ];
    }
}
