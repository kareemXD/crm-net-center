<?php

namespace App\Http\Controllers;

use App\Crm\MaritalStatus;
use App\Crm\Customer;
use App\Crm\Executive;
use App\Crm\Origen;
use App\Crm\CardType;
use App\Crm\Category;
use App\Crm\ClientDocument;
use App\Crm\Move;
use App\Crm\Activity;
use App\Crm\Status;
use App\Crm\UserSafety;
use App\User;
use Auth;
use DataTables as Datatables;
use Alert;
use Storage;
use PDF;
use Illuminate\Http\Request;
use App\Crm\CustomerHistory;
use App\Services\Encrypt;
use Mail;
use App\Mail\DeleteCustomerMail;
use Carbon\Carbon;
use DB;
use App\Crm\SubOrigen;

class CustomerController extends Controller
{
	private $maritalStatus, $customer, $executive, $user, $origen, $cardType, $clientDocument, $move, $activity, $status, $customerHistory, $encrypt, $userSafety, $subOrigen;

	//instanciar clases y objetos
	public function __construct(
		User $user,
		MaritalStatus $maritalStatus,
		Customer $customer,
		Executive $executive,
        Origen $origen,
        CardType $cardType,
        Category $category,
        ClientDocument $clientDocument,
        Move $move,
        Activity $activity,
        Status $status,
        CustomerHistory $customerHistory,
        Encrypt $encrypt,
        UserSafety $userSafety,
        SubOrigen $subOrigen
	){
        $this->middleware("auth");
		$this->user = $user;
		$this->maritalStatus = $maritalStatus;
		$this->customer = $customer;
		$this->executive = $executive;
        $this->origen = $origen;
        $this->cardType = $cardType;
        $this->category = $category;
        $this->clientDocument = $clientDocument;
        $this->move = $move;
        $this->activity = $activity;
        $this->status = $status;
        $this->customerHistory = $customerHistory;
        $this->encrypt = $encrypt;
        $this->userSafety = $userSafety;
        $this->subOrigen = $subOrigen;
	}

	public function list()
	{
        if(Auth::user()->hasAccessApp('customer_list_all')){
            $customers = $this->customer->list()->orderBy("fecha_alta", "desc")->take(25)->get();
            $executives = $this->executive->where("estatus", "A")->orderBy("nombre", "asc")->get();
            $categories = $this->category->orderBy("name", "asc")->get();
        }elseif (Auth::user()->hasAccessApp('customer_list')) {
            $customers = $this->customer->list()->orderBy("fecha_alta", "desc")->take(25)->get();
            $executives = Auth::user()->executive;
            $categories = $this->category->orderBy("name", "asc")->get();
        }else{
            return abort(403, 'Unauthorized action.');
        }

		return view("customer.list", compact("customers", "executives", "categories"));
	}

    public function create($project_id = null)
    {
        $countries =  DB::table("pais")->orderBy("paisnombre", "asc")->get();
        $states = DB::table("estado")->where("ubicacionpaisid",42)->get();
        $categories = $this->category->all();
        $cardTypes = $this->cardType->all();
        $origens = $this->origen->whereRaw("nombre != ''")->orderby("nombre", "ASC")->get();
    	$executives = $this->executive->orderby("nombre", "ASC")->get();
    	$maritalStatuses = $this->maritalStatus->orderby("nombre", "ASC")->get();
    	return view('customer.create', compact('maritalStatuses', 'executives', 'origens', 'cardTypes', "categories", "countries", "states"));
    }

    public function edit($id, Request $request)
    {
        $customer = $this->customer->find($id);

        if(!is_null($customer))
        {
            $countries =  DB::table("pais")->orderBy("paisnombre", "asc")->get();
            $states = DB::table("estado")->where("ubicacionpaisid", $customer->country)->get();
            $categories = $this->category->all();
            $cardTypes = $this->cardType->all();
            $origens = $this->origen->whereRaw("nombre != ''")->orderby("nombre", "ASC")->get();
            $suborigens = $this->subOrigen->where("origen_id", $customer->origen)->where("status", 1)->get();
            $executives = $this->executive->orderby("nombre", "ASC")->get();
            $maritalStatuses = $this->maritalStatus->orderby("nombre", "ASC")->get();
            if($request->ajax()) {
                return view('customer.edit-ajax', compact('maritalStatuses', 'executives', 'origens', 'cardTypes', "categories", "customer", "countries", "states", "suborigens"));
            }
            return view('customer.edit', compact('maritalStatuses', 'executives', 'origens', 'cardTypes', "categories", "customer",  "countries", "states", "suborigens"));
        }
    }

    public function storeFiles(Request $request)
    {
        if($request->ajax())
        {
            $file =  $request->file("file");
            $id = $request->input("customer_id");
            $name = str_random(10)."/".$request->input("name");
            $file->storeAs("public/documents", $name);
            $document = new ClientDocument;
            $document->cmp_uploadfile = $name;
            $document->iduno = $id;
            $document->file_status = true;
            $document->save();
            return json_encode([true, $name, $document->idmuchos]);
        }
        return json_encode(false);
    }

    public function store(Request $request){
        if($request->ajax())
        {
            $vencimiento1 = (strlen($request->input('card-expiration')) > 6 )? substr($request->input('card-expiration'), 0, 2): "";
            $vencimiento2 = (strlen($request->input('card-expiration')) > 6 )? substr($request->input('card-expiration'), -2) : "";
            $card = str_replace(' ', '', $request->input('customer-card'));
            $data = [
                'ejecutivo' => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                'origen' => $request->input("origen"),
                'vendedor' => ($request->input('seller') != null)? $request->input('seller'): "",
                'nombre' => ($request->input('customer-name') != null)? $request->input('customer-name'): "",
                'edad' => ($request->input('age') != null)? $request->input("age") : null,
                'ocupacion' => ($request->input('customer-job') != null)? $request->input("customer-job") : "",
                'nombre_co' => ($request->input('customer-name-co') != null)? $request->input('customer-name-co') : "",
                'edad_co' => ($request->input('age-co') != null)? $request->input('age-co') : null,
                'ocupacion_co' => ($request->input('customer-job-co') != null)? $request->input('customer-job-co') : "",
                'tel_cel' => ($request->input('cell-phone') != null)? $request->input('cell-phone'): "",
                'tel_casa' => ($request->input('home-phone') != null)? $request->input('home-phone') : "",
                'tel_oficina' => ($request->input("office-phone") != null)? $request->input("office-phone") : "",
                'extencion' => ($request->input('extension') != null)? $request->input('extension') : "",
                'estado' => ($request->input('customer-state') != null)? $request->input('customer-state') : "",
                'email' => ($request->input('customer-email') != null)? $request->input('customer-email') : "",
                'checkin' => (strlen($request->input('check-in')) >= 10)? substr($request->input('check-in'), 0,10)." 00:00:00" : null,
                'checkout' => (strlen($request->input('check-out')) >= 10)? substr($request->input('check-out'), 0,10)." 00:00:00" : null,
                'plan' => $request->input('customer-plan'),
                'adultos' => $request->input('customer-adults'),
                'menores' => $request->input('customer-children'),
                'edadmenores' => ($request->input('children-age') != null)? $request->input('children-age') : "",
                'folioreserva' => ($request->input('reservation-sheet') != null)? $request->input('reservation-sheet') : "",
                'noches_en_certificado' => $request->input('certification-nights'),
                'noches_en_hotel' => $request->input('hotel-nights'),
                'certi_hotel' => $request->input('reservation-type'),
                'precio_total' => $request->input('total-price'),
                'total_certificado' => $request->input('certificate-price'),
                'total_hotel' => $request->input('total-hotel'),
                'pagado' => $request->input('paid-out'),
                'pendiente' => $request->input('pending-payment'),
                'tarjeta' => ( $card != null && strlen($card) > 11)? substr($card, 0, 12) : "",
                'four_digits' => ($card != null && strlen($card > 3))? substr($card, -4) : "",
                'banco' => ($request->input('customer-bank') != null)? $request->input('customer-bank') : "",
                'vencimiento1' => $vencimiento1,
                'vencimiento2' =>$vencimiento2,
                'codigo' => (strlen($request->input('card-code')) > 2 )? $request->input('card-code') : "",
                'tipotarjeta' => $request->input('card-type'),
                'num_tarjetas' => ($request->input('cards-number') != null)? $request->input('cards-number') : 0,
                'visa' => $request->input('has-visa'),
                'mc' => $request->input('has-mc'),
                'amex' => $request->input('has-amex'),
                'ingreso_mensual' => ($request->input('monthly-income') != null)? $request->input('monthly-income') : 0,
                'estado_civil' => $request->input('marital-status'),
                'numero_contrato' => ($request->input('contract-number') != null)? $request->input('contract-number') : "",
                'total_membresia' => $request->input('membership-total'),
                'solicita_factura' => $request->input('requires-invoice'),
                'habitacion' => ($request->input('room-type') != null)? $request->input('room-type') : "",
                'comentario' => ($request->input('comments-tracing') != null)? $request->input('comments-tracing'): "",
                'observaciones' => ($request->input('membership-observations') != null)? $request->input('membership-observations') : "",
                'tipo_q' => $request->input('qs-type'),
                'fecha_venta' => (strlen($request->input('sale-date')) >= 10)? substr($request->input('sale-date'), 0,10)." 00:00:00" : null,
                'tienefirmados' => $request->input('has-signed'),
                'categoria' => $request->input('category-client'),
                'tipo' => 'CLIENTE',
                'modificado_por' =>  (!is_null(Auth::user()->executive))? Auth::user()->executive->id : Auth::user()->id,
                'interesados' => 'S',
                'padre' => (!is_null(Auth::user()->executive))? Auth::user()->executive->padre : Auth::user()->id,
                'prefix_name' => $request->input('prefix-name'),
                'prefix_name2' => $request->input('prefix-name2'),
                'address' => is_null($request->input('address'))? '' : $request->input('address'),
                'has_vacation_ownership' => ($request->input('has-vacation-ownership') == "Y")? true : false,
                'take_presentation' => ($request->input('take-presentation') == "Y")? true  : false,
                'date_presentation' => ($request->input('date-presentation') != "" && $request->input('date-presentation') != null)? $request->input('date-presentation') : "",
                'homeowner' => ($request->input('homeowner') == "Y")? true : false,
                'many_times' => is_null($request->input('many-times'))? 0 : $request->input('many-times'),
                'suborigen' => (is_null($request->input('edit-sub-origen')))? 0 : $request->input('edit-sub-origen'),
                'country' => is_null($request->input('create-country'))? 42 : $request->input('create-country'),
                'state' => is_null($request->input('create-state'))? $customer->state : $request->input('create-state')
            ];
            $customer = $this->customer->create($data);
            $this->audit($data, $customer, "create");
            $customer->fecha_alta = $customer->created_at;
            $customer->fecha_modificacion = $customer->updated_at;
            $customer->save();

            if($request->input('has-note') == "S"){
                $datetime_move = date("Y-m-d H:i:s", strtotime($request->input('date-hour')));
                $move = $this->move->create([
                    "cliente" => $customer->id,
                    "actividad" => $request->input('note-activity'),
                    "fecha" => $datetime_move,
                    "estatus" => $request->input('note-status'),
                    "observaciones" => $request->input('note-details'),
                    "ejecutivo" => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                    "titulo" => $request->input('note-title'),
                    "padre" => $customer->padre
                ]);
            }
            Alert::success('Cliente creado correctamente')->flash();
            return json_encode($customer);
        }
    }

    public function delete(Request $request)
    {
    	$id = $request->input("id");
    	$customer = $this->customer->find($id);
        $customer_old = $customer;
        foreach($customer->customerHistories as $customerHistory)
        {
            $customerHistory->delete();
        }

        if(!is_null($customer))
        {
            $customer->delete();
        }

        Mail::to("leonardo.lorenzana@buganviliasclub.com.mx")->send(new DeleteCustomerMail($customer_old));

    	return json_encode($customer);
    }

    public function apiCustomer(Request $request)
    {
        //mando a llamar una funcion privada para el filtro avaanzado mandando como parametro el objeto request y recibiendo el eloquent de la tabla de clientes
        $customer = $this->checkAdvancedFilter($request);
        //dd($request->all());
        //revisar si tiene documentos firmados
    	return Datatables::eloquent($customer)->editColumn('edit', function(Customer $customer){
            return '<button data-toggle="modal" data-target="#modalEdit" data-route="'.route('editCustomer', $customer->id).'"  data-client-id="'.$customer->id.'" class="btn btn-default" type="button"><i class="fa fa-edit" aria-hidden="true"></i></button>';
        })
    	->editColumn("checkin",  function(Customer $customer) {
            return substr($customer->checkin, 0, 10);
        })
    	->editColumn("plan", function(Customer $customer){
    		return ($customer->plan == "T")? "Todo Incluido": ($customer->plan == "E")? "EUROPEO": "";
    	})->editColumn("delete", function(Customer $customer){
            if(Auth::user()->hasAccessApp('customer_delete')){
               $td = '<td><button data-toggle="modal" data-target="#modalDelete" data-client-id="'.$customer->id.'" class="btn btn-danger" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'; 
           }else{
                $td='';
            }
    		return $td;
    	})->rawColumns(['edit', 'delete', 'telefonos', 'emails'])->make(true);
    }

    //cargar formulario de notas en caso de que lleve 
    public function getNoteClient()
    {
        $activities = $this->activity->all();
        $statuses = $this->status->all();

        return view("includes.customer.note", compact('activities', 'statuses'));
    }

    public function getNoteClientEdit($id)
    {
        $customer = $this->customer->find($id);
        $move = $customer->moves()->first();
        $activities = $this->activity->all();
        $statuses = $this->status->all();
        if(!is_null($move))
        {
            return view("includes.customer-edit.note", compact('activities', 'statuses', 'move'));
        }else{
            return view("includes.customer.note", compact('activities', 'statuses'));
        }
    }

    public function update(Request $request)
    {
        $card = str_replace(' ', '', $request->input('customer-card'));

        if($request->ajax())
        {
            $customer = $this->customer->find($request->input('id'));
            if(is_null($customer))
            {
                return json_encode(false);
            }

            if((strpos($card, '#') !== false || strlen($card) < 15) && !Auth::user()->hasAccessApp('customer_card')){
                $card = $customer->tarjeta_decrypt.$customer->four_digits;
            }

            $vencimiento1 = (strlen($request->input('card-expiration')) > 6 )? substr($request->input('card-expiration'), 0, 2): "";
            $vencimiento2 = (strlen($request->input('card-expiration')) > 6 )? substr($request->input('card-expiration'), -2) : "";
            $data = [
                'ejecutivo' => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                'origen' => $request->input("origen"),
                'vendedor' => ($request->input('seller') != null)? $request->input('seller'): "",
                'nombre' => ($request->input('customer-name') != null)? $request->input('customer-name'): "",
                'edad' => ($request->input('age') != null)? $request->input("age") : null,
                'ocupacion' => ($request->input('customer-job') != null)? $request->input("customer-job") : "",
                'nombre_co' => ($request->input('customer-name-co') != null)? $request->input('customer-name-co') : "",
                'edad_co' => ($request->input('age-co') != null)? $request->input('age-co') : null,
                'ocupacion_co' => ($request->input('customer-job-co') != null)? $request->input('customer-job-co') : "",
                'tel_cel' => ($request->input('cell-phone') != null)? $request->input('cell-phone'): "",
                'tel_casa' => ($request->input('home-phone') != null)? $request->input('home-phone') : "",
                'tel_oficina' => ($request->input("office-phone") != null)? $request->input("office-phone") : "",
                'extencion' => ($request->input('extension') != null)? $request->input('extension') : "",
                'estado' => ($request->input('customer-state') != null)? $request->input('customer-state') : "",
                'email' => ($request->input('customer-email') != null)? $request->input('customer-email') : "",
                'checkin' => (strlen($request->input('check-in')) >= 10)? substr($request->input('check-in'), 0,10)." 00:00:00" : null,
                'checkout' => (strlen($request->input('check-out')) >= 10)? substr($request->input('check-out'), 0,10)." 00:00:00" : null,
                'plan' => $request->input('customer-plan'),
                'adultos' => $request->input('customer-adults'),
                'menores' => $request->input('customer-children'),
                'edadmenores' => ($request->input('children-age') != null)? $request->input('children-age') : "",
                'folioreserva' => ($request->input('reservation-sheet') != null)? $request->input('reservation-sheet') : "",
                'noches_en_certificado' => $request->input('certification-nights'),
                'noches_en_hotel' => $request->input('hotel-nights'),
                'certi_hotel' => $request->input('reservation-type'),
                'precio_total' => $request->input('total-price'),
                'total_certificado' => $request->input('certificate-price'),
                'total_hotel' => $request->input('total-hotel'),
                'pagado' => $request->input('paid-out'),
                'pendiente' => $request->input('pending-payment'),
                'banco' => ($request->input('customer-bank') != null)? $request->input('customer-bank') : "",
                'vencimiento1' => $vencimiento1,
                'vencimiento2' => $vencimiento2,
                'codigo' => (strlen($request->input('card-code')) > 2 )? $request->input('card-code') : "",
                'tipotarjeta' => $request->input('card-type'),
                'num_tarjetas' => ($request->input('cards-number') != null)? $request->input('cards-number') : 0,
                'visa' => $request->input('has-visa'),
                'mc' => $request->input('has-mc'),
                'amex' => $request->input('has-amex'),
                'ingreso_mensual' => ($request->input('monthly-income') != null)? $request->input('monthly-income') : 0,
                'estado_civil' => $request->input('marital-status'),
                'numero_contrato' => ($request->input('contract-number') != null)? $request->input('contract-number') : "",
                'total_membresia' => $request->input('membership-total'),
                'solicita_factura' => $request->input('requires-invoice'),
                'habitacion' => ($request->input('room-type') != null)? $request->input('room-type') : "",
                'comentario' => ($request->input('comments-tracing') != null)? $request->input('comments-tracing'): "",
                'observaciones' => ($request->input('membership-observations') != null)? $request->input('membership-observations') : "",
                'tipo_q' => $request->input('qs-type'),
                'fecha_venta' => (strlen($request->input('sale-date')) >= 10)? substr($request->input('sale-date'), 0,10)." 00:00:00" : null,
                'tienefirmados' => $request->input('has-signed'),
                'categoria' => $request->input('category-client'),
                'tipo' => 'CLIENTE',
                'modificado_por' =>  (!is_null(Auth::user()->executive))? Auth::user()->executive->id : Auth::user()->id,
                'interesados' => 'S',
                'padre' => (!is_null(Auth::user()->executive))? Auth::user()->executive->padre : Auth::user()->id,
                'tarjeta' => ( $card != null && strlen($card) > 11)? substr($card, 0, 12) : "",
                'four_digits' => ($card != null && strlen($card > 3))? substr($card, -4) : "",
                'prefix_name' => $request->input('prefix-name'),
                'prefix_name2' => $request->input('prefix-name2'),
                'address' => is_null($request->input('address'))? '' : $request->input('address'),
                'has_vacation_ownership' => ($request->input('has-vacation-ownership') == "Y")? true : false,
                'take_presentation' => ($request->input('take-presentation') == "Y")? true  : false,
                'date_presentation' => ($request->input('date-presentation') != "" && $request->input('date-presentation') != null)? $request->input('date-presentation') : "",
                'homeowner' => ($request->input('homeowner') == "Y")? true : false,
                'many_times' => is_null($request->input('many-times'))? 0 : $request->input('many-times'),
                'suborigen' => is_null($request->input('edit-sub-origen'))? 0 : $request->input('edit-sub-origen'),
                'country' => is_null($request->input('edit-country'))? 42 : $request->input('edit-country'),
                'state' => is_null($request->input('edit-state'))? $customer->state : $request->input('edit-state')
            ];

            $this->audit($data, $customer);
            $customer->update($data);
            $customer->fecha_modificacion = $customer->updated_at;
            $customer->save();
            if($request->input('has-note') == "S"){
                $move = $customer->moves()->first();
                $datetime_move = date("Y-m-d H:i:s", strtotime($request->input('date-hour')));
                if(!is_null($move))
                {
                    $move = $move->update([
                        "cliente" => $customer->id,
                        "actividad" => $request->input('note-activity'),
                        "fecha" => $datetime_move,
                        "estatus" => $request->input('note-status'),
                        "observaciones" => $request->input('note-details'),
                        "ejecutivo" => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                        "titulo" => $request->input('note-title'),
                        "padre" => $customer->padre
                    ]);
                }else{
                    $move = $this->move->create([
                        "cliente" => $customer->id,
                        "actividad" => $request->input('note-activity'),
                        "fecha" => $datetime_move,
                        "estatus" => $request->input('note-status'),
                        "observaciones" => $request->input('note-details'),
                        "ejecutivo" => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                        "titulo" => $request->input('note-title'),
                        "padre" => $customer->padre
                    ]);
                }

            }else{
                if(!is_null($customer->moves))
                {
                    foreach($customer->moves as $move) {
                        $move->delete();
                    }
                }
            }

            if($request->input('drop-files') != null)
            {
                foreach($request->input('drop-files') as $doc)
                {
                    $document = $this->clientDocument->find($doc);
                    if(!is_null($document))
                    {
                        if($document->customer->id == $customer->id)
                        {
                            if(file_exists(storage_path('app/public/documents/'.$document->cmp_uploadfile)))
                            {
                                $baseName = basename($document->cmp_uploadfile);
                                $directory = str_replace($baseName, "", $document->cmp_uploadfile);
                                //se elimina el directorio con el archivo
                                Storage::deleteDirectory('app/public/documents/'.$directory);
                            }
                            $document->delete();
                        }
                    }
                }
            }
            
            return json_encode($customer);
        }
    }
    public function formatoNq($id)
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.formats.formato-nq', compact('customer'));
            //return view('partials.formats.formato-nq', compact('customer'));
            return $pdf->download('formato-nq.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }

    public function formatoQa($id)
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.formats.formato-qa', compact('customer'));
            //return  view('partials.formats.formato-qa', compact('customer'));
            return $pdf->download('formato-qa.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }

    public function formatoQb($id)
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.formats.formato-qb', compact('customer'));
            //return view('partials.formats.formato-qb', compact('customer'));
            return $pdf->download('formato-qb.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }

    //funcion privada para el filtro avanzado
    private function checkAdvancedFilter($request)
    {
        if($request->input('customer-order') != null)
        {
            $order = $request->input('customer-order');
            $typeOrder = ($request->input('operator-customer-order') != null)? $request->input('operator-customer-order') : "ASC";
            $customer = $this->customer->list()->orderBy($order, $typeOrder);
        }else{
            //ordenamiento por default
            $data = $request->input("order");
            if($data[0]["column"] == "0")
            {
                $customer = $this->customer->list()->orderBy("fecha_alta", "desc");
            }else{
                $customer = $this->customer->list();
            }
            //fin ordenamiento por default
        }
        //se revisa el o los ejecutivos
        if(is_array($request->input("executive")))
        {
            if($request->input("operator-executive") == "=")
            {
                $customer->whereIn("ejecutivo", $request->input("executive"));
            }elseif($request->input("operator-executive") == "!="){
                $customer->whereNotIn("ejecutivo", $request->input("executive"));
            }
        }

        //si hay filtro por llegada se revisa
        if($request->input('filter-check-in') != null)
        {
            $operator = ($request->input("operator-check-in") != null)? $request->input("operator-check-in") : "=";
            $value = $request->input('filter-check-in');
            if($operator == 2)
            {
                $value2 = ($request->input('check-in-2') != null)? $request->input('check-in-2') : date("Y-m-d");
                if($value2 > $value)
                {
                    $customer->whereBetween("checkin", [$value, $value2]);
                }elseif($value > $value2){
                    $customer->whereBetween("checkin", [$value2, $value]);
                }else{
                    $customer->where("checkin", $value);
                }
            }else{
                $customer->where("checkin", $operator, $value);
            }
        }

        //si hay filtro por día de salida
        if($request->input('filter-check-out') != null)
        {
            $operator = ($request->input("operator-check-out") != null)? $request->input("operator-check-out") : "=";
            $value = $request->input('filter-check-out');
            if($operator == 2)
            {
                $value2 = ($request->input('check-out-2') != null)? $request->input('check-out-2') : date("Y-m-d");
                if($value2 > $value)
                {
                    $customer->whereBetween("checkout", [$value, $value2]);
                }elseif($value > $value2)
                {
                    $customer->whereBetween("checkout", [$value2, $value]);
                }else{
                    $customer->where("checkout",$value);
                }
            }else{
                $customer->where("checkout", $operator, $value);
            }        
        }

        //se revisa el filtro de cuanto a pagado
        if($request->input('filter-paid-out') != null)
        {
            $operator = ($request->input("operator-paid-out") != null)? $request->input("operator-paid-out") : "=";
            $value = str_replace("$", "", $request->input('filter-paid-out'));
            $value = str_replace(",", "", $value);
            $value = intval($value);
            $customer->where("pagado", $operator, $value);
        }
        //revisar pago pendiente
        if($request->input('filter-pending-payment') != null)
        {
            $operator = ($request->input("operator-pending-payment") != null)? $request->input("operator-pending-payment") : "=";
            $value = str_replace("$", "", $request->input('filter-pending-payment'));
            $value = str_replace(",", "", $value);
            $value = intval($value);
            $customer->where("pendiente", $operator, $value);
        }
        //checar si tiene documentos firmados
        if($request->input('filter-has-signed') == "S")
        {
            $customer->where("tienefirmados", "S");
        }
        if($request->input('filter-has-signed') == "N")
        {
            $customer->where("tienefirmados", "N");
        }

        //filtrar por categorias
        if(is_array($request->input('category-type')))
        {
            $operator = $request->input("operator-category-type");
            $value = $request->input('category-type');
            switch ($operator) {
                case "=":
                    $customer->whereIn("categoria", $value);
                    break;
                
                case "!=":
                    $customer->whereNotIn("categoria", $value);
                    break;
            }
        }
        return $customer;
    }
    //fin
    //funcion que retorna el documento de conirmacion de paquetes con fecha
    public function confirmacionPackageWithDate($id) 
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.confirmations.package-date', compact('customer'));
            //return view('partials.confirmations.package-date', compact('customer'));
            return $pdf->download('Confimacion-con-fecha-paquete.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }
    //fin de paquete con fecha
    public function confirmationOpenDate($id)
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.confirmations.open-date', compact('customer'));
            //return view('partials.confirmations.open-date', compact('customer'));
            return $pdf->download('Confimacion-con-fecha-abierta.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }

    public function confirmationWithDateHotel($id)
    {
        $customer = $this->customer->find($id);
        if(!is_null($customer))
        {
            $pdf = PDF::loadView('partials.confirmations.with-date-hotel', compact('customer'));
            //return view('partials.confirmations.with-date-hotel', compact('customer'));
            return $pdf->download('Confirmación con fecha de viaje - Hotelería.pdf');
        }

        Alert::warning('Cliente no encontrado')->flash();
        return redirect()->back();
    }
    //auditoria a la tabla customer
    public function audit($data, $model, $action = "Actualización")
    {
        $oldValue = "";
        $newValue = "";
        $count = 0;
        if($action == "create")
        {
             $data = ["title" => "Creado", "old_value" => "Fue creado", "new_value" => "Fué creado", "username" => Auth::user()->name, "login" => Auth::user()->login, "customer_id" => $model->id];
            $customerHistory = $this->customerHistory->create($data);
            return true;
        }
        //atributos
        $attributes = $this->getAttributesCustomer();
        //excepciones
        $excepts = [           
            "fecha_alta", //se hace al crear
            "tipo",//se checa si es prospecto o cliente
            "fecha_modificacion",// se crea en el momento que se modifica
            "modificado_por", //id del ejecutivo
            "interesados",//por default poner "S"
            "padre", //id del ejecutivo padre
        ];
        $encrypted = [
            "tel_cel",
            "tel_casa",
            "tel_oficina",
            "email",
            "tarjeta"
        ];
        foreach($data as $key => $value)
        {
            if(in_array($key, $encrypted)){
                $value = $this->encrypt->encrypt($value);
            }
            if($value != $model[$key] && !in_array($key, $excepts))
            {

                $oldValue .=  $attributes[$key]. ": ".$model[$key]. " -";
                $newValue .= $attributes[$key]. ": ".$value. " -";
                $count++;
            }
        }
        if($count > 0)
        {
            $data = ["title" => $action, "old_value" => $oldValue, "new_value" => $newValue, "username" => Auth::user()->name, "login" => Auth::user()->login, "customer_id" => $model->id];
            $customerHistory = $this->customerHistory->create($data);
        }

        return true;
    }

    //atributos del cliente
    private function getAttributesCustomer()
    {
        return [
            "ejecutivo" => "Ejecutivo",
            "origen" => "Origen",
            "vendedor" => "Vendedor",
            "nombre" => "Nombre",
            "edad" => "Edad",
            "ocupacion" => "Ocupacion",
            "nombre_co" => "Nombre acompañante",
            "edad_co" => "Edad acompañante",
            "ocupacion_co" => "Ocupacion acompañante",
            "tel_cel" => "Teléfono celular",
            "tel_casa" => "Teléfono casa",
            "tel_oficina" => "Teléfono oficina",
            "extencion" => "Extensión",
            "estado" => "Ciudad o país",
            "email" => "Correo electrónico",
            "checkin" => "Fecha llegada",
            "checkout" => "Fecha Salida",
            "plan" => "Plan",
            "adultos" => "Adultos",
            "menores" => "Menores",
            "edadmenores" => "Edad De Menores",
            "folioreserva" => "Folio de reserva",
            "noches_en_certificado" => "Noches en certificado",
            "noches_en_hotel" => "Noches en hotel",
            "certi_hotel" => "Tipo(Hotel, Certificado)",
            "precio_total" => "Precio Total",
            "total_certificado" => "Total Certificado",
            "total_hotel" => "Total Hotel",
            "pagado" => "Pagado",
            "pendiente" => "Pendiente",
            "four_digits" => "Ultimos 4 digitos",
            "tarjeta" => "Tarjeta",
            "banco" => "Banco",
            "vencimiento1" => "Vencimiento mes",
            "vencimiento2" => "Vencimiento año",
            "codigo" => "Código de seguridad",
            "tipotarjeta" => "Tipo de tarjeta",
            "num_tarjetas" => "Número de tarjetas",
            "visa" => "¿Tiene visa?",
            "mc" => "¿Tiene Master Card?",
            "amex" => "¿Tiene AMEX?",
            "ingreso_mensual" => "Ingreso Mensual",
            "estado_civil" => "Estado civil",
            "numero_contrato" => "Número de contrato",
            "total_membresia" => "Total membresía",
            "solicita_factura" => "¿Solicita factura?",
            "habitacion" => "Habitación",
            "comentario" => "Comentario",
            "observaciones" => "Observaciones",
            "tipo_q" => "Tipo Q",
            "fecha_venta" => "Fecha de venta",
            "tienefirmados" => "Tiene documentos firmados.",
            "categoria" => "Categoría",
            'prefix_name' => "Prefijo Nombre",
            'prefix_name2' => "Prefijo Nombre acompañante",
            'has_vacation_ownership' => "¿Tiene Propiedad vacacional?",
            'address' => "Dirección",
            'take_presentation' => "¿A tomado presentación?",
            'date_presentation' => "Fecha Presentación",
            'homeowner' => "¿Tiene casa propia?",
            'many_times' => "¿Cuantas veces vacaciona por año?",
            "suborigen" => "Sub Origen",
            'country' => "Pais",
            'state' => "Estado"
        ];

    }

    /*get customer notifications*/
    public function getNotificationsToday()
    {
        $user = Auth::user();
        $executive = [];
        //si el ejecutivo no a sido creado se asignará uno
        if(is_null($user->executive)) {
            $data = [
                'nombre' => $user->name,
                'login' => $user->login,
                'fecha_alta' => date('Y-m-d'),
                'estatus' => 'A', 
                'nivel' => 1,
                'padre' => 1,
                'extencion' => ""
            ];

            $executive = $this->executive->create($data);
        }
        $user = $user->refresh();
        
        $moves = $this->move->Where("fecha", ">", Carbon::today())->where("ejecutivo", @$user->executive->id)->where("estatus", 1)->with('customer', 'executive', 'activity')->get();
        return json_encode($moves);
    }
    /*fin de las notificaciones del dia*/
    /*Cambiar el estatus de una notificacion*/
    public function changeNotificationStatus($move_id, Request $request)
    {
        $move = $this->move->find($move_id);
        if(is_null($move)) {
            abort(404);
        }
        $move->estatus = $request->input("estatus_id");
        $move->save();
        return json_encode($move);
    }
    /*Fin de cambiar el estatus de una notificacion*/

    /*obtener estados por pais*/
    public function getStates($country_id)
    {
        $response = '';
        foreach(DB::table("estado")->where("ubicacionpaisid", $country_id)->get() as $state){
            $response .= '<option value="'.$state->id.'">'.$state->estadonombre.'</option>';
        }

        return $response;
    }
//fin controlador
}
