<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables as Datatables;
use App\Imports\ContactImport;
use App\Crm\Email;
use App\Crm\Executive;
use App\Crm\Customer;
use Twilio\Rest\Client;
use Twilio\TwiML;
use App\Http\Requests\Project\StoreEmailTracingRequest;
use App\Crm\EmailTracing;
use Auth;
use App\Crm\CustomerSource;
use Excel;

//survey
use App\CRM\Survey;
use App\CRM\SurveyDatosCliente;
use App\CRM\SurveyDatosReserva;
use App\CRM\SurveyDatosTarjeta;
use App\Services\Encrypt;

class ProjectController extends Controller
{
	private $email, $executive, $customerSource;
	public function __construct(
		Email $email,
        Executive $executive,
        Customer $customer,
        EmailTracing $emailTracing,
        CustomerSource $customerSource,
        Survey $survey,
        SurveyDatosCliente $surveyDatosCliente,
        SurveyDatosReserva $surveyDatosReserva,
        SurveyDatosTarjeta $surveyDatosTarjeta
	){
        $this->middleware("auth");
        $this->email = $email;
        $this->executive = $executive;
        $this->customer = $customer;
        $this->emailTracing = $emailTracing;
        $this->customerSource = $customerSource;
        $this->survey = $survey;
        $this->surveyDatosCliente = $surveyDatosCliente;
        $this->surveyDatosReserva = $surveyDatosReserva;
        $this->surveyDatosTarjeta = $surveyDatosTarjeta;
        $this->encrypt = new Encrypt();
	}

    public function list($origen = null)
    {
        $executives = $this->executive->where("estatus", "A")->orderBy("nombre", "ASC")->get();
        if(is_null($origen)) {
            $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
            ->select("correos.*", "origen.nombre as origenName")->distinct()->orderBy("fechaalta", "desc")->take(25)->get();
            return view('project.list', compact('emails',"executives", "origen"));
        }
        switch ($origen) {
            case 'R':
                $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query) use($origen){
                          return $query->where("correos.origen", $origen)->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct()->orderBy("fechaalta", "desc")->take(25)->get();
                break;
                case '21':
                $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query) use ($origen){
                          return $query->where("correos.origen", "X")->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct()->orderBy("fechaalta", "desc")->take(25)->get();
                break;
                case 'Y':
                 $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query) use ($origen){
                          return $query->where("correos.origen", "Y")->whereIn("ejecutivo", [Auth::user()->executive->id]);
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct()->orderBy("fechaalta", "desc")->take(25)->get();
                break;
            default:
                # code...
                break;
        }

        return view('project.list', compact('emails',"executives", "origen"));

    }

    public function apiProjectList(Request $request, $origen=null)
    {
        $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
        ->where(function($query){
                  return $query->where("correos.origen", "R")->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
        })
        ->select("correos.*", "origen.nombre as origenName")->distinct();
        if(is_null($origen)) {
            $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
            ->select("correos.*", "origen.nombre as origenName")->distinct()->orderBy("fechaalta", "desc");
        }
        switch ($origen) {
            case 'R':
                $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query){
                          return $query->where("correos.origen", "R")->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct();
                break;
            case '21':
                $origen = "X";
                $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query) use ($origen){
                        if(Auth::user()->hasAccessApp("customer_list_all")){
                          return $query->where("correos.origen", $origen);
                        }else{
                            return $query->where("correos.origen", $origen)->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
                        }
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct();
                break;
                case 'Y':
                $origen = "Y";

                 $emails = $this->email->leftJoin('origen', 'origen.code', '=', 'correos.origen')
                    ->where(function($query) use ($origen){
                        if(Auth::user()->hasAccessApp("customer_list_all")){
                          return $query->where("correos.origen", $origen);
                        }else{
                            return $query->where("correos.origen", $origen)->whereIn("ejecutivo", [44, Auth::user()->executive->id]);
                        }
                    })
                    ->select("correos.*", "origen.nombre as origenName")->distinct();
                break;
            
            default:
                # code...
                break;
        }

        $data = $request->input("order");
        if($data[0]["column"] == "0")
        {
            $emails = $emails->orderBy("fechaalta", "desc");
        }
        return Datatables::eloquent($emails)->editColumn('assign', function(Email $email){
            return view('includes.email.assign', compact('email'));
        })
        ->setRowClass(function (Email $email) {
            if(count($email->emailTracings) > 0){
                $emailTracing = $this->emailTracing->where('email_id', $email->id)->orderBy("created_at", "desc")->first();
                $is_booked = $this->emailTracing->where("id", $emailTracing->id)
                ->whereRaw(" upper(title) like '%BOOK%'" )->first();
                if(!is_null($is_booked)){
                    return "is-booked";
                }

                $is_cancel = $this->emailTracing->where("id", $emailTracing->id)
                ->whereRaw(" upper(title) like 'CANCEL%'" )->first();

                if(!is_null($is_cancel)){
                    return "is-cancel";
                }

                $is_arrived = $this->emailTracing->where("id", $emailTracing->id)
                ->whereRaw("(upper(title) like '%EN CASA%' or upper(title) like '%ARRIVED%')" )
                ->first();
                if(!is_null($is_arrived)){
 
                    return "is-arrived";
                }

                return "is-contacted";

            }

            if(strpos(strtoupper($email->nombre), "RCI") !== false) {
                return "is-rci";
            }
        })
        ->editColumn('origenName', function(Email $email){
            switch ($email->origen) {
                case 'R':
                     return 'PROYECTOS';
                    break;
                case 'X':
                     return 'NC exchanges';
                    break;
                
                default:
                   return '';
                    break;
            }
        })->rawColumns(['assign', 'delete', 'email_link'])->make(true);
    }

    public function assignCustomerToExecutive($email_id, Request $request)
    {
        $email = $this->email->find($email_id);
        $card = str_replace(' ', '', $request->input('customer-card'));
        $origen = $email->origen;
        switch ($email->origen) {
            case 'R':
                $origen = 10;
                # code...
                break;
           case 'X':
                $origen = 21;
                # code...
                break;
            case 'L':
                $origen = 17;
                # code...
                break;
            case 'M':
                $origen = 7;
                # code...
                break;
            case 'E':
                $origen = 11;
                # code...
                break;
            
            default:
                # code...
                break;
        }
        $customer = $this->customer->create([
            'ejecutivo' => $request->input("executive-id"),
            'origen' => $email->origen ,
            'vendedor' =>  "",
            'nombre' => $email->nombre,
            'edad' => null,
            'ocupacion' => "",
            'nombre_co' => "",
            'edad_co' => null,
            'ocupacion_co' => "",
            'tel_cel' => $email->telefono,
            'tel_casa' => "",
            'tel_oficina' => "",
            'extencion' =>  "",
            'estado' => "",
            'email' =>  $origen,
            'checkin' => null,
            'checkout' =>  null,
            'plan' => "",
            'adultos' => "",
            'menores' => "",
            'edadmenores' => "",
            'folioreserva' => "",
            'comentario' => $email->comentario,
            'padre' => (!is_null(Auth::user()->executive))? Auth::user()->executive->padre : Auth::user()->id
        ]);

        $email->padre = $customer->padre;
        $email->ejecutivo = $customer->ejecutivo;
        $email->estatus = 1;
        $email->save();
        $customerSource = $this->customerSource->create([
            "email_id" => $email->id,
            "customer_id" => $customer->id,
            "executive_id" => $customer->ejecutivo,
            "login" => Auth::user()->login
        ]);

        $email->delete();
        return redirect()->route("editCustomer", $customer->id);
    }

    //make calls
    public function call()
    {
        // Your Account SID and Auth Token from twilio.com/console
        $account_sid = 'AC5bee4eb6c8743c193537c27d46f73412';
        $auth_token = 'eb1258f0e99a2404394e1f81f3a5c653';
        // In production, these should be environment variables. E.g.:
        // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

        // A Twilio number you own with Voice capabilities
        $twilio_number = "+19093445271";

        // Where to make a voice call (your cell phone?)
        $to_number = "+523222949223";

        $client = new Client($account_sid, $auth_token);
        $client->account->calls->create(  
            $to_number,
            $twilio_number,
            array(
                "url" => "http://demo.twilio.com/docs/voice.xml"
            )
        );
    }

    public function answerCall(){
        $response = new TwiML;
        // Read a message aloud to the caller
        $response->say(
            "Thank you for calling! Have a great day.", 
            array("voice" => "alice")
        );

        echo $response;
    }

    /*retornar la vista de los seguimientos de los correos de proyects*/
    public function getEmailTracing($email_id)
    {
        $email = $this->email->find($email_id);

        if(!is_null($email)) {
            return view('project.tracings-ajax', compact('email'));
        }
    }

    public function storeEmailTracing($email_id, StoreEmailTracingRequest $request)
    {
        $emailTracing = $this->emailTracing->create([
            'title' => $request->input("title"), 
            'comment' => $request->input('comment'),
            'executive_name' => Auth::user()->name,
            'login' => Auth::user()->login,
            'email_id' => $email_id
        ]);

        $email = $this->email->find($email_id);
        if(!is_null($email))
        {
            $email->fecha_contacto = \Carbon\Carbon::now();
            $email->save();
        }

        return json_encode($emailTracing);
    }


    /**Metodo para subir datos**/

    public function upload(){
        return view('contact.upload');
    }

    public function postUpload(Request $request)
    {
        if(is_null($request->file("upload-contact"))) {
            return redirect()->back()->withErrors("Archivo no válido");
        }
        $file_parts = pathinfo($request->file("upload-contact")->getClientOriginalName());
        if($file_parts["extension"] != "xlsx"){
            return redirect()->back()->withErrors("Archivo no válido");
        }

        $datos = Excel::toArray(new ContactImport,$request->file("upload-contact"));
        if(is_null($datos)){
            return redirect()->back()->withErrors("Archivo no válido");
        }
        if(!isset($datos[0])){
             return redirect()->back()->withErrors("Archivo no válido");
        }
        $uploads = 0;
        //quitamos encabezado
        unset($datos[0][0]);
        foreach($datos[0] as $dato)
        {   
            if(count($dato) == 22 && !is_null($dato[0]))
            {
                //$dato[21] = is_int($dato[21])? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($dato[21]) : $dato[21];
                if(strtolower($dato[0]) == "survey" && !is_null($dato[2]) && (!is_null($dato[13]) || !is_null($dato[8])  || !is_null($dato[9])))
                {
                    $r = $this->insertSurvey($dato, $request);
                    if($r==1){
                        $uploads++;
                    }

                }else if(!is_null($dato[2]) && (!is_null($dato[13]) || !is_null($dato[8])  || !is_null($dato[9]))){
                    $r = $this->uploadContact($dato);
                    if($r==1){
                        $uploads++;
                    }
                }
            }
        }

        if($uploads == 0){
             return redirect()->back()->withErrors("No se subió ningun dato");
        }

        return redirect()->back()->with(["success" => "se subieron ".$uploads." datos"]);
       
    }

    public function insertSurvey($data, Request $request)
    {

        if(!is_null($this->surveyDatosCliente->where("email", $this->encrypt->encrypt($data[13]))->where("nombre", $data[2])->first()))
        {
            return 0;
        }

        try {
            $comentarios = "";
            if(!is_null($data[21])){
                $comentarios.= $data[21];
            }
            if(!is_null($data[10])){
                $comentarios.= "\n".$data[10]." " ;
            }
            $ejecutivo = is_null($data[1])? 1 : $data[1];
            //primero creamos los datos del survey
            $survey = $this->survey->create([
                'comprobante_qa' => is_null($request->input('comprobante-qa'))? "": $request->input('comprobante-qa'),
                'comentario_validacion' => is_null($request->input('comentario-validacion'))? "": $request->input('comentario-validacion'),
                'fecha_primer_contacto' => date("Y-m-d H:i:s"), 
                'executive_id' => $ejecutivo, 
                'origen_id' => 10,
                'vendedor' => is_null($request->input('vendedor'))? "": $request->input('vendedor'), 
                'folio' => is_null($request->input('folio'))? "": $request->input('folio'), 
                'a_visitado' => ($request->input('a-visitado')=="S"), 
                'tomado_presentacion' => ($request->input('tomado-presentacion') == "S"),
                'hace_cuanto' => is_null($request->input('hace-cuanto'))? "": $request->input('hace-cuanto'),
                'se_hizo_socio' => ($request->input('se-hizo-socio')=="S"), 
                'numero_aprobacion' => is_null($request->input('numero-aprobacion'))? "": $request->input('numero-aprobacion'), 
                'requiere_factura' => ($request->input('requiere-factura') == "S"), 
                'ingreso_mensual' => is_null($data[19])? 0 : doubleval(str_replace(",", "",str_replace("$", "", $data[19]))), 
                'estado_civil' => is_null($data[20])? 0: $data[20], 
                'numero_tarjetas' =>  is_null($data[15])?0 : $data[15],
                'precio_total' =>  is_null($request->input('precio-total'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('precio-total')))),
                'pagado' => is_null($request->input('pagado'))? 0 :doubleval(str_replace(",", "",str_replace("$", "", $request->input('pagado')))), 
                'saldo_pendiente' => is_null($request->input('saldo-pendiente'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('saldo-pendiente')))),
                'comentarios' => $comentarios,
                'desglose_paquete' => is_null($request->input('desglose-paquete'))? "": $request->input('desglose-paquete'), 
                'desglose_hoteleria' => is_null($request->input('desglose-hoteleria'))? "": $request->input('desglose-hoteleria'), 
                'comentarios_verificacion' => is_null($request->input('comentarios-verificacion'))? "": $request->input('comentarios-verificacion')
            ]);

            //guardar datos del cliente y el acompañante
            $surveyDatosCliente = $this->surveyDatosCliente->create([
                'nombre' => $data[2],
                'edad' => is_null($data[3])? "" : $data[3],
                'ocupacion' =>  is_null($data[4])? "" : $data[4],
                'telefono_celular' => is_null($data[8])? "": $this->encrypt->encrypt($data[8]),
                'telefono_casa' => is_null($data[9])? "": $this->encrypt->encrypt($data[9]),
                'estado' => is_null($data[12])? "" : $data[12],
                'email' => is_null($data[13])? '': $this->encrypt->encrypt($data[13]),
                'tipo' => 'P',
                'survey_id' => $survey->id
            ]);
            //si lleva acompañante tambien se registra
            if(!is_null($data[5]))
            {
                $surveyDatosCliente2 = $this->surveyDatosCliente->create([
                    'nombre' => $data[5],
                    'edad' => is_null($data[6])? "" : $data[6],
                    'ocupacion' =>  is_null($data[7])? "" : $data[7],
                    'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
                    'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
                    'estado' => is_null($data[12])? "" : $data[12],
                    'email' => is_null($data[13])? '':$this->encrypt->encrypt($data[13]),
                    'tipo' => 'A',
                    'survey_id' => $survey->id
                ]);
            }

            //datos de reservacion
            $surveyDatosReserva = $this->surveyDatosReserva->create([
                'checkin' => is_null($request->input('checkin'))? "": $request->input('checkin'), 
                'checkout' => is_null($request->input('checkout'))? "": $request->input('checkout'), 
                'plan' => is_null($request->input('plan'))? "" : $request->input('plan'), 
                'adultos' => is_null($request->input('adultos'))? "": $request->input('adultos'), 
                'menores' => is_null($request->input('menores'))? "":  $request->input('menores'), 
                'edad_menores' => is_null($request->input('edad-menores'))? "": $request->input('edad-menores'),
                'tipo_habitacion' => is_null($request->input('tipo-habitacion'))? "" : $request->input('tipo-habitacion'),
                'cuantas_habitaciones' => is_null($request->input('cuantas_habitaciones'))? "": $request->input('cuantas-habitaciones'),
                'viaja_con_familiares' => ($request->input('viaja-con-familiares') =='S'),
                'survey_id' => $survey->id
            ]);

            //datos de la tarjeta
            $vencimiento = "";
            if(@strlen($request->input('vencimiento-p')) ==7)
            {
                $vencimiento = date('20'.substr($request->input('vencimiento-p'),-2 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
            }elseif(@strlen($request->input('vencimiento-p')) ==9)
            {
                $vencimiento = date(substr($request->input('vencimiento-p'),-4 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
            }
            //se crea la tarjeta de pago
            $surveyDatosTarjeta = $this->surveyDatosTarjeta->create([
                'numero_tarjeta' => is_null($request->input('numero-tarjeta-p'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-p')),
                'banco' => is_null($data[14])? "": $data[14],
                'vencimiento' => $vencimiento,
                'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-p'))? "": $request->input('tipo-tarjeta-p'),
                'codigo' => is_null($request->input('codigo-p'))? "": $request->input('codigo-p'),
                'pago_garantia' => "P", //se coloca como P por ser de Pago
                'survey_id' => $survey->id
            ]);
            $vencmiento = "";
            if(@strlen($request->input('vencimiento-g')) ==7)
            {
                $vencimiento = date('20'.substr($request->input('vencimiento-g'),-2 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
            }elseif(@strlen($request->input('vencimiento-g')) ==9)
            {
                $vencimiento = date(substr($request->input('vencimiento-g'),-4 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
            }
            //se crea la tarjeta de garantia
            $surveyDatosTarjetaG = $this->surveyDatosTarjeta->create([
                'numero_tarjeta' => is_null($request->input('numero-tarjeta-g'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-g')),
                'banco' => is_null($data[14])? "": $data[14],
                'vencimiento' => $vencimiento,
                'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-g'))? "": $request->input('tipo-tarjeta-g'),
                'codigo' => is_null($request->input('codigo-g'))? "": $request->input('codigo-g'),
                'pago_garantia' => "G", //se coloca como P por ser de Pago
                'survey_id' => $survey->id
            ]);
        } catch (Exception $e) {
            //eliminar registros en caso de ocurrir un error
            if(isset($surveyDatosTarjetaG)){
                $surveyDatosTarjetaG->delete();
            }
            if(isset($surveyDatosTarjeta)){
                $surveyDatosTarjeta->delete();
            }
            if(isset($surveyDatosReserva)){
                $surveyDatosReserva->delete();
            }

            if(isset($surveyDatosCliente2)){
                $surveyDatosCliente2->delete();
            }
            if(isset($surveyDatosCliente)){
                $surveyDatosCliente->delete();
            }
            if(isset($survey)){
                $survey->delete();
            }
        
        }
        
        return 1;
    }


    public function uploadContact($data)
    {
        if(!is_null($this->email->where("correo", $data[13])->where("nombre", $data[2])->first()))
        {
            return 0;
        }
        
        $telefono = is_null($data[8])? "": $data[8];
        $telefono2 = is_null($data[9])? "": $data[9];
        if(!is_null($this->email->where("telefono", $telefono. ", ".$telefono2)->where("nombre", $data[1])->first()))
        {
            return 0;
        }

        $telefono = "";
        $telefono = is_null($data[8])? "": $data[8];
        $telefono2 = is_null($data[9])? "": $data[9];
        $ejecutivo = is_null($data[1])? 1 : $data[1];
        $origen = $data[0];
        if($data[0] == "R"){
            $ejecutivo = 44;
            $origen="R";
        }
        if($data[0]==21){
            $origen="X";
        }
        $this->email->create([
            "ejecutivo" => $ejecutivo,
            "correo" => !is_null($data[13])? $data[13]: '',
            "nombre" => !is_null($data[2])? $data[2]: '',
            "fechaalta" => date("Y-m-d"),
            "fechabaja" => null,
            "estatus" => 1,
            "cliente" => 0,
            "lista_correos" => 0,
            "padre" => 0,
            "lista_externa" => !is_null($data[21])? $data[21]:'',
            "telefono" => $telefono. ", ".$telefono2,
            "ip" => null,
            "origen" => $origen,
            "fecha_contacto" =>  date("Y-m-d H:i:s"),
            "comentario" => !is_null($data[21])? $data[21]:'',
            "horacontacto" => date("H:i:s")
        ]);
        return 1;
    }

    public function getEmailInfo($email_id)
    {
        $email = $this->email->find($email_id);
        if(!is_null($email)){
            return view("includes.email.info", compact('email'));
        }

        return '';
    }

    public function delete($email_id){
        $email = $this->email->find($email_id);
        if(is_null($email)){
            return "error";
        }
        $email->delete();
        return "success";
    }
}
