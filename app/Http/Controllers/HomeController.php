<?php

namespace App\Http\Controllers;
use App\Arrival;
use Illuminate\Http\Request;

use App\User;
use Auth;

class HomeController extends Controller
{
	public function __construct(User $user)
	{
		$this->middleware("auth");
		$this->user = $user;

	}

	public function index()
	{
    	return view('welcome');
	}

	public function arrivals()
	{
		return datatables()->eloquent(Arrival::query())->make(true);
	}
}
