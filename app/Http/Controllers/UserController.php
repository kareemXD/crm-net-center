<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;

//modelos de usuarios
use App\Crm\SecLogged;
use App\Crm\ADMIN\Permission;
use App\Crm\ADMIN\Group;
use App\Crm\ADMIN\UserGroup;
use App\Crm\ADMIN\UserPermission;
use App\Crm\UserSafety;
use App\Services\Encrypt;
use App\Crm\Executive;
use Auth;
use Hash;

class UserController extends Controller
{
    private $secLogged;
	private $group;
    private $permission;
    private $userGroup;
    private $userPermission;
    private $userSafety; /*variable de los usuarios del sistema*/
    private $encrypt;
    private $executive;

    public function __construct(
    	SecLogged $secLogged,
    	Group $group,
        Permission $permission,
        UserGroup $userGroup,
        UserPermission $userPermission,
        UserSafety $userSafety,
        Encrypt $encrypt,
        Executive $executive
    ){
        $this->middleware('auth');
    	$this->group = $group;
    	$this->secLogged = $secLogged;
        $this->permission = $permission;
        $this->userGroup = $userGroup;
        $this->userPermission = $userPermission;
        $this->userSafety = $userSafety;
        $this->encrypt = $encrypt;
        $this->executive = $executive;
    }

    public function create()
    {

    	$groups = $this->group->distinct()->get();
        $permissions = $this->permission->all();
    	return view("user.create", compact('groups', 'permissions'));
    }

    public function store(StoreRequest $request)
    {

        if(!is_null($this->group->find($request->input("user-group"))))
        {
            $user = $this->userSafety->create([
                "login" => $request->input("username"),
                "pswd" =>  Hash::make(md5($request->input("password"))),
                "name" => $request->input("name"),
                "email" => $request->input("email"),
                "active" => $request->input("active")
            ]);

            $executive = $this->executive->create([
                "nombre" => $user->name,
                "login" => $user->login,
                "fecha_alta" => date("Y-m-d"),
                "estatus" => ($user->active == "Y")? "A" : "B",
                "nivel" => 1,
                "padre" => 1
            ]);

            $userGroup = $this->userGroup->create([
                "login" => $user->login,
                "group_id" => $request->input("user-group")
            ]);
            return redirect()->back()->with("success","Usuario creado correctamente");

        }else{
            return redirect()->back()->withErrors("user-group", "Grupo no existente")->withInput();
        }

    }

    public function list()
    {
        $roles = $this->group->all();
        $users = $this->userSafety->orderBy('created_at', 'desc')->get();
        return view('user.list', compact('users', 'roles'));
    }

    public function changeStatus($login, Request $request)
    {
        $user = $this->userSafety->find($login);


        if(is_null($user) || is_null($request->input("status"))){
            return json_encode(["error" => "Usuario no válido"]);
        }

        if(!(Auth::user()->level_user >  $user->level_user || Auth::user()->hasAccessApp("all"))){
            return json_encode(["error" =>"El nivel de su usuario no permite modificar este"]);
        }
        $status = json_decode($request->input('status'));
        if($status === true) {
            $user->active = "Y";
            $user->save();

            $executive = $user->executive;
            if(!is_null($executive)) {
                $executive->estatus = "A";
                $executive->save();
            }
            return json_encode(["success" => $status]);
        }
        if($status === false) {
            $user->active = "N";
            $user->save();

            $executive = $user->executive;
            if(!is_null($executive)) {
                $executive->estatus = "B";
                $executive->save();
            }
            return json_encode(["success" => $status]);
        }
        return json_encode(["error" =>"No se recibió el estatus"]);
    }

    public function changeRole($login, Request $request)
    {
        if(!$request->ajax()){
            return "Error: <a href='".route("home")."'>Click para regresar al inicio </a>";
        }

        $group = $this->group->find($request->input("group_id"));
        $user = $this->userSafety->find($login);
        if(is_null($user)) {
            return json_encode(["error" => "Usuario no válido"]);
        }
        if(!(  (Auth::user()->level_user >  $user->level_user && $group->level <= Auth::user()->level_user  ) || Auth::user()->hasAccessApp("all"))){
            return json_encode(["error" =>"El nivel de su usuario no permite cambiar a un rol mayor"]);
        }
        $userGroup = $this->userGroup->where("login", $login)->first();
        //si no se manda un grupo se elmina el grupo asignado al usuario
        if(is_null($group)){
            if(!is_null($userGroup)) {
                $userGroup->delete();
            }
            return json_encode(["success" => 0]);

        }
        if(is_null($userGroup)) {
            $userGroup = $this->userGroup->create([
                'login' => $user->login,
                'group_id' => $group->group_id
            ]);
        }else {
            $userGroup->group_id = $group->group_id;
            $userGroup->save();
        }
        return json_encode(["success" => $userGroup->group_id]);
    }

    public function delete($login, Request $request)
    {
        if(!$request->ajax()){
            return "Error: <a href='".route("home")."'>Click para regresar al inicio </a>";
        }
        $user = $this->userSafety->find($login);
        if(is_null($login)) {
            return json_encode(["error" => "No se encotró el usuario"]);
        }
        if(Auth::user()->login == $user->login){
            return json_encode(["error" => "No se puede eliminar usted mismo"]);
        }
        if(!(Auth::user()->level_user >  $user->level_user || Auth::user()->hasAccessApp("all"))){
            return json_encode(["error" =>"El nivel de su usuario no le permite eliminarlo"]);
        }
        if((Auth::user()->login == "kareem.lorenzana" || Auth::user()->login == "david.nunez" || Auth::user()->login == "david.nunez.grupo")){
            return json_encode(["error" =>"Este usuario no puede ser eliminado"]);
        }
        foreach($user->userRoles as $userRole) {
            $userRole->delete();
        }
        if(!is_null($user->executive)) {
            $user->executive->delete();
        }
        $user->delete();

        return json_encode(["success" => "Usuario eliminado correctamente"]);
    }

    public function edit($login, Request $request)
    {
        if($request->ajax()) {
            $user = $this->userSafety->find($login);
            if(is_null($user)) {
                return "Sin información.";
            }

            $groups = $this->group->distinct()->get();
            $permissions = $this->permission->all();
            return view("user.edit-ajax", compact("user", "groups", "permissions"));
        }
    }

    /*update user information*/
    public function update($login, UpdateRequest $request)
    {
        $user = $this->userSafety->find($login);
        if(is_null($user)) {
            return json_encode(['error' => "El usuario es inválido"]);
        }
        //se la contraseña no es nula o tiene caracteres se valida
        if($request->input("password") != "" && !is_null($request->input("password"))) {
            $password = Hash::make(md5($request->input("password")));
            $user->pswd = $password;
        }
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->active = $request->input("active");
        $user->save();
        if(is_null($user->userRole)) {
            $userGroup = $this->userGroup->create([
                "login" => $user->login,
                "group_id" => $request->input("user-group")
            ]);
        }else{
            $userGroup = $user->userRole->update([
                "login" => $user->login,
                "group_id" => $request->input("user-group")
            ]);
        }

        $executive = $user->executive;
        if(!is_null($executive)) {
            $executive = $this->executive->update([
                "nombre" => $user->name,
                "fecha_alta" => date("Y-m-d"),
                "estatus" => ($user->active == "Y")? "A" : "B",
                "nivel" => 1,
                "padre" => 1
            ]);
        }else{

            $executive = $this->executive->create([
                "nombre" => $user->name,
                "login" => $user->login,
                "fecha_alta" => date("Y-m-d"),
                "estatus" => ($user->active == "Y")? "A" : "B",
                "nivel" => 1,
                "padre" => 1
            ]);
        }
        //mandar relacion
        $user = $user->fresh();
        $user->userRole;

        return json_encode(["success" => $user]);
    }
}
