<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Crm\MaritalStatus;
use App\Crm\Customer;
use App\Crm\Executive;
use App\Crm\Origen;
use App\Crm\CardType;
use App\Crm\Category;
use App\Crm\ClientDocument;
use App\Crm\Move;
use App\Crm\Activity;
use App\Crm\Status;
use App\Crm\UserSafety;
use App\CRM\Country;
use App\User;
use Auth;
use DataTables as Datatables;
use Alert;
use Storage;
use PDF;
use App\Services\Encrypt;
use Mail;
use App\Mail\DeleteCustomerMail;
use Carbon\Carbon;
use DB;
use Excel;
use App\Exports\CustomerExport;
class ReportController extends Controller
{
	private $maritalStatus, $customer, $executive, $user, $origen, $cardType, $clientDocument, $move, $activity, $status, $encrypt, $userSafety, $country;

	//instanciar clases y objetos
	public function __construct(
		User $user,
		MaritalStatus $maritalStatus,
		Customer $customer,
		Executive $executive,
        Origen $origen,
        CardType $cardType,
        Category $category,
        ClientDocument $clientDocument,
        Move $move,
        Activity $activity,
        Status $status,
        Encrypt $encrypt,
        UserSafety $userSafety,
        Country $country
	){
        $this->middleware("auth");
        $this->middleware("report");
		$this->user = $user;
		$this->maritalStatus = $maritalStatus;
		$this->customer = $customer;
		$this->executive = $executive;
        $this->origen = $origen;
        $this->cardType = $cardType;
        $this->category = $category;
        $this->clientDocument = $clientDocument;
        $this->move = $move;
        $this->activity = $activity;
        $this->status = $status;
        $this->encrypt = $encrypt;
        $this->userSafety = $userSafety;
        $this->country = $country;
	}

	//inicio sin filtrar
	public function index(Request $request)
	{

        $executives = $this->executive->where("estatus", "A")->orderBy("nombre", "asc")->get();
        $categories = $this->category->select("categories.*", DB::raw("(select count(*) from clientes where clientes.categoria = categories.code) as cantidad"))->orderBy("name", "asc")->get();

        $llegadas = $this->customer->select("checkin", DB::raw("(select count(*) from clientes as c where c.checkin = clientes.checkin  ) as cantidad"))->whereNotNull("checkin")->distinct()->limit(25)->get();
        $origens = $this->origen->where("nombre", "!=", "")->select(
			"origen.*",
			DB::raw("(select count(*) from clientes where clientes.origen=origen.id) as Cantidad")
		)->get();
        $documentos_firmados = $this->customer->select(
            DB::raw("if(tienefirmados = 'S', 'SI', 'NO') as tienefirmados"),
            DB::raw("(select count(*) from clientes c where c.tienefirmados = clientes.tienefirmados) as cantidad")
        )->distinct()->limit(2)->get();
        $tipoqs = $this->customer->whereRaw("upper(tipo_q) in ('QA', 'QB', 'NQ')")->select(
        	"tipo_q",
        	DB::raw("(select count(*) from clientes as c where c.tipo_q =clientes.tipo_q  ) as cantidad")
        )->distinct()->get();
        $countries = $this->country->orderBy("paisnombre", "asc")->get();
        $states = DB::table("estado")->where("ubicacionpaisid", 42)->get();

		return view('reports.index', compact('executives', 'categories', 'llegadas', 'origens', 'tipoqs', 'countries', 'documentos_firmados', "states"));
	}

	//obtener estadisticas del filtro avanzado
	public function statics(Request $request)
	{
		if(count($request->input()) == 0){
			return redirect()->route('reportCustomer');
		}
        //filtro de codigo para obtener los clientes
		$customers = $this->checkAdvancedFilter($request);
        //sacar todos sus ids
        $ids = $customers->pluck("id")->toArray();
        if(count($ids) === 0){
            $ids = [0];
        }
        $ids_string = implode(", ", $ids);

        if(request()->input("filter-conexion") == "new"){
            $executives = $this->executive->where("estatus", "A")->orderBy("nombre", "asc")->get();
            $categories = $this->category->select("categories.*", DB::raw("(select count(*) from clientes where clientes.categoria = categories.code and clientes.id in ($ids_string)) as cantidad"))->whereRaw("(select count(*) from clientes where clientes.categoria = categories.code and clientes.id in ($ids_string)) > 0")->orderBy("name", "asc")->get();

            $llegadas = DB::table("clientes")->select("checkin", DB::raw("(select count(*) from clientes as c where c.checkin = clientes.checkin and c.id in ($ids_string) ) as cantidad"))->whereNotNull("checkin")->whereIn("id", $ids)->distinct()->limit(25)->get();

            $documentos_firmados = $this->customer->select(
                DB::raw("if(tienefirmados = 'S', 'SI', 'NO') as tienefirmados"),
                DB::raw("(select count(*) from clientes c where c.tienefirmados = clientes.tienefirmados and c.id in ($ids_string) ) as cantidad")
            )->whereIn("id", $ids)->distinct()->limit(2)->get();
            $origens = $this->origen->where("nombre", "!=", "")->select(
                "origen.*",
                DB::raw("(select count(*) from clientes where clientes.origen=origen.id and clientes.id in ($ids_string)) as cantidad")
            )->whereRaw("(select count(*) from clientes where clientes.origen=origen.id and clientes.id in ($ids_string)) > 0")->get();

            $tipoqs = $this->customer->select(
                "tipo_q",
                DB::raw("(select count(*) from clientes as c where c.tipo_q =clientes.tipo_q and c.id in ($ids_string) ) as cantidad")
            )->whereRaw("upper(tipo_q) in ('QA', 'QB', 'NQ')")->whereIn("id", $ids)->whereRaw("(select count(*) from clientes as c where c.tipo_q =clientes.tipo_q and c.id in ($ids_string) ) > 0")->distinct()->get();

            $countries = $this->country->orderBy("paisnombre", "asc")->get();
            $states = DB::table("estado")->where("ubicacionpaisid", 42)->get();

            $customers = $customers->paginate(25);
        }else{
            $databaseName = DB::connection('crm_netcenter')->getDatabaseName();

            $executives = DB::connection("crm_netcenter")->table("ejecutivos")->where("estatus", "A")->orderBy("nombre", "asc")->get();

            $categories = $this->category->select("categories.*", DB::raw("(select count(*) from ".$databaseName.".clientes as clientes where clientes.categoria = categories.code and clientes.id in ($ids_string)) as cantidad"))->whereRaw("(select count(*) from ".$databaseName.".clientes where clientes.categoria = categories.code and clientes.id in ($ids_string)) > 0")->orderBy("name", "asc")->get();

            $llegadas = DB::connection("crm_netcenter")->table("clientes")->select("checkin", DB::raw("(select count(*) from ".$databaseName.".clientes as c where c.checkin = clientes.checkin and c.id in ($ids_string) ) as cantidad"))->whereNotNull("checkin")->whereIn("id", $ids)->distinct()->limit(25)->get();

            $documentos_firmados = DB::connection("crm_netcenter")->table("clientes")->select(
                DB::raw("if(tienefirmados = 'S', 'SI', 'NO') as tienefirmados"),
                DB::raw("(select count(*) from clientes c where c.tienefirmados = clientes.tienefirmados and c.id in ($ids_string) ) as cantidad")
            )->whereIn("id", $ids)->distinct()->limit(2)->get();
            $origens = $this->origen->where("nombre", "!=", "")->select(
                "origen.*",
                DB::raw("(select count(*) from ".$databaseName.".clientes as clientes where clientes.origen=origen.id and clientes.id in ($ids_string)) as cantidad")
            )->whereRaw("(select count(*) from  ".$databaseName.".clientes as clientes where clientes.origen=origen.id and clientes.id in ($ids_string)) > 0")->get();

            $tipoqs = DB::connection("crm_netcenter")->table("clientes")->select(
                "tipo_q",
                DB::raw("(select count(*) from clientes as c where c.tipo_q =clientes.tipo_q and c.id in ($ids_string) ) as cantidad")
            )->whereRaw("upper(tipo_q) in ('QA', 'QB', 'NQ')")->whereIn("id", $ids)->whereRaw("(select count(*) from clientes as c where c.tipo_q =clientes.tipo_q and c.id in ($ids_string) ) > 0")->distinct()->get();

            $countries = $this->country->orderBy("paisnombre", "asc")->get();
            $states = DB::table("estado")->where("ubicacionpaisid", 42)->get();
            
            $customers = $customers->paginate(25);
        }

        $dataSend = compact('executives', 'categories', 'llegadas', 'origens', 'tipoqs', 'countries', 'customers', 'documentos_firmados', "states");

		return view('reports.index', $dataSend);
	}

    //imprimir reporte completo
    public function printStatics(Request $request)
    {
        $customers = $this->checkAdvancedFilter($request);
        return Excel::download(new CustomerExport($this), 'customer.xlsx');

    }

    public function checkAdvancedFilterStatic($request){
        return $this->checkAdvancedFilter($request);
    }
	//private function for get reports
    //funcion privada para el filtro avanzado
    private function checkAdvancedFilter($request)
    {
        if($request->input('customer-order') != null)
        {
            $order = $request->input('customer-order');
            $typeOrder = ($request->input('operator-customer-order') != null)? $request->input('operator-customer-order') : "ASC";
            $customer = $this->customer->list()->orderBy($order, $typeOrder);
        }else{
            //ordenamiento por default
            $data = $request->input("order");
            if($data[0]["column"] == "0")
            {
                $customer = $this->customer->list()->orderBy("fecha_alta", "desc");
            }else{
                $customer = $this->customer->list();
            }
            //fin ordenamiento por default
        }
        //se revisa el o los ejecutivos
        if(is_array($request->input("executive")))
        {
            if($request->input("operator-executive") == "=")
            {
                $customer->whereIn("ejecutivo", $request->input("executive"));
            }elseif($request->input("operator-executive") == "!="){
                $customer->whereNotIn("ejecutivo", $request->input("executive"));
            }
        }

        //si hay filtro por llegada se revisa
        if($request->input('filter-check-in') != null)
        {
            $operator = ($request->input("operator-check-in") != null)? $request->input("operator-check-in") : "=";
            $value = $request->input('filter-check-in');
            if($operator == 2)
            {
                $value2 = ($request->input('check-in-2') != null)? $request->input('check-in-2') : date("Y-m-d");
                if($value2 > $value)
                {
                    $customer->whereBetween("checkin", [$value, $value2]);
                }elseif($value > $value2){
                    $customer->whereBetween("checkin", [$value2, $value]);
                }else{
                    $customer->where("checkin", $value);
                }
            }else{
                $customer->where("checkin", $operator, $value);
            }
        }

        //si hay filtro por día de salida
        if($request->input('filter-check-out') != null)
        {
            $operator = ($request->input("operator-check-out") != null)? $request->input("operator-check-out") : "=";
            $value = $request->input('filter-check-out');
            if($operator == 2)
            {
                $value2 = ($request->input('check-out-2') != null)? $request->input('check-out-2') : date("Y-m-d");
                if($value2 > $value)
                {
                    $customer->whereBetween("checkout", [$value, $value2]);
                }elseif($value > $value2)
                {
                    $customer->whereBetween("checkout", [$value2, $value]);
                }else{
                    $customer->where("checkout",$value);
                }
            }else{
                $customer->where("checkout", $operator, $value);
            }        
        }

        //se revisa el filtro de cuanto a pagado
        if($request->input('filter-paid-out') != null)
        {
            $operator = ($request->input("operator-paid-out") != null)? $request->input("operator-paid-out") : "=";
            $value = str_replace("$", "", $request->input('filter-paid-out'));
            $value = str_replace(",", "", $value);
            $value = intval($value);
            $customer->where("pagado", $operator, $value);
        }
        //revisar pago pendiente
        if($request->input('filter-pending-payment') != null)
        {
            $operator = ($request->input("operator-pending-payment") != null)? $request->input("operator-pending-payment") : "=";
            $value = str_replace("$", "", $request->input('filter-pending-payment'));
            $value = str_replace(",", "", $value);
            $value = intval($value);
            $customer->where("pendiente", $operator, $value);
        }
        //checar si tiene documentos firmados
        if($request->input('filter-has-signed') == "S")
        {
            $customer->where("tienefirmados", "S");
        }
        if($request->input('filter-has-signed') == "N")
        {
            $customer->where("tienefirmados", "N");
        }

        //filtrar por categorias
        if(is_array($request->input('category-type')))
        {
            $operator = $request->input("operator-category-type");
            $value = $request->input('category-type');
            switch ($operator) {
                case "=":
                    $customer->whereIn("categoria", $value);
                    break;
                
                case "!=":
                    $customer->whereNotIn("categoria", $value);
                    break;
            }
        }

        if(is_array($request->input('filter-origen')))
        {
            $operator = $request->input("operator-origen");
            $value = $request->input('filter-origen');
            switch ($operator) {
                case "=":
                    $customer->whereIn("origen", $value);
                    break;
                
                case "!=":
                    $customer->whereNotIn("origen", $value);
                    break;
            }        
        }

        if(is_array($request->input('filter-tipoq')))
        {
            $operator = $request->input("operator-tipoq");
            $value = $request->input('filter-tipoq');
            switch ($operator) {
                case "=":
                    $customer->whereIn("tipo_q", $value);
                    break;
                
                case "!=":
                    $customer->whereNotIn("tipo_q", $value);
                    break;
            }        
        }


        return $customer;
    }
    //fin

   
	
}
