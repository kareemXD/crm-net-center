<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crm\ADMIN\Permission;
use App\Crm\ADMIN\Group;
use App\Crm\ADMIN\RolePermission;
use App\Crm\Executive;
use Auth;
use App\Http\Requests\Role\StoreRequest as RoleStoreRequest;
class AdminController extends Controller
{
	private $permission;
	private $group;
    private $rolePermission;
    private $executive;

    public function __construct(
    	Permission $permission,
    	Group $group,
        RolePermission $rolePermission,
        Executive $executive
    ){
    	$this->permission = $permission;
    	$this->group = $group;
        $this->rolePermission = $rolePermission;
        $this->executive = $executive;
    }

    //mostrar lista de permisos de usuario
    public function permissionList()
    {
    	$permissions = $this->permission->all();
    	return view('admin.permission.list', compact('permissions'));
    }

    public function updatePermission(Request $request, $id)
    {
    	$permission = $this->permission->find($id);
    	if(!is_null($permission))
    	{
    		if($request->input("description") != null && strlen($request->input("description")) > 1)
    		{
    			$permission->description = $request->input("description");
    			$permission->save();
    			$message = [
	    			"type" => "Success",
	    			"message" => "Datos modificados",
	    			"title" => "Correcto"
    			];
    		}else{
    			$message = [
	    			"type" => "Error",
	    			"message" => "La descripcion del permiso es un campo obligatorio",
	    			"title" => "Ups!"
    			];
    		}

    		return json_encode($message);
    	}else{
    		$message = [
    			"type" => "Error",
    			"message" => "Hubo un problema, no se encontro el permiso",
    			"title" => "Falla" 
    			];

    		return json_encode($message);
       	}
    }

    //roles
    public function roleList()
    {   
        if(!Auth::user()->hasAccessApp("roles")){
            return "Sin permisos";
        }
        $permissions = $this->permission->all();
    	$groups = $this->group->OrderBy("group_id", "asc")->get();
    	return view('admin.role.list', compact('groups', 'permissions'));
    }

    //update roles
    public function updateRole(Request $request, $group_id)
    {
        if($request->ajax() === FALSE)
        {
            return "false";
        }
        $group = $this->group->find($group_id);

        if(!is_null($group))
        {   
            
            $group->description = $request->input("description");
            $group->level = $request->input("edit-level");
            $group->save();
            foreach ($group->rolePermissions as $rolePermission)
            {
                $rolePermission->delete();    
            }
            if(!is_null($request->input("permission")))
            {
                foreach($request->input("permission") as $permission_id)
                {
                    $permission = $this->permission->find($permission_id);
                    if(!is_null($permission))
                    {
                        $this->rolePermission->create([
                            "group_id" => $group->group_id,
                            "permission_id" => $permission_id
                        ]);
                    }
                }
            }
            $message = [
                    "type" => "Success",
                    "message" => "Datos modificados",
                    "title" => "Correcto",
                    "permissions" => $group->permissions,
                    "description" => $request->input("description"),
                    "level" => $request->input("edit-level")
                ];
        }
        if(isset($message))
        {
            return json_encode($message);
        }else{
            return "false";
        }
    }

    public function createRole()
    {
        $permissions = $this->permission->all();
        return view("admin.role.create", compact("permissions"));
    }

    public function storeRole(RoleStoreRequest $request)
    {
            $group = $this->group->create([
                "description" => $request->input("description")
            ]);

            foreach($request->input("permission") as $permission_id)
            {
                $permission = $this->permission->find($permission_id);
                if(!is_null($permission))
                {
                    $this->rolePermission->create([
                        "group_id" => $group->group_id,
                        "permission_id" => $permission_id
                    ]);
                }
            }
            

            return redirect()->back()->with(["success" => "Grupo creado correctamente"]);
    }

    public function executiveList(Request $request)
    {
        $executives = $this->executive->all();
        return view("executive.list", compact('executives'));
    }

    //*modificar ejecutivo*//
    public function executiveEdit(Request $request, $id)
    {
        $executive = $this->executive->find($id);

        if($request->ajax()){
            return view('executive.edit-ajax', compact('executive'));
        }

        return view('executive.edit', compact('executive'));
    }

    /*metodo para actualizar datos de un ejecutivo*/
    public function executiveUpdate($id, Request $request)
    {
        $executive = $this->executive->find($id);
        $executive->nombre = $request->input('executive-nombre');
        $executive->estatus = ($request->input('is-active') == "on")? "A" : "B";
        $executive->extension = $request->input('executive-extension');
        $executive->save();

        if($request->ajax())
        {
            return json_encode($executive);
        }
        return redirect()->back()->with(["success" => "Datos guardados correctamente"]);
    } 

    public function executiveChangeStatus($id, Request $request)
    {
        
        $executive = $this->executive->findOrFail($id);
        $executive->estatus = ($executive->status != "A")? "A": "B";
        $executive->save();
        return json_encode($executive);
    }
}
