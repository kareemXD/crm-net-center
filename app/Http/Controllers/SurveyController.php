<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CRM\Executive;
use App\CRM\Origen;
use App\CRM\MaritalStatus;
use App\CRM\Survey;
use App\CRM\SurveyDatosCliente;
use App\CRM\SurveyDatosReserva;
use App\CRM\SurveyDatosTarjeta;
use DataTables;
use App\Services\Encrypt;



class SurveyController extends Controller
{
	private $executive, $origen, $maritalStatus, $survey, $surveyDatosCliente, $surveyDatosReserva, $surveyDatosTarjeta;
    private $encrypt;

	public function __construct(
		MaritalStatus $maritalStatus,
		Executive $executive,
		Origen $origen,
		Survey $survey,
		SurveyDatosCliente $surveyDatosCliente,
		SurveyDatosReserva $surveyDatosReserva,
		SurveyDatosTarjeta $surveyDatosTarjeta

	){
		$this->middleware("auth");
		$this->maritalStatus = $maritalStatus;
		$this->executive = $executive;
		$this->origen = $origen;
		$this->survey = $survey;
		$this->surveyDatosCliente = $surveyDatosCliente;
		$this->surveyDatosReserva = $surveyDatosReserva;
		$this->surveyDatosTarjeta = $surveyDatosTarjeta;
		$this->encrypt = new Encrypt();

	}


	public function list(Request $request)
	{
		if(Auth::user()->hasAccessApp("survey_show_all"))
		{
			$surveys = $this->survey->take(25)->orderBy('created_at', 'desc')->get();
		}elseif(Auth::user()->hasAccessApp("survey_show")){
			$surveys = $this->survey->where("executive_id", Auth::user()->executive->id)->take(25)->orderBy('created_at', 'desc')->get();
		}else{
			return "Sin permisos";
		}

		return view('survey.list', compact('surveys'));
	}

    public function create()
    {
    	$estadosCiviles = $this->maritalStatus->all();
    	$executives = $this->executive->where('estatus', 'A')->orderBy('nombre', 'asc')->get();
    	$origenes = $this->origen->where('nombre', '!=', '')->orderBy('nombre', 'asc')->get();
    	return view('survey.create', compact('executives', 'origenes', 'estadosCiviles'));
    }

    public function store(Request $request)
    {
    	try {
  			//primero creamos los datos del survey
	    	$survey = $this->survey->create([
	    		'comprobante_qa' => is_null($request->input('comprobante-qa'))? "": $request->input('comprobante-qa'),
	    		'comentario_validacion' => is_null($request->input('comentario-validacion'))? "": $request->input('comentario-validacion'),
	    		'fecha_primer_contacto' => date("Y-m-d H:i:s", strtotime($request->input('fecha-primer-contacto'))), 
	    		'executive_id' => $request->input('executive-id'), 
	    		'origen_id' => $request->input('origen-id'),
	    		'vendedor' => is_null($request->input('vendedor'))? "": $request->input('vendedor'), 
	    		'folio' => is_null($request->input('folio'))? "": $request->input('folio'), 
	    		'a_visitado' => ($request->input('a-visitado')=="S"), 
	    		'tomado_presentacion' => ($request->input('tomado-presentacion') == "S"),
	    		'hace_cuanto' => is_null($request->input('hace-cuanto'))? "": $request->input('hace-cuanto'),
	    		'se_hizo_socio' => ($request->input('se-hizo-socio')=="S"), 
	    		'numero_aprobacion' => is_null($request->input('numero-aprobacion'))? "": $request->input('numero-aprobacion'), 
	    		'requiere_factura' => ($request->input('requiere-factura') == "S"), 
	    		'ingreso_mensual' => is_null($request->input('ingreso-mensual'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('ingreso-mensual')))), 
	    		'estado_civil' => is_null($request->input('estado-civil'))? 0: $request->input('estado-civil'), 
	    		'numero_tarjetas' =>  is_null($request->input('numero-tarjetas'))?0 : $request->input('numero-tarjetas'),
	    		'precio_total' =>  is_null($request->input('precio-total'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('precio-total')))),
	    		'pagado' => is_null($request->input('pagado'))? 0 :doubleval(str_replace(",", "",str_replace("$", "", $request->input('pagado')))), 
	    		'saldo_pendiente' => is_null($request->input('saldo-pendiente'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('saldo-pendiente')))),
	    		'comentarios' => is_null($request->input('comentarios'))? "": $request->input('comentarios'),
	    		'desglose_paquete' => is_null($request->input('desglose-paquete'))? "": $request->input('desglose-paquete'), 
	    		'desglose_hoteleria' => is_null($request->input('desglose-hoteleria'))? "": $request->input('desglose-hoteleria'), 
	    		'comentarios_verificacion' => is_null($request->input('comentarios-verificacion'))? "": $request->input('comentarios-verificacion')
	    	]);

	    	//guardar datos del cliente y el acompañante
	    	$surveyDatosCliente = $this->surveyDatosCliente->create([
	    		'nombre' => $request->input('nombre'),
	    		'edad' => is_null($request->input('edad'))? "" : $request->input('edad'),
	    		'ocupacion' => is_null($request->input('ocupacion'))? "" :  $request->input('ocupacion'),
	    		'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
	    		'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
	    		'estado' => is_null($request->input('estado'))? "" : $request->input('estado'),
	    		'email' => is_null($request->input('email'))? '':$this->encrypt->encrypt($request->input('email')),
	    		'tipo' => 'P',
	    		'survey_id' => $survey->id
	    	]);
	    	//si lleva acompañante tambien se registra
	    	if(!is_null($request->input('nombre-ac')))
	    	{
		    	$surveyDatosCliente2 = $this->surveyDatosCliente->create([
		    		'nombre' => $request->input('nombre-ac'),
		    		'edad' => is_null($request->input('edad-ac'))? "": $request->input('edad-ac'),
	    			'ocupacion' => is_null($request->input('ocupacion-ac'))? "" :  $request->input('ocupacion-ac'),
	    			'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
	    			'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
	    			'estado' => is_null($request->input('estado'))? "" : $request->input('estado'),
	    			'email' => is_null($request->input('email'))? '':$this->encrypt->encrypt($request->input('email')),
		    		'tipo' => 'A',
		    		'survey_id' => $survey->id
		    	]);
	    	}

	    	//datos de reservacion
	    	$surveyDatosReserva = $this->surveyDatosReserva->create([
	    		'checkin' => is_null($request->input('checkin'))? "": $request->input('checkin'), 
	    		'checkout' => is_null($request->input('checkout'))? "": $request->input('checkout'), 
	    		'plan' => is_null($request->input('plan'))? "" : $request->input('plan'), 
	    		'adultos' => is_null($request->input('adultos'))? "": $request->input('adultos'), 
	    		'menores' => is_null($request->input('menores'))? "":  $request->input('menores'), 
	    		'edad_menores' => is_null($request->input('edad-menores'))? "": $request->input('edad-menores'),
	    		'tipo_habitacion' => is_null($request->input('tipo-habitacion'))? "" : $request->input('tipo-habitacion'),
	    		'cuantas_habitaciones' => is_null($request->input('cuantas-habitaciones'))? "0": $request->input('cuantas-habitaciones'),
	    		'viaja_con_familiares' => ($request->input('viaja-con-familiares') =='S'),
	    		'survey_id' => $survey->id
	    	]);

	    	//datos de la tarjeta
	    	$vencimiento = "";
	    	if(@strlen($request->input('vencimiento-p')) ==7)
	    	{
	    		$vencimiento = date('20'.substr($request->input('vencimiento-p'),-2 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
	    	}elseif(@strlen($request->input('vencimiento-p')) ==9)
	    	{
	    		$vencimiento = date(substr($request->input('vencimiento-p'),-4 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
	    	}
	    	//se crea la tarjeta de pago
	    	$surveyDatosTarjeta = $this->surveyDatosTarjeta->create([
	    		'numero_tarjeta' => is_null($request->input('numero-tarjeta-p'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-p')),
	    		'banco' => is_null($request->input('banco-p'))? "": $request->input('banco-p'),
	    		'vencimiento' => $vencimiento,
	    		'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-p'))? "": $request->input('tipo-tarjeta-p'),
	    		'codigo' => is_null($request->input('codigo-p'))? "": $request->input('codigo-p'),
	    		'pago_garantia' => "P", //se coloca como P por ser de Pago
	    		'survey_id' => $survey->id
	    	]);
	    	$vencmiento = "";
	    	if(@strlen($request->input('vencimiento-g')) ==7)
	    	{
	    		$vencimiento = date('20'.substr($request->input('vencimiento-g'),-2 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
	    	}elseif(@strlen($request->input('vencimiento-g')) ==9)
	    	{
	    		$vencimiento = date(substr($request->input('vencimiento-g'),-4 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
	    	}
	    	//se crea la tarjeta de garantia
	    	$surveyDatosTarjetaG = $this->surveyDatosTarjeta->create([
	    		'numero_tarjeta' => is_null($request->input('numero-tarjeta-g'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-g')),
	    		'banco' => is_null($request->input('banco-g'))? "": $request->input('banco-g'),
	    		'vencimiento' => $vencimiento,
	    		'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-g'))? "": $request->input('tipo-tarjeta-g'),
	    		'codigo' => is_null($request->input('codigo-g'))? "": $request->input('codigo-g'),
	    		'pago_garantia' => "G", //se coloca como P por ser de Pago
	    		'survey_id' => $survey->id
	    	]);
    	} catch (Exception $e) {
    		//eliminar registros en caso de ocurrir un error
    		if(isset($surveyDatosTarjetaG)){
    			$surveyDatosTarjetaG->delete();
    		}
    		if(isset($surveyDatosTarjeta)){
    			$surveyDatosTarjeta->delete();
    		}
    		if(isset($surveyDatosReserva)){
    			$surveyDatosReserva->delete();
    		}

    		if(isset($surveyDatosCliente2)){
    			$surveyDatosCliente2->delete();
    		}
    		if(isset($surveyDatosCliente)){
    			$surveyDatosCliente->delete();
    		}
    		if(isset($survey)){
    			$survey->delete();
    		}
    		return redirect()->back()->withErrrs("Hubo un problema al registrar survey, revise datos")->withInput($request->all());
    	}
    	
    	return redirect()->route('listSurvey')->with(["success" => "Registrado correctamente"]);
    }

    public function edit($id, Request $request)
    {
    	$survey = $this->survey->find($id);

    	if(is_null($survey)){
    		return redirect()->back()->withErrors("No se encontró el Survey");
    	}

    	$estadosCiviles = $this->maritalStatus->all();
    	$executives = $this->executive->where('estatus', 'A')->orderBy('nombre', 'asc')->get();
    	$origenes = $this->origen->where('nombre', '!=', '')->orderBy('nombre', 'asc')->get();
    	return view('survey.edit', compact('survey','executives', 'origenes', 'estadosCiviles'));
    }

    public function update($id, Request $request)
    {
    	$survey = $this->survey->find($id);

    	if(is_null($survey)){
    		return redirect()->back()->withErrors("No se encontró el suvvey solicitado")->withInput($request->all());
    	}
    	try {
  			//primero creamos los datos del survey
	    	$survey->update([
	    		'comprobante_qa' => is_null($request->input('comprobante-qa'))? "": $request->input('comprobante-qa'),
	    		'comentario_validacion' => is_null($request->input('comentario-validacion'))? "": $request->input('comentario-validacion'),
	    		'fecha_primer_contacto' => date("Y-m-d H:i:s", strtotime($request->input('fecha-primer-contacto'))), 
	    		'executive_id' => $request->input('executive-id'), 
	    		'origen_id' => $request->input('origen-id'),
	    		'vendedor' => is_null($request->input('vendedor'))? "": $request->input('vendedor'), 
	    		'folio' => is_null($request->input('folio'))? "": $request->input('folio'), 
	    		'a_visitado' => ($request->input('a-visitado')=="S"), 
	    		'tomado_presentacion' => ($request->input('tomado-presentacion') == "S"),
	    		'hace_cuanto' => is_null($request->input('hace-cuanto'))? "": $request->input('hace-cuanto'),
	    		'se_hizo_socio' => ($request->input('se-hizo-socio')=="S"), 
	    		'numero_aprobacion' => is_null($request->input('numero-aprobacion'))? "": $request->input('numero-aprobacion'), 
	    		'requiere_factura' => ($request->input('requiere-factura') == "S"), 
	    		'ingreso_mensual' => is_null($request->input('ingreso-mensual'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('ingreso-mensual')))), 
	    		'estado_civil' => is_null($request->input('estado-civil'))? 0: $request->input('estado-civil'), 
	    		'numero_tarjetas' =>  is_null($request->input('numero-tarjetas'))?0 : $request->input('numero-tarjetas'),
	    		'precio_total' =>  is_null($request->input('precio-total'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('precio-total')))),
	    		'pagado' => is_null($request->input('pagado'))? 0 :doubleval(str_replace(",", "",str_replace("$", "", $request->input('pagado')))), 
	    		'saldo_pendiente' => is_null($request->input('saldo-pendiente'))? 0 : doubleval(str_replace(",", "",str_replace("$", "", $request->input('saldo-pendiente')))),
	    		'comentarios' => is_null($request->input('comentarios'))? "": $request->input('comentarios'),
	    		'desglose_paquete' => is_null($request->input('desglose-paquete'))? "": $request->input('desglose-paquete'), 
	    		'desglose_hoteleria' => is_null($request->input('desglose-hoteleria'))? "": $request->input('desglose-hoteleria'), 
	    		'comentarios_verificacion' => is_null($request->input('comentarios-verificacion'))? "": $request->input('comentarios-verificacion')
	    	]);
	    	$surveyDatosCliente = $survey->surveyDatosClientes("P");
	    	if(!is_null($surveyDatosCliente))
	    	{
	    		$surveyDatosCliente->update([
		    		'nombre' => $request->input('nombre'),
		    		'edad' => is_null($request->input('edad'))? "" : $request->input('edad'),
		    		'ocupacion' => is_null($request->input('ocupacion'))? "" :  $request->input('ocupacion'),
		    		'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
		    		'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
		    		'estado' => is_null($request->input('estado'))? "" : $request->input('estado'),
		    		'email' => is_null($request->input('email'))? '':$this->encrypt->encrypt($request->input('email')),
		    		'tipo' => 'P',
		    		'survey_id' => $survey->id
		    	]);
	    	}

	    	$surveyDatosCliente2 = $survey->surveyDatosClientes("A");
	    	//si lleva acompañante tambien se registra
	    	if(!is_null($surveyDatosCliente2) && $surveyDatosCliente2 != "")
	    	{
		    	$surveyDatosCliente2->update([
		    		'nombre' => $request->input('nombre-ac'),
		    		'edad' => is_null($request->input('edad-ac'))? "": $request->input('edad-ac'),
	    			'ocupacion' => is_null($request->input('ocupacion-ac'))? "" :  $request->input('ocupacion-ac'),
	    			'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
	    			'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
	    			'estado' => is_null($request->input('estado'))? "" : $request->input('estado'),
	    			'email' => is_null($request->input('email'))? '':$this->encrypt->encrypt($request->input('email')),
		    		'tipo' => 'A',
		    		'survey_id' => $survey->id
		    	]);
	    	}else if(!is_null($request->input('nombre-ac'))) {
	    		//se crean los datos del segundo cliente en caso de no existir datos
		    	$surveyDatosCliente2 = $this->surveyDatosCliente->create([
		    		'nombre' => $request->input('nombre-ac'),
		    		'edad' => is_null($request->input('edad-ac'))? "": $request->input('edad-ac'),
	    			'ocupacion' => is_null($request->input('ocupacion-ac'))? "" :  $request->input('ocupacion-ac'),
	    			'telefono_celular' => is_null($request->input('telefono-celular'))? "": $this->encrypt->encrypt($request->input('telefono-celular')),
	    			'telefono_casa' => is_null($request->input('telefono-casa'))? "" : $this->encrypt->encrypt($request->input('telefono-casa')),
	    			'estado' => is_null($request->input('estado'))? "" : $request->input('estado'),
	    			'email' => is_null($request->input('email'))? '':$this->encrypt->encrypt($request->input('email')),
		    		'tipo' => 'A',
		    		'survey_id' => $survey->id
		    	]);
	    	}

	    	$surveyDatosReserva = $survey->surveyDatosReserva;
	    	//datos de reservacion
	    	$surveyDatosReserva->update([
	    		'checkin' => is_null($request->input('checkin'))? "": $request->input('checkin'), 
	    		'checkout' => is_null($request->input('checkout'))? "": $request->input('checkout'), 
	    		'plan' => is_null($request->input('plan'))? "" : $request->input('plan'), 
	    		'adultos' => is_null($request->input('adultos'))? "": $request->input('adultos'), 
	    		'menores' => is_null($request->input('menores'))? "":  $request->input('menores'), 
	    		'edad_menores' => is_null($request->input('edad-menores'))? "": $request->input('edad-menores'),
	    		'tipo_habitacion' => is_null($request->input('tipo-habitacion'))? "" : $request->input('tipo-habitacion'),
	    		'cuantas_habitaciones' => is_null($request->input('cuantas-habitaciones'))? "": $request->input('cuantas-habitaciones'),
	    		'viaja_con_familiares' => ($request->input('viaja-con-familiares') =='S'),
	    		'survey_id' => $survey->id
	    	]);

	    	//datos de la tarjeta
	    	$vencimiento = "";
	    	if(@strlen($request->input('vencimiento-p')) ==7)
	    	{
	    		$vencimiento = date('20'.substr($request->input('vencimiento-p'),-2 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
	    	}elseif(@strlen($request->input('vencimiento-p')) ==9)
	    	{
	    		$vencimiento = date(substr($request->input('vencimiento-p'),-4 ).'-'.substr($request->input('vencimiento-p'),0,2).'-01');
	    	}

	    	$surveyDatosTarjeta = $survey->surveyDatosTarjetas('P');

	    	$tarjetaPago = $surveyDatosTarjeta->numero_tarjeta;
	    	if(Auth::user()->hasAccessApp('survey_decrypt_card')){
	    		$tarjetaPago = is_null($request->input('numero-tarjeta-p'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-p'));
	    	}
	    	//se crea la tarjeta de pago
	    	$surveyDatosTarjeta->update([
	    		'numero_tarjeta' => $tarjetaPago,
	    		'banco' => is_null($request->input('banco-p'))? "": $request->input('banco-p'),
	    		'vencimiento' => $vencimiento,
	    		'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-p'))? "": $request->input('tipo-tarjeta-p'),
	    		'codigo' => is_null($request->input('codigo-p'))? "": $request->input('codigo-p'),
	    		'pago_garantia' => "P", //se coloca como P por ser de Pago
	    		'survey_id' => $survey->id
	    	]);
	    	$vencmiento = "";

	    	if(@strlen($request->input('vencimiento-g')) ==7)
	    	{
	    		$vencimiento = date('20'.substr($request->input('vencimiento-g'),-2 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
	    	}elseif(@strlen($request->input('vencimiento-g')) ==9)
	    	{
	    		$vencimiento = date(substr($request->input('vencimiento-g'),-4 ).'-'.substr($request->input('vencimiento-g'),0,2).'-01');
	    	}
	    	$surveyDatosTarjetaG = $survey->surveyDatosTarjetas('G');

	    	$tarjetaGarantia = $surveyDatosTarjetaG->numero_tarjeta;
	    	if(Auth::user()->hasAccessApp('survey_decrypt_card')){
	    		$tarjetaGarantia = is_null($request->input('numero-tarjeta-g'))? "": $this->encrypt->encrypt($request->input('numero-tarjeta-g'));
	    	}
	    	//se crea la tarjeta de garantia
	    	$surveyDatosTarjetaG->update([
	    		'numero_tarjeta' => $tarjetaGarantia,
	    		'banco' => is_null($request->input('banco-g'))? "": $request->input('banco-g'),
	    		'vencimiento' => $vencimiento,
	    		'tipo_tarjeta' => is_null($request->input('tipo-tarjeta-g'))? "": $request->input('tipo-tarjeta-g'),
	    		'codigo' => is_null($request->input('codigo-g'))? "": $request->input('codigo-g'),
	    		'pago_garantia' => "G", //se coloca como P por ser de Pago
	    		'survey_id' => $survey->id
	    	]);

    	} catch (Exception $e) {
    		//eliminar registros en caso de ocurrir un error
    		if(isset($surveyDatosTarjetaG)){
    			$surveyDatosTarjetaG->delete();
    		}
    		if(isset($surveyDatosTarjeta)){
    			$surveyDatosTarjeta->delete();
    		}
    		if(isset($surveyDatosReserva)){
    			$surveyDatosReserva->delete();
    		}

    		if(isset($surveyDatosCliente2)){
    			$surveyDatosCliente2->delete();
    		}
    		if(isset($surveyDatosCliente)){
    			$surveyDatosCliente->delete();
    		}
    		if(isset($survey)){
    			$survey->delete();
    		}
    		return redirect()->back()->withErrrs("Hubo un problema al registrar survey, revise datos")->withInput($request->all());
    	}
    	return redirect()->back()->with(["success" => "Modificado correctamente"]);
    }

    //**api para refresh de datos de los surveys**//
    public function apiSurveyList(Request $request)
	{
		if(Auth::user()->hasAccessApp("survey_show_all"))
		{
			$surveys = $this->survey->with('surveyDatosCliente');
			
			
		}elseif(Auth::user()->hasAccessApp("survey_show")){
			$surveys = $this->survey
			->with("surveyDatosCliente")
			->where("executive_id", Auth::user()->executive->id);
		}else{
			$datatable = DataTables::of($this->survey->where("id", 0)
			->with("surveyDatosCliente")->get())
        ->rawColumns(['survey_operation', 'assign'])->make(true);
        return $datatable;
		}


        $datatable = DataTables::of($surveys->get())
        ->rawColumns(['survey_operation', 'assign'])->make(true);
        return $datatable;
    }

    public function delete($id)
    {
    		$survey = $this->survey->find($id);
    		if(is_null($survey))
    		{
    			return redirect()->back()->withErrors("Dato no válido o inexistente");
    		}

	    	$surveyDatosTarjetaG = $survey->surveyDatosTarjetas('G');
	    	$surveyDatosTarjeta = $survey->surveyDatosTarjetas('P');
	    	$surveyDatosReserva = $survey->surveyDatosReserva;
	    	$surveyDatosCliente2 = $survey->surveyDatosClientes("A");
	    	$surveyDatosCliente = $survey->surveyDatosClientes("P");

    	    //eliminar registros en caso de ocurrir un error
    		if(!is_null($surveyDatosTarjetaG) && $surveyDatosTarjetaG != ""){
    			$surveyDatosTarjetaG->delete();
    		}
    		if(!is_null($surveyDatosTarjeta) && $surveyDatosTarjeta != ""){
    			$surveyDatosTarjeta->delete();
    		}
    		if(!is_null($surveyDatosReserva) && $surveyDatosReserva != ""){
    			$surveyDatosReserva->delete();
    		}

    		if(!is_null($surveyDatosCliente2) && $surveyDatosCliente2 != ""){
    			$surveyDatosCliente2->delete();
    		}
    		if(!is_null($surveyDatosCliente) && $surveyDatosCliente != ""){
    			$surveyDatosCliente->delete();
    		}

    		$survey->delete();

    		return redirect()->back()->with(["success" => "Eliminado correctamente"]);

    }

    public function assign($id)
    {
    	$survey = $this->survey->find($id);

    	if(is_null($survey)) {
    		abort(404);
    	}

    	 $card = $survey->surveyDatosTarjeta->
         $data = [
                'ejecutivo' => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                'origen' => $request->input("origen"),
                'vendedor' => ($request->input('seller') != null)? $request->input('seller'): "",
                'nombre' => ($request->input('customer-name') != null)? $request->input('customer-name'): "",
                'edad' => ($request->input('age') != null)? $request->input("age") : null,
                'ocupacion' => ($request->input('customer-job') != null)? $request->input("customer-job") : "",
                'nombre_co' => ($request->input('customer-name-co') != null)? $request->input('customer-name-co') : "",
                'edad_co' => ($request->input('age-co') != null)? $request->input('age-co') : null,
                'ocupacion_co' => ($request->input('customer-job-co') != null)? $request->input('customer-job-co') : "",
                'tel_cel' => ($request->input('cell-phone') != null)? $request->input('cell-phone'): "",
                'tel_casa' => ($request->input('home-phone') != null)? $request->input('home-phone') : "",
                'tel_oficina' => ($request->input("office-phone") != null)? $request->input("office-phone") : "",
                'extencion' => ($request->input('extension') != null)? $request->input('extension') : "",
                'estado' => ($request->input('customer-state') != null)? $request->input('customer-state') : "",
                'email' => ($request->input('customer-email') != null)? $request->input('customer-email') : "",
                'checkin' => (strlen($request->input('check-in')) >= 10)? substr($request->input('check-in'), 0,10)." 00:00:00" : null,
                'checkout' => (strlen($request->input('check-out')) >= 10)? substr($request->input('check-out'), 0,10)." 00:00:00" : null,
                'plan' => $request->input('customer-plan'),
                'adultos' => $request->input('customer-adults'),
                'menores' => $request->input('customer-children'),
                'edadmenores' => ($request->input('children-age') != null)? $request->input('children-age') : "",
                'folioreserva' => ($request->input('reservation-sheet') != null)? $request->input('reservation-sheet') : "",
                'noches_en_certificado' => $request->input('certification-nights'),
                'noches_en_hotel' => $request->input('hotel-nights'),
                'certi_hotel' => $request->input('reservation-type'),
                'precio_total' => $request->input('total-price'),
                'total_certificado' => $request->input('certificate-price'),
                'total_hotel' => $request->input('total-hotel'),
                'pagado' => $request->input('paid-out'),
                'pendiente' => $request->input('pending-payment'),
                'tarjeta' => ( $card != null && strlen($card) > 11)? substr($card, 0, 12) : "",
                'four_digits' => ($card != null && strlen($card > 3))? substr($card, -4) : "",
                'banco' => ($request->input('customer-bank') != null)? $request->input('customer-bank') : "",
                'vencimiento1' => (strlen($request->input('card-expiration')) > 8 )? substr($request->input('card-expiration'), 0, 2): "",
                'vencimiento2' => (strlen($request->input('card-expiration')) > 8 )? substr($request->input('card-expiration'), -2) : "",
                'codigo' => (strlen($request->input('card-code')) > 2 )? $request->input('card-code') : "",
                'tipotarjeta' => $request->input('card-type'),
                'num_tarjetas' => ($request->input('cards-number') != null)? $request->input('cards-number') : 0,
                'visa' => $request->input('has-visa'),
                'mc' => $request->input('has-mc'),
                'amex' => $request->input('has-amex'),
                'ingreso_mensual' => ($request->input('monthly-income') != null)? $request->input('monthly-income') : 0,
                'estado_civil' => $request->input('marital-status'),
                'numero_contrato' => ($request->input('contract-number') != null)? $request->input('contract-number') : "",
                'total_membresia' => $request->input('membership-total'),
                'solicita_factura' => $request->input('requires-invoice'),
                'habitacion' => ($request->input('room-type') != null)? $request->input('room-type') : "",
                'comentario' => ($request->input('comments-tracing') != null)? $request->input('comments-tracing'): "",
                'observaciones' => ($request->input('membership-observations') != null)? $request->input('membership-observations') : "",
                'tipo_q' => $request->input('qs-type'),
                'fecha_venta' => (strlen($request->input('sale-date')) >= 10)? substr($request->input('sale-date'), 0,10)." 00:00:00" : null,
                'tienefirmados' => $request->input('has-signed'),
                'categoria' => $request->input('category-client'),
                'tipo' => 'CLIENTE',
                'modificado_por' =>  (!is_null(Auth::user()->executive))? Auth::user()->executive->id : Auth::user()->id,
                'interesados' => 'S',
                'padre' => (!is_null(Auth::user()->executive))? Auth::user()->executive->padre : Auth::user()->id,
                'prefix_name' => $request->input('prefix-name'),
                'prefix_name2' => $request->input('prefix-name2'),
                'address' => is_null($request->input('address'))? '' : $request->input('address'),
                'has_vacation_ownership' => ($request->input('has-vacation-ownership') == "Y")? true : false,
                'take_presentation' => ($request->input('take-presentation') == "Y")? true  : false,
                'date_presentation' => ($request->input('date-presentation') != "" && $request->input('date-presentation') != null)? $request->input('date-presentation') : "",
                'homeowner' => ($request->input('homeowner') == "Y")? true : false,
                'many_times' => is_null($request->input('many-times'))? 0 : $request->input('many-times')
            ];
            $customer = $this->customer->create($data);
            $this->audit($data, $customer, "create");
            $customer->fecha_alta = $customer->created_at;
            $customer->fecha_modificacion = $customer->updated_at;
            $customer->save();

            if($request->input('has-note') == "S"){
                $datetime_move = date("Y-m-d H:i:s", strtotime($request->input('date-hour')));
                $move = $this->move->create([
                    "cliente" => $customer->id,
                    "actividad" => $request->input('note-activity'),
                    "fecha" => $datetime_move,
                    "estatus" => $request->input('note-status'),
                    "observaciones" => $request->input('note-details'),
                    "ejecutivo" => ($request->input('executive') != null)? $request->input('executive'): ((!is_null(Auth::user()->executive))? Auth::user()->executive->id : ""),
                    "titulo" => $request->input('note-title'),
                    "padre" => $customer->padre
                ]);
            }
            Alert::success('Cliente creado correctamente')->flash();
            return json_encode($customer);
    }
}
