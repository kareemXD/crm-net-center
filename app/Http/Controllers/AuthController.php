<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Crm\UserSafety;
use App\Crm\SecLogged;
use App\Http\Requests\LoginRequest;
use Alert;
use Hash;

class AuthController extends Controller
{
	private $userSafety;
    private $secureLogged;
	public function __construct(
		UserSafety $userSafety,
        SecLogged $secureLogged
	){
		$this->middleware('guest', ['except' => 'logout']);
		$this->userSafety = $userSafety;
        $this->secureLogged = $secureLogged;
	}
    public function getLogin()
    {
    	return view("auth.login");
    }

    public function logout()
    {
        $login = $this->secureLogged->create([
            "login" => Auth::user()->login,
            "date_login" => date("Y-m-d h:i:sa"),
            "sc_session" => "_SC_LOGOUT_SC_",
            "ip" => $_SERVER['REMOTE_ADDR']
        ]);
    	Auth::logout();
    	return redirect()->route("login");
    }

    public function postLogin(LoginRequest $request)
    {
        $password = md5($request->input('password'));
        $status = false;
        $userSafety = $this->userSafety->where("login", $request->input("username"))->first();

        if(!is_null($userSafety))
        {
            if(Hash::check($password, $userSafety->pswd))
            {
                $status = true;
            }
        }
        //checar si el login es por ajax
    	if($request->ajax())
        {
            if($status)
            {
                Auth::loginUsingId($userSafety->login, true);
                return json_encode(route("home"));
            }else{
                return json_encode(false);
            }
        }else{ //en caso contrario
            if($status)
            {
                if($userSafety->active == "Y")
                {
                    $login = $this->secureLogged->create([
                        "login" => $userSafety->login,
                        "date_login" => date("Y-m-d h:i:sa"),
                        "sc_session" => "_SC_TRUE_SC_",
                        "ip" => $_SERVER['REMOTE_ADDR']
                    ]);

                    Auth::loginUsingId($userSafety->login, true);
                    return redirect()->intended(route("home"));
                }else{
                    Alert::info("El usuario no se encuentra activo")->flash();
                    return redirect()->back()->withInput();
                }

            }else{
                Alert::error("los datos no son correctos")->flash();
                return redirect()->back()->withInput();
            }
        }
    }
}
