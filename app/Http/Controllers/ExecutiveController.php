<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crm\Executive;
use Auth;

class ExecutiveController extends Controller
{
	private $executive;

    public function __construct(
    	Executive $executive
    ){
    	$this->executive = $executive;
    }
}
