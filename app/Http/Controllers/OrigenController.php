<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Crm\Origen;
use App\Http\Requests\Origen\UpdateOrigenRequest;
use DataTables as Datatables;
use App\Crm\SubOrigen;

use App\Http\Requests\Origen\StoreUpdateSubOrigenRequest;

class OrigenController extends Controller
{
	private $origen, $subOrigen;

	public function __construct
	(
		Origen $origen,
		SubOrigen $subOrigen
	){
		$this->origen = $origen;
		$this->subOrigen = $subOrigen;
	}

	public function listOrigen()
	{
		if(!Auth::user()->hasAccessApp("origen_modify")){
			return "Sin permisos";
		}
		$origens = $this->origen->where("nombre", "!=", "")->orderBy("id", "asc")->get();
		$dataSend = compact('origens');
		return view('origen.list', $dataSend);
	}

	public function editOrigen(Request $request, $id)
	{
		$origen = $this->origen->find($id);
		if(!is_null($origen))
		{
			return view('includes.origen.edit', compact('origen'));
		}

		return json_encode("error");
	}

	public function updateOrigen(UpdateOrigenRequest $request, $id)
	{	
		if(!Auth::user()->hasAccessApp("origen_modify")){
			return "Sin permisos";
		}
		$origen = $this->origen->find($id);
		if(!is_null($origen))
		{
			$origen->nombre = $request->input("origen-nombre");
			$origen->code = is_null($request->input("origen-code"))? "": strtoupper($request->input("origen-code")) ;
			$origen->save();

			return json_encode($origen);
		}
	}

	public function apiListOrigen(Request $request)
	{

        $origenes = $this->origen->where("nombre", "!=", "")->orderBy("id", "asc");

    	return Datatables::eloquent($origenes)->editColumn('actions', function(Origen $origen){
            return '<button data-target="#modalEdit" data-toggle="modal" data-route="'.route('origenEdit', $origen->id) .'" class="btn btn-info" title="Editar"><i class="fa fa-edit"></i></button><a class="btn btn-secondary" href="'.route('subOrigenList', $origen->id) .'">SubOrigen</a>';
        })->rawColumns(['actions'])->make(true);
	}

	public function createOrigen()
	{
		return view("includes.origen.create");
	}

	public function storeOrigen(UpdateOrigenRequest $request)
	{
		if(!Auth::user()->hasAccessApp("origen_modify")){
			return "Sin permisos";
		}
		$origen =  $this->origen->create([
			'nombre' => $request->input('origen-nombre'),
			'code' => !is_null($request->input('origen-code'))? strtoupper($request->input('origen-code')) : ''
		]);

		return json_encode($origen);
	}

	/* seccion de suborigenes*/

	public function listSuborigen($origen_id=null)
	{
		if(!Auth::user()->hasAccessApp("origen_modify")){
			return "Sin permisos";
		}
		$origens = $this->origen->where("nombre", "!=", "")->orderBy("id", "asc")->get();
		if(!is_null($origen_id)){
			$subOrigens = $this->subOrigen->where("origen_id", $origen_id)->get();
		}else{
			$subOrigens = $this->subOrigen->all();
		}

		$dataSend = compact('origens', 'subOrigens', 'origen_id');

		return view("origen.suborigen", $dataSend);

	}

	public function editSubOrigen($id)
	{
		$origens = $this->origen->where("nombre", "!=", "")->orderBy("id", "asc")->get();
		$subOrigen = $this->subOrigen->find($id);
		$dataSend = compact('origens', 'subOrigen');
		if(!is_null($subOrigen)){
			return view('includes.origen.suborigen-edit', $dataSend);

		}

		abort(403);

	}

	public function updateSubOrigen(StoreUpdateSubOrigenRequest $request, $id)
	{
		if(!Auth::user()->hasAccessApp("origen_modify")){
			return "Sin permisos";
		}
		$subOrigen = $this->subOrigen->find($id);

		
		if(!is_null($subOrigen)){
			$subOrigen = $subOrigen->update([
				'name' => $request->input('suborigen-name'),
				'code' => !is_null($request->input('suborigen-code'))? strtoupper($request->input('suborigen-code')) : '',
				'origen_id' => $request->input('suborigen-origen-id'),
				'status' => ($request->input('suborigen-status') == "si")? 1 : 0
			]);
			return json_encode($subOrigen);
		}

		abort(403);
	}

	public function createSubOrigen($origen_id = null)
	{
		$origens = $this->origen->where("nombre", "!=", "")->orderBy("id", "asc")->get();

		$dataSend = compact('origens', 'origen_id');
		return view("includes.origen.suborigen-create", $dataSend);
	}

	public function storeSubOrigen(StoreUpdateSubOrigenRequest $request)
	{
		$subOrigen = $this->subOrigen->create([
			'name' => $request->input('suborigen-name'),
			'code' => !is_null($request->input('suborigen-code'))? strtoupper($request->input('suborigen-code')) : '',
			'origen_id' => $request->input('suborigen-origen-id'),
			'status' => ($request->input('suborigen-status') == "si")? 1 : 0
		]);

		return json_encode($subOrigen);
	}

	public function apiListSubOrigen(Request $request, $origen_id = null)
	{
		if(is_null($origen_id)){
			$subOrigens = $this->subOrigen->leftJoin("origen", "origen.id", "=", "sub_origenes.origen_id")
			->select("sub_origenes.*", "origen.nombre")->orderBy("sub_origenes.id", "asc");

		}else{
			$subOrigens = $this->subOrigen->leftJoin("origen", "origen.id", "=", "sub_origenes.origen_id")->where("origen_id", $origen_id)->select("sub_origenes.*", "origen.nombre")->orderBy("sub_origenes.id", "asc");
		}

    	return Datatables::eloquent($subOrigens)->editColumn('actions', function(SubOrigen $subOrigen){
            return '<button data-target="#modalEdit" data-toggle="modal" data-route="'. route('subOrigenEdit', $subOrigen->id).'" class="btn btn-info" title="Editar"><i class="fa fa-edit"></i></button>';
        })->editColumn('status', function(SubOrigen $subOrigen){
        	if($subOrigen->status==true){
        		$checked = 'checked="checked"';
        	}else{
        		$checked = '';
        	}

            return '<div class="material-switch">
			        	<input '.$checked.' data-route="'.route("subOrigenChangeStatus", $subOrigen->id) .'" value="si"    id="list_status_suborigen_'.$subOrigen->id .'" name="list-suborigen-status" type="checkbox">
			        	<label for="list_status_suborigen_'. $subOrigen->id .'" class="label-primary"></label>
			     	</div>';
        })->rawColumns(['actions', 'status'])->make(true);
	}

	public function changeStatusSubOrigen(Request $request, $id)
	{
		$subOrigen = $this->subOrigen->find($id);
		if(!is_null($subOrigen)){
			$subOrigen->status = ($request->input("suborigen-status") == "si")? true : false;

			$subOrigen->save();
			return json_encode($subOrigen);
		}
		abort(403);
	}

	public function loadSubOrigen($origen_id){
		$response = '';
		if(count($this->subOrigen->where("origen_id", $origen_id)->where("status", true)->get()) > 0){
			foreach($this->subOrigen->where("origen_id", $origen_id)->where("status", true)->get() as $subOrigen){
				$response .= '<option value="'.$subOrigen->id.'">'.$subOrigen->name.'</option>';
			}
		}else{
			$response = '<option value="0">No definido</option>';
		}


		return $response;
	}
}
