<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $connection = "intra";
    protected $table = "roles";
    protected $fillable = [
    	'name'
    ];

    public function applicationRoles()
    {
    	return $this->hasMany("App\ApplicationRoles");
    }

    public function applications()
    {
    	return $this->belongsToMany("App\Application", "application_roles");
    }

    public function users()
    {
    	return $this->belongsToMany("App\User", "application_users");
    }
}
