<?php

namespace App\Imports;
use App\CRM\Survey;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ContactImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        return new StdClass([
			"origen" => '',
			"nombre" => '',
			"edad" => '',
			"ocupacion" => '',
			"nombre_co" => '',
			"edad_co" => '',
			"ocupacion_co" => '',
			"tel_cel" => '',
			"tel_casa" => '',
			"tel_oficina" => '',
			"extencion" => '',
			"estado" => '',
			"email" => '',
			"banco" => '',
			"num_tarjetas" => '',
			"visa" => '',
			"mc" => '',
			"amex" => '',
			"ingreso_mensual" => '',
			"estado_civil" => '',
			"observaciones" => '',
        ]);
    }
}
