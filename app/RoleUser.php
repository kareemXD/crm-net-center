<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
	protected = $connection = "intra";
    protected $table = "role_users";
    protected $fillable = [
    	'role_id',
    	'user_id'
    ];
    public $timestamps = false;

    public function role()
    {
    	return $this->belongsTo("App\Role");
    }

    public function user()
    {
    	return $this->belongsTo("App\User");
    }
}
