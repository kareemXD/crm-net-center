<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationUser extends Model
{
	protected $connection = "intra";
    protected $table = "application_users";
    protected $fillable = [
    	"user_id",
    	"application_id"
    ];

    //relations

    public function user()
    {
    	return $this->belongsTo("App\Models\User", "user_id");
    }

    public function application()
    {
    	return $this->belongsTo("App\Application", "application_id");
    }
}
