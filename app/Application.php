<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
	protected $connection = "intra";
    protected $table = "applications";

    protected $fillable = [
        'name',
        'path'
    ];

    public function applicationUsers()
    {
    	return $this->hasMany("App\ApplicationUser");
    }

    public function users()
    {
    	return $this->belongsToMany("App\User", "application_users");
    }

    public function roles()
    {
    	return $this->belongsToMany("App\Role", "application_roles");
    }
}
