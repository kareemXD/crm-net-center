<?php

	namespace App\Services;

	class Encrypt{
		private $encrypt_method, $secret_key, $secret_password, $encrypt;

		public function __construct()
		{
			$this->encrypt_method = config("encrypt.encrypt_method");
			$this->secret_key = config("encrypt.encrypt_method");
			$this->secret_password = config("encrypt.secret_password");
		}

		public function setEncrypt($encrypt)
		{
			$this->encrypt = $encrypt;
		}

		public function getEncrypt()
		{
			return $this->encrypt;
		}
		/*Método para encriptar strings*/
		public function encrypt($string)
		{
			$action = "encrypt";
			if(strlen(trim($string)) > 0)
			{
				$newString = $this->descryptOrEncrypt($action, $string);
				return $newString;
			}

			return false;
		}


		/**método para desencriptar string **/
		public function decrypt($string)
		{
			$action = "decrypt";
			if(strlen(trim($string)) > 0)
			{
				$newString = $this->descryptOrEncrypt($action, $string);
				return $newString;
			}

			return false;
		}


		/**Método general para encriptar o desencriptar **/
		private function descryptOrEncrypt($action, $string)
		{
		    $output = false;

		    $encrypt_method = $this->encrypt_method;
		    $secret_key = $this->secret_key;
		    $secret_password = $this->secret_password;

		    // hash
		    $key = hash('sha256', $secret_key);
		    
		    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		    $iv = substr(hash('sha256', $secret_password), 0, 16);

		    if( $action == 'encrypt' ) {
		        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		        $output = base64_encode($output);
		    }
		    else if( $action == 'decrypt' ){
		        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		    }

		    return $output;
		}

		/**Servicio para revisar si una cadena es un email válido*/
		public function checkEmail($email)
		{
			if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    return true;
			}

			return false;
		}

		/*Servicio para convertir string en booleano*/
		public function toBool($var) 
    {
        if (!is_string($var)) return (bool) $var;
        switch (strtolower($var)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'y':
                return true;
            default:
                return false;
        }
    }

	}