<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Crm\Customer;

class EcryptCrm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'encrypt:crm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $customers = Customer::all();

        foreach($customers as $cus)
        {
            $customer = Customer::find($cus->id);
            $position = strpos($customer->email, "@");
            $customer->email = str_replace(substr($customer->email, 0, $position), str_random(10), $customer->email);
            $customer->tarjeta = rand(1000, 9999)."".rand(1000, 9999)."".rand(1000, 9999);
            $customer->tel_cel = rand(1000, 9999)."".rand(100000, 999999);
            $customer->tel_casa = rand(1000, 9999)."".rand(100000, 999999);
            $customer->tel_oficina = rand(1000, 9999)."".rand(100000, 999999);
            $customer->save();
        }
        dd("completado");
    }
}
