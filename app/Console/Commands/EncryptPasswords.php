<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CRM\UserSafety;
use Hash;
class EncryptPasswords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passwords:encrypt';
    private $userSafety;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para encriptar informacion de las passwords y contraseñas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        UserSafety $userSafety
    )
    {
        $this->userSafety = $userSafety;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->userSafety->all() as $index => $user)
        {
            if(!(strlen($user->pswd) == 60 && preg_match('/^\$2y\$/', $user->pswd )))
            {
                $user->pswd = Hash::make($user->pswd);
                $user->save();
            }
        }  
    }
}
