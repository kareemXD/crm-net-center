<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Encrypt;
use App\Crm\Customer;

class EncryptCrmData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'encrypt:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encryptacion de datos para cuidar datos de tarjeta correo y telefono';
    private $encrypt, $customer;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        Encrypt $encrypt,
        Customer $customer
    ){
        parent::__construct();
        $this->encrypt = $encrypt;
        $this->customer = $customer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $customers = $this->customer->all();

        foreach($customers as $customer)
        {
            $customer->email = $customer->email;
            $customer->tarjeta = $customer->tarjeta;
            $customer->tel_cel = $customer->tel_cel;
            $customer->tel_casa = $customer->tel_casa;
            $customer->tel_oficina = $customer->tel_oficina;
            $customer->save();
        }

        dd("completado");
    }

}
