<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Crm\Email;
class DeleteExchanges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:exchanges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comand para eliminar registros de exchanges';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $email;

    public function __construct(Email $email)
    {
        $this->email = $email;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $exchanges = $this->email->where(function($query){
            $query->where("origen", "X")->orWhere("origen", "x");
        })->whereRaw("(fecha_contacto < now() - INTERVAL 2 WEEK)");

        $exchangesCount = $exchanges->count();

        $exchanges->update(["origen" => "Y"]);
 
        dd("listo se reubicaron ".$exchangesCount." contactos de NC Exchanges");
    }
}
