<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Crm\ADMIN\UserGroup;
class DeleteNotGroup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'group:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Eliminar grupos que no corresponden';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userGroups = UserGroup::where("group_id", ">", 5)->get();
        foreach($userGroups as $userGroup)
        {
            $userGroup->delete();
        }

        dd("true");
    }
}
