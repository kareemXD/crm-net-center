<?php

namespace App\Exports;

use App\CRM\Customer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Http\Controllers\ReportController;

class CustomerExport implements FromView
{
	public function __construct(ReportController $reportController){
		$this->reportController = $reportController;
	}
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        $customers = $this->reportController->checkAdvancedFilterStatic(request());
        $customers = $customers->get();
        return view("exports.reports.customer-list", compact("customers"));
    }
}
