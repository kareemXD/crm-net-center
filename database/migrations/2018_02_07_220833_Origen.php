<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Origen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //crear columna de codigo de origen
        Schema::table('origen', function (Blueprint $table) {
            if(!(Schema::hasColumn('origen', 'code')))
            {
                $table->string("code", 1)->nulleable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //quitar la columna
        Schema::table('origen', function (Blueprint $table) {
            if(Schema::hasColumn('origen', 'code'))
            {
                $table->dropColumn('code');
            }
        });
    }
}
