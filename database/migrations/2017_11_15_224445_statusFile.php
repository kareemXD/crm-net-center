<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatusFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos_clientes', function (Blueprint $table) {
            if(!Schema::hasColumn('documentos_clientes', 'file_status')){
                $table->boolean("file_status");
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos_clientes', function (Blueprint $table) {
            if(Schema::hasColumn('documentos_clientes', 'file_status')){
                 $table->dropColumn('file_status');
            }
        });
    }
}
