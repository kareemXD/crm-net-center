<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerSubOrigenAndCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {

            if(!Schema::hasColumn('clientes', 'suborigen')) {
                $table->integer('suborigen')->unsigned()->default(0);
            }

            if(!Schema::hasColumn('clientes', 'country')) {
                $table->integer('country')->unsigned()->default(42);
            }
            if(!Schema::hasColumn('clientes', 'state')) {
                $table->integer('state')->unsigned()->default(2204);
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
