<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevelToRol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasColumn('sec_seguridadgroups', 'level')) {
            Schema::table('sec_seguridadgroups', function (Blueprint $table) {
                $table->integer('level')->unsigned()->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if(Schema::hasColumn('sec_seguridadgroups', 'level')) {
            Schema::table('sec_seguridadgroups', function (Blueprint $table) {
                 $table->dropColumn('level');
            });
        }
    }
}
