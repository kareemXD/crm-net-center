<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecGroupAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //se crea la migracion para la tabla de permisos y la de grupo y permisos
    public function up()
    {

        //la tabla de permisos 
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string("permission");
            $table->string("description");
            $table->timestamps();
        });

        //los permisos de acceso que tiene un cierto grupo
        Schema::create('group_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("permission_id")->unsiged();
            $table->integer("group_id")->unsigned();
            $table->timestamps();

        });


        //tabla de pertenencia de un usuario a permisos
        Schema::create('user_permission', function(Blueprint $table){
            $table->increments('id');
            $table->string("login");
            $table->integer("permission_id")->unsigned();
            $table->timestamps();
        });



        //tabla de relacion para le pertenencia de un usuario a cierto grupo
        Schema::create('user_groups', function(Blueprint $table){
            $table->increments('id');
            $table->string('login')->index();
            $table->integer('group_id')->unsigned();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //se elimina las tablas sobre roles y permisos las cuales apuntan a dps tablas ya definidas
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('group_permissions');
        Schema::dropIfExists('user_permission');
        Schema::dropIfExists('user_groups');
    }
}
