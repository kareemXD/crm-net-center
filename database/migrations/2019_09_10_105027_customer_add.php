<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {

            //sr o sra
            if(!Schema::hasColumn('clientes', 'prefix_name')) {
                $table->string('prefix_name', 20)->default('');
            }            
            //sr o sra acompañante
            if(!Schema::hasColumn('clientes', 'prefix_name2')) {
                $table->string('prefix_name2', 20)->default('');
            }
            //checar si tiene propiedad vacacional
            if(!Schema::hasColumn('clientes', 'has_vacation_ownership')) {
                $table->boolean('has_vacation_ownership')->default(false);
            }
            //address
            if(!Schema::hasColumn('clientes', 'address')) {
                $table->text('address')->nulleable()->default('');
            }
            //a tomado presentacion
            if(!Schema::hasColumn('clientes', 'take_presentation')) {
                $table->boolean('take_presentation')->default(false);
            }
            //fecha de presentacion
            if(!Schema::hasColumn('clientes', 'date_presentation')) {
                $table->date('date_presentation')->nulleable();
            }
            //revisar si es propietario de vivienda
            if(!Schema::hasColumn('clientes','homeowner')) {
                $table->boolean('homeowner')->default(false);
            }
            //Añadir cuantas veces vacaciona al año
            if(!Schema::hasColumn('clientes','many_times')) {
                $table->integer('many_times')->nulleable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            //sr o sra
            if(Schema::hasColumn('clientes', 'prefix_name')) {
                $table->dropColumn('prefix_name');
            }
            //sr o sra acompañante
            if(Schema::hasColumn('clientes', 'prefix_name2')) {
                $table->dropColumn('prefix_name2');
            }
            //checar si tiene propiedad vacacional
            if(Schema::hasColumn('clientes', 'has_vacation_ownership')) {
                $table->dropColumn('has_vacation_ownership');
            }
            //address
            if(Schema::hasColumn('clientes', 'address')) {
                $table->dropColumn('address');
            }
            //a tomado presentacion
            if(Schema::hasColumn('clientes', 'take_presentation')) {
                $table->dropColumn('take_presentation');
            }
            //fecha de presentacion
            if(Schema::hasColumn('clientes', 'date_presentation')) {
                $table->dropColumn('date_presentation');
            }
            //revisar si es propietario de vivienda
            if(Schema::hasColumn('clientes','homeowner')) {
                $table->dropColumn('homeowner');
            }
            //Añadir cuantas veces vacaciona al año
            if(Schema::hasColumn('clientes','many_times')) {
                $table->dropColumn('many_times');
            }

        });
    }
}
