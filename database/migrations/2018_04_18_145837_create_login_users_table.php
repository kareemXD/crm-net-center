<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sec_seguridadlogged', function (Blueprint $table) {
            if(!(Schema::hasColumn('sec_seguridadlogged','id')))
            {
                $table->increments('id');
            }

            if(!(Schema::hasColumn('sec_seguridadlogged','created_at')))
            {
                $table->timestamps();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sec_seguridadlogged', function (Blueprint $table) {
            if((Schema::hasColumn('sec_seguridadlogged', "id")))
            {
                $table->unsignedInteger('id')->change();
                $table->dropPrimary();
                $table->dropColumn('id');
            }
            if((Schema::hasColumn('sec_seguridadlogged','created_at')))
            {
                $table->dropColumn('created_at');
            }
            if((Schema::hasColumn('sec_seguridadlogged','updated_at')))
            {
                $table->dropColumn('updated_at');
            }
        });
    }
}
