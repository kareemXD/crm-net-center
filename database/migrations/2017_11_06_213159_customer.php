<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {

            if (Schema::hasColumn('clientes', 'deleted_at')) {
                //
            }else{
                $table->timestamps();
                $table->softDeletes();
            }
            if(Schema::hasColumn('clientes', '4digitos')) {
                $table->renameColumn('4digitos', 'four_digits');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {

            if (Schema::hasColumn('clientes', 'deleted_at')) {
                 $table->dropColumn('deleted_at');
            }            
            if (Schema::hasColumn('clientes', 'created_at')) {
                 $table->dropColumn('created_at');
            }
            if (Schema::hasColumn('clientes', 'updated_at')) {
                 $table->dropColumn('updated_at');
            }
            if(Schema::hasColumn('clientes', 'four_digits')) {
                $table->renameColumn('four_digits', '4digitos');
            }
        });
    }
}
