<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserSafetyToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sec_seguridadusers', function (Blueprint $table) {
            if(!(Schema::hasColumn('sec_seguridadusers', 'deleted_at'))){
                $table->softDeletes();
            }

            if(!(Schema::hasColumn('sec_seguridadusers', 'remember_token'))){
                $table->rememberToken();
            }
            if(!(Schema::hasColumn('sec_seguridadusers', 'created_at'))){
                $table->timestamps();
            }

            $table->string("pswd")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sec_seguridadusers', function (Blueprint $table) {

            if(Schema::hasColumn('sec_seguridadusers', 'deleted_at')){
                $table->dropColumn("deleted_at");
            }

            if((Schema::hasColumn('sec_seguridadusers', 'remember_token'))){
                $table->dropColumn("remember_token");
            }
            if((Schema::hasColumn('sec_seguridadusers', 'created_at'))){
                $table->dropColumn("created_at");
            }
            if((Schema::hasColumn('sec_seguridadusers', 'updated_at'))){
                $table->dropColumn("updated_at");
            }
        });
    }
}
