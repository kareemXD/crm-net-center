<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nulleable();
            $table->text('old_value');
            $table->text('new_value');
            $table->string('username');
            $table->string("login");
            $table->integer("email_id")->unsigned();
            $table->timestamps();
        });

        Schema::table('ejecutivos', function(Blueprint $table){
            $table->string("extension", 10)->nulleable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_histories');
        if(Schema::hasColumn('ejecutivos', 'extension'))
        {
            Schema::table('ejecutivos', function(Blueprint $table){
                $table->dropColumn('extension');
            });
        }
    }
}
