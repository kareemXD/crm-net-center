<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //tabla de informacion general
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comprobante_qa', 10);
            $table->string('comentario_validacion', 2);
            $table->timestamp('fecha_primer_contacto');
            $table->integer('executive_id')->unsigned();
            $table->integer('origen_id')->unsigned();
            $table->string('vendedor', 50);
            $table->string('folio');
            $table->boolean('a_visitado')->default(false);
            $table->boolean('tomado_presentacion')->default(false);
            $table->string('hace_cuanto')->nulleable();
            $table->boolean('se_hizo_socio')->default(false);
            $table->string('numero_aprobacion')->nulleable();
            $table->boolean('requiere_factura')->default(false);
            $table->double('ingreso_mensual', 8 , 2);
            $table->string("estado_civil")->nulleable();
            $table->integer("numero_tarjetas")->default(0);
            $table->double('precio_total', 8, 2);
            $table->double('pagado', 8, 2);
            $table->double('saldo_pendiente', 8, 2);
            $table->text('comentarios');
            $table->text('desglose_paquete');
            $table->text('desglose_hoteleria');
            $table->text('comentarios_verificacion');
            $table->timestamps();
        });

                //los datos del cliente y el acompañante minimo lleva uno
        Schema::create('survey_datos_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nulleable()->default("");
            $table->string('edad')->nulleable()->default("");
            $table->string('ocupacion');
            $table->string('telefono_celular');
            $table->string('telefono_casa');
            $table->integer('pais'); //seleccion del pais
            $table->integer('estado'); //seleccion del estado
            $table->string('email');
            $table->string('tipo', 1)->default('P'); //P para principal A para acompañante
            $table->timestamps();
        });

        //son alrededor de dos tarjetas las que se agregan
        Schema::create('survey_datos_tarjetas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_tarjeta')->nulleable();
            $table->string('banco')->nulleable();
            $table->date('vencimiento')->nulleable();
            $table->string('tipo_tarjeta')->nulleable();
            $table->string('codigo', 4);
            $table->string('pago_garantia')->default("P");// P o G si es tarjeta de pago o garantia
            $table->timestamps();
        });

        Schema::create('survey_datos_reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('checkin');
            $table->date('checkout');
            $table->string('plan');
            $table->integer('adultos')->default(0);
            $table->integer('menores')->default(0);
            $table->string('edad_menores')->nulleable();
            $table->string('tipo_habitacion')->nulleable();
            $table->integer('cuantas_habitaciones');
            $table->boolean('viaja_con_familiares');
            $table->timestamps();
        });
        //llaves foraneas
        Schema::table('survey_datos_clientes', function (Blueprint $table) {
            $table->integer('survey_id')->unsigned();
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

        Schema::table('survey_datos_tarjetas', function (Blueprint $table) {
            $table->integer('survey_id')->unsigned();
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

        Schema::table('survey_datos_reservas', function (Blueprint $table) {
            $table->integer('survey_id')->unsigned();
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_datos_clientes');
        Schema::dropIfExists('survey_datos_tarjetas');
        Schema::dropIfExists('survey_datos_reservas');
        Schema::dropIfExists('surveys');
    }
}
