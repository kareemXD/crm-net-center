<?php

use Illuminate\Database\Seeder;

class CardTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('card_types')->insert([
            [
            	"code" => "V",
            	"name" => "VISA"
            ],
            [
            	"code" => "M",
            	"name" => "MASTER CARD"
            ],
            [
            	"code" => "A",
            	"name" => "AMERICAN EXPRESS"
            ],
        ]);
    }
}
