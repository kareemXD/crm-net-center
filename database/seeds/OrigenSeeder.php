<?php

use Illuminate\Database\Seeder;

class OrigenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('origen')->insert([
         	[
         		'code' => 'R',
         		'nombre' => 'FACEBOOK REDES SOCIALES'

         	],
            [
                'code' => 'P',
                'nombre' => 'YPJ'

            ]
         ]);
    }
}
