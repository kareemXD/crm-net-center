<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
            	"code" => "N",
            	"name" => "NO INTERESADO",
                "color" => "#AEAEAE"//no interesado

            ],
            [
            	"code" => "S",
            	"name" => "SIN CONTACTAR",
                "color" => "#eee"//blanco
            ],
            [
            	"code" => "I",
            	"name" => "INTERESADOS",
                "color" => "#3ECE00"//verde
            ],
            [
            	"code" => "V",
            	"name" => "VENTA(TODO BIEN)",
                "color" => "#068600"//verde oscuro
            ],
            [
            	"code" => "D",
            	"name" => "DATO ERRONEO",
                "color" => "#F5F900"//amarillo
            ],
            [
            	"code" => "C",
            	"name" => "VENTA(CON PENDIENTE)",
                "color" => "#FFC85E"//color piel
            ],
            [
            	"code" => "P",
            	"name" => "PRIORIDAD",
                "color" => "#FF0000"//rojo
            ],
            [
            	"code" => "M",
            	"name" => "CAMBIO",
                "color" => "#E27E03"//color cafe 
            ],
            [
            	"code" => "K",
            	"name" => "CANCELACIÓN",
                "color" => "#97F193"//verde claro
            ],
        ]);
    }
}
