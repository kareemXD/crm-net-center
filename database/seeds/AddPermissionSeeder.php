<?php

use Illuminate\Database\Seeder;

class AddPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            //permisos de usuario
            [
                'permission' => 'contact_fill',
                'description' => 'Llenar informacion de los contactos en el crm'
            ],
            [
            	'permission' => 'survey',
                'description' => 'ver la seccion de surveys'
            ],
            [
            	'permission' => 'survey_delete',
                'description' => 'Eliminar surveys'
            ],
            [
            	'permission'=>'survey_show_all',
            	'description' => 'Ver todos los surveys'

            ],
            [
            	'permission'=>'survey_show_all',
            	'description' => 'Ver todos los surveys aunque no pertenezcan a un ejecutivo'

            ],
            [
                'permission' => 'upload_contact',
                'description' => 'Subir archivo para los leads de todos los origenes'
            ],[
                'permission' => 'proyectos',
                'description' => 'Ver seccion de proyectos'
            ],[
                'permission' => 'externos',
                'description' => 'Ver leads que son de origen referidos'
            ],
            [
                'permission' => 'referidos',
                'description' => 'Ver leads que caen de socios'
            ],
            [
                'permission' => 'mercadotecnia',
                'description' => 'Ver los leads que vienen de mercadotecnia'
            ],
            [
                'permission' => 'facebook_redes_sociales',
                'description' => 'Son los leads que caen a facebook redes sociales'
            ],
            [
                'permission' => 'facebook_landig_page',
                'description' => 'Son los permisos para ver los permisos de la landing page de facebook'
            ],
            [
                'permission' => 'multidestino',
                'description' => 'Permiso para acceder a los leads de multidestino'
            ],
            [
                'permission' => 'mercadotecnia',
                'description' => 'Son los permisos para ver los leads que caen a mercadotecnia'
            ]

         ]);
            
    }
}
