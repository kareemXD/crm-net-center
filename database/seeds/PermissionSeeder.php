<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions_current = ['user_create',
            'user_view',
            'user_update',
            'user_update_roles',
            'user_delete',
            'proyectos_view',
            'proyectos_assign',
            'proyectos_view_all',
            'customer_create',
            'customer_edit',
            'executive_select',
            'client_data_edit',
            'card_data_edit',
            'client_document_edit',
            'customer_list_all',
            'customer_list',
            'customer_advanced_filter_all',
            'customer_advanced_filter',
            'customer_update',
            'customer_delete',
            'change_user_rol',
            'report',
            'leads_rci',
            'contact_fill',
            'survey_show',
            'survey_delete',
            'survey_show_all',
            'upload_contact',
            'proyectos',
            'externos',
            'externos_show',
            'externos_show_all',
            'referidos',
            'referidos_show',
            'referidos_show_all',
            'mercadotecnia',
            'mercadotecnia_show',
            'mercadotecnia_show_all',
            'facebook_redes_sociales',
            'facebook_redes_sociales_show',
            'facebook_redes_sociales_show_all',
            'facebook_landig_page',
            'facebook_landig_page_show',
            'facebook_landig_page_show_all',
            'multidestino',
            'multidestino_show',
            'multidestino_show_all',
            'roles',
            'origen_modify',
            'survey_decrypt_card'
        ];

        if(Schema::hasTable('permissions')){
            DB::table("permissions")->whereNotIn("permission", $permissions_current)->delete();
        }

        DB::table('permissions')->insert([
            //permisos de usuario
            [
                'permission' => 'user_create',
                'description' => 'Crear usuario'
            ],[
                'permission' => 'user_view',
                'description' => 'Ver lista de usuarios'
            ],[
                'permission' => 'user_update',
                'description' => 'Actualizar información de usuario (excepto roles y permisos de usuarios con estatus mayor)'
            ],[
                'permission' => 'user_update_roles',
                'description' => 'Actualizar información de usuario Incluyendo roles y permisos'
            ],[
                'permission' => 'user_delete',
                'description' => 'Eliminar a un ejecutivo de la lista de usuarios'
            ],
            //permisos de proyectos
         	[
         		'permission' => 'proyectos_view',
         		'description' => 'Ver seccion de proyectos'

         	],[
         		'permission' => 'proyectos_assign',
         		'description' => 'Asignar a un prospecto a ejecutivo'
           	],[
                'permission' => 'proyectos_view_all',
                'description' => 'ver todos los leads de proyectos'
            ],
         	//permiso padre
         	[         		
         		'permission' => 'customer_create',
         		'description' => 'Crear Cliente'
         	],
         	[
         		'permission' => 'customer_edit',
         		'description' => 'Ver los datos del cliente pero sin poder realizar la edicion'
         	]
         	//subpermisos para editar
            //
            ,[
                'permission' => 'executive_select',
                'description' => 'Seleccionar un ejecutivo que no sea el usuario que lo usa'
            ]
         	,[
         		'permission' => 'client_data_edit',
         		'description' => 'Ver informacion de los datos del cliente con todos los ejecutivos'
         	],[
         		'permission' => 'card_data_edit',
         		'description' => 'Ver toda la informacion de datos de tarjeta'
         	],[
         		'permission' => 'client_document_edit',
         		'description' => 'Ver los documentos del cliente y poder eliminarlos'
         	],
         	//fin subpermisos de editar
         	[
         		'permission' => 'customer_list_all',
         		'description' => 'ver todos los prospectos y clientes'
         	],
         	//permisos de seccion de lista
         	[
         		'permission' => 'customer_list',
         		'description' => 'ver la lista propia de cada ejecutivo'
         	],[
         		'permission' => 'customer_advanced_filter_all',
         		'description' => 'Ver todas la funcionalidades del filtro avanzado'
         	],[
         		'permission' => 'customer_advanced_filter',
         		'description' => 'Ver filtro avanzado para el ejecutivo'
         	],[
         		'permission' => 'customer_update',
         		'description' => 'Actualizar informacion de un cliente o prospecto'
         	],[
         		'permission' => 'customer_delete',
         		'description' => 'Hacer eliminado lógico de un cliente'
         	],[
                'permission' => 'change_user_rol',
                'description' => 'Cambiar Rol directamente de la lista de usuarios'
            ],
            [
                'permission' => 'report',
                'description' => 'Acceder a los reportes de netcenter'
            ],
            [
                'permission' => 'leads_rci',
                'description' => 'Leads de RCI'
            ],
            //permisos extras
            //permisos de usuario
            [
                'permission' => 'contact_fill',
                'description' => 'Llenar informacion de los contactos en el crm'
            ],
            [
                'permission' => 'survey_show',
                'description' => 'ver la seccion de surveys'
            ],
            [
                'permission' => 'survey_delete',
                'description' => 'Eliminar surveys'
            ],
            [
                'permission'=>'survey_show_all',
                'description' => 'Ver todos los surveys aunque no pertenezcan a un ejecutivo'

            ],
            [
                'permission' => 'upload_contact',
                'description' => 'Subir archivo para los leads de todos los origenes'
            ],[
                'permission' => 'proyectos',
                'description' => 'Ver seccion de proyectos'
            ]
            ,[
                'permission' => 'externos',
                'description' => 'Ver leads que son de origen externos'
            ],
            [
                'permission' => 'externos_show',
                'description' => 'Ver leads que son de origen externos'
            ]
            ,
            [
                'permission' => 'externos_show_all',
                'description' => 'Ver leads que son de origen externos de todos los ejecutivos'
            ]
            ,
            [
                'permission' => 'referidos',
                'description' => 'Ver leads que caen de referidos'
            ],
            [
                'permission' => 'referidos_show',
                'description' => 'Ver leads que caen de referidos del ejecutivo'
            ],
            [
                'permission' => 'referidos_show_all',
                'description' => 'Ver leads que caen de referidos de todos los ejecutivos'
            ],
            [
                'permission' => 'mercadotecnia',
                'description' => 'acceder a la seccion de mercadotecnia'
            ],[
                'permission' => 'mercadotecnia_show',
                'description' => 'Ver los leads que vienen de mercadotecnia del ejecutivo'
            ],[
                'permission' => 'mercadotecnia_show_all',
                'description' => 'Ver los leads que vienen de mercadotecnia del ejecutivo'
            ],
            [
                'permission' => 'facebook_redes_sociales',
                'description' => 'Son los leads que caen a facebook redes sociales'
            ],[
                'permission' => 'facebook_redes_sociales_show',
                'description' => 'Son los leads que caen a facebook redes sociales'
            ],[
                'permission' => 'facebook_redes_sociales_show_all',
                'description' => 'Son los leads que caen a facebook redes sociales'
            ],
            [
                'permission' => 'facebook_landig_page',
                'description' => 'Son los permisos para ver los permisos de la landing page de facebook'
            ], [
                'permission' => 'facebook_landig_page_show',
                'description' => 'Son los permisos para ver los permisos de la landing page de facebook'
            ], [
                'permission' => 'facebook_landig_page_show_all',
                'description' => 'Son los permisos para ver los permisos de la landing page de facebook'
            ],
            [
                'permission' => 'multidestino',
                'description' => 'Permiso para acceder a los leads de multidestino'
            ],
            [
                'permission' => 'multidestino_show',
                'description' => 'Permiso para acceder a los leads de multidestino'
            ],[
                'permission' => 'multidestino_show_all',
                'description' => 'Permiso para acceder a los leads de multidestino de todos los ejecutivos'
            ],
            [
                'permission' => 'roles',
                'description' => 'Modificar roles para agregar permisos de un tipo de usuario'
            ],[
                'permission' => 'origen_modify',
                'description' => 'Modificar origenes y sub origenes'
            ],[
                'permission' => 'customer_card',
                'description' => 'modificar informacion de la tarjeta guardada'
            ],[
                'permission' => 'survey_decrypt_card',
                'description' => 'mostrar la tarjeta en la seccion del survey'
            ]

         ]);

        //eliminar permisos repetidos
        if(Schema::hasTable('permissions')){
            $permissions_not_delete = [];
            foreach(DB::table("permissions")->whereIn("permission", $permissions_current)->get() as $permission){
                $permissions_not_delete[] = $permission->id;
                DB::table('permissions')->where("permission", $permission->permission)->whereNotIn("id", $permissions_not_delete)->delete();
            }
        }

    }
}
