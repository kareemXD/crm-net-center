<?php

use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('sec_seguridadusers')->where('login','kareem.lorenzana')->first();

        if(is_null($user))
        {
        	DB::table('sec_seguridadusers')->insert([
                "login" => 'kareem.lorenzana',
                "pswd" => bcrypt(md5('Kareem.01@')),
                "name" => 'Kareem Leonardo Lorenzana Guizar',
                "email" => 'leo.lorenzana@buganviliasclub.com.mx',
                "active" => 'Y'
            ]);
        }
    }
}
