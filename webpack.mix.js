let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
	'public/js/app.js',
  'resources/assets/js/custom/selectivizr-min.js',
  //
  'resources/assets/js/custom/jquery.mousewheel.js',
  'resources/assets/js/custom/jquery.vmap.min.js',
  'resources/assets/js/custom/jquery.vmap.sampledata.js',
  'resources/assets/js/custom/jquery.vmap.world.js',
  'resources/assets/js/custom/jquery.bootstrap.wizard.js',
  'resources/assets/js/custom/fullcalendar.min.js',
  'resources/assets/js/custom/gcal.js',
  'resources/assets/js/custom/jquery.easy-pie-chart.js',
  'resources(assets/js/custom/chartsj-plugin-datalabels.js',
  'resources/assets/js/custom/moment.js',
  'resources/assets/js/custom/moment-with-locales.js',
  'resources/assets/js/custom/bootstrap-fileupload.js',
  'resources/assets/js/custom/bootstrap-colorpicker.js',
  'resources/assets/js/custom/bootstrap-switch.min.js',
  'resources/assets/js/custom/select2.js',
  'resources/assets/js/custom/jquery.payment.js',
  'resources/assets/js/custom/jquery.priceformat.js',
  'resources/assets/js/custom/pnotify.custom.min.js',
  //'resources/assets/js/custom/jquery.dataTables.js',
  //'resources/assets/js/custom/dataTables.bootstrap.js',
  'resources/assets/js/custom/excanvas.min.js',
  'resources/assets/js/custom/jquery.isotope.min.js',
  'resources/assets/js/custom/isotope_extras.js',
  'resources/assets/js/custom/modernizr.custom.js',
  'resources/assets/js/custom/jquery.fancybox.pack.js',
  'resources/assets/js/custom/styleswitcher.js',
  'resources/assets/js/custom/wysiwyg.js',
  'resources/assets/js/custom/typeahead.js',
  'resources/assets/js/custom/summernote.min.js',
  'resources/assets/js/custom/jquery.inputmask.min.js',
  'resources/assets/js/custom/jquery.validate.js',
  'resources/assets/js/custom/typeahead.js',
  'resources/assets/js/custom/spin.min.js',
  'resources/assets/js/custom/ladda.min.js',
  'resources/assets/js/custom/mockjax.js',
  'resources/assets/js/custom/bootstrap-editable.min.js',
  'resources/assets/js/custom/xeditable-demo-mock.js',
  'resources/assets/js/custom/xeditable-demo.js',
  'resources/assets/js/custom/address.js',
  'resources/assets/js/custom/daterange-picker.js',
  'resources/assets/js/custom/date.js',
  'resources/assets/js/custom/morris.min.js',
  'resources/assets/js/custom/skycons.js',
  'resources/assets/js/custom/fitvids.js',
  'resources/assets/js/custom/jquery.sparkline.min.js',
  'resources/assets/js/custom/dropzone.js',
  'resources/assets/js/custom/bootstrap-datetimepicker.js',
  'resources/assets/js/custom/bootstrap-datepicker.js',
  'resources/assets/js/custom/bootstrap-timepicker.js',
  'resources/assets/js/custom/main.js',
  'resources/assets/js/custom/respond.js',
  'resources/assets/js/custom/jquery.dataTables.min.js',
  'resources/assets/js/custom/dataTables.bootstrap4.min.js',
  'resources/assets/js/custom/dataTables.responsive.min.js',
  'resources/assets/js/custom/responsive.bootstrap4.min.js',
  'resources/assets/js/fontawesome.min.js',
  'resources/assets/js/custom/jspdf.debug.js',
  'resources/assets/js/kareem.js'], 'public/js/all.js');

