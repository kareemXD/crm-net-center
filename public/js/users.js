$(function(){
	var table = inizializeDataTable();
	/*activar o desactivar usuarios directamente de la lista*/
	$(document).on("change", "input[data-switch]", function(event){
		var $this = $(this);
		var route = $this.data('route');
		var status = $this.prop("checked");
		var before_status = !$this.prop("checked");
		$.post(route, {status: status, _token: $("meta[name='csrf-token']").attr("content") }).done(function(response){
			try{
				response_status = JSON.parse(response);
				if(response_status.error != null){
					new PNotify({
						title: "Error",
						text: response_status.error,
						type:"error"
					});
					$this.prop("checked", before_status);
				}else if(response_status.success != null){
					$this.prop("checked", response_status.success);
					new PNotify({
						title: "Listo!",
						text: "Estatus cambiado correctamente",
						type: "success"
					});
				}
			}
			catch(error) {
				new PNotify({
					title: "Error",
					text: "Algo está fallando",
					type:"error"
				});
				$this.prop("checked", before_status);
			}
		}).fail(function(xhr){
			new PNotify({
				title: "Error",
				text: "Algo está fallando",
				type: "error"
			});
			$this.prop("checked", before_status);
		});
	});
	/*cambiar rol directamente de la lista*/
	$(document).on("change", "select[data-change-role]", function(e){
		$this = $(this);
		var oldValue = $this.data('oldValue');
		var route = $this.data('route');
		var data = {'group_id': $this.val(), _token: $("meta[name='csrf-token']").attr('content')};
		$.post(route, data).done(function(response){
			try{
				response_status = JSON.parse(response);
				if(response_status.error != null){
					new PNotify({
						title: "Error",
						text:response_status.error,
						type:"danger"
					});
					$this.val(oldValue);
				}else if(response_status.success != null){
					$this.val(response_status.success);
					new PNotify({
						title: "Listo!",
						text: "Rol modificado correctamente",
						type: "success"
					});
				}
			}
			catch(error) {
				new PNotify({
					title: "Error",
					text: "Algo está fallando",
					type:"error"
				});
				$this.val(oldValue);
			}
		}).fail(function(xhr){
			new PNotify({
				title: "Error",
				text: "Algo está fallando",
				type: "error"
			});
			$this.val(oldValue);
		});
	});
	/*fin cambiar rol directamente de la lista*/
	$("#modalDeleteUser").on("show.bs.modal", function(event){
		$button = $(event.relatedTarget);
		$(this).find("form").attr("action", $button.data("route"));
		$("#formDeleteUser").data("button", $button);
	});

	$("#formDeleteUser").on("submit", function(event){
		event.preventDefault();
		$this = $(this);
		$.post($this.attr("action"), $this.serializeArray()).done(function(response){
			try{
					response_status = JSON.parse(response);
					if(response_status.error != null){
						new PNotify({
							title: "Error",
							text: response_status.error,
							type:"error"
						});
		
					}else if(response_status.success != null){
						new PNotify({
							title: "Listo!",
							text: "Usuario eliminado correctamente",
							type: "success"
						});
						$button_delete = $($this.data("button"));
						if($button_delete.length > 0){
							$button_delete.parent().parent().remove();
						}
						$("#modalDeleteUser").modal("toggle");
					}
			}catch(error){
				new PNotify({
					title: "Error",
					text: "Algo está fallando",
					type:"error"
				});
			}
		});
	});
	//fin de eliminar usuario
	/*modificar usuarios*/
	$("#modalEditUser").on("show.bs.modal", function(event){
		$this = $(this);
		$button = $(event.relatedTarget);

		$this.find('.modal-body').prepend('<div id="page-loader"><span class="preloader-interior"></span></div>');
    $("page-loader").fadeIn(500);
		$this.find(".modal-body").load($button.data("route"), function(){
			$this.data('button', $button);
		});
	});

	/*mandar formulario para modificar usuario*/
	$(document).on("submit", "#updateUserForm", function(event){
		event.preventDefault();
		$this = $(this);
		$button = $this.find("button[type='submit']");
		$button.button("loading");
		$(".has-error > span.help-block").remove();
		$(".has-error").removeClass("has-error");
		//se envía el formulario
		$.ajax({
			url: $this.attr("action"),
			type: $this.attr("method"),
			data: $this.serializeArray(),
			success: function(response) {
				var user;
				try{
					var data = JSON.parse(response);
					if(data.success != null) {
						user = data.success;
						var tr = $("#modalEditUser").data("button").parent().parent();
						//nombre
						var tds = $(tr).find("td");
						if(tds.length === 6) {
							$(tds[0]).text(user.login);
							$(tds[1]).text(user.name);
							$(tds[2]).text(user.email);
							select_roles = $(tds[3]).find("select");
							checkbox_active = $(tds[4]).find("input[type='checkbox']");
							if(select_roles.length > 0) {
								$(select_roles).val(user.user_role.group_id);
							}
							if(checkbox_active.length > 0) {
								if(user.active == "Y") {
									$(checkbox_active).prop("checked", true);
								}else if(user.active == "N") {
									$(checkbox_active).prop("checked", false);
								}
							}
						}
						new PNotify({
							title: "Correcto!",
							text: "Cambios realizados correctamente",
							type: "success"
						});
						$("#modalEditUser").modal("toggle");
						$button.button("reset");
					}else if(data.error) {
						$("#modalEditUser").modal("toggle");
						new PNotify({
							title: "Ups!",
							text: data.error,
							type: "error"
						});
						$button.button("reset");
					}else{
						$("#modalEditUser").modal("toggle");
						new PNotify({
							title: "Ups!",
							text: data.error,
							type: "error"
						});
					}

				}catch(error){
					$button.button("reset");
					new PNotify({
						title: "Error Desconocido",
						text: "Los datos se guardaron pero hay un error.", 
						type: "error"
					});
				}
			},
			error: function(data) {
				$button.button("reset");
				if(data.status === 422) {
					response = JSON.parse(data.responseText);
					//obtener errores de validacion
					//c
					$.each(response.errors, function(index, value){
						$element = $("input[name='"+index+ "']");
						if($element.length === 0) {
							$element = $("select[name='"+index+ "']");
						}
						if($element.length > 0) {
							$element.parent().addClass("has-error");
							$element.parent().append(`<span id="help_password_confirm" class="help-block">
						`+ value[0] +`</span>`);
						}
					});//end jquery each
				}else{
					new PNotify({
						title: "Error Desconocido",
						text: "Al parecer el servidor no está funcionando correctamente", 
						type: "error"
					});
				}
			}
		});
	});

	/*datatable */
	function inizializeDataTable() {
	    var $table = $("#dataTable").DataTable({
	    	"processing": false,
	      "serverSide": false,
	    	"columns": [
	        {data: 'login'},
	        {data: 'name'},
	        {data: 'email'},
	        {data: 'groupName'},
	        {data: 'estatus'},
	        {data: 'edit'}
	      ],
	      "pageLength": 25,
	      "language":{
	          "decimal":        "",
	          "emptyTable":     "No hay datos disponibles en esta tabla",
	          "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
	          "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
	          "infoFiltered":   "(filtrado de _MAX_ total registros)",
	          "infoPostFix":    "",
	          "thousands":      ",",
	          "lengthMenu":     "Mostrar _MENU_ registros",
	          "loadingRecords": "Cargando...",
	          "processing":     "Procesando...",
	          "search":         "Buscar:",
	          "zeroRecords":    "No encontrados en la tabla",
	          "paginate": {
	              "first":      "Primer",
	              "last":       "Ultimo",
	              "next":       "Siguiente",
	              "previous":   "Anterior"
	          },
	          "aria": {
	              "sortAscending":  ": Activar para ordenar la columna ascendente",
	              "sortDescending": ": Activar para ordenar la columna descendente"
	          }
	      }
	    });
	    return $table;
	}
	/*fin datatable */

});
