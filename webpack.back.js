const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath('public');
mix.scripts([
		'resources/assets/js/jquery-2.2.0.min.js',
		'resources/assets/js/bootstrap.min.js',
		'resources/assets/js/pace.min.js',
		'resources/assets/js/jquery.slimscroll.min.js',
		'resources/assets/js/fastclick.js',
		'resources/assets/js/admin.js', //este es el app.min.js
		'resources/assets/js/page.js', 
		//'resources/assets/js/jquery.dataTables.min.js', 
		//'resources/assets/js/dataTables.bootstrap.js', 
		'resources/assets/js/select2.js', 
		'resources/assets/js/bootstrap-datepicker.js', 
		'resources/assets/js/bootstrap-datepicker.es.js', 
		'resources/assets/js/jquery.priceformat.js',
		'resources/assets/js/jquery.payment.js',
		'resources/assets/js/moment.js',
		'resources/assets/js/moment-with-locales.js',
		'resources/assets/js/bootstrap-datetimepicker.js',
		'resources/assets/js/bootstrap-datetimepicker.js',
		'resources/assets/js/pnotify.custom.min.js',
		'resources/assets/js/dropzone.js',
		'resources/assets/js/app.js',
	], 'public/js/app.js');

	mix.styles([
		'resources/assets/css/bootstrap.min.css',
		'resources/assets/css/font-awesome.min.css',
		'resources/assets/css/ionicons.min.css',
		'resources/assets/css/AdminLTE.min.css',
		'resources/assets/css/_all-skins.min.css',
		'resources/assets/css/pace.min.css',
		'resources/assets/css/pnotify.custom.min.css',
		'resources/assets/css/backpack.base.css',
		//'resources/assets/css/dataTables.bootstrap.css',
		'resources/assets/css/select2.css',
		'resources/assets/css/datepicker3.css',
		'resources/assets/css/font-awesome.css',
		'resources/assets/css/kareem.css',
	], 'public/css/app.css');

	mix.scripts([
		'resources/assets/js/jquery-2.2.0.min.js',
		//'resources/assets/js/bootstrap.min.js',
		//'resources/assets/js/pace.min.js',
		'resources/assets/js/jquery.dataTables.min.js', 
		'resources/assets/js/dataTables.bootstrap.js', 
		], 'public/js/all_before.js');
	mix.scripts([
		//'resources/assets/js/vue.js',
		//'resources/assets/js/axios.js',

		//'resources/assets/js/jquery.slimscroll.min.js',
		//'resources/assets/js/fastclick.js',
		//'resources/assets/js/admin.js', //este es el app.min.js
		//'resources/assets/js/page.js', 
		'resources/assets/js/select2.js', 
		'resources/assets/js/bootstrap-datepicker.js', 
		'resources/assets/js/bootstrap-datepicker.es.js', 
		'resources/assets/js/jquery.priceformat.js',
		'resources/assets/js/jquery.payment.js',
		'resources/assets/js/moment.js',
		'resources/assets/js/moment-with-locales.js',
		'resources/assets/js/bootstrap-datetimepicker.js',
		'resources/assets/js/bootstrap-datetimepicker.js',
		'resources/assets/js/pnotify.custom.min.js',
		'resources/assets/js/dropzone.js',
		//'resources/assets/js/app.js',
	], 'public/js/all.js');

	mix.styles([
		'resources/assets/css/bootstrap.min.css',
		'resources/assets/css/font-awesome.min.css',
		'resources/assets/css/ionicons.min.css',
		//'resources/assets/css/AdminLTE.min.css',
		//'resources/assets/css/_all-skins.min.css',
		'resources/assets/css/pace.min.css',
		'resources/assets/css/pnotify.custom.min.css',
		//'resources/assets/css/backpack.base.css',
		'resources/assets/css/dataTables.bootstrap.css',
		'resources/assets/css/select2.css',
		'resources/assets/css/datepicker3.css',
		'resources/assets/css/font-awesome.css',
		'resources/assets/css/kareem.css',
	], 'public/css/all.css');

	mix.sass('public/theme/style.sass', 'public/theme/style.css').options({
    	processCssUrls: false
   	});

	mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');