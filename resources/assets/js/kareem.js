window.notifications = [];
window.dates_notifications = [];
window.dd = console.log;
var notification_window = false;
var timestamp = 0;
var routeList = JSON.parse(document.getElementById('routeList').content);
//*Inicialize functions*//
getNotificationsToday();
//función que retorna las notificaciones de la aplicacion

function getNotificationsToday(){
	if($("meta[name='notifications-today']").length === 0){
		return;
	}
  $.ajax({
      url: $("meta[name='notifications-today']").attr('content'),
      type: 'get',
      success(response) {
      	notifications   = JSON.parse(response);
        dates_notifications = notifications.map(function(notification){
                return (notification.fecha).substring(0, (notification.fecha.length - 3));
        });
      }
  });
}
function formatDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
$(function(){
  var currentDate = new Date();
  var seconds = currentDate.getSeconds();
  var minutes = currentDate.getMinutes();
  var hours = currentDate.getHours();
  var date = formatDate();
  //console.clear();
  var interval = setInterval(function(){
      currentDate = new Date();
      seconds = currentDate.getSeconds();
      minutes = currentDate.getMinutes();
      hours = currentDate.getHours();
      date = formatDate();
      let hour_string = (hours < 10)? ("0" + hours) : hours;
      let minutes_string = (minutes < 10)? ("0" + minutes ): minutes;
      let seconds_string = (seconds < 10)? ("0" + seconds): seconds;
      timestamp = (date + " " + hour_string + ":" + minutes_string);
      
      if(dates_notifications.includes(timestamp) && notification_window === false) {
        notification_window = true;
        Swal.fire({
          title: 'Nota',
          html: `<table id="tableNotifications" class="table table-hover">
            <thead>
              <tr>
                <th>Titulo</th><th>Nota</th><th>Hora</th><th>Cliente</th><th>Estatus</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>`,
          width: '800px',
          onBeforeOpen: () => {
            $.each(dates_notifications, function(index, dates_notification){
                if(timestamp == dates_notification){
                  let options = ``;
                  let route = routeList.changeNotificationStatus.replace("{move_id}", notifications[index].id);
                  $.each([{index:1, value:'PENDIENTE'}, {index:2, value:'CERRADO'}], function(i, v){
                    if(notifications[index].estatus == v.index) {
                      options = options + `<option selected value="`+v.index+`">`+v.value+`</option>` 
                    }else{
                      options = options + `<option value="`+v.index+`">`+v.value+`</option>` 
                    }
                  });
                  $("#tableNotifications > tbody").append(`<tr>
                    <td>`+notifications[index].titulo+`</td><td>`+notifications[index].observaciones+`</td><td>`+notifications[index].fecha+`</td><td>`+notifications[index].customer.nombre+`</td>
                    <td><select  data-route="` + route + `" name="notification-estatus" class="form-control">`+options+`</select></td></tr>`);
                }
            });
          },
          onClose: () => {
            //se pone en falso para cerrarlo
            notification_window = false;
          }
        }).then((result) => {
         
        })
      }

  }, 1000);/*
  $('button[data-loading-text]').click(function () {
      $button = $(this);
      var loadingText = $($button).data('loadingText');
      if ($button.html() !== loadingText) {
        $button.data('original-text', $button.html());
        $button.html(loadingText);
        $button.prop("disabled", true);
      }
      setTimeout(function () {
        $button.html($button.data('original-text'));
        $button.prop("disabled", false);
      }, 3000)
  }); */
  $(document).on('change', 'select[name="notification-estatus"]', function(event){
    let route = $(this).data('route');
    $this = $(this);
    $.ajax({
      url: route,
      type: 'post',
      data:{_token: $("meta[name='csrf-token']").attr("content"), 'estatus_id': $this.val()},
      success: function(response) {
        new PNotify({
          title: 'correcto',
          text: 'Se modificó el Estatus de la nota correctamente',
          type: 'success'
        });
      },
      error: function (error) {
        new PNotify({
          title: 'Ups!',
          text: 'Hubo un Problema al cambiar el Estatus comuniquese con el administrador.',
          type: 'error'
        })
      }
    });
  });

  $(document).on("click", ".border-check", function(e){
    $this = $(this);
    $input = $this.parent().find("input[type='radio']");
    if($input.length === 1){
      $input.prop("checked", true);
    } 
  });
});


$(document).ready(function(e){
  $("#page-loader").fadeOut(1000);
});