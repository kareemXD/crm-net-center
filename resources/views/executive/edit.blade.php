@extends('layout-2')
@section('title', 'Modificar datos del ejecutivo $executive->name')
@section('content')

<div class="page-title">
    <h1>
       Modificar datos del ejecutivo {{ $executive->name }}
    </h1>
</div>
<div id="page-loader"><span class="preloader-interior"></span></div>
	<div class="row">

		<div class="col-md-12 col-sm-12">

      	<div class="widget-container fluid-height clearfix">
      		<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="{{ route('admin.listExecutive') }}">Ejecutivos</a></li>
			    <li class="breadcrumb-item active" aria-current="page">Modificar</li>
			  </ol>
			</nav>
			<div class="widget-content padded clearfix">
				@include('executive.edit-ajax')
			</div>
		</div>
	</div>
</div>
@endsection
@section('after_scripts')
    <script type="text/javascript">
    	$(function(){
    		console.log($("#message_flash").data());
    		if($("#message_flash").data("messageSuccess") != ""){
    			new PNotify({
		          title: 'correcto',
		          text: $("#message_flash").data("messageSuccess"),
		          type: 'success'
		        });
    		}else if($("#message_flash").data("messageSuccess") != "" ){
    			alert("hola mundo");
    		}
    	});
    </script>
@endsection
