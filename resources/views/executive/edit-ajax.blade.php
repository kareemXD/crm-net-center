<form action="{{ route('admin.editExecutive', $executive->id) }}" method="post" accept-charset="utf-8" class="needs-validation" novalidate>
	{{ csrf_field() }}
	{{ method_field('put') }}
	<div class="form-group">
		<label for="executive_nombre" class="control-label">Nombre</label>
		<input required id="executive_nombre" type="text" class="form-control" name="executive-nombre" value="{{ $executive->nombre }}">
		<div class="invalid-feedback">
        	Llene correctamente este campo
      </div>
      <div class="form-group">
      	<label class="control-label"> Estatus </label>
      	<div class="material-switch">
        	<input @if($executive->estatus=="A") checked @endif id="is_active_edit" name="is-active" type="checkbox"/>
	        <label for="is_active_edit" class="label-primary"></label>
	      </div>
      </div>
      <div class="form-group">
      	<label for="executive_extension" class="control-label">Extensión (Tel)</label>
      	<input required type="text" name="executive-extension" class="form-control" value="{{ $executive->extension }}">
      </div>	
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary btn-block btn-lg">Guardar Datos</button>
	</div>
</form>
<script>
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
console.log("validado");
</script>