@extends('layout-2')
@section('title', 'Lista de ejecutivos')
@section('content')
<div class="page-title">
    <h1>
       Lista de Ejecutivos
    </h1>
</div>
<div id="page-loader"><span class="preloader-interior"></span></div>

	<div class="row">
		<div class="col-md-12 col-sm-12">
      <div class="widget-container fluid-height clearfix">
				<div class="widget-content padded clearfix">
					<div id="table_content">
						
						<table id="dataTable" style="width:100%" class="table table-bordered table-striped display dataTable">
							<thead class="thead-dark text-white">
								<tr>
									<th>Nombre</th>
									<th>Fecha Alta</th>
									<th>Estatus</th>
									<th>Extension</th>
									<th>Operación</th>
								</tr>
							</thead>
							<tbody>
								@foreach($executives as $executive)
								<tr>
									<td>{{ $executive->nombre }}</td>
									<td>{{ $executive->fecha_alta }}</td>
									<td>
						              <div class="material-switch">
						                <input data-switch data-route="{{ route('admin.changeStatusExecutive', $executive->id) }}" @if($executive->estatus=="A") checked @endif id="is_active_{{$executive->login}}" name="is_active['{{$executive->login}}']" type="checkbox"/>
						                <label for="is_active_{{$executive->login}}" class="label-primary"></label>
						              </div>
									</td>
									<td>{{ $executive->extension }}</td>
									<td>
										<button title="Modificar" data-route="{{ route('admin.editExecutive', $executive->id) }}" data-toggle="modal" data-target="#modalEdit" class="btn btn-primary"><i class="fa fa-edit"></i></button>
									</td>	
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Nombre</th>
									<th>Fecha Alta</th>
									<th>Estatus</th>
									<th>Extension</th>
									<th>Operacion</th>
								</tr>
							</tfoot>
						</table>
						
					</div>

				</div>
		</div>
	</div>
</div>
@include('includes.executive.modal-edit')
@endsection
@section('after_scripts')
    <script type="text/javascript">
    	$(function(){
    		inizializeDataTable();
    		/**events listeners generales**/
    		$(document).on("change","input[data-switch]", function(e){
    			$this = $(this);
    			$route= $this.data('route');
    			$.post($route, {_token:$("meta[name='csrf-token']").attr("content")}).done(function(response){
    				var error = 0;
    				try{
    					executive = JSON.parse(response);
    				}catch(error){
    					error = 1;
    				}
    				if(error === 0){
    					console.log(executive);
    					new PNotify({
	    					title: 'Correcto',
	    					text: 'Se cambió el estatus',
	    					type: 'info'
	    				});

    				}else{
    					new PNotify({
	    					title: 'Ups!',
	    					text: 'Hubo un error',
	    					type: 'error'
	    				});

	    				if($this.prop("checked")){
	    					$this.prop("checked", false);
	    				}else{
	    					$this.prop("checked", true);
	    				}
    				}

    			});
    		});
    		/**Event listeners para modales**/
    		$("#modalEdit").on("show.bs.modal", function(e){
    			$this = $(this);
    			$button = $(e.relatedTarget);
    			$.ajax({
    				url: $button.data('route'),
    				type: 'get',
    				success(response){
    					try{
    						$this.find('.modal-body').html(response);
    					}catch(error){

    					}
    				},
    				error(){

    				}
    			});
    		
    		});
    		if($("#message_flash").data("messageSuccess") != undefined && $("#message_flash").data("messageSuccess") != "") {
    			new PNotify({
    				title: "Correcto!",
    				type: "success",
    				text: $("#message_flash").data("messageSuccess")
    			});
    		}else if($("#message_flash").data("messageError") != undefined && $("#message_flash").data("messageError") != ""){
				   new PNotify({
    				title: "Error!",
    				type: "success",
    				text: $("#message_flash").data("messageError")
    			}); 			
    		}
    		
    	});
    	/*datatable */
	function inizializeDataTable() {
	    var $table = $("#dataTable").DataTable({
	    	"processing": false,
	      "serverSide": false,
	    	"columns": [
	        {data: 'nombre'},
	        {data: 'fecha_alta'},
	        {data: 'estatus'},
	        {data: 'extension'},
	        {data: 'operacion'},
	      ],
	      "pageLength": 25,
	      "language":{
	          "decimal":        "",
	          "emptyTable":     "No hay datos disponibles en esta tabla",
	          "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
	          "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
	          "infoFiltered":   "(filtrado de _MAX_ total registros)",
	          "infoPostFix":    "",
	          "thousands":      ",",
	          "lengthMenu":     "Mostrar _MENU_ registros",
	          "loadingRecords": "Cargando...",
	          "processing":     "Procesando...",
	          "search":         "Buscar:",
	          "zeroRecords":    "No encontrados en la tabla",
	          "paginate": {
	              "first":      "Primer",
	              "last":       "Ultimo",
	              "next":       "Siguiente",
	              "previous":   "Anterior"
	          },
	          "aria": {
	              "sortAscending":  ": Activar para ordenar la columna ascendente",
	              "sortDescending": ": Activar para ordenar la columna descendente"
	          }
	      }
	    });
	    return $table;
	}
	/*fin datatable */
    </script>
@endsection
