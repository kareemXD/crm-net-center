<form id="formAddTracing" action="{{ route('storeEmailTracing', $email->id) }}" method="post">
	{{ csrf_field() }}
	<h2>Seguimientos</h2>
	<div class="form-group">
		<label for="title" class="control-label">Titulo</label>
		<input type="text" class="form-control" name="title" id="title" required>
	</div>
	<div class="form-group">
		<label for="comment" class="control-label">Seguimiento</label>
		<textarea name="comment" id="comment" rows="3" class="form-control"></textarea>
	</div>

  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
  <button  id="addTracing" type="submit" class="btn btn-primary">Asignar</button>
</form>
<hr>
<table id="tableTracings" class="table table-bordered table-hover">
	<thead class="thead-dark">
		<tr>
			<th>Titulo</th>
			<th>Seguimiento</th>
			<th>Ejecutivo</th>
			<th>Fecha y hora</th>
		</tr>
	</thead>
	<tbody>
		@foreach($email->emailTracings()->latest()->get() as $emailTracing)
		<tr>
			<td>{{ $emailTracing->title }}</td>
			<td>{{ $emailTracing->comment }}</td>
			<td>{{ $emailTracing->executive_name }}</td>
			<td>{{ $emailTracing->created_at }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
	$(function(){
		$("#formAddTracing").on("submit", function(event){
			event.preventDefault();
			var button = $("#addTracing");
			$(button).button("loading");
			var route = $(this).attr("action");
			var method = $(this).attr("method");
			var data = $(this).serializeArray();
			$(".has-error").removeClass("has-error");
			$.ajax({
				url: route,
				type: method,
				data: data,
				success(response) {
					$(button).button("reset");
					response = JSON.parse(response);
					$("#tableTracings > tbody").prepend(`<tr>
						<td>`+response.title+`</td>
						<td>`+response.comment+`</td>
						<td>`+response.executive_name+`</td>
						<td>`+response.created_at+`</td>
					</tr>`);
					$("[name='title']").val("");
					$("[name='comment']").val("");
				},
				error(data) {
					$(button).button("reset");
					if(data.status === 422) {

							let errors = JSON.parse(data.responseText).errors;

							let key = Object.keys(errors)[0];
							new PNotify({
								title: "Ups!",
								text: errors[key][0],
								type: "error"
							});

							$.each(errors, function(index, value){
								$("[name='"+index+"']").parent().addClass("has-error");
							});
					}
				}
			});
		});
	});
</script>