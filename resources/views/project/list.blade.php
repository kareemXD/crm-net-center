@extends('layout-2')
@section('title', 'Lista de Proyectos')
@section('content')
<style>
	#DataTables_Table_0_wrapper{
		width:100%;
	}
	.is-booked{
		background: #2bd294 !important;
	}
	.is-rci{
		background: #43ded0 !important;
	}
	.is-cancel{
		background:#b02bd2 !important;
		color:#fff !important;
	}
	.is-cancel > td > a{
		color:#fff;
		font-weight: bold; 
		word-wrap:break-word; 

	}

	.is-arrived{
		background:#fb7f00 !important;
	}
	td > a {
		display: block;
		max-width: 100%;   
		word-break: break-all;
	}
	td{
		max-width: 16%;
	}
</style>
<div id="page-loader"><span class="preloader-interior"></span></div>
	
	<div class="row widget-container" style="padding:15px">
		<table class="table table-bordered table-striped">
			<thead >
				<tr>
					<th>	</th>
					<th>NOMBRE</th>
					@if($origen=="21")
					<th>Correo</th>
					@endif
					<th>COMENTARIO</th>
					<th>ORIGEN</th>
					<th>PLAN</th>
					<th>FECHA DE CONTÁCTO</th>
					<th>HORARIO</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
	<!-- inicio del modal de asignar -->
	<div class="modal fade" id="modal_assign" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Asignar nuevo Prospecto</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form id="formModalAssign" action="" method="post">
	      		{{ csrf_field() }}
	      		<h2>¿A que ejecutivo se le va a asignar?</h2>
	      		<div class="form-group">
	      			<select class="form-control select2" name="executive-id" >
			        	@foreach($executives as $index => $executive)
			        	<option value="{{$executive->id}}">{{ $executive->nombre }}</option>
			        	@endforeach
			        </select>
	      		</div>

		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button  id="btnAssign" type="submit" class="btn btn-primary">Asignar</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- fin del modal de asignar -->
		<!--ver numero de telefono -->
	<div class="modal fade" id="modal_phone" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Info de contacto</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div id="emailData"></div>

	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

	      </div>
	    </div>
	  </div>
	</div>
	<!-- fin del modal de asignar -->
	<!--Modal pára cargar seguimientos-->
		<div class="modal fade" id="modalTracing" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	    	<div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Seguimientos</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div style="min-height: 200px;" class="modal-body">
	      	
	      </div>
	    </div>
	  </div>
	</div>
	<!---->
		<!--Modal pára cargar seguimientos-->
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	    	<div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">¿Seguro de querer eliminar dato?</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form id="formDeleteContact" action="" method="post">
	      		{{ csrf_field() }}
	      		<button type="submit" id="btn_delete" class="btn btn-primary">Eliminar</button>
	      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

	      	</form>

	      </div>
	    </div>
	  </div>
	</div>
	<!---->
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){
		
        $.fn.dataTable.ext.errMode = 'none';

		$("#modal_assign").on("show.bs.modal", function(event){
			$button = $(event.relatedTarget);
			$("#formModalAssign").attr("action", $button.data("route"));
			$("#formModalAssign").data("button", $button);
		});

		//ver informacion de contacto
		$("#modal_phone").on("show.bs.modal", function(e){
			$button = $(e.relatedTarget);
			$route = $button.data("route");
			$("#emailData").load($route);

		});

		$("#formModalAssign").on("submit", function(e){
			//e.preventDefault();
			$this = $(this);
			$button = $("#btnAssign");
			$button.button("loading");
			/*$.ajax({
				type : $this.attr("method"),
				data: $this.serializeArray(),
				url : $this.attr("action"),
				success: function(response){
					console.log(response);
				}
			});*/

		});
		$("#modalTracing").on("show.bs.modal", function(event){
			$this = $(this);
			$button = $(event.relatedTarget);
      		$this.find('.modal-body').prepend('<div id="page-loader"><span class="preloader-interior"></span></div>');
      		$this.find('.modal-body').load($button.data("route"));
 		});

 		$("#modalDelete").on("show.bs.modal", function(event){
 			$button = $(event.relatedTarget);
 			$("#formDeleteContact").attr("action", $button.data("route"));
 		});

 		$("#formDeleteContact").on("submit", function(event){
 			$this = $(this);
 			event.preventDefault();
 			$.ajax({
 				url: $this.attr("action"),
 				type: $this.attr("method"),
 				data: $this.serializeArray(),
 				success: function(response){
 					try{
 						if(response == "success") {
 							new PNotify({
 								title: "Listo",
 								text: "dato eliminado",
 								type:"success"
 							});
 						}
 					}catch(error){
	 					new PNotify({
	 						title: "Error",
	 						text: "Hubo un error en el try, contacte a su supervisor.",
	 						type: "error"
	 					});
 					}

	                window.$table.draw(false);
	                window.$table.ajax.reload(null, false);
 				},
 				error: function(error){
 					new PNotify({
 						title: "Error",
 						text: "Hubo un error, contacte a su supervisor.",
 						type: "error"
 					});
 				}
 			});

 			$("#modalDelete").modal("hide");

 		});

	$(".select2").select2();
	$("#page-loader").fadeOut(500);
	window.$table = inizializeDataTable();
     setInterval(() => {
     	@if(Auth::user()->hasAccessApp("updateExchanges"))
        window.$table.ajax.reload(null, false);
        @endif
    }, 10000);
	function inizializeDataTable() {
	        $table = $(".table").DataTable({
	                "processing": true,
	                "serverSide": true,
	                ajax: '{{ route("api.projectList", ['origen'=>$origen]) }}',
	                "columns": [
                    	{data: 'assign', name: 'assign', orderable: false, searchable: false},
	                	{data: "nombre"},
		                @if($origen=="21" || $origen=="15")
						{data: "email_link",name:"correo"},
						@endif
	                	{data: "comentario"},
	                	{data: "origenName", name: 'origen.nombre'},
	                	{data: "lista_externa"},
	                	{data: "fecha_contacto"},
	                	{data: "horacontacto"}
	                ],
	                "pageLength": 25,
	                "language":{
	                    "decimal":        "",
	                    "emptyTable":     "No hay datos disponibles en esta tabla",
	                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
	                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
	                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
	                    "infoPostFix":    "",
	                    "thousands":      ",",
	                    "lengthMenu":     "Mostrar _MENU_ registros",
	                    "loadingRecords": "Cargando...",
	                    "processing":     "Procesando...",
	                    "search":         "Buscar:",
	                    "zeroRecords":    "No encontrados en la tabla",
	                    "paginate": {
	                        "first":      "Primer",
	                        "last":       "Ultimo",
	                        "next":       "Siguiente",
	                        "previous":   "Anterior"
	                    },
	                    "aria": {
	                        "sortAscending":  ": Activar para ordenar la columna ascendente",
	                        "sortDescending": ": Activar para ordenar la columna descendente"
	                    }
	                }
	            });

		        return $table;
		    }
	});
</script>
@endsection