@extends('errors::layout')
@section('title', '404')
@section('message')
Página no encontrada <br> <a href="{{ route('home') }}">Regresar al inicio</a>
@endsection
