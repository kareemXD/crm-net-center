@extends("layout-login")
@section("content")
<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">CRM: Inicio de sesión</div>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route("postLogin") }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Usuario</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar Sesión
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("after_scripts")
<script src="{{ asset('plugins/vide/jquery.vide.js') }}"></script>

@include("partials.inc.alerts")
@endsection
@section("after_styles")
<style>
     html, body {
      margin: 0;
      width: 100%;
      height: 100% !important;
    }
    .box{
        background: rgba(1,1,1,0);
        color:#fff;
        border:rgba(255,255,255,0.8) solid 3px
    }
    .box-header{
        color:#fff;
    }
    .row{
        padding: 25% 0 0 0;
    }
    input[type="text"], input[type="password"]{
        color:#fff;
        background-color:rgba(255,255,255,0.1);
        border: #fff solid 1px;
    }
    input[type="text"]:focus, input[type="text"]:active, input[type="password"]:focus, input[type="password"]:active{
        color:#fff;
        background-color:inherit;
        border: #fff solid 2px;
    }
    .btn-primary{
        background-color:rgba(255,255,255,0);
        border-color:#fff;
    }
    .btn-link{
        color:#fff;
    }
    .box-header.with-border, .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title{
        text-align:center;
        font-size: 36px;
    }

</style>
@endsection