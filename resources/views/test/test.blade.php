
@extends('layout')
@section("content")
<div id="main" class="container">
		<div class="row">
			<div class="col-sm-4">
				<h1>VUEjs - AJAX axios</h1>
				<ul class="list-group">
					<li v-for="item in lists" class="list-group-item">						
						@{{ item.name }}
					</li>
				</ul>
			</div>
			<div class="col-sm-8">
				<h1>JSON</h1>
				<pre>
					@{{ $data }}
				</pre>
			</div>
		</div>
	</div>
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){
    var RxEmail = /[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i;

	var route = "{{ route("getWebPage") }}";
	var text ="";
		$.get(route, function(e){}).done(function(res){
			 console.log(res);
		});
	});

</script>
@endsection