<html>
	<head>
			<title>Cliente {{ $customer->id }}</title>
	</head>
	<body>
		<h2>Información del cliente eliminado</h2>
		<ul>
			@foreach($customer->getFillable() as $key)
			<li>{{ $key.": ".$customer[$key] }}</li>
			@endforeach
		</ul>
	</body>
</html>