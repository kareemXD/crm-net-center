@extends('layout-2')
@section('title', 'Lista de Surveys')
@section('content')
<style>
	#search_form{
		width:100% !important;
	}
</style>
<div id="page-loader"><span class="preloader-interior"></span></div>
		<h3 class="box-title">Reporte de clientes/prospectos</h3>

	<div class="widget-container">
		<div class="container-fluid">
			<div class="row">
				<div class="col-3 mt-3">
					<div id="accordion">
					  <div class="card">
					    <div class="card-header bg-primary" id="headingOne">
					      <h5 class="mb-0">
					        <button class="btn btn-link text-white" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          LLEGADAS
					        </button>
					      </h5>
					    </div>

					    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					      <div class="card-body">
					      	@if(count($llegadas) > 0)
						        @foreach($llegadas as $llegada)
						       		<a href="{{ route('reportStatics', array_merge(request()->post(), [
						       		"filter-check-in" =>  is_null($llegada->checkin)? "NULL" : substr($llegada->checkin, 0,10),
						       		"operator-check-in" => "="
						       		] ))  }}">{{ is_null($llegada->checkin)? "NULL" : substr($llegada->checkin, 0,10) }} ( {{$llegada->cantidad}} ) </a> <br>
						        @endforeach
					        @else
					        SIN DATOS
					        @endif
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header bg-primary" id="headingTwo">
					      <h5 class="mb-0">
					        <button class="btn btn-link text-white text-nowrap collapsed " data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					         DOCUMENTOS FIRMADOS
					        </button>
					      </h5>
					    </div>
					    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					      <div class="card-body">
					      	@if(count($documentos_firmados) != 0)
						        @foreach($documentos_firmados as $firmados)
						        	<a href="{{ route('reportStatics', array_merge(request()->post(), ["filter-has-signed" =>  ($firmados->tienefirmados == "SI"? "S" : "N")] ))  }}">{{ $firmados->tienefirmados }} ( {{ $firmados->cantidad }} ) </a> <br>
						        @endforeach
					        @else
					        SIN DATOS
					        @endif
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header bg-primary" id="headingThree">
					      <h5 class="mb-0">
					        <button class="btn btn-link text-white collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					          TIPO VENTA
					        </button>
					      </h5>
					    </div>
					    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
					      <div class="card-body">
					      	@if(count($categories) == 0)
					      	SIN DATOS
					      	@endif
					        @foreach($categories as $category)
					        	<a href="{{ route('reportStatics', array_merge(request()->post(), [
					        	"operator-category-type" =>  "=", 
					        	"category-type" => [$category->code]
					        	] ))  }}" class="btn btn-link text-nowrap">{{ $category->name }} ( {{ $category->cantidad }} )</a> <br>
				         	@endforeach
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header bg-primary" id="headingFour">
					      <h5 class="mb-0">
					        <button class="btn btn-link text-white collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					          ORIGEN
					        </button>
					      </h5>
					    </div>
					    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
					      <div class="card-body">
					      	@if(count($origens) == 0)
					      	SIN DATOS
					      	@endif
					        @foreach($origens as $origen)
						        <a href="{{ route('reportStatics', array_merge(request()->post(), [
						        	"operator-origen" =>  "=", 
						        	"filter-origen" => [$origen->id]
						        	] ))  }}" class="btn btn-link text-nowrap p-0">{{ $origen->nombre }} ( {{ $origen->Cantidad }} )</a> <br>
				         	@endforeach
					          
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header bg-primary" id="headingFive">
					      <h5 class="mb-0">
					        <button class="btn btn-link text-white collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
					          TIPO QS
					        </button>
					      </h5>
					    </div>
					    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
					      <div class="card-body">
					      	@if(count($tipoqs) == 0)
							SIN DATOS
					      	@endif
					        @foreach($tipoqs as $tipoq)
				          		<a href="{{ route('reportStatics', array_merge(request()->post(), [
						        	"operator-tipoq" =>  "=", 
						        	"filter-tipoq" => [$tipoq->tipo_q]
						        	] ))  }}" class="btn btn-link">{{ $tipoq->tipo_q }} ( {{$tipoq->cantidad}} )</a> <br>
				         	@endforeach
					      </div>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-9 mt-3">
					@include('includes.form.advanced-filter')
				</div>				
			</div>
			@if(count(request()->post()) > 0 )
			<div class="row">
				<div class="col-auto">
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Graficas
						  </button>
						<div class="dropdown-menu btn-loading-check" aria-labelledby="dropdownMenuButton">
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-arrival"])) }}"><i class="fa fa-chart"></i>Grafica Llegadas <i class="fa fa-bar-chart"></i></a>
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-arrival", "chart-type" => "pie"])) }}"><i class="fa fa-chart"></i>Grafica Llegadas  <i class="fa fa-pie-chart"></i></a>
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-origen"])) }}"><i class="fa fa-chart"></i>Grafica Origenes  <i class="fa fa-bar-chart"></i></a>
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-origen", "chart-type" => "pie"])) }}"><i class="fa fa-chart"></i>Grafica Origenes  <i class="fa fa-pie-chart"></i></a>
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-sale-type"])) }}"><i class="fa fa-chart"></i>Grafica Tipo de Venta  <i class="fa fa-bar-chart"></i></a>
						  	<a class="dropdown-item" href="{{ route("reportStatics", array_merge(request()->post(), ["filter-view-type" => "graph-sale-type", "chart-type" => "pie"])) }}"><i class="fa fa-chart"></i>Grafica Tipo de Venta  <i class="fa fa-pie-chart"></i></a>

						</div>
					</div>
				</div>
				@if(!in_array(request()->post("filter-view-type"), ["graph-arrival", "graph-origen", "graph-sale-type"]))
				<div class="col-auto pr-0">
					<a id="report_print_button" class="btn btn-secondary" href="{{ route("sendReportStatic", request()->post()) }}"><i class="fa fa-print"></i></a>
				</div>
				@endif
			</div>
			@endif
			@include("includes.report.arrival-report")
			@include("includes.report.origen-report")
			@include("includes.report.sale-type-report")

			@if(count(request()->post()) > 0 && !in_array(request()->post("filter-view-type"), ["graph-arrival", "graph-origen", "graph-sale-type"]))
			{{--<div class="row">

			
				
				<div class="col-auto pl-0">
					<select class="form-control bg-secondary text-white" name="filter-report-type" id="select_report_type">
						<option data-href="{{ route("sendReportStatic", array_merge(request()->post()),["filter-report-type" => 1]) }}" value="1">Reporte Llegadas</option>
						<option data-href="{{ route("sendReportStatic", array_merge(request()->post()),["filter-report-type" => 2]) }}" value="2">Reporte de tipo venta</option>
						<option data-href="{{ route("sendReportStatic", array_merge(request()->post()),["filter-report-type" => 3]) }}" value="3">Reporte de categoria</option>
					</select>
				</div>
				
			</div>--}}
			<div class="row">
				<div class="col-12">
					<table class="table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Llega</th>
								<th>Sale</th>
								<th>Fecha venta</th>
								<th>Tipo venta</th>
							</tr>
						</thead>
						<tbody>
							@foreach($customers as $customer)
							<tr>
								<td>{{ $customer->nombre }}</td>
								<td>
									@if($customer->checkin == null)
									SIN DATOS
									@elseif($customer->checkin == "0000-00-00 00:00:00")
									0000-00-00
									@else
									{{ date("Y-m-d", strtotime($customer->checkin)) }}
									@endif
								</td>
								<td>
									@if($customer->checkout == null)
									SIN DATOS
									@elseif($customer->checkout == "0000-00-00 00:00:00")
									0000-00-00
									@else
									{{ date("Y-m-d", strtotime($customer->checkout)) }}
									@endif								
								</td>
								<td>
									@if($customer->fecha_venta == null)
									SIN DATOS
									@elseif($customer->fecha_venta == "0000-00-00 00:00:00")
									0000-00-00
									@else
									{{ date("Y-m-d", strtotime($customer->fecha_venta)) }}
									@endif	
								</td>
								<td>{{ $customer->categoryName }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $customers->appends(Request::input())->links() }}
				</div>
			</div>
			@endif
		</div>
		
	</div>
	
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){

	$table = inizializeDataTable();
	$(".select2").select2();
    $(".select2-multiple").select2({
        tags: true
    });

    $('.date-picker').datepicker({
        'format': 'yyyy-mm-dd',
    });
    $('.date-time-picker').datetimepicker({
        format:'YYYY-MM-DD hh:mm:00 a',
        locale: 'es'
    });
	$("#page-loader").fadeOut(500);
	$(document).on("change", "#select_report_type", function(event){
		$this = $(this);
		$("#report_print_button").attr("href", $this.children("option:selected").data("href"));
	});
	//select operator
    $("select.operator").on("change", function(event){
        $this = $(this);
        if($this.val() != "2") {
            $this.parent().next().next().slideUp(500);
        }else{
            $this.parent().next().next().slideDown(500);
        }
    });
    //fin de validar select
    if($("#message_flash").data("messageError") != "" &&  $("#message_flash").data("messageError") != undefined){
        new PNotify({
            title: "Ups!",
            text: $("#message_flash").data("messageError"),
            type: "error"
        })
    }
    if($("#message_flash").data("messageSuccess") != "" &&  $("#message_flash").data("messageSuccess") != undefined){
        new PNotify({
            title: "Correcto",
            text: $("#message_flash").data("messageSuccess"),
            type: "success"
        })
    }

    $(document).on("submit", "#search_form", function(event){
    	let $this = $(this);
    	let $button = $this.find("button[type='submit']");
    	let oldHtml = $button.html();
    	$button.html('Filtrando... <i class="fas fa-spinner fa-spin"></i>');
    	$button.prop("disabled", true);
    	
    });
    $(document).on("change load", "#report_countries", function(e){
    	$this = $(this);
    	$states_array = $this.children("option:selected").data("state");
    	$states = '<option value="0">Todos Los Estados </option>';
    	$.each($states_array, function(index, state){
    		$states = $states + '<option value="'+ state.id +'">'+ state.estadonombre +'</option>';
    	});

    	$("#report_states").html($states);
    });
    /*loading button*/
    $(document).on("click",".btn-loading-check", function(event){
    	$this = $(this);
    	let oldHtml = $this.html();
    	$this.html('...<i class="fas fa-spinner fa-spin"></i>');
    	$this.prop("disabled", true);
    	setTimeout(function(){
    		$this.html(oldHtml);
    		 $this.prop("disabled", false);
    	}, 10000);
    });
    /*fin loading button**/
	function inizializeDataTable() {
        $table = $("#reportsTable").DataTable({
                //"processing": true,
                //"serverSide": true,
                //ajax: ,
                "columns": [
                	{data: 'assign', name: 'assign', orderable: false, searchable: false},
                	{data: "survey_datos_cliente.nombre", name: 'nombre'},
                	{data: "survey_datos_cliente.email_decrypt", name: 'email_decrypt'},
                	{data: "survey_datos_cliente.telefono_celular_decrypt", name: 'telefono_celular_decrypt'},
                	{data: "survey_datos_cliente.telefono_casa_decrypt", name: 'telefono_casa_decrypt'},
                	{data: "comentarios"},
                	{data: "created_at"},
                	{data: 'survey_operation', name: 'survey_operation', orderable: false, searchable: false},
                ],
                "pageLength": 25,
                "language":{
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en esta tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No encontrados en la tabla",
                    "paginate": {
                        "first":      "Primer",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna ascendente",
                        "sortDescending": ": Activar para ordenar la columna descendente"
                    }
                }
            });
	        return $table;
	    }

	    function cleanAdvancedFilter() {
	        $('select#executive_name').select2("val", "[]");
	        //checkin
	        $('input[name="check-in"]').val("");
	        $('input[name="check-in-2"]').val("");
	        //checkout
	        $('input[name="check-out"]').val("");
	        $('input[name="check-out-2"]').val("");
	        //pendiente
	        $("input[name='pending-payment']").val("");     
	        //pagado                   
	        $("input[name='paid-out']").val("");
	        //checar si tiene documentos firmados
	        $("input[name='has-signed']:checked").prop("checked", false);
	        //checar el tipo categoria
	        $("select[name='category-type']").select2("val", "[]");
	        //elegir un orden
	        $("select[name='customer-order']").val("");
	    }
	}); 
</script>
@endsection