<table class="table">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Llega</th>
			<th>Sale</th>
			<th>Fecha venta</th>
			<th>Tipo venta</th>
		</tr>
	</thead>
	<tbody>
		@foreach($customers as $customer)
		<tr>
			<td>{{ $customer->nombre }}</td>
			<td>
				@if($customer->checkin == null)
				SIN DATOS
				@elseif($customer->checkin == "0000-00-00 00:00:00")
				0000-00-00
				@else
				{{ date("Y-m-d", strtotime($customer->checkin)) }}
				@endif
			</td>
			<td>
				@if($customer->checkout == null)
				SIN DATOS
				@elseif($customer->checkout == "0000-00-00 00:00:00")
				0000-00-00
				@else
				{{ date("Y-m-d", strtotime($customer->checkout)) }}
				@endif								
			</td>
			<td>
				@if($customer->fecha_venta == null)
				SIN DATOS
				@elseif($customer->fecha_venta == "0000-00-00 00:00:00")
				0000-00-00
				@else
				{{ date("Y-m-d", strtotime($customer->fecha_venta)) }}
				@endif	
			</td>
			<td>{{ @$customer->categoryName }}</td>
		</tr>
		@endforeach
	</tbody>
</table>