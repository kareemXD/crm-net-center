<form class="edit" method="post" action="{{ route('updateCustomer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<label for="customer-card">TARJETA</label>
				@if(Auth::user()->hasAccessApp('customer_card'))
				<input  type="text" class="form-control cc-number @if($errors->has('customer-card')) danger @endif" name="customer-card" id="customerCard" value="{{ $customer->numero_tarjeta }}" >
				@else
				<input @if(strlen($customer->numero_tarjeta) > 12) readonly @endif type="text" class="form-control @if(!strlen($customer->numero_tarjeta) > 12) cc-card @endif @if($errors->has('customer-card')) danger @endif" name="customer-card" id="customerCard" value="{{ $customer->numero_tarjeta }}" >
				@endif
				<div class="text-red @if($errors->has('customer-card')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="customer-bank">BANCO</label>
				<input type="text" class="form-control @if($errors->has('customer-bank')) @endif" name="customer-bank" id="customerBank" value="{{ $customer->banco }}">
				<div class="text-red @if($errors->has('customer-bank')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-3 col-sm-2 no-padding">
				<div class="row">
					<div class="col-md-7 col-sm-7">
						<label for="vence">VENCE</label>
						<input placeholder="mm / yyyy" type="text" class="form-control cc-exp @if($errors->has('card-expiration')) visible @endif" name="card-expiration" id="cardExpiration" value="{{ $customer->expiration }}">
					</div>
					<div class="col-md-5 col-sm-5">
						<label for="card-code">CÓDIGO</label>
						<input type="text" class="form-control cc-cvc @if($errors->has('card-code')) danger @endif" placeholder="---" value="{{ $customer->codigo }}" name="card-code" id="cardCode" >
						<div class="text-red @if($errors->has('card-code')) visible @endif">Campo requerido</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="card-type">TIPO DE TARJETA</label>
				<select name="card-type" id="cardType" class="form-control select2">
					@foreach($cardTypes as $cardType)
					<option  @if($customer->tipotarjeta == $cardType->code) selected @endif value="{{ $cardType->code }}">{{ $cardType->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-2 col-sm-2">
				<label for="cards-number">NÚMERO DE TARJETAS</label>
				<input  type="number" class="form-control @if($errors->has('cards-number')) danger @endif" name="cards-number" id="cardsNumber" value="{{ $customer->num_tarjetas }}">
				<div class="text-red @if($errors->has('cards-number')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-2 col-sm-2 text-center">
				<label for="has-visa">¿VISA?</label>
				<select name="has-visa" id="hasVisa" class="form-control">
					<option @if($customer->visa == "N") selected @endif value="N">NO</option>
					<option @if($customer->visa == "S") selected @endif value="S">SI</option>
				</select>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="has-mc">¿MC?</label>
				<select name="has-mc" id="hasMc" class="form-control">
					<option @if($customer->mc == "N") selected @endif value="N" selected="selected" >NO</option>
					<option @if($customer->mc == "S") selected @endif value="S">SI</option>
				</select>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="has-amex">¿AMEX?</label>
				<select name="has-amex" id="hasAmex" class="form-control">
					<option @if($customer->amex == "N") selected @endif value="N" selected="selected" >NO</option>
					<option @if($customer->amex == "S") selected @endif value="S">SI</option>
				</select>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="monthly-income">INGRESO MENSUAL</label>
				<input name="monthly-income" type="text" class="form-control price-format @if($errors->has('monthly-income')) danger @endif" value="{{ ( $customer->ingreso_mensual == "")? 0 : $customer->ingreso_mensual }}" id="monthlyIncome" >
				<div class="text-red @if($errors->has('monthly-income')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="marital-status">ESTADO CIVIL</label>
				<select name="marital-status" id="maritalStatus" class="form-control">
					@foreach($maritalStatuses as $maritalStatus)
						<option @if($customer->estado_civil == $maritalStatus->id) selected @endif value="{{ $maritalStatus->id }}">{{ $maritalStatus->nombre }}</option>
					@endforeach
				</select>
			</div>
		</div>
		@include('partials.button-update')
	</div>
</form>