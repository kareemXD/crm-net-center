<form  class="edit" method="post" action="{{ route('updateCustomer') }}" id="eses" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<div class="row with-border">
			<div class="col-md-7 col-sm-7 no-padding" >
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<label for="check-in">CHECK IN</label> <i class="fa fa-calendar" aria-hidden="true"></i>
						<input name="check-in"  data-bs-datepicker="{'format':'yyyy-mm-dd'}"type="text" class="form-control date-picker @if($errors->has('check-in')) danger @endif" id="checkIn" value="{{ substr($customer->checkin, 0, 10) }}">
						<div class="text-red @if($errors->has('check-in')) visible @endif"></div>
					</div>
					<div class="col-md-4 col-sm-4">
						<label for="check-out">CHECK OUT</label> <i class="fa fa-calendar" aria-hidden="true"></i>
						<input name="check-out" data-bs-datepicker="yyy-mm-dd" type="text" class="form-control date-picker @if($errors->has('check-out')) danger @endif" id="checkOut" value="{{ substr($customer->checkout, 0, 10) }}">
						<div class="text-red @if($errors->has('check-out')) visible @endif"></div>
					</div>
					<div class="col-md-4 col-sm-4">
						<label for="customer-plan">PLAN</label>
						<select data-placeholder="Seleccione una opción" name="customer-plan" id="customerPlan" class="form-control @if($errors->has('customer-plan')) danger @endif">
							<option @if($customer->plan == "E") selected @endif value="E">EUROPEO</option>
							<option @if($customer->plan == "T") selected @endif value="T">TODO INCLUIDO</option>
						</select>
						<div class="text-red @if($errors->has('customer-plan')) visible @endif"></div>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-sm-5 no-padding">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<label for="customer-adults">ADULTOS</label>
						<input min="0"  type="number" class="form-control @if($errors->has('customer-adults')) danger @endif" name="customer-adults" id="customerAdults" value="{{ $customer->adultos }}">
						<div class="text-red @if($errors->has('customer-adults')) visible @endif">Campo Requerido</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<label for="customer-children">MENORES</label>
						<input  min="0" class="form-control @if($errors->has('customer-children')) danger @endif" type="number" value="{{ $customer->menores }}" name="customer-children" id="customerChildren" >
						<div class="text-red @if($errors->has('customer-children')) visible @endif">Campo Requerido</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<label for="children-age">EDAD MENORES</label>
						<input name="children-age" id="childrenAge" value="{{ $customer->edadmenores }}" type="text" class="form-control @if($errors->has('children-age')) danger @endif">
						<div class="text-red @if($errors->has('children-age')) visible @endif">Campo requerido</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-3 col-sm-3">
				<label for="reservation-sheet">FOLIO DE RESERVACION</label>
				<input name="reservation-sheet" type="text" class="form-control @if($errors->has('reservation-sheet')) danger @endif" value="{{ $customer->folioreserva }}" id="reservationSheet" >
				<div class="text-red @if($errors->has('reservation-sheet')) visible @endif">Campo Requerido</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="certification-nights"> NOCHES CERTIFICADAS</label>
				<input type="number" name="certification-nights" id="certificationNights" value="{{ $customer->noches_en_certificado }}" class="form-control @if($errors->has('certification-nights')) danger @endif"  >
				<div class="text-red @if($errors->has('certification-nights')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="hotel-nights">NOCHES EN HOTEL</label>
				<input type="number" class="form-control @if($errors->has('hotel-nights')) danger @endif" name="hotel-nights" value="{{ $customer->noches_en_hotel }}" id="hotelNights" >
				<div class="text-red @if($errors->has('hotel-nights')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="reservation-type">TIPO</label>
				<div class="form-group">
					<div class="custom-control custom-radio">
						<input @if($customer->certi_hotel == "C") checked @endif  name="reservation-type" checked id="reservation_type_c" type="radio" class="custom-control-input" value="C">
						<label for="reservation_type_c" class="custom-control-label">CERTIFICADO</label>
					</div>
					<div class="custom-control custom-radio">
						<input  @if($customer->certi_hotel == "H") checked @endif  type="radio" class="custom-control-input" name="reservation-type" id="reservation_type_h" value="H">
						<label for="reservation_type_h" class="custom-control-label">HOTEL</label> 
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-2 col-sm-2">
				<label for="total-price">PRECIO TOTAL</label>
				<input type="text" readonly name="total-price" id="totalPrice" class="form-control price-format @if($errors->has('total-price')) danger @endif" value="{{ ($customer->precio_total == "")? 0 :$customer->precio_total }}">
				<div class="text-red @if($errors->has('total-price')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="total-certificate">TOTAL CERTIFICADO</label>
				<input type="text" class="form-control price-format @if($errors->has('total-certificate')) danger @endif" name="certificate-price" id="totalCertificate" value="{{ ($customer->total_certificado == "")? 0 :$customer->total_certificado }}">
				<div class="text-red @if($errors->has('certificate-price')) visible @endif"> Campo requerido</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<label for="total-hotel">TOTAL HOTEL</label>
				<input type="text" class="form-control price-format @if($errors->has('total-hotel')) danger @endif" name="total-hotel" value="{{ ($customer->total_hotel == "")? 0 :$customer->total_hotel }}" id="totalHotel">
				<div class="text-red @if($errors->has('total-hotel')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1">
				<label for="paid-out">PAGADO</label>
				<input type="text" class="form-control price-format @if($errors->has('paid-out')) danger @endif" name="paid-out" id="paidOut" value="{{ ($customer->pagado == "")? 0 :$customer->pagado }}">
			</div>
			<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1">
				<label for="pending-payment">PAGO PENDIENTE</label>
				<input type="text" name="pending-payment" value="{{ ($customer->pendiente == "")? 0 :$customer->pendiente }}" name="pending-payment" id="pendingPayment" class="form-control price-format @if($errors->has('pending-payment')) visible @endif">
			</div>
		</div>
		@include('partials.button-update')
	</div>
</form>
