<hr>
<div class="row">
	<div class="col-md-4 col-sm-4">
		<label for="date-hour">FECHA Y HORA</label>
		<input type="text" class="form-control date-time-picker" name="date-hour" value="{{ $move->fecha }}">
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-activity">ACTIVIDAD</label>
		<select name="note-activity" class="form-control">
		@foreach($activities as $activity)
			<option @if($move->actividad == $activity->id) selected @endif value="{{ $activity->id }}">{{ $activity->nombre }}</option>
		@endforeach
		</select>
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-title">TÍTULO</label>
		<input type="text" class="form-control" value="{{ $move->titulo }}" name="note-title">
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-8 col-sm-8">
		<label for="note-details">DETALLES</label>
		<textarea name="note-details"  rows="6" class="form-control">{{ $move->observaciones }}</textarea>
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-status">Estatus</label>
		<select name="note-status" class="form-control">
		@foreach($statuses as $status)
			<option @if($status->id == $move->estatus) selected @endif value="{{$status->id}}">{{($status->nombre == null )? "NULO" : $status->nombre}}</option>
		@endforeach
		</select>
	</div>
</div>