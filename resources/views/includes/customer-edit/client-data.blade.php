<form class="edit"  method="post" action="{{ route('updateCustomer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<input type="hidden" value="{{ $customer->id }}" name="id">
		<div class="row with-border">
			<div class="col-4">
				<label class="text-left" for="executive">Ejecutivo </label>
				@if(Auth::user()->hasAccessApp('user_view'))
				<select class="form-control select2" name="executive" id="executive">
					@foreach($executives as $executive)
						<option @if($customer->ejecutivo == $executive->id) selected @endif value="{{ $executive->id }}">{{ $executive->nombre }}</option>
					@endforeach
				</select>
				@else

				<h4><small>{{ $customer->executive->nombre }}</small></h4>
				@endif
			</div>
			<div class="col-4">
				<label for="edit_origen">ORIGEN</label>
				<select class="form-control select2" name="origen" id="edit_origen">
					@foreach($origens as $index => $origen)
						<option data-route="{{ route("subOrigenLoad", $origen->id) }}" @if($customer->origen == $origen->id) selected @endif value="{{ $origen->id }}">{{ $origen->nombre }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-4">
				<label for="origen">SUB ORIGEN</label>
				<select class="form-control select2" name="edit-sub-origen" id="sub_origen_edit">
					@if(count($suborigens) == 0)
						<option  value="0">No definido</option>

					@endif
					@foreach($suborigens as $index => $sub)
					<option @if($customer->suborigen == $sub->id ) selected @endif value="{{ $sub->id }}">{{ ($sub->name) }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-4">
				<label for="seller">VENDEDOR</label>
				<input value="{{$customer->vendedor}}" class="form-control" name="seller" id="seller" type="text">
			</div>
			<div class="col-4">
				<label for="interested">Fecha de Creación</label>
				<label>{{$customer->formatDate($customer->fecha_alta)}}</label>
				
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-5 col-sm-5">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<select name="prefix-name" id="prefixName" class="form-control">
							<option @if($customer->prefix_name == "Sr.") selected @endif value="Sr.">Sr.</option>
							<option  @if($customer->prefix_name == "Sra.") selected @endif value="Sra.">Sra.</option>
						</select>
					</div>
					<div class="col-md-9 col-sm-9">
						<input placeholder="NOMBRE" class="form-control @if($errors->has('customer-name')) danger-input @endif" name="customer-name" id="customer_name" value="{{ $customer->nombre }}"  type="text">
						<div class="text-red @if($errors->has('customer-name')) visible @endif"><strong>Campo Requerido</strong></div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input placeholder="EDAD" name="age" class="form-control @if($errors->has('age'))danger-input @endif" id="age" value="{{ $customer->edad }}" type="number">
				<div class="text-red @if($errors->has('age')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-5 col-sm-5">
				<input placeholder="OCUPACIÓN" type="text" value="{{ $customer->ocupacion }}" name="customer-job" id="customer_job" class="form-control @if($errors->has('customer-job')) danger-input @endif">
				<div class="text-red @if($errors->has('customer-job')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-5 col-sm-5">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<select name="prefix-name2" id="prefixName2" class="form-control">
							<option @if($customer->prefix_name == "Sr.") selected @endif value="Sr.">Sr.</option>
							<option  @if($customer->prefix_name == "Sra.") selected @endif value="Sra.">Sra.</option>
						</select>
					</div>
					<div class="col-md-9 col-sm-9">
						<input placeholder="NOMBRE" class="form-control @if($errors->has('customer-name-co')) danger-input @endif" name="customer-name-co" id="customer_name_co" value="{{ $customer->nombre_co }}"  type="text">
						<div class="text-red @if($errors->has('customer-name-co')) visible @endif"><strong>Campo Requerido</strong></div>
					</div>	
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input placeholder="EDAD" name="age-co" class="form-control @if($errors->has('age-co'))danger-input @endif" id="age_co" value="{{ $customer->edad_co }}" type="number">
				<div class="text-red @if($errors->has('age-co')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-5 col-sm-5">
				<input placeholder="OCUPACIÓN" type="text" value="{{ $customer->ocupacion_co }}" name="customer-job-co" id="customer_job_co" class="form-control @if($errors->has('customer-job-co')) danger-input @endif">
				<div class="text-red @if($errors->has('customer-job-co')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-3 col-sm-3">
				<input placeholder="TELÉFONO CELULAR" name="cell-phone" id="cellPhone" value="{{ $customer->tel_cel_decrypt }}" class="form-control @if($errors->has('cell-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('cell-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-3 col-sm-3">
				<input name="home-phone" placeholder="TELÉFONO DE CASA" id="homePhone" value="{{ $customer->tel_casa_decrypt }}" class="form-control @if($errors->has('home-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('home-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
				<input placeholder="TELÉFONO DE OFICINA" name="office-phone" id="officePhone" value="{{ $customer->tel_oficina_decrypt }}" class="form-control @if($errors->has('office-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('office-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input class="form-control @if($errors->has('extension')) danger @endif" type="text" placeholder="EXT" name="extension" value="{{ $customer->extencion }}" id="extension">
				<div class="text-red @if($errors->has('extension')) visible @endif"></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-7 col-sm-7">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<label for="homeowner">¿Propietario vivienda?</label>
						<div class="form-group">
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" checked name="homeowner" id="homeownerY" class="custom-control-input" value="Y">
								<label for="homeownerY" class="custom-control-label">SI</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input class="custom-control-input" type="radio" @if($customer->homeowner == false) checked @endif name="homeowner" id="homeownerN" value="N">
								<label  for="homeownerN" class="custom-control-label"><span>NO</label>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<label for="customer-email">Email</label>
						<input class="form-control @if($errors->has('customer-email')) danger @endif" type="text" name="customer-email" value="{{ $customer->email_decrypt }}" id="customerEmail" placeholder="E-Mail">
						<div class="text-red @if($errors->has('customer-email')) danger @endif"> <ul>@foreach($errors->get('customer-email') as $error) <li>$error</li> @endforeach</ul></div>
						<label for="customer-state">Estado</label>
						<input type="text" placeholder="ESTADO" class="form-control @if($errors->has('customer-state')) danger @endif" name="customer-state" value="{{ $customer->estado }}" id="customerState">
						<div class="text-red"></div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-6">
								<label for="edit_country">Pais</label>
								<select class="form-control select2" name="edit-country" id="edit_country">
									@foreach($countries as $country)
									<option data-route="{{ route('getStatesFromCountry', $country->id) }}" @if($customer->country == $country->id) selected @endif  value="{{ $country->id }}">{{ $country->paisnombre }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-6">
								<label for="edit_state">Estado</label>
								<select class="form-control select2" name="edit-state" id="edit_state">
									@foreach($states as $state)
									<option @if($state->id == $customer->state) selected @endif value="{{ $state->id }}">{{ $state->estadonombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
						
					</div>
				</div>

			</div>
			<div class="col-md-5 col-sm-5">
				<label for="address">Dirección</label>
				<textarea maxlength="300" placeholder="Dirección" name="address" id="address" rows="8" class="form-control @if($errors->has("address")) danger @endif">{{ $customer->address }}</textarea>
					<div class="text-red @if($errors->has('address')) danger @endif"> <ul>@foreach($errors->get('address') as $error) <li>$error</li> @endforeach</ul></div>
			</div>
		</div>
		@include('partials.button-update')
	</div>
</form>
