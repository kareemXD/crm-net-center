<form class="edit" method="post" action="{{ route('updateCustomer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<div class="row">
			@foreach($categories as $category)
			<div class="col-md-2 col-sm-2 custom-radio-kareem">
				<input class="hiden" id="category-{{$category->code}}" type="radio"  name ="category-client" value="{{ $category->code }}" @if($customer->categoria == $category->code ) checked @endif>
				<div style="background:{{$category->color}}" class="col-md-12 col-sm-12 border-check">

					<label for="category-client">{{ $category->name }}</label>
				</div>			
			</div>
			@endforeach 
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<label for="has-note">¿Agregar Recordatorio?</label>
				<div class="form-group">
					<div class="custom-control custom-radio custom-control-inline">
						<input @if(count($customer->moves) > 0) checked @endif data-route="{{ route('getNoteClientEdit', $customer->id) }}" type="radio" class="custom-control-input"  name ="has-note" value="S" id="has_note_s" >
						<label for="has_note_s" class="custom-control-label">SI</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input @if(count($customer->moves) == 0) checked  @endif  type="radio" class="custom-control-input"  name ="has-note" value="N" id="has_note_n" >
						<label for="has_note_n" class="custom-control-label">NO</label>
					</div>
				</div>
			</div>
		</div>
		<div id="notas"></div>
		@include('partials.button-update')
	</div>
</form>
