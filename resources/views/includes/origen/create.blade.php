<form class="needs-validation" novalidate id="formCreateOrigen" action="{{ route("origenCreate") }}" method="post">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="create_origen_nombre">Nombre</label>
		<input name="origen-nombre" id="create_origen_nombre" required type="text" class="form-control" value="{{ old('origen-nombre') }}" placeholder="coloque nombre del origen" >
	</div>
	<div class="form-group">
		<label for="create_origen_code">Code</label>
		<input name="origen-code" id="create_origen_code" 
		maxlength="1" max="1"
                data-bv-stringlength-message="no puede tener mas de 1 caracter"
		 type="text" class="form-control" value="{{ old('origen-code') }}" placeholder="coloque el codigo del origen" >
	</div>
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>

