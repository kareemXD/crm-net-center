<form class="needs-validation" novalidate id="formCreateSuborigen" action="{{ route("subOrigenCreate") }}" method="post">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="create_suborigen_nombre">Nombre</label>
		<input name="suborigen-name" id="create_suborigen_nombre" required type="text" class="form-control" value="{{ old('suborigen-name') }}" placeholder="coloque nombre del suborigen" >
	</div>
	<div class="form-group">
		<label for="create_suborigen_code">Code</label>
		<input name="suborigen-code" id="create_suborigen_code" 
		maxlength="1" max="1"
                data-bv-stringlength-message="no puede tener mas de 1 caracter"
		 type="text" class="form-control" value="{{ old('suborigen-code') }}" placeholder="coloque el codigo del suborigen" >
	</div>
	<div class="form-group">
		<label for="create_origen_id">Origen</label>
		<select class="form-control" name="suborigen-origen-id" id="create_origen_id">
			@foreach($origens as $origen)
			<option @if($origen_id == $origen->id) selected @endif value="{{ $origen->id }}">{{ $origen->nombre }}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<label for="">Estatus</label>
		<div class="material-switch">
        	<input value="si"  checked="checked"  id="create_status_suborigen" name="suborigen-status" type="checkbox">
        	<label for="create_status_suborigen" class="label-primary"></label>
     	</div>
	</div>
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>

