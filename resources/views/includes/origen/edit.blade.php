<form class="needs-validation" novalidate id="formOrigenEdit" action="{{ route("origenEdit", $origen->id) }}" method="post">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="origen_nombre">Nombre</label>
		<input name="origen-nombre" id="origen_nombre" required type="text" class="form-control" value="{{ $origen->nombre }}" placeholder="coloque nombre del origen" >
	</div>
	<div class="form-group">
		<label for="origen_code">Code</label>
		<input name="origen-code" id="origen_code" minlength="1" type="text" class="form-control" value="{{ $origen->code }}" placeholder="coloque el codigo del origen" >
	</div>
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="submit" class="btn btn-primary">Guardar cambios</button>
</form>

