<form method="get" id="search_form"  role="form" action="{{ route("reportStatics") }}"  enctype="multipart/form-data" >
    <div class="container">
        <div class="row">
            <div class="col-1 offset-1"> 
                <label for="executive">Conexión</label>
            </div>
            <div class="col-8 offset-1">
                <div class="row">
                    <div class="col-auto">
                        <label for="conexion_normal" class="radio">
                            <input checked id="conexion_normal" type="radio" value="new" name="filter-conexion"><span> normal</span>
                        </label>
                    </div>
                    <div class="col-auto">
                        <label for="conexion_old" class="radio">
                            <input @if(request()->input("filter-conexion") == "old") checked @endif id="conexion_old" type="radio" value="old" name="filter-conexion"><span> Antigua</span>
                        </label>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1 text-nowrap"> 
                <label for="executive">Ejecutivo</label>
            </div>
            <div class="col-md-8 col-sm-8"> 
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-executive" id="operator_executive" class="form-control">
                            <option @if(Request::input("operator-executive") == "=") selected @endif selected value="=">Igual</option>
                            <option @if(Request::input("operator-executive") == "!=") selected @endif  value="!=">Diferente A</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <select id="executive_name" name="executive[]" class="select2-multiple form-control" multiple="multiple">
                            @foreach($executives as $executive)
                                @if(!is_null(@$executive->id ))
                                @php
                                $executives_list = is_array(Request::input('executive')) ? Request::input('executive') : [];
                                @endphp
                                <option @if(in_array($executive->id, $executives_list)) selected @endif value="{{ $executive->id }}">{{ $executive->nombre }}</option>
                                @endif
                            @endforeach
                        </select> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1">
                <label for="check-in">Llegada</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-check-in" id="operator_check_in" class="form-control operator">
                            <option @if(Request::input("operator-check-in") == "2") selected @endif value="2">Entre Fechas</option>
                            <option @if(Request::input("operator-check-in") == ">=") selected @endif value=">="> Mayor o igual</option>
                            <option @if(Request::input("operator-check-in") == "=") selected @endif value="=">Igual</option>
                            <option @if(Request::input("operator-check-in") == ">") selected @endif value=">">Mayor</option>
                            <option @if(Request::input("operator-check-in") == "<") selected @endif value="<">Menor</option>
                            <option @if(Request::input("operator-check-in") == "<=") selected @endif value="<=">Menor o igual</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input value="{{ Request::input("filter-check-in") }}" type="text" class="form-control date-picker" name="filter-check-in" id="check_in">   
                    </div>                    
                    <div class="col-md-8 col-sm-8 offset-md-4 offset-sm-4 form-group">
                        <input value="{{ Request::input("check-in-2") }}" type="text" placeholder="Fecha 2" class="form-control date-picker" name="check-in-2" id="check_in_2">   
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="check-out">Salida</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-check-out" id="operator_check_out" class="form-control operator">
                            <option @if(Request::input("operator-check-out") == "2") selected @endif value="2">Entre Fechas</option>
                            <option @if(Request::input("operator-check-out") == ">=") selected @endif value=">="> Mayor o igual</option>
                            <option @if(Request::input("operator-check-out") == "=") selected @endif value="=">Igual</option>
                            <option @if(Request::input("operator-check-out") == ">") selected @endif value=">">Mayor</option>
                            <option @if(Request::input("operator-check-out") == "<") selected @endif value="<">Menor</option>
                            <option @if(Request::input("operator-check-out") == "<=") selected @endif value="<=">Menor o igual</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input value="{{ Request::input("filter-check-out") }}" type="text" class="form-control date-picker" name="filter-check-out" id="check_out">
                    </div>
                    <div class="col-md-8 col-sm-8 offset-md-4 offset-sm-4 form-group">
                        <input value="{{ Request::input("check-out-2") }}" type="text" class="form-control date-picker" name="check-out-2" id="check_out_2">
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="pending-payment">$pendiente</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-pending-payment" id="operator_payment_paymen" class="form-control operator">
                            <option @if(Request::input("operator-pending-payment") == ">=") selected @endif value=">="> Mayor o igual</option>
                            <option @if(Request::input("operator-pending-payment") == "=") selected @endif value="=">Igual</option>
                            <option @if(Request::input("operator-pending-payment") == ">") selected @endif value=">">Mayor</option>
                            <option @if(Request::input("operator-pending-payment") == "<") selected @endif value="<">Menor</option>
                            <option @if(Request::input("operator-pending-payment") == "<=") selected @endif value="<=">Menor o igual</option>
                        </select>    
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input value="{{ Request::input("filter-pending-payment") }}" type="text" class="form-control price-format-null" name="filter-pending-payment" >
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="paid-out">$pagado</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-paid-out" id="operator_paid_out" class="form-control">
                            <option @if(Request::input("operator-paid-out") == ">=") selected @endif value=">="> Mayor o igual</option>
                            <option @if(Request::input("operator-paid-out") == "=") @endif value="=">Igual</option>
                            <option @if(Request::input("operator-paid-out") == ">") selected @endif value=">">Mayor</option>
                            <option @if(Request::input("operator-paid-out") == "<") selected @endif value="<">Menor</option>
                            <option @if(Request::input("operator-paid-out") == "<=") selected @endif value="<=">Menor o igual</option>
                        </select>    
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input value="{{ Request::input("filter-paid-out") }}" type="text" class="form-control price-format-null" name="filter-paid-out" >
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-3 col-sm-3 offset-md-1 offset-sm-1"> 
                <label for="paid-out">Datos firmados</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-3 col-sm-3 text-center">
                        <label for="has_signed_s" class="radio">
                            <input @if(Request::input("filter-has-signed") == "S") checked @endif id="has_signed_s" type="radio" value="S" name="filter-has-signed"><span>SI</span>
                        </label>
                    </div>
                    <div class="col-md-3 col-sm-3 text-center">
                        <label for="has_signed_n" class="radio">
                            <input  @if(Request::input("filter-has-signed") == "N") checked @endif id="has_signed_n" type="radio" value="N" name="filter-has-signed"><span>NO</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 com-md-offset-1 offset-sm-1 text-nowrap">
                <label for="category_type">Tipo Venta</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-category-type" id="operator_category_type" class="form-control">
                            <option @if(Request::input("operator-category-type") == "=") selected @endif value="=">Igual</option>
                            <option @if(Request::input("operator-category-type") == "!=") selected @endif value="!=">Diferente A</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        @php
                            $categories_list = is_array(Request::input('category-type')) ? Request::input('category-type') : [];
                        @endphp
                        <select name="category-type[]" class="select2-multiple form-control" id="category_type" multiple="multiple">
                        @foreach($categories as $category)
                            <option @if(in_array($category->code, $categories_list)) selected @endif value="{{ $category->code }}">{{ $category->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>   
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1">
                <label for="customer_order">Orden</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select placeholder="seleccionar orden" class="form-control" name="customer-order" id="customer_order">
                            <option @if(Request::input("customer-order") == "") selected @endif value=""></option>
                            <option @if(Request::input("customer-order") == "fecha_alta") selected @endif value="fecha_alta">Fecha de creación</option>
                            <option @if(Request::input("customer-order") == "nombre") selected @endif value="nombre">Nombre del vendedor</option>
                            <option @if(Request::input("customer-order") == "fecha_venta") selected @endif value="fecha_venta">Fecha de venta</option>
                            <option @if(Request::input("fecha_modificacion") == "fecha_venta") selected @endif value="fecha_modificacion">Fecha de Modificación</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="operator-customer-order" id="operator_customer_order">
                            <option @if(Request::input("operator-customer-order") == "asc") selected @endif value="asc">Ascendiente</option>
                            <option @if(Request::input("operator-customer-order") == "desc") selected @endif  value="desc">Descendiente</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-1 offset-1">
                <label for="">Ubicacion</label>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-6 p-0">
                        @php
                        $selected = ((Request::input('report-countries') == ""))? 42 : Request::input('report-countries');
                        @endphp
                        <select class="form-control" name="report-countries" id="report_countries">
                            @foreach($countries as $country)
                            <option @if($selected == $country->id) selected @endif value="{{ $country->id }}" data-state="{{ json_encode($country->states) }}">{{$country->paisnombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-6">
                        <select class="form-control" name="report-states" id="report_states">
                            @foreach($states as $state)
                            <option @if($state->estadonombre == "SIN ASIGNAR") selected @endif value="{{ $state->id }}">{{ $state->estadonombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div style="margin-top:10px" class="text-center">
                    <button class="btn btn-primary  btn-loading" type="submit">Filtrar</button>
                    <a href="{{ route('reportCustomer') }}" id="btn_clean_filter" class="btn btn-secondary btn-loading">Limpiar </a>
                </div>
            </div>
        </div>
    </div>
    
</form>