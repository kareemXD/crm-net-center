@if(Auth::user()->hasAccessApp('proyectos_delete'))
	<button class="btn btn-danger" type="button" data-toggle="modal" data-route="{{ route('proyectDelete', $email->id) }}" data-target="#modalDelete" ><i class="fa fa-trash"></i></button>
@endif
@if(Auth::user()->hasAccessApp('proyectos_assign'))
                <button data-route="{{ route("assignCustomerToExecutive", $email->id) }}" data-toggle="modal" data-target="#modal_assign" title="asignar" class="btn btn-primary" >
                    <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                </button>
@endif
<button data-route="{{ route("getEmailTracing", $email->id) }}" data-toggle="modal" data-target="#modalTracing" title="Seguimiento" 
	@if(count($email->emailTracings) > 0)
		class="btn btn-secondary"
	@else
		class="btn btn-primary"

	@endif

	 >
                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                </button>
<button class="btn btn-primary" data-route="{{ route("getEmailInfo", $email->id) }}" data-toggle="modal" data-target="#modal_phone"><i class="fa fa-eye"></i></button>
