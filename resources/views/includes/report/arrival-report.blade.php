@if(count(request()->post()) > 0 && request()->input("filter-view-type") == "graph-arrival")
<div class="text-center">
	<button id="pdfFile" class="btn btn-primary"><i class="fa fa-download"></i>Guardar PDF</button>
</div>
<div data-chart-type="{{ request()->input("chart-type") }}" data-customer="{{ json_encode($llegadas) }}" id="customer_checkin"></div>
<canvas style="background:#ffffff;" id="chartCheckin" width="400" height="400"></canvas>
<script>
	$("#pdfFile").on("click", function(event){
		var canvas = document.querySelector('#chartCheckin');
		//creates image
		var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
	  
		//creates PDF from img
		//creates PDF from img
  		var pdf = new jsPDF('l', 'pt', [1653, 2480]);
		pdf.addImage(canvas, 'PNG', 0, 0 );
		pdf.save('reporte-llegada'+ Math.random(299) +'.pdf');


	});
	var ctx = document.getElementById('chartCheckin').getContext('2d');
	var datos_llegadas = $("#customer_checkin").data("customer");
	var chart_type = ($("#customer_checkin").data("chartType") == "pie")? "pie" : "bar";
	if((datos_llegadas).length > 0){
		let $labels = [];
		let $data = [];
		let $backgroundColor = [];
		let $borderColor = [];
		let total = 0;		
		$.each(datos_llegadas, function(index, llegada){
			total += llegada.cantidad;
		});
		$.each(datos_llegadas, function(index, llegada){
			let percent = parseInt((llegada.cantidad/total)*100) + '%';
			$labels.push(llegada.checkin + ": " +percent );
			$data.push(llegada.cantidad);

			$backgroundColor.push(radomColor());
			$borderColor.push(radomBorderColor())
		});
		

		var myChart = new Chart(ctx, {
		    type: chart_type,
		    data: {
		        labels: $labels,
		        datasets: [{
		            label: 'Llegadas',
		            data: $data,
		            backgroundColor: $backgroundColor,
		            borderColor: $borderColor,
		            borderWidth: 1
		        }]
		    },
		    options: {

		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}else{
		$("#chartCheckin").prev().html("<h3>Sin datos.</h3>");
	}
	//generate random background color
	function radomColor() {
		var x = Math. floor(Math. random() * 256);
		var y = Math. floor(Math. random() * 256);
		var z = Math. floor(Math. random() * 256);
		var a = Math. floor(Math.random() * 10);
		var bgColor = "rgba(" + x + "," + y + "," + z + ", "+a+")";
		return bgColor;
	}
	function radomBorderColor() {
		var x = Math. floor(Math. random() * 256);
		var y = Math. floor(Math. random() * 256);
		var z = Math. floor(Math. random() * 256);
		var a = Math. floor(Math.random() * 10);
		var bgColor = "rgba(" + x + "," + y + "," + z + ", 1)";
		return bgColor;
	}
	</script>
@endif
