<hr>
<div class="row">
	<div class="col-md-4 col-sm-4">
		<label for="date-hour">FECHA Y HORA</label>
		<input type="text" class="form-control date-time-picker" name="date-hour" value="{{ old('date-hour') }}">
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-activity">ACTIVIDAD</label>
		<select name="note-activity" class="form-control">
		@foreach($activities as $activity)
			<option value="{{ $activity->id }}">{{ $activity->nombre }}</option>
		@endforeach
		</select>
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-title">TÍTULO</label>
		<input type="text" class="form-control" value="{{ old('note-title') }}" name="note-title">
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-8 col-sm-8">
		<label for="note-details">DETALLES</label>
		<textarea name="note-details"  rows="6" class="form-control">{{ old('note-details') }}</textarea>
	</div>
	<div class="col-md-4 col-sm-4">
		<label for="note-status">Estatus</label>
		<select name="note-status" class="form-control">
		@foreach($statuses as $status)
			<option value="{{$status->id}}">{{($status->nombre == null )? "NULO" : $status->nombre}}</option>
		@endforeach
		</select>
	</div>
</div>