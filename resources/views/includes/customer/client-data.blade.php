<form  method="post" action="{{ route('storeCustomer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<div class="row with-border">
			<div class="col-4">
				<label class="text-left" for="executive">Ejecutivo </label>
				@if(Auth::user()->hasAccessApp("executive_select"))
				<select class="form-control select2" name="executive" id="executive">
					@foreach($executives as $executive)
						<option @if(old('executive') == $executive->id) selected @endif value="{{ $executive->id }}">{{ $executive->nombre }}</option>
					@endforeach
				</select>
				@else
				<br>
				<label for="">{{ Auth::user()->executive->nombre }}</label>
				<input  name="executive" id="executive" type="hidden" value="{{ Auth::user()->executive->id }}" >
				@endif
			</div>
			<div class="col-4">
				<label for="origen">ORIGEN</label>
				<select class="form-control select2" name="origen" id="origen">
					@foreach($origens as $index => $origen)
						<option data-route="{{ route("subOrigenLoad", $origen->id) }}"  value="{{ $origen->id }}">{{ $origen->nombre }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-4">
				<label for="origen">SUB ORIGEN</label>
				<select class="form-control select2" name="create-sub-origen" id="sub_origen_create">
					<option value="0">No definido</option>
				</select>
			</div>

			<div class="col-4">
				<label for="seller">VENDEDOR</label>
				<input class="form-control" name="seller" id="seller" type="text">
			</div>
			<div class="col-md-4">
				<label for="interested">Fecha de Creación</label>
				<p>Sin crear</p>
				
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-5 col-sm-5">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<select name="prefix-name" id="prefixName" class="form-control">
							<option @if(old('prefix-name') == "Sr.") selected @endif value="Sr.">Sr.</option>
							<option  @if(old('prefix-name') == "Sra.") selected @endif value="Sra.">Sra.</option>
						</select>
					</div>
					<div class="col-md-9 col-sm-9">
						<input placeholder="NOMBRE" class="form-control @if($errors->has('customer-name')) danger-input @endif" name="customer-name" id="customer_name" value="{{ old('customer-name') }}"  type="text">
						<div class="text-red @if($errors->has('customer-name')) visible @endif"><strong>Campo Requerido</strong></div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input placeholder="EDAD" name="age" class="form-control @if($errors->has('age'))danger-input @endif" id="age" value="{{ old('age') }}" type="number">
				<div class="text-red @if($errors->has('age')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-5 col-sm-5">
				<input placeholder="OCUPACIÓN" type="text" value="{{ old('customer-job') }}" name="customer-job" id="customer_job" class="form-control @if($errors->has('customer-job')) danger-input @endif">
				<div class="text-red @if($errors->has('customer-job')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-5 col-sm-5">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<select name="prefix-name2" id="prefixName2" class="form-control">
							<option @if(old('prefix-name2') == "Sr.") selected @endif value="Sr.">Sr.</option>
							<option  @if(old('prefix-name2') == "Sra.") selected @endif value="Sra.">Sra.</option>
						</select>
					</div>
					<div class="col-md-9 col-sm-9">
						<input placeholder="NOMBRE" class="form-control @if($errors->has('customer-name-co')) danger-input @endif" name="customer-name-co" id="customer_name_co" value col-sm-3="{{ old('customer-name-co') }}"  type="text">
						<div class="text-red @if($errors->has('customer-name-co')) visible @endif"><strong>Campo Requerido</strong></div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input placeholder="EDAD" name="age-co" class="form-control @if($errors->has('age-co'))danger-input @endif" id="age_co" value="{{ old('age-co') }}" type="text">
				<div class="text-red @if($errors->has('age-co')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-5 col-sm-5">
				<input placeholder="OCUPACIÓN" type="text" value="{{ old('customer-job-co') }}" name="customer-job-co" id="customer_job_co" class="form-control @if($errors->has('customer-job-co')) danger-input @endif">
				<div class="text-red @if($errors->has('customer-job-co')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-3 col-sm-3">
				<input placeholder="TELÉFONO CELULAR" name="cell-phone" id="cellPhone" value="{{ old('cell-phone') }}" class="form-control @if($errors->has('cell-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('cell-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-3 col-sm-3">
				<input name="home-phone" placeholder="TELÉFONO DE CASA" id="homePhone" value="{{ old('home-phone') }}" class="form-control @if($errors->has('home-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('home-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
				<input placeholder="TELÉFONO DE OFICINA" name="office-phone" id="officePhone" value="{{ old('office-phone') }}" class="form-control @if($errors->has('office-phone')) danger @endif" type="text">
				<div class="text-red @if($errors->has('office-phone')) visible @endif"><strong>Campo requerido</strong></div>
			</div>
			<div class="col-md-2 col-sm-2">
				<input class="form-control @if($errors->has('extension')) danger @endif" type="text" placeholder="EXT" name="extension" value="{{ old('extension') }}" id="extension">
				<div class="text-red @if($errors->has('extension')) visible @endif"></div>
			</div>
		</div>
		<hr>
		<div class="row with-border">
			<div class="col-md-7 col-sm-7">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<label for="homeowner">¿Propietario vivienda?</label>
						<div class="form-group">
								<div class="custom-control custom-radio custom-control-inline">
							    <input  @if(old('homeowner') != "Y") checked @endif value="Y" type="radio" class="custom-control-input" id="homeownerY" name="homeowner">
							    <label class="custom-control-label" for="homeownerY">SI</label>
						    </div>
							<div class="custom-control custom-radio custom-control-inline">
							    <input  @if(old('homeowner') != "Y") checked @endif value="N" type="radio" class="custom-control-input" id="homeownerN" name="homeowner">
							    <label class="custom-control-label" for="homeownerN">NO</label>
						    </div>
						</div>
					</div>
					<div class="vol-12">
						<div class="form-group">
							<label for="customerEmail">Email</label>
							<input class="form-control @if($errors->has('customer-email')) danger @endif" type="text" name="customer-email" value="{{ old('customer-email') }}" id="customerEmail" placeholder="E-Mail">
							<div class="text-red @if($errors->has('customer-email')) danger @endif"> <ul>@foreach($errors->get('customer-email') as $error) <li>$error</li> @endforeach</ul></div>
							<div class="text-red"></div>

						</div>
						<div class="form-group">
							<label for="customerState">Estado</label>
							<input type="text" placeholder="ESTADO" class="form-control @if($errors->has('customer-state')) danger @endif" name="customer-state" value="{{ old('customer-state') }}" id="customerState">
						</div>
					</div>	
					<div class="col-12">
						<div class="row">
							<div class="col-6">
								<label for="create_country">Pais</label>
								<select  class="form-control select2" name="create-country" id="create_country">
									@foreach($countries as $country)
									<option @if($country->paisnombre == "México") selected @endif data-route="{{ route('getStatesFromCountry', $country->id) }}"  value="{{ $country->id }}">{{ $country->paisnombre }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-6">
								<label for="create_state">Estado</label>
								<select class="form-control select2" name="create-state" id="create_state">
									@foreach($states as $state)
									<option value="{{ $state->id }}">{{ $state->estadonombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>				
				</div>
			</div>
			<div class="col-md-5 col-sm-5">
				<div class="form-group">
					<label for="address">Dirección</label>
					<textarea placeholder="Dirección" name="address" id="address" rows="8" class="form-control @if($errors->has("address")) danger @endif">{{ old('address') }}</textarea>
						<div class="text-red @if($errors->has('address')) danger @endif"> <ul>@foreach($errors->get('address') as $error) <li>$error</li> @endforeach</ul></div>
				</div>

			</div>
		</div>
		@include('partials.button-submit')
	</div>
</form>
