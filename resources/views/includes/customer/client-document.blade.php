	<div class="row">
		<div class="col-md-3 div-col-sm-3">

			<form method="post" action="{{ route('storeCustomer') }}" >
                <label for="has-signed">¿TIENE FIRMADOS?</label>

				<div class="form-group">                    
                    <div class="custom-control custom-radio custom-control-inline">
                        <input  type="radio" class="custom-control-input" id="has_signedS" name="has-signed"  value="S">
                        <label class="custom-control-label" for="has_signedS">SI</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="has_signedN" name="has-signed"  value="N">
                        <label class="custom-control-label" for="has_signedN" >NO</label>
                    </div>
				</div>
			</form>
		</div>
		<div class="col-md-9 div-col-sm-9">
		<label for="dropzone">
			Documentos
		</label><br>
		<form data-max-files="{{ config('crm.customer.max_files_upload') }}" action="{{ route('storeCustomerFile') }}" class="dropzone" id="dropZoneDocuments" enctype="multipart/form-data"> 
      		<div id="dd" class="fallback">
			    <input name="file" type="file"  multiple />
			</div>
      	</form>
      	<form method="post" action="{{ route('storeCustomer') }}" >
      		@include("partials.button-submit")
      	</form>
      		
		</div>
	</div>


	<script>
		var dropzone = initializeDropZone() ;
		//se inicializa el dropzone
        function initializeDropZone() {
            Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastre o de clic aquí para explorar";
            Dropzone.prototype.defaultOptions.dictFallbackMessage = "Tu navegador no soporta esta funcionalidad";
            Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
            Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big (10MiB). Max filesize: 10MiB.";
            Dropzone.prototype.defaultOptions.dictInvalidFileType = "El formato del archivo no es permitido";
            Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar carga";
            Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Seguro que quieres cancelar esta carga?";
            Dropzone.prototype.defaultOptions.dictRemoveFile = "Remover archivo";
            Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "No puedes subir mas archivos.";
            Dropzone.autoDiscover = false;
            var dropZone = $('form#dropZoneDocuments');
            var max_files = $(dropZone).data("maxFiles") ;

            var myDropzone = new Dropzone('form#dropZoneDocuments', {
            paramName: "file",
            acceptedFiles: "image/*, text/plain, .pdf, .docx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
            maxFilesize: 10,
            autoProcessQueue: false,
            maxFiles: max_files,
            parallelUploads: max_files,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            url: $(dropZone).attr("action"),
            addRemoveLinks: true,
            //previewTemplate: $('#preview-template').html(),
            dictRemoveFile: "Eliminar",
            init: function() {
            	var count = 0;
                var maxImageWidth = 120,
                maxImageHeight = 120;
                var myDropzoneInput = this;
                this.on('addedfile', function(file) {
                	if(this.files.length > max_files) {
                    this.removeFile(file);
                        new PNotify({
                              title: 'Error',
                              text: "No puedes subir mas archivos.",
                              type: "alert"
                        });
                	}else{
	                    $('.dz-message').hide();
	                    switch(file.type) {
	                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/word.png") }}");
	                            break;
	                        case "application/pdf":
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/pdf.png") }}");
	                            break;
	                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/excel.png") }}");
	                            break;
	                        case "application/vnd.ms-excel":
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/excel.png") }}");
	                            break;
	                        case "text/plain":
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/txt.png") }}");
	                            break;
	                        default:
	                            myDropzone.emit("thumbnail", file, "{{ asset("images/thumbnails/file.png") }}");
	                    }
                	}
                });
                this.on('maxfilesexceeded', function(file) {
                    this.removeFile(file);
                });                
                this.on('error', function(file, message) {

                        new PNotify({
                              title: 'Error',
                              text: message,
                              type: "alert"
                        });
                    this.removeFile(file);
                    
                });
                this.on('removedfile', function(file) {
                    var actualQueue = this.files.length;
                    if(actualQueue < 1) {
                        $('.dz-message').show();
                    }
                    //se agrega un array de documentos que se eliminarán
                    if(file.id != null){
                    	$("form").first().append('<input type="hidden" value="'+file.id+'" name="drop-files[]" >');
                    }
                });
                this.on("success", function(file, response) {
                var a = document.createElement('a');
                    path = JSON.parse(response);
                    a.setAttribute('href',"{{ asset("storage/documents") }}/" + path[1]);
                    a.setAttribute('class',"dz-download");
                    a.innerHTML = 'Descargar<i class="fa fa-download" aria-hidden="true"></i>';
                    file.previewTemplate.appendChild(a);
                    $(file.previewTemplate).find("a.dz-remove").remove();
                });
            }
        });

        return myDropzone;
    }

    function sendDropZone(dropzone) {
	        dropzone.on("sending", function(file, xhr, formData) {
	            formData.append('_token', '{{ csrf_token() }}');
	            formData.append('name', file.name);
	        });
	        dropzone.processQueue();

    }
	</script>