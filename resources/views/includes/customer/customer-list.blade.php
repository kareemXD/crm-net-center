@foreach($customers as $customer)
	<tr>
		<td><button data-toggle="modal" data-target="#modalEdit" data-route="{{ route('editCustomer', $customer->id) }}" data-client-id="{{$customer->id}}" class="btn btn-default" type="button"><i class="fa fa-edit" aria-hidden="true"></i></button></td>
		<td>{{  substr($customer->checkin, 0, 10)}}</td>
		<td>{{ $customer->nombre }}</td>
		<td>{{ $customer->tipo_q }} </td>
		<td>{{ $customer->folioreserva }}</td>
		<td>{{ ($customer->plan == "T")? "Todo Incluido": ($customer->plan == "E")? "EUROPEO": "" }}</td>
		<td>{{ (!is_null($customer->executive))? $customer->executive->nombre : ""  }}</td>
		<td>{{ (!is_null($customer->category))? $customer->category->name : ""  }}</td>
		@if(Auth::user()->hasAccessApp("delete"))
		<td><button data-toggle="modal" data-target="#modalDelete" data-client-id="{{$customer->id}}" class="btn btn-danger" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
		@endif
	</tr>
@endforeach 