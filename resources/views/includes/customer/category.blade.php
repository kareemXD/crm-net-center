<form method="post" action="{{ route('storeCustomer') }}" accept-charset="UTF-8" enctype="multipart/form-data">
	<div class="form-group">
		<hr>
		<div class="row">
			@foreach($categories as $category)
			<div class="col-md-2 col-sm-2 custom-radio-kareem">
				<input class="hiden" id="category-{{$category->code}}" type="radio"  name ="category-client" value="{{ $category->code }}" >	
				<div style="background:{{$category->color}};" class="col-md-12 col-sm-12 border-check" >
					<label for="category-client">{{ $category->name }}</label>
				</div>	
			</div>
			@endforeach 
		</div>
		<hr>
		<div class="row">
			<div class="col-12">
				<label for="has-note">¿Agregar Recordatorio?</label>
				<div class="form-group">
					<div class="custom-control custom-radio custom-control-inline">
						<input id="has_note_s" data-route="{{ route('getNoteClient') }}" type="radio" class="custom-control-input"  name ="has-note" value="S" >
						<label for="has_note_s" class="custom-control-label">SI</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input id="has_note_n" checked type="radio" class="custom-control-input"  name ="has-note" value="N" >
						<label class="custom-control-label" for="has_note_n">NO</label>
					</div>
				</div>
			</div>
		</div>
		<div id="notas"></div>
		@include('partials.button-submit')
	</div>
</form>

