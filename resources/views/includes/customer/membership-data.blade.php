<form action="{{ route('storeCustomer') }}" method="post"  accept-charset="UTF-8" enctype="multipart/form-data" >
	<div class="form-group">
		<hr>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<label for="contract-number">NÚMERO DE CONTRATO</label>
				<input type="text" name="contract-number" id="contractNumber" value="{{ old('contract-number') }}" class="form-control @if($errors->has('contract-number')) danger @endif" >
				<div class="text-red @if($errors->has('contract-number')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<label for="membership-total">TOTAL MEMBRESÍA</label>
				<input type="text" name="membership-total" value="{{ (old('membership-total') =="")? 0 : old('membership-total') }}" id="membershipTotal" class="form-control price-format @if($errors->has('membership-total')) danger @endif">
				<div class="text-red @if($errors->has('membership-total')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<label for="requires-invoice">¿SOLICITA FACTURA?</label>
				<div class="form-group">
					<div class="custom-control custom-radio">
						<input class="custom-control-input" id="requires_invoice_s" type="radio" name="requires-invoice" checked="true" value="S">
						<label for="requires_invoice_s" class="custom-control-label">SI</label>
					</div>
				</div>

				<div class="form-group">
					<div class="custom-control custom-radio">
						<input type="radio" name="requires-invoice" id="requires_invoice_n" value="N" class="custom-control-input">
						<label class="custom-control-label" for="requires_invoice_n">NO</label>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<label for="room-type">TIPO HABITACIÓN</label>
				<input name="room-type" type="text" placeholder="HABITACIÓN(1)" class="form-control @if($errors->has('room-type')) danger @endif" name="room-type" id="roomType" value="{{ old('room-type') }}">
				<div class="text-red @if($errors->has('room-type')) visible @endif">Campo Requerido</div>
			</div>
			<div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
				<label for="sale-date">FECHA DE VENTA <i class="fa fa-calendar" aria-hidden="true"></i></label>
				<input type="text" name="sale-date" id="saleDate" value="{{ old('sale-date') }}" class="form-control date-picker @if($errors->has('sale-date')) danger @endif">
				<div class="text-red @if($errors->has('sale-date')) visible @endif">Campo requerido</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<label for="comments-tracing">COMENTARIOS/SEGUIMIENTOS</label>
				<textarea name="comments-tracing" id="commentsTracing"  rows="6" class="form-control @if($errors->has('comments-tracing')) danger @endif">{{ old('comments-tracing') }}</textarea>
				<div class="text-red @if($errors->has('comments-tracing')) visible @endif">Campo requerido</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<label for="membership-observations">OBSERVACIONES</label>
				<textarea class="form-control @if($errors->has('membership-observations')) danger @endif" name="membership-observations" id="membershipObservations" rows="6">{{ old('membership-observations') }}</textarea>
				<div class="text-red @if($errors->has('membership-observations')) visible @endif">Campo requerido</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<label for="hasVacationOwnership">¿Tiene propiedad vacacional?</label>
				<br>
				<div class="custom-control custom-radio custom-control-inline">
					<input name="has-vacation-ownership" type="radio" checked  class="custom-control-input" value="Y" id="hasVacationOwnershipY">
					<label for="hasVacationOwnershipY" class="custom-control-label">SI</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input @if(old('has-vacation-ownership') != "Y") checked @endif name="has-vacation-ownership" type="radio"  class="custom-control-input" value="N" id="hasVacationOwnershipN">
					<label for="hasVacationOwnershipN" class="custom-control-label">NO</label>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="takePresentation">¿A Tomado Presentación?</label>
				<br>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" name="take-presentation" value="Y" id="takenPresentationY" checked >
					<label for="takenPresentationY" class="custom-control-label">SI</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" @if(old('take-presentation') != "Y") checked @endif name="take-presentation" value="N" id="takenPresentationN">
					<label for="takenPresentationN" class="custom-control-label">NO</label>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="datePresentation"> Fecha de la presentación</label>
				<input data-bs-datepicker="yyy-mm-dd" type="text" class="form-control date-picker @if($errors->has('date-presentation')) danger @endif" placeholder="Fecha presentacion" value="{{ old('date-presentation') }}" name="date-presentation" id="datePresentation">
			</div>
			<div class="col-md-3 col-sm-3">
				<label for="manyTimes">¿Cuantas veces vacaciona al año?</label>
				<input type="number" class="form-control @if($errors->has('many-times')) danger @endif" id="manyTimes" name="many-times" value="{{ old('many-times') }}">
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-3 col-sm-3 no-padding">
				<div class="col-md-6 col-sm-6">
					<label for="qs-type">Tipo Q's</label>
				</div>
				<div class="col-md-6 col-sm-6">
					<select name="qs-type" id="qsType">
						<option value="QA">QA</option>
						<option value="QB">QB</option>
						<option value="NQ">NQ</option>
					</select>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 no-padding" >
				<div class="col-md-6 col-sm-6">
					<label for="">FORMATO PAQ.VAC.NQ</label>
				</div>
				<div class="col-md-6 col-sm-6">
					<a href="#"><img src="#" alt=""></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 no-padding" >
				<div class="col-md-6 col-sm-6">
					<label for="">FORMATO PAQ.VAC.QA</label>
				</div>
				<div class="col-md-6 col-sm-6">
					<a href="#"><img src="#" alt=""></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 no-padding" >
				<div class="col-md-6 col-sm-6">
					<label for="">FORMATO PAQ.VAC.QB</label>
				</div>
				<div class="col-md-6 col-sm-6">
					<a href="#"><img src="#" alt=""></a>
				</div>
			</div>
		</div>
		<div class="bg-primary">
			<a id="confirmation_section" href="#confirmations_div" class="text-white" data-toggle="collapse" data-target="#confirmations_div" ><i class="fa fa-minus-circle" aria-hidden="true"></i> CONFIRMACIONES</a>
		</div>
		<div id="confirmations_div" class="row collapse in">
			<div class="col-md-3 col-sm-3">
				<img src="{{ asset('images/pdf-icon.png') }}" class="img-responsive" width="50px" alt="">
			</div>
			<div class="col-md-3 col-sm-3">
				<img src="{{ asset('images/pdf-icon.png') }}" class="img-responsive" width="50px" alt="">
			</div>
			<div class="col-md-3 col-sm-3">
				<img src="{{ asset('images/pdf-icon.png') }}" class="img-responsive" width="50px" alt="">
			</div>
			<div class="col-md-3 col-sm-3">
				<img src="{{ asset('images/pdf-icon.png') }}" class="img-responsive" width="50px" alt="">
			</div>
		</div>
		@include('partials.button-submit')
	</div>
</form>