@if(Auth::user()->hasAccessApp('survey_delete'))
	<a href="#" data-delete-route="{{ route("deleteSurvey", $survey->id) }}" class="btn btn-danger" data-target="#modalDelete" data-toggle="modal"><i class="fa fa-trash"></i></a>
@endif
<a class="btn btn-info" href="{{ route('editSurvey', $survey->id) }}"><i class="fa fa-edit"></i></a>