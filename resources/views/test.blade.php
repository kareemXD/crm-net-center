@extends('layout')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-body table-responsive">
				<div class="dataTables_wrapper form-inline dt-bootstrap" >
					<div class="row">
						<div class="col-sm-12">
												<table class="table table-bordered table-striped display dataTable">
						<thead>
							<tr role="row">
								<td class="sorting">Customer</td>
								<td class="sorting">arrival date</td>
								<td class="sorting">departure date</td>
							</tr>
						</thead>
						<tbody>
							@foreach($arrivals as $arrival)
								<tr>
									<td>{{ $arrival->customer_name }}</td>
									<td>{{ $arrival->arrival_date }}</td>
									<td>{{ $arrival->departure_date }}</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td class="sorting">Customer</td>
								<td class="sorting">arrival date</td>
								<td class="sorting">departure date</td>
							</tr>
						</tfoot>
					
					</table>
						</div>

					</div>

				</div>

			</div>
		</div>
	</div>
</div>

@endsection
@section('after_scripts')

	<script>

		$(".table").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "api/arrivals",
			"columns": [
				{data: 'customer_name'},
				{data: 'arrival_date'},
				{data: 'departure_date'}
			],
			"pageLength": 10,
			"order": [[ 1, "asc" ]],
			"language":{
			    "decimal":        "",
			    "emptyTable":     "No hay datos disponibles en esta tabla",
			    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
			    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
			    "infoFiltered":   "(filtrado de _MAX_ total registros)",
			    "infoPostFix":    "",
			    "thousands":      ",",
			    "lengthMenu":     "Mostrar _MENU_ registros",
			    "loadingRecords": "Cargando...",
			    "processing":     "Procesando...",
			    "search":         "Buscar:",
			    "zeroRecords":    "No encontrados en la tabla",
			    "paginate": {
			        "first":      "Primer",
			        "last":       "Ultimo",
			        "next":       "Siguiente",
			        "previous":   "Anterior"
			    },
			    "aria": {
			        "sortAscending":  ": Activar para ordenar la columna ascendente",
			        "sortDescending": ": Activar para ordenar la columna descendente"
			    }
			}
		});
	</script>
@endsection
