@extends('layout-2')
@section('title', 'Lista de Surveys')
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>

	<div class="row widget-container" style="padding:15px">
		<table id="surveyListTable" class="table table-bordered table-striped">
			<thead >
				<tr>
					<th>	</th>
					<th>NOMBRE</th>
					<th>CORREO</th>
					<th>TELEFONO CELULAR</th>
					<th>TELEFONO CASA</th>
					<th>COMENTARIO</th>
					<th>FECHA ALTA</th>
					<th>OPERACION</th>
				</tr>
			</thead>
			<tbody>
				<tr></tr>
			</tbody>
		</table>
		
	</div>

	{{--moodal delete--}}
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Eliminar Survey</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        ¿Seguro que quiere eliminar el survey?
	      </div>
	      <div class="modal-footer">
	      	<form id="formDeleteSurvey" action="" method="post">
	      		{{ csrf_field() }}
	      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	        	<button  type="submit" class="btn btn-danger">Confirmar</button>
	      	</form>

	      </div>
	    </div>
	  </div>
	</div>
	{{--moodal delete--}}
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){
	$("#modalDelete").on("show.bs.modal", function(event){
		$button = $(event.relatedTarget);
		$(this).find("form").attr("action", $button.data("deleteRoute"));
	});
	$table = inizializeDataTable();
	$(".select2").select2();
	$("#page-loader").fadeOut(500);

	$("#formDeleteSurvey").on("submit", function(event){
		event.preventDefault();
		$this = $(this);
		$.ajax({
			url: $this.attr("action"),
			type: $this.attr("method"),
			data: $this.serializeArray(),
			success: function(response){
		        new PNotify({
		            title: "Correcto",
		            text: "Registro Eliminado",
		            type: "success"
		        });
		        $table.ajax.reload(null, false);
			},
			error:function(err){
				new PNotify({
		            title: "Correcto",
		            text: $("#message_flash").data("messageSuccess"),
		            type: "success"
		        });
			},
		});
		$("#modalDelete").modal("hide");


	});

    if($("#message_flash").data("messageError") != "" &&  $("#message_flash").data("messageError") != undefined){
        new PNotify({
            title: "Ups!",
            text: $("#message_flash").data("messageError"),
            type: "error"
        })
    }
    if($("#message_flash").data("messageSuccess") != "" &&  $("#message_flash").data("messageSuccess") != undefined){
        new PNotify({
            title: "Correcto",
            text: $("#message_flash").data("messageSuccess"),
            type: "success"
        })
    }


	function inizializeDataTable() {
        $table = $("#surveyListTable").DataTable({
                "processing": true,
                "serverSide": true,
                ajax: '{{ route("api.surveyList") }}',
                "columns": [
                	{data: 'assign', name: 'assign', orderable: false, searchable: false},
                	{data: "survey_datos_cliente.nombre", name: 'nombre'},
                	{data: "survey_datos_cliente.email_decrypt", name: 'email_decrypt'},
                	{data: "survey_datos_cliente.telefono_celular_decrypt", name: 'telefono_celular_decrypt'},
                	{data: "survey_datos_cliente.telefono_casa_decrypt", name: 'telefono_casa_decrypt'},
                	{data: "comentarios"},
                	{data: "created_at"},
                	{data: 'survey_operation', name: 'survey_operation', orderable: false, searchable: false},
                ],
                "pageLength": 25,
                "language":{
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en esta tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No encontrados en la tabla",
                    "paginate": {
                        "first":      "Primer",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna ascendente",
                        "sortDescending": ": Activar para ordenar la columna descendente"
                    }
                }
            });
	        return $table;
	    }
	}); 
</script>
@endsection