@extends('layout-2')
@section("title", "Crear Survey")
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>
<div class="widget-container">
    <div class="container">
        <h3 class="box-title m5">Registrar Survey</h3>
        <form id="formCreateSurvey" action="{{ route('createSurvey') }}" method="post" class="needs-validation" novalidate enctype="multipart/form-data" >
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-6">
                    <label class="control-label">Con  que comprueba que es QA?</label>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('comprobante-qa') == "Acta") checked @endif type="radio" class="custom-control-input" id="comprobante_qa_acta" name="comprobante-qa" value="Acta"></input>
                            <label class="custom-control-label" for="comprobante_qa_acta">Acta</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('comprobante-qa') == "Anillo") checked @endif type="radio" class="custom-control-input" id="comprobante_qa_anillo" name="comprobante-qa" value="Anillo"></input>
                            <label class="custom-control-label" for="comprobante_qa_anillo">Anillo</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('comprobante-qa') == "Ifes") checked @endif type="radio" class="custom-control-input" id="comprobante_qa_ifes" name="comprobante-qa" value="Ifes"></input>
                            <label class="custom-control-label" for="comprobante_qa_ifes">Ifes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('comprobante-qa') == "Hijos") checked @endif type="radio" class="custom-control-input" id="comprobante_qa_hijos" name="comprobante-qa" value="Hijos"></input>
                            <label class="custom-control-label" for="comprobante_qa_hijos">Hijos</label>
                        </div>
                    </div>
                    <label class="control-label">Comentarios para validación</label>

                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('comentario-validacion') == "QA") checked @endif value="QA" type="radio" name="comentario-validacion" id="comentario_validacion_qa" class="custom-control-input">
                            <label class="custom-control-label" for="comentario_validacion_qa">QA</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input  @if(old('comentario-validacion') == "QB") checked @endif value="QB" type="radio" name="comentario-validacion" id="comentario_validacion_qb" class="custom-control-input">
                            <label class="custom-control-label" for="comentario_validacion_qb">QB</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input  @if(old('comentario-validacion') == "NQ") checked @endif  value="NQ" type="radio" name="comentario-validacion" id="comentario_validacion_nq" class="custom-control-input">
                            <label class="custom-control-label" for="comentario_validacion_nq">NQ</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{--Primer contacto--}}
                        <label class="control-label" for="fecha_primer_contacto" >Primer Contacto</label>
                        <input value="{{ old('fecha-primer-contacto') }}" id="fecha_primer_contacto" type="text" name="fecha-primer-contacto" class="form-control date-time-picker">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="estado_civil">Estado Civil</label>
                        <select class="form-control" id="estado_civil" name="estado-civil">
                            <option value=""></option>
                            @foreach($estadosCiviles as $index => $estadoCivil)
                                <option @if(old('estado-civil') == $estadoCivil->id) selected @endif   value="{{ $estadoCivil->id }}">{{ $estadoCivil->nombre }}</option>
                            @endforeach
                        </select>
                    </div> 
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{--eejcutivo--}}
                        <label class="control-label" id="executive_id">Ejecutivo</label>
                        @if(Auth::user()->hasAccessApp('user_view'))
                        <select name="executive-id" id="executive_id" class="form-control select2">
                            @foreach($executives as $index => $executive)
                            <option @if($executive->id == old('executive-id')) selected @endif value="{{ $executive->id }}" >{{ $executive->nombre }}</option>
                            @endforeach
                        </select>
                        @else
                            <br>
                            <input type="hidden" name="executive-id" value="{{ Auth::user()->executive->id }}">
                            <label class="control-label">{{ Auth::user()->executive->nombre }}</label><br>
                            <hr>
                        @endif
                        {{-- origen  --}}
                        <label class="control-label" for="origen_id">Origen</label>
                        <select name="origen-id" id="origen_id" class="form-control select2">
                            @foreach($origenes as  $index => $origen)
                            <option value="{{ $origen->id }}" @if(old('origen-id') == $origen->id) selected @endif>{{ $origen->nombre }}</option>
                            @endforeach
                        </select>
                        <label class="control-label" for="vendedor">Vendedor</label>
                        <input type="text" name="vendedor" id="vendedor" class="form-control" value="{{ old('vendedor') }}">
                        <label class="control-label" for="folio">Folio</label>
                        <input type="text" value="{{ old('folio') }}" class="form-control" name="folio" id="folio">
                    </div>
                </div>
            </div>
            {{--Fin primer bloque--}}
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        {{--nombre--}}
                        <label class="control-label" for="nombre" >Nombre:</label>
                        <input  class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" required >
                        <div class="invalid-feedback">
                            Favor de ingresar el nombre del cliente.
                        </div>
                    </div>
                    <div class="form-group">
                        {{--edad--}}
                        <label for="edad" class="control-label">Edad (años)</label>
                        <input type="number" name="edad" id="edad" min="18" max="105"  class="form-control" value="{{ old('edad') }}">
                        <div class="invalid-feedback">
                            Ingrese una edad válida(mayor de edad).
                        </div>
                    </div>
                    <div class="form-group">
                        {{--ocupacion--}}
                        <label for="ocupacion" class="control-label">Ocupacion</label>
                        <input type="text" name="ocupacion" id="ocupacion" class="form-control" value="{{ old('ocupacion') }}">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{--nombre--}}
                        <label class="control-label" for="nombre_ac" >Nombre acompañante:</label>
                        <input  class="form-control" type="text" name="nombre-ac" id="nombre_ac" value="{{ old('nombre-ac') }}" >
                    </div>
                    <div class="form-group">
                        {{--edad--}}
                        <label for="edad_ac" class="control-label">Edad acompañante (años) </label>
                        <input type="number" name="edad-ac" id="edad_ac" min="18" max="105"  class="form-control" value="{{ old('edad-ac') }}">
                        <div class="invalid-feedback">
                            Ingrese una edad válida(mayor de edad).
                        </div>
                    </div>
                    <div class="form-group">
                        {{--ocupacion--}}
                        <label for="ocupacion_ac" class="control-label">Ocupacion acompañante</label>
                        <input type="text" name="ocupacion-ac" id="ocupacion_ac" class="form-control" value="{{ old('ocupacion-ac') }}">
                    </div>                    
                </div>
                <div class="col-12">
                    <div class="form-row">
                        <div class="col-3">
                            <label class="control-label" for="telefono_celular">Teléfono celular</label>
                            <input class="form-control mr-2" type="text" name="telefono-celular" id="telefono_celular" value="{{ old('telefono-celular') }}" placeholder="Teléfono Celular">
                        </div>
                        <div class="col-3">
                            <label class="control-label" for="telefono_casa">Teléfono Casa</label>
                            <input class="form-control" type="text" name="telefono-casa" id="telefono_casa" value="{{ old('telefono-casa') }}" placeholder="Teléfono Casa">
                        </div>
                        <div class="col-3">
                            <label class="control-label" for="email">Email</label>
                            <input class="form-control" type="email" id="email" name="email" placeholder="Correo Electrónico" value="{{ old('email') }}">
                            <div class="invalid-feedback">
                                El correo es un campo obligatorio.
                            </div>
                        </div>
                        <div class="col-3">
                            <label for="estado">Estado</label>
                            <input type="text" name="estado" class="form-control" value="{{ old('estado') }}" id="estado">
                        </div>
                    </div>
                </div>
            </div>
            {{--datos membrecia--}}
            <div class="form-row">
                <div class="col-3">
                    {{--a visitado el hotel--}}
                    <label for="a_visitado" class="control-label">¿Ha visitado el hotel anteriormente? </label>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('a-visitado') == "S") checked @endif type="radio" name="a-visitado" class="custom-control-input" value="S"  id="a_visitado_s">
                            <label class="custom-control-label" for="a_visitado_s">SI</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('a-visitado') == "N") checked @endif type="radio" name="a-visitado" class="custom-control-input" value="N"  id="a_visitado_n">
                            <label class="custom-control-label" for="a_visitado_n">NO</label>
                        </div>
                    </div>

                </div>
                <div class="col-3">
                    {{--a visitado el hotel--}}
                    <label for="tomado_presentacion" class="control-label">¿Ha tomado presentación de Buganvialias Resort? </label>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('tomado-presentacion') == "S") checked @endif type="radio" name="tomado-presentacion" class="custom-control-input" value="S"  id="tomado_presentacion_s">
                            <label class="custom-control-label" for="tomado_presentacion_s">SI</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input @if(old('tomado-presentacion') == "N") checked @endif type="radio" name="tomado-presentacion" class="custom-control-input" value="N"  id="tomado_presentacion_n">
                            <label class="custom-control-label" for="tomado_presentacion_n">NO</label>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <label class="control-label" for="hace_cuanto">¿Hace cuanto?</label>
                    <input type="text" name="hace-cuanto" value="{{ old('hace-cuanto') }}" class="form-control" id="hace_cuanto">
                </div>
                <div class="col-3">
                    <label class="control-label" for="se_hizo_socio">¿Se hizo socio?</label>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="se-hizo-socio" id="se_hizo_socio_s" value="S" @if(old('se-hizo-socio') == 'S') checked @endif class="custom-control-input">
                            <label class="custom-control-label" for="se_hizo_socio_s">SI</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="se-hizo-socio" id="se_hizo_socio_n" value="N" @if(old('se-hizo-socio') == 'N') checked @endif class="custom-control-input">
                            <label class="custom-control-label" for="se_hizo_socio_n">NO</label>
                        </div>
                    </div>

                </div>
            </div>
            {{--datos de tarjeta--}}
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="numero_aprobacion" class="control-label">Número de aprobación</label>
                        <input type="text" name="numero-aprobacion" id="numero_aprobacion" value="{{ old('numero-aprobacion') }}" class="form-control">
                    </div>
                </div>
                <div class="col-3">
                    <label for="requiere_factura" class="control-label">Requiere Factura</label>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="requiere-factura" id="requiere_factura_s" class="custom-control-input" @if(old('requiere-factura') == 'S') checked @endif value="S" >
                            <label for="requiere_factura_s" class="custom-control-label">SI</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="requiere-factura" id="requiere_factura_n" class="custom-control-input" @if(old('requiere-factura') == 'N') checked @endif value="N">
                            <label class="custom-control-label" for="requiere_factura_n">NO</label>
                        </div>
                    </div>  
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="ingreso_mensual">Ingreso Mensual</label>
                        <input type="text" name="ingreso-mensual" id="ingreso_mensual" class="form-control price-format" value="{{ old('ingreso-mensual') }}">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="numero_tarjetas">Número de tarjetas</label>
                        <input type="number" name="numero-tarjetas" id="numero_tarjetas" class="form-control" value="{{ old('numero-tarjetas') }}">
                    </div>
                </div> 
            </div>
            <div class="form-row">
                <div class="col-12">
                    <label class="control-label">Tarjeta de garantía</label>
                    <div class="form-row">
                        <div class="col-4">
                            <input type="text" name="numero-tarjeta-g" value="{{ old('numero-tarjeta-g') }}" id="numero_tarjeta_g" class="form-control cc-number" placeholder="número de tarjeta">
                        </div>
                        <div class="col-2">
                            <input type="text" name="vencimiento-g" id="vencimiento_g" value="{{ old('vencimiento-g') }}" class="form-control cc-exp" placeholder="mm / aaaa" >
                        </div>
                        <div class="col-2">
                            <input type="text" name="codigo-g" id="codigo_g" value="{{ old('codigo-g') }}" class="form-control cc-cvc" placeholder="CVC">
                        </div>
                        <div class="col-2">
                            <input type="text" name="banco-g" id="banco_g" value="{{ old('banco-g') }}" class="form-control" placeholder="BANCO">
                        </div>
                        <div class="col-2">
                            <select name="tipo-tarjeta-g" id="tipo_tarjeta_g" class="form-control" placeholder="Tipo Tarjeta">
                                <option value="">SELECCIONE TIPO DE TARJETA</option>
                                <option @if(old('tipo-tarjeta-g') == "Visa") selected @endif value="Visa">Visa</option>
                                <option @if(old('tipo-tarjeta-g') == "MC") selected @endif value="MC">MC</option>
                                <option @if(old('tipo-tarjeta-g') == "AMEX") selected @endif value="AMEX">AMEX</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <label class="control-label">Tarjeta de pago</label>
                    <div class="form-row">
                        <div class="col-4">
                            <input type="text" name="numero-tarjeta-p" id="numero_tarjeta_p" value="{{ old('numero-tarjeta-p') }}" class="form-control cc-number" placeholder="número de tarjeta">
                        </div>
                        <div class="col-2">
                            <input type="text" name="vencimiento-p" id="vencimiento_p" value="{{ old('vencimiento-p') }}" class="form-control cc-exp" placeholder="mm / aaaa" >
                        </div>
                        <div class="col-2">
                            <input type="text" name="codigo-p" id="codigo_p" value="{{ old('codigo-p') }}" class="form-control cc-cvc" placeholder="CVC">
                        </div>
                        <div class="col-2">
                            <input type="text" name="banco-p" id="banco_p" value="{{ old('banco-p') }}" class="form-control" placeholder="BANCO">
                        </div>
                        <div class="col-2">
                            <select name="tipo-tarjeta-p" id="tipo_tarjeta_p" class="form-control" placeholder="Tipo Tarjeta">
                                <option value="">SELECCIONE TIPO DE TARJETA</option>
                                <option @if(old('tipo-tarjeta-p') == "Visa") selected @endif value="Visa">Visa</option>
                                <option @if(old('tipo-tarjeta-p') == "MC") selected @endif value="MC">MC</option>
                                <option @if(old('tipo-tarjeta-p') == "AMEX") selected @endif value="AMEX">AMEX</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label class="control-label" for="checkin">Ckeck In</label>
                        <input type="text" name="checkin" id="checkin" class="form-control date-picker" placeholder="aaaa-mm-dd" value="{{ old('checkin') }}">
                    </div>
                </div>
                <div class="col-4">
                    <label class="control-label" for="checkout">Check Out</label>
                    <input type="text" name="checkout" id="checkout" class="form-control date-picker" placeholder="aaaa-mm-dd" id="checkout" value="{{ old('checkout') }}" >
                </div>
                <div class="col-4">
                    <label class="control-label" for="plan">Plan</label>
                    <select class="form-control" name="plan" id="plan">
                        <option @if(old('plan') == 'E') selected @endif value="E">EUROPEO</option>
                        <option @if(old('plan') == 'T') selected @endif value="T">TODO INCLUIDO</option>
                        <option @if(old('plan') == 'D') selected @endif value="D">DESAYUNOS</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="adultos">Adultos</label>
                        <input type="number" data-number class="form-control" name="adultos" id="adultos" value="{{ old('adultos') }}">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="menores">Menores</label>
                        <input type="number" data-number class="form-control" name="menores" id="menores" value="{{ old('menores') }}">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="edad_menores">Edad Menores</label>
                        <input type="text" placeholder="Separado por comas" name="edad-menores" id="edad_menores" class="form-control" value="{{ old('edad-menores') }}">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="tipo_habitacion">Tipo de habitacion</label>
                        <div class="form-group">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="tipo_habitacion_jr" name="tipo-habitacion" value="JR" @if(old('tipo-habitacion') == "JR") checked @endif>
                                <label class="custom-control-label" for="tipo_habitacion_jr">Jr</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="tipo_habitacion_ss" name="tipo-habitacion" value="SS" @if(old('tipo-habitacion') == "SS") checked @endif>
                                <label class="custom-control-label" for="tipo_habitacion_ss">SS</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="tipo_habitacion_deluxe" name="tipo-habitacion" value="DELUXE" @if(old('tipo-habitacion') == "DELUXE") checked @endif>
                                <label class="custom-control-label" for="tipo_habitacion_deluxe">Deluxe</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="tipo_habitacion_studio" name="tipo-habitacion" value="STUDIO" @if(old('tipo-habitacion') == "STUDIO") checked @endif>
                                <label class="custom-control-label" for="tipo_habitacion_studio">STUDIO</label>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="cuantas_habitaciones">Cuantas habitaciones son?</label>
                        <input type="number" name="cuantas-habitaciones" id="cuantas_habitaciones" class="form-control" data-number value="{{ old('cuantas-habitaciones') }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">¿Viaja Con Familiares misma fecha?</label>
                        <div class="form-group">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="viaja-con-familiares" id="viaja_con_familiares_s" class="custom-control-input" value="S" @if(old('viaja-con-familiares') == "S") checked @endif>
                                <label class="custom-control-label" for="viaja_con_familiares_s">SI</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="viaja-con-familiares" id="viaja_con_familiares_n" class="custom-control-input" value="N" @if(old('viaja-con-familiares') == "N") checked @endif>
                                <label class="custom-control-label" for="viaja_con_familiares_n">NO</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="precio_total">Precio total:</label>
                        <input type="text" name="precio-total" id="precio_total" class="form-control price-format" value="{{ old('precio-total') }}">
                    </div>
                    <div class="form-group">
                        <label for="pagado" class="control-label">Pagado:</label>
                        <input type="text" name="pagado" id="pagado" class="form-control price-format" value="{{ old('pagado') }}">
                    </div>
                    <div class="form-group">
                        <label for="saldo_pendiente" class="control-label">Saldo Pendiente</label>
                        <input type="text" name="saldo-pendiente" id="saldo_pendiente" class="form-control price-format" value="{{ old('saldo-pendiente') }}" >
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label" id="comentarios">Comentarios para Ceci:</label>
                        <textarea maxlength="255" class="form-control" rows="8" name="comentarios" id="comentarios">{{ old('comentarios') }}</textarea>
                        <div class="invalid-feedback">
                            No debe pasar los 5 caracteres.
                        </div>
                    </div>
                </div>  
            </div>
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label class="control-label" for="desgloce_paquete">Desglose de Paquete</label>
                        <textarea rows="5" class="form-control" id="desglose_paquete" name="desglose-paquete">{{ old('desglose-paquete') }}</textarea>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="desglose_hoteleria" class="control-label">Desglose Hotelería</label>
                        <textarea rows="5" class="form-control" id="desglose_hoteleria" name="desglose-hoteleria">{{ old('desglose-hoteleria') }}</textarea>
                    </div>
                </div>
                <div class="col-4">
                    <label class="control-label" for="comentarios_verificacion">Comentario Verificación</label>
                    <textarea rows="5" class="form-control" id="comentarios_verificacion" name="comentarios-verificacion">{{ old('comentarios-verificacion') }}</textarea>
                </div>
            </div>
           <button type="submit" data-loading="Guardando<i class='fa fa-cog fa-spin'></i>" class="btn btn-primary">Guardar <i class="fa fa-save"></i></button>
        </form>
    </div>
</div>
@endsection
@section('after_scripts')
	<script>
        /*validacion de bootstrap*/
        (function() {
          'use strict';
          window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
                var $button = $(form).find("button[type='submit']");
                var loadingText = $($button).data('loading');
                if ($button.html() !== loadingText) {
                    $button.data('original-text', $button.html());
                    $button.html(loadingText);
                    $button.prop("disabled", true);
                }
                if($('.was-validated :invalid').length > 0){
                    $button.html($button.data('original-text'));
                    $button.prop("disabled", false);
                }else{}
                setTimeout(function () {
                    $button.html($button.data('original-text'));
                    $button.prop("disabled", false);
                }, 3000);

              }, false);
            });
          }, false);
        })();
		$(function(){
            //inicialize functions
            inicializeCheckCategories();
            inicializePriceFormat();
            inicializeLibraries();
            inicializeInputs();

            $(document).on("keyup click", ".price-format", function (e){
                var total = ($("#precio_total").val()==""? 0 : parseFloat($("#precio_total").val().replace("$", "").replace(",", "")));
                var pagado = ($("#pagado").val()==""? 0 : parseFloat($("#pagado").val().replace("$", "").replace(",", "")));
                $("#saldo_pendiente").val("$"+ parseFloat(total-pagado).toFixed(2));  
                
            });
            $(document).on("keydown click", "#formCreateSurvey", function(event){

                $email = $(this).find("input[type='email']");
                $telefonoCelular = $(this).find("#telefono_celular");
                $telefonoCasa = $(this).find("#telefono_casa");

                if($email.val()=="" && $telefonoCelular.val() == "" && $telefonoCasa==""){
                    $email.attr("required", "required");
                    $telefonoCelular.attr("required", "required");
                    $telefonoCasa.attr("required", "required");
                }else{

                    if($telefonoCelular.val() != "" || $telefonoCasa.val() != "" || $email.val() != "") {
                        $email.removeAttr('required');
                        $telefonoCasa.removeAttr('required');
                        $telefonoCelular.removeAttr('required');
                    }else{
                        $telefonoCelular.attr("required", "required");
                        $telefonoCasa.attr("required", "required");
                        $email.attr("required", "required");
                    }

                }

            });


            if($("#message_flash").data("messageError") != "" &&  $("#message_flash").data("messageError") != undefined){
                new PNotify({
                    title: "Ups!",
                    text: $("#message_flash").data("messageError"),
                    type: "error"
                })
            }

		});
        //fin del jquery inicialize
        function inicializeCheckCategories() {
            var categoryCheck = $("input[name='category-client']");
            var checked = false;
            $.each(categoryCheck, function(i,v){
                if($(v).attr("checked") == "checked"){
                    checked = true;
                }
            });
            //ver si ninguna categoría esta seleccionar, seleccionar una por default
            if(checked === false){
                 $("input[name='category-client'][value='S']").first().attr("checked", "checked");
            }
        }

    function inicializePriceFormat() {
        $(".price-format").priceFormat({
            prefix: '$',
            thousandsSeparator: ',',
            allowNegative: false
        });
        $(".price-format").attr("maxlength","11");

    }
    function reiniPriceFormat(){
        $.each($(".price-format"), function(index, element){
            var value = parseFloat($(element).val());
            $(element).val('$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
    }

    function inicializeLibraries() {

        $(".select2").select2();
            setTimeout(function(){
                $('.text-red.visible').slideUp(8000, function(){
                    $('.text-red.visible').removeClass('visible');
                });

            }, 5000);

            $('.date-picker').datepicker({
                'format': 'yyyy-mm-dd',
            });
            $('.date-time-picker').datetimepicker({
                format:'YYYY-MM-DD hh:mm:00 a',
                locale: 'es'
            });

            jQuery(function($) {
                $('[data-numeric]').payment('restrictNumeric');
                $('.cc-number').payment('formatCardNumber');
                $('.cc-exp').payment('formatCardExpiry');
                $('.cc-cvc').payment('formatCardCVC');

                $.fn.toggleInputError = function(erred) {
                    this.parent('.form-group').toggleClass('has-error', erred);
                    return this;
                };
            });
    }

    function inicializeInputs() {
        $(document).on('keyup', 'input[name="customer-name"]', function(event){validateName(true);});
        $(document).on('keyup', 'input[name="cell-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="home-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="office-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="customer-email"]', function(event){validateEmail(true);});
    }

       
	</script>

@endsection
