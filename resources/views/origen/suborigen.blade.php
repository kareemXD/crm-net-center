@extends('layout-2')
@section('title', 'Lista de Origenes')
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>
<div class="row widget-container" style="padding:15px">
	<div class="row">
		<div class="col-auto">
			<button data-route="{{ route('subOrigenCreate', $origen_id) }}" data-toggle="modal" data-target="#modalCreate" class="btn btn-primary"><i class="fas fa fa-plus"></i></button>
		</div>
		<div class="col-auto">
			<select class="form-control" name="origenes" id="origenes_id">
				<option {{ route("subOrigenList") }} value="">TODOS</option>
				@foreach($origens as $origen)
				<option data-route="{{ route("subOrigenList", $origen->id) }}" @if($origen_id == $origen->id) selected @endif value="{{ $origen->id }}">{{ $origen->nombre }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<table  class="table" id="tableSubOrigen" data-api-route="{{ route("api.subOrigenList", $origen_id) }}">
		<thead>
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>CÓDIGO</th>
				<th>ORIGEN</th>
				<th>ESTATUS</th>
				<th>OPERACIÓN</th>
			</tr>
		</thead>
		<tbody>
			@foreach($subOrigens as $subOrigen)
			<tr>
				<td>{{ $subOrigen->id }}</td>
				<td>{{ $subOrigen->name }}</td>
				<td>{{ $subOrigen->code }}</td>
				<td>{{ $subOrigen->origen->nombre }}</td>
				<td>
					<div class="material-switch">
			        	<input data-route="{{ route("subOrigenChangeStatus", $subOrigen->id) }}" value="si"  @if($subOrigen->status==true) checked="checked" @endif  id="list_status_suborigen_{{ $subOrigen->id }}" name="list-suborigen-status" type="checkbox">
			        	<label for="list_status_suborigen_{{ $subOrigen->id }}" class="label-primary"></label>
			     	</div>
			    </td>
				<td>
					<button data-target="#modalEdit" data-toggle="modal" data-route="{{ route('subOrigenEdit', $subOrigen->id) }}" class="btn btn-info" title="Editar"><i class="fa fa-edit"></i></button>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
{{-- Modal de editr--}}
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalEditLabel">Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
{{--fin modal editar--}}
{{-- Modal de crear--}}
<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="modalCreateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalmodalCreate">Registrar Sub Origen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
{{--fin modal crear--}}
@endsection
@section("after_scripts")
<script type="text/javascript">
	$(function(){
		//window.$table = inizializeDataTable();
		$("#modalEdit, #modalCreate").on("show.bs.modal", function(event){
			let $button = $(event.relatedTarget);
			$(this).find(".modal-body").load($button.data("route"));
		});
		window.$table = inizializeDataTable();
		//envio de datos
		$(document).on("submit", "#formCreateSuborigen", function(event){
			let form = $(this)[0];
			let $form = $(this);
			event.preventDefault();
	        event.stopPropagation();
	       	form.classList.add('was-validated');
	       	$button = $form.find("button[type='submit']");
	       	let oldhtml = $button.html();
	       	$button.html('Guardando... <i class="fas fa-spinner fa-spin"></i>');
	       	$button.prop("disabled", true);
	       	//si todo sale bien se manda el formulario de edicion
			if (form.checkValidity() !== false) {
				$.ajax({
					url: $form.attr("action"),
					type: $form.attr("method"),
					data: $form.serializeArray(),
					success:function(response){

						new PNotify({
							title: "Correcto",
							text: "Datos guardados correctamente",
							type:"success"
						});
						$button.prop("disabled", false);
						$button.html(oldhtml);
						$("#modalCreate").modal("hide");
						window.$table.draw(false);
	                	window.$table.ajax.reload(null, false);


					},
					error:function(error){
						if(error.status === 422){
							showErrors(error);
						}else{
							alert("algo inesperado sucedió, contacte con el administrador.");
							window.location.reload();
						}
						$button.prop("disabled", false);
						$button.html(oldhtml);

					}
				});
	        }else{
	        	$button.prop("disabled", false);
				$button.html(oldhtml);	
	        }
		});

				//envio de datos
		$(document).on("submit", "#formEditSuborigen", function(event){
			let form = $(this)[0];
			let $form = $(this);
			event.preventDefault();
	        event.stopPropagation();
	       	form.classList.add('was-validated');
	       	$button = $form.find("button[type='submit']");
	       	let oldhtml = $button.html();
	       	$button.html('Guardando... <i class="fas fa-spinner fa-spin"></i>');
	       	$button.prop("disabled", true);
	       	//si todo sale bien se manda el formulario de edicion
			if (form.checkValidity() !== false) {
				$.ajax({
					url: $form.attr("action"),
					type: $form.attr("method"),
					data: $form.serializeArray(),
					success:function(response){

						new PNotify({
							title: "Correcto",
							text: "Datos Modificados correctamente",
							type:"success"
						});
						$button.prop("disabled", false);
						$button.html(oldhtml);
						$("#modalEdit").modal("hide");
						window.$table.draw(false);
	                	window.$table.ajax.reload(null, false);


					},
					error:function(error){
						if(error.status === 422){
							showErrors(error);
						}else{
							alert("algo inesperado sucedió, contacte con el administrador.");
							window.location.reload();
						}
						$button.prop("disabled", false);
						$button.html(oldhtml);

					}
				});
	        }else{
	        	$button.prop("disabled", false);
				$button.html(oldhtml);	
	        }
		});

		//cambiar status desde la lista
		$(document).on("change", "input[name='list-suborigen-status']", function(event){
			$this = $(this);
			$data = {};
			$checked  = $this.prop('checked');
			if($checked) {
				$data = {_token: $("meta[name='csrf-token']").attr("content"), 'suborigen-status': "si"}
			}else{
				$data = {_token: $("meta[name='csrf-token']").attr("content"), 'suborigen-status': "no"}
			}
			console.log($data);
			$.ajax({
				url : $this.data('route'),
				type: 'post',
				data: $data,
				success: function(response){
					new PNotify({
						title: "correcto",
						text: "Estatus Cambiado",
						type: "success"
					});
				},
				error: function(error){
					new PNotify({
						title: "Ups!",
						text: "Algo pasó, contacte con el administrador",
						type: "error"
					});
					$this.prop("checked", !$checked);
				}
			});

		});

		//cambiar de ruta al cambiar de origen
		$(document).on("change", "select#origenes_id", function(event){
			window.location.href = $('option:selected', this).data('route');
		});

		function showErrors(error){
			if(error.status === 422) {
				$.each(JSON.parse(error.responseText).errors, function(index, data){
					new PNotify({ 
						title : 'Ups',
						text : data[0],
						type: 'error'
					});
				});
			}
		}
		//datatables
		function inizializeDataTable() {
			$tableSubOrigen = $("#tableSubOrigen");
	        $table = $("#tableSubOrigen").DataTable({
	                "processing": true,
	                "serverSide": true,
	                ajax: $tableSubOrigen.data("apiRoute"),
	                "columns": [
	                	{data: "id"},
	                	{data: "name"},
	                	{data: "code"},
	                	{data: "nombre",  name: 'origen.nombre'},
	                	{data: "status",  name: 'status', orderable: false, searchable: false},
	                	{data: 'actions', name: 'actions', orderable: false, searchable: false}
	                ],
	                "pageLength": 25,
	                "language":{
	                    "decimal":        "",
	                    "emptyTable":     "No hay datos disponibles en esta tabla",
	                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
	                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
	                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
	                    "infoPostFix":    "",
	                    "thousands":      ",",
	                    "lengthMenu":     "Mostrar _MENU_ registros",
	                    "loadingRecords": "Cargando...",
	                    "processing":     "Procesando...",
	                    "search":         "Buscar:",
	                    "zeroRecords":    "No encontrados en la tabla",
	                    "paginate": {
	                        "first":      "Primer",
	                        "last":       "Ultimo",
	                        "next":       "Siguiente",
	                        "previous":   "Anterior"
	                    },
	                    "aria": {
	                        "sortAscending":  ": Activar para ordenar la columna ascendente",
	                        "sortDescending": ": Activar para ordenar la columna descendente"
	                    }
	                }
	            });
	        	$tableSubOrigen.parent().attr("style", "width: 100%;");
	        	$tableSubOrigen.attr("style", "width: 100%;");
	        	$("#tableSubOrigen_wrapper").attr("style", "width: 100%;");

		        return $table;
		    }
	});

</script>
@endsection