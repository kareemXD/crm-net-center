@extends('layout-2')
@section('title', 'Lista de Origenes')
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>

<div class="row widget-container" style="padding:15px">
	<button data-route="{{ route('origenCreate') }}" data-toggle="modal" data-target="#modalCreate" class="btn btn-primary"><i class="fas fa fa-plus"></i></button>
	<table  class="table" id="tableOrigen" data-api-route="{{ route("api.origenList") }}">
		<thead>
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>CÓDIGO</th>
				<th>Operación</th>
			</tr>
		</thead>
		<tbody>
			@foreach($origens as $origen)
			<tr>
				<td>{{ $origen->id }}</td>
				<td>{{ $origen->nombre }}</td>
				<td>{{ $origen->code }}</td>
				<td>
					<button data-target="#modalEdit" data-toggle="modal" data-route="{{ route('origenEdit', $origen->id) }}" class="btn btn-info" title="Editar"><i class="fa fa-edit"></i></button>
					<a class="btn btn-secondary" href="{{ route('subOrigenList', $origen->id) }}">Suborigen</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
{{-- Modal de editr--}}
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalEditLabel">Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
{{--fin modal editar--}}
{{-- Modal de crear--}}
<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="modalCreateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalmodalCreate">Registrar Origen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>
{{--fin modal crear--}}
@endsection
@section("after_scripts")
<script type="text/javascript">
	$(function(){
		window.$table = inizializeDataTable();

		$("#modalEdit, #modalCreate").on("show.bs.modal", function(event){
			let $button = $(event.relatedTarget);
			let route = $button.data("route");
			let $this =$(this);

			$this.find(".modal-body").load(route);
		});


		//editar datos de edicion
		$(document).on("submit", "#formOrigenEdit", function(event){
			let form = $(this)[0];
			let $form = $(this);
			event.preventDefault();
	        event.stopPropagation();
	       	form.classList.add('was-validated');
	       	$button = $form.find("button[type='submit']");
	       	let oldhtml = $button.html();
	       	$button.html('Guardando... <i class="fas fa-spinner fa-spin"></i>');
	       	$button.prop("disabled", true);
	       	//si todo sale bien se manda el formulario de edicion
			if (form.checkValidity() !== false) {
				$.ajax({
					url: $form.attr("action"),
					type: $form.attr("method"),
					data: $form.serializeArray(),
					success:function(response){

						new PNotify({
							title: "Correcto",
							text: "Datos modificados correctamente",
							type:"success"
						});
						$button.prop("disabled", false);
						$button.html(oldhtml);
						$("#modalEdit").modal("hide");
						window.$table.draw(false);
	                	window.$table.ajax.reload(null, false);


					},
					error:function(error){
						console.log(error);
						if(error.status === 422){
							showErrors(error);
						}else{
							alert("algo inesperado sucedió, contacte con el administrador.");
							window.location.reload();
						}
						$button.prop("disabled", false);
						$button.html(oldhtml);

					}
				});
	        }else{
	        	$button.prop("disabled", false);
				$button.html(oldhtml);	
	        }
		});

		//registrar origen
		$(document).on("submit", "#formCreateOrigen", function(event){
			let form = $(this)[0];
			let $form = $(this);
			event.preventDefault();
	        event.stopPropagation();
	       	form.classList.add('was-validated');
	       	$button = $form.find("button[type='submit']");
	       	let oldhtml = $button.html();
	       	$button.html('Guardando... <i class="fas fa-spinner fa-spin"></i>');
	       	$button.prop("disabled", true);
	       	//si todo sale bien se manda el formulario de edicion
			if (form.checkValidity() !== false) {
				$.ajax({
					url: $form.attr("action"),
					type: $form.attr("method"),
					data: $form.serializeArray(),
					success: function(){
						new PNotify({
							title: "Correcto",
							text: "Origen creado correctamente",
							type:"success"
						});
						$button.prop("disabled", false);
						$button.html(oldhtml);
						$("#modalCreate").modal("hide");
						window.$table.draw(false);
	                	window.$table.ajax.reload(null, false);

					},				
					error:function(error){
						if(error.status === 422){
							showErrors(error);
						}else{
							alert("algo inesperado sucedió, contacte con el administrador.");
							window.location.reload();
						}
						$button.prop("disabled", false);
						$button.html(oldhtml);
					}
				})
			}else{
				$button.prop("disabled", false);
				$button.html(oldhtml);
			}
		})

		function showErrors(error){
			if(error.status === 422) {
				$.each(JSON.parse(error.responseText).errors, function(index, data){
					new PNotify({ 
						title : 'Ups',
						text : data[0],
						type: 'error'
					});
				});
			}
		}
		//datatables
		function inizializeDataTable() {
			$tableOrigen = $("#tableOrigen");
	        $table = $("#tableOrigen").DataTable({
	                "processing": true,
	                "serverSide": true,
	                ajax: $tableOrigen.data("apiRoute"),
	                "columns": [
	                	{data: "id"},
	                	{data: "nombre"},
	                	{data: "code"},
	                	{data: 'actions', name: 'actions', orderable: false, searchable: false}
	                ],
	                "pageLength": 25,
	                "language":{
	                    "decimal":        "",
	                    "emptyTable":     "No hay datos disponibles en esta tabla",
	                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
	                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
	                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
	                    "infoPostFix":    "",
	                    "thousands":      ",",
	                    "lengthMenu":     "Mostrar _MENU_ registros",
	                    "loadingRecords": "Cargando...",
	                    "processing":     "Procesando...",
	                    "search":         "Buscar:",
	                    "zeroRecords":    "No encontrados en la tabla",
	                    "paginate": {
	                        "first":      "Primer",
	                        "last":       "Ultimo",
	                        "next":       "Siguiente",
	                        "previous":   "Anterior"
	                    },
	                    "aria": {
	                        "sortAscending":  ": Activar para ordenar la columna ascendente",
	                        "sortDescending": ": Activar para ordenar la columna descendente"
	                    }
	                }
	            });
	        	$tableOrigen.parent().attr("style", "width: 100%;");
	        	$tableOrigen.attr("style", "width: 100%;");
	        	$("#tableOrigen_wrapper").attr("style", "width: 100%;");

		        return $table;
		    }
	});

</script>
@endsection