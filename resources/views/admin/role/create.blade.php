@extends('layout-2')
@section("title", "Crear Grupo")
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>
<div class="page-title">
    <h1>
       Crear grupo
    </h1>
</div>
<div class="widget-container">
  <div class="row">
  	<div class="col-10 col-offset-1">
	   	<form id="createForm" action="{{ route("admin.storeRole") }}" method="post">
		  	{{ csrf_field() }}

		    <div class="form-group @if($errors->has('description')) has-error @endif">
		        <label for="description" class="control-label" >Descripción</label>
		        <input value="{{ old('description') }}" type="text" class="form-control" name="description" describedby="help_description">
		        <span id="help_description" class="help-block">
						{{ $errors->first("description") }}
						</span>
		    </div>
		    <div class="form-group @if($errors->has("permission")) has-error @endif">
		        <label for="permission" class="control-label" >Permisos</label>
		        <select data-tags="true" multiple name="permission[]" id="permission" class="select2-selection--multiple form-control" describedby="help_permission">
		            @foreach($permissions as $permission)
		            <option @if( is_array(old("permission")) ) @if(in_array($permission->id, old("permission"))) selected @endif @endif value="{{ $permission->id }}">{{ $permission->permission }}</option>
		            @endforeach
		        </select>
		        <span id="help_permission" class="help-block">{{ $errors->first("permission") }}</span>
		    </div>

		    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    <button type="submit" class="btn btn-primary">Guardar Cambios</button>

	  	</form> 		
  	</div>
  </div>
</div>
<div id="message_success_role" data-message="{{ (Session::has("success"))? Session::get("success") : "" }}"></div>
<!--Editar permiso-->
<script type="text/javascript">
    $(function(){
    	$("#page-loader").fadeOut(500);
        $(".select2-selection--multiple").select2({
            tags: "true"
        });
        if($("#message_success_role").data("message") != "" && $("#message_success_role").data("message") != undefined) {
        	new PNotify({
        		title: "Correcto.",
        		text: $("#message_success_role").data("message"),
        		type: "success"
        	});
        }
    });

</script>
@endsection