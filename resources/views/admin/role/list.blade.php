@extends('layout-2')
@section("title", "Grupos - Lista")
@section('content')
 	<div class="page-title">
        <h1>
           Lista de Grupos de usuarios
        </h1>
    </div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
            <div class="widget-container fluid-height clearfix">

				<div class="widget-content padded clearfix">
					<table class="table table-bordered table-striped display dataTable">
						<thead class="thead-dark">
							<tr>
								<th>Operación</th>
								<th>Identificador</th>
								<th>Nivel</th>
								<th>Descripción</th>
							</tr>
						</thead>
						<tbody>
							@foreach($groups as $group)
								<tr>
									<td><button data-edit-route="{{ route('admin.updateRole', $group->group_id) }}" data-description="{{ $group->description }}" data-permissions="{{ json_encode($group->permissions) }}" data-edit-level="{{ $group->level }}" data-toggle="modal" data-target="#modalEdit" class="btn btn-primary"><i class="fa fa-edit"></i></button> </td>
									<td>{{ $group->group_id }}</td>
									<td>{{ $group->level }}</td>
									<td>{{ $group->description }}</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Operación</th>
								<th>Identificador</th>
								<th>Descripción</th>
							</tr>
						</tfoot>
						@include('partials.admin.modal-edit-group')
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("after_scripts")
<script type="text/javascript">
	$(function(){
		var selectedValues = new Array();
		$button = "";
		$("#modalEdit").on("shown.bs.modal", function(e){
			$button = $(e.relatedTarget);
			$modal = $(this);
			if(parseInt($("meta#user_level").attr("content")) > parseInt($button.data("editLevel"))) {
				$form = $modal.find("form").first();
				$form.attr("action", $button.data("editRoute"));
				$("input[name='description']").val($button.data("description"));
				$("input[name='edit-level']").val($button.data("editLevel"));
				$.each($button.data("permissions"), function(index, value){
					selectedValues.push(value.id);
				});
				$("select#permission").val(selectedValues).select2({tags:"true"});
				selectedValues = [];
			}else{
				setTimeout(function(){
					$("#modalEdit").modal("hide");
				new PNotify({
					title: "sin permisos",
					text: "no puedes modificar un rol del mismo nivel o mayor al tuyo",
					type: "warning"
				});	
			}, 500);
			
			}
		});
	});
</script>
@endsection