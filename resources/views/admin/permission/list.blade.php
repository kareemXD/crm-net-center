@extends('layout-2')
@section("title", "Permisos de usuario - Lista")
@section('content')
 	<div class="page-title">
        <h1>
           Lista de Permisos de usuario
        </h1>
    </div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
            <div class="widget-container fluid-height clearfix">

				<div class="widget-content padded clearfix">
					<table class="table table-bordered table-striped display dataTable">
						<thead class="thead-dark">
							<tr>
								<th>Operación</th>
								<th>Identificador</th>
								<th>Código de permiso(No modificable)</th>
								<th>Descripción</th>
							</tr>
						</thead>
						<tbody>
							@foreach($permissions as $permission)
								<tr>
									<td><button data-permission="{{ $permission->permission }}" data-description="{{ $permission->description }}" data-toggle="modal" data-target="#modalEdit" data-edit-route="{{ route('admin.updatePermission', $permission->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></button></td>
									<td>{{ $permission->id }}</td>
									<td>{{ $permission->permission }}</td>
									<td>{{ $permission->description }}</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Operación</th>
								<th>Identificador</th>
								<th>Código de permiso(No modificable)</th>
								<th>Descripción</th>
							</tr>
						</tfoot>
						@include('partials.admin.modal-edit-permission')
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("after_scripts")
<script type="text/javascript">
	$(function(){
		$("#modalEdit").on("show.bs.modal", function(e){
			$button = $(e.relatedTarget);
			$modal = $(this);
			$form = $modal.find("form").first();
			$form.attr("action", $button.data("editRoute"));
			$("textarea[name='description']").text($button.data("description"));
            $("input#permission").val($button.data("permission"));
		});
	});
</script>
@endsection