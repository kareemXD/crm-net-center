    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <section class="slidebar">
        
      </section>
      @if (Auth::check())
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">CRM</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

          <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>Clientes/Prospectos</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  <li><a href="{{ route("listCustomer") }}"><i class="fa fa-list"></i> <span>Lista</span></a></li>
                  <li><a href="{{ route("createCustomer") }}"><i class="fa fa-user-plus"></i> <span>Grabar prospecto/cliente</span></a></li>
              </ul>
          </li>
          <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>Proyectos</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  <li><a href="{{ route("proyectList") }}"><i class="fa fa-list"></i> <span>Lista</span></a></li>
              </ul>
          </li>
          <!-- ======================================= -->
          <li class="header">{{ Auth::user()->name }}</li>
          <li><a href="{{ route("logout") }}"><i class="fa fa-sign-out"></i> <span>Salir</span></a></li>
        </ul>
      </section>
      @endif
      <!-- /.sidebar -->
    </aside>

