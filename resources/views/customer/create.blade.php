@extends('layout-2')
@section("title", "Crear Cliente/prospecto")
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>
<div class="widget-container">
    <h3 class="box-title">Crear cliente</h3>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link" id="client_data" data-toggle="tab" href="#clientData" role="tab" aria-controls="clientData" aria-selected="true">DATOS DEL CLIENTE</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="reservation_data" data-toggle="tab" href="#reservationData" role="tab" aria-controls="reservationData" aria-selected="true">DATOS DE RESERVACIÓN</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="card_data" data-toggle="tab" href="#cardData" role="tab" aria-controls="cardData" aria-selected="true">DATOS DE TARJETA</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="membership_data" data-toggle="tab" href="#membershipData" role="tab" aria-controls="membershipData" aria-selected="true">DATOS DE MEMBRESÍA</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="client_document" data-toggle="tab" href="#clientDocument" role="tab" aria-controls="clientDocument" aria-selected="true">DOCUMENTOS DEL CLIENTE</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="category_client" data-toggle="tab" href="#categoryClient" role="tab" aria-controls="categoryClient" aria-selected="true">CATEGORIA</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane" id="clientData" role="tabpanel" aria-labelledby="client_data">
            @include('includes.customer.client-data')
        </div>
        <div class="tab-pane" id="reservationData" role="tabpanel" aria-labelledby="reservation_data">
            @include('includes.customer.reservation-data')
        </div>
        <div class="tab-pane" id="cardData" role="tabpanel" aria-labelledby="card_data">
            @include('includes.customer.card-data')
        </div>
        <div class="tab-pane" id="membershipData" role="tabpanel" aria-labelledby="membership_data">
            @include('includes.customer.membership-data')
        </div>
        <div class="tab-pane" id="clientDocument" role="tabpanel" aria-labelledby="client_document">
            @include('includes.customer.client-document')
        </div>
        <div class="tab-pane" id="categoryClient" role="tabpanel" aria-labelledby="category_client">
            @include('includes.customer.category')
        </div>      
    </div>
</div>
@endsection
@section('after_scripts')
	<script>
		$(function(){
            //inicialize functions
            inicializeCheckCategories();
            inicializePriceFormat();
            inicializeLibraries();
            inicializeInputs();
            $(".nav-link").first().click();
            $("#page-loader").fadeOut(500, function(){
                 $(".nav-link").first().click();
            });
            $(document).on("keyup", ".price-format", function(event){
                var totalCertificate = parseFloat($("#totalCertificate").val().replace("$", "").replace(",", ""));
                var totalHotel = parseFloat($("#totalHotel").val().replace("$", "").replace(",", ""));
                var paying = parseFloat($("#paidOut").val().replace("$", "").replace(",", ""));
                var total = (totalCertificate + totalHotel);
               
                if(paying > total)
                {
                    $("#paidOut").val(0);
                }else{
                        $("#totalPrice").val('$' + total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                        $("#pendingPayment").val('$' + (total-paying).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                }

            });
            $(document).on('click','#confirmation_section', function(e) {
                var i = $(this).find("i").first();
                if(i.hasClass("fa-minus-circle")){
                    i.removeClass("fa-minus-circle");
                    i.addClass("fa-plus-circle")
                }else{
                    i.removeClass("fa-plus-circle");
                    i.addClass("fa-minus-circle")
                }
            });
            //mostrar la seccion de notas 
            $(document).on("change", "input[name='has-note']", function(event){
                if($(this).val() == "S"){
                    var route = $(this).data("route");
                    $("#notas").load(route, function(){
                        inicializeLibraries();
                    });
                }else{
                    document.getElementById("notas").innerHTML="";
                }
            });
            //eventos al enviar un formulario
            $(document).on("submit", "form", function(event){
                event.preventDefault();
                if(validateForm() ) {
                    var customer_id;
                    $.each($(".price-format"), function(index, element){
                        var value = parseFloat($(element).val().replace("$", "").replace(",", ""));
                        $(element).val(value);
                    });
                    var dataArray = [];
                    $.each($("form"), function(index, form){
                        var data = $(form).serializeArray();
                        var btn = $(form).find("button[type='submit']");
                        var loadingText = $(btn).data('loadingText');
                        if ($(btn).html() !== loadingText) {
                          $(btn).data('original-text', $(btn).html());
                          $(btn).html(loadingText);
                          $(btn).prop("disabled", true)
                        }
                       //send form ajax
                        dataArray = dataArray.concat(data);
                        setTimeout(function () {
                            $(btn).html($(btn).data('original-text'));
                            $(btn).prop("disabled", false)
                        }, 3000);
                    });
                    var submit = $.ajax({
                        method: $(this).attr("method"),
                        headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $(this).attr("action"),
                        data: dataArray,
                        error: function(response){

                        },
                        success: function(response){
                            var object = JSON.parse(response);
                            dropzone.on("sending", function(file, xhr, formData) {
                                formData.append('customer_id', object.id);
                                formData.append('_token', '{{ csrf_token() }}');
                                formData.append('name', file.name);
                            });
                            dropzone.processQueue();
                            location.replace("{{ route('listCustomer') }}");
                        }
                    });
                    reiniPriceFormat();
                }else{
                    new PNotify({
                          title: 'Alerta',
                          text: "Asegurese de haber llenado correctamente los campos Teléfono, nombre y correo electrónico",
                          type: "warning"
                    });
                }
            });

            //**agregar carga de origenes y suborigenes y de paises y estados *//

            $(document).on("change","#origen", function(event){
                $route = $('option:selected', this).data('route');
                $("#sub_origen_create").load($route, function(res){
                    inicializeLibraries();
                    inicializeInputs();
                });
            });

            $(document).on("change", "#edit_country", function(event){
                $route = $('option:selected', this).data('route');
                $("#edit_state").load($route, function(res){
                    inicializeLibraries();
                    inicializeInputs();
                })
            });
            /**fin de los campos**/



		});
        //fin del jquery inicialize
        function inicializeCheckCategories() {
            var categoryCheck = $("input[name='category-client']");
            var checked = false;
            $.each(categoryCheck, function(i,v){
                if($(v).attr("checked") == "checked"){
                    checked = true;
                }
            });
            //ver si ninguna categoría esta seleccionar, seleccionar una por default
            if(checked === false){
                 $("input[name='category-client'][value='S']").first().attr("checked", "checked");
            }
        }

    function inicializePriceFormat() {
        $(".price-format").priceFormat({
            prefix: '$',
            thousandsSeparator: ',',
            allowNegative: false
        });
        $(".price-format").attr("maxlength","11");
    }
    function reiniPriceFormat(){
        $.each($(".price-format"), function(index, element){
            var value = parseFloat($(element).val());
            $(element).val('$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
    }

//validaciones
    function validateForm() {
        var flag = true;
        flag = validateName(flag)
        flag = validatePhone(flag);
        flag = validateEmail(flag);
        if($("input[name='has-note']:checked").val() == "S"){
            flag = validateNotes(flag);
        }

        return flag;
    }

    function validateName(flag) {
        //validar nombre
        var name = $("input[name='customer-name']");
        if($(name).val() == ""){
            flag= false;
            if(!$(name).parent().hasClass('has-error')) {
                $(name).parent().addClass('has-error');
                $(name).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else{
            if($(name).parent().hasClass('has-error')) {
                $(name).parent().removeClass('has-error');
                $(name).parent().find('.help-block').slideUp();
                $(name).parent().find('.help-block').remove();
            }
        }

        return flag;
    }

    function validatePhone(flag) {
        var cell_phone = $("input[name='cell-phone']");
        var home_phone = $("input[name='home-phone']");
        var office_phone = $("input[name='office-phone']");
        if($(cell_phone).val()=="" && $(home_phone).val()=="" && $(office_phone).val()=="") {
            flag=false;
            if(!$(cell_phone).parent().hasClass('has-error')){
                $(cell_phone).parent().addClass("has-error");
                $(home_phone).parent().addClass("has-error");
                $(office_phone).parent().addClass("has-error");
                $(cell_phone).parent().append('<span class="help-block">Debe ingresar al menos un teléfono</span>');
            }
        }else{
            if($(cell_phone).parent().hasClass('has-error')) {
                $(cell_phone).parent().removeClass('has-error');
                $(home_phone).parent().removeClass('has-error');
                $(office_phone).parent().removeClass('has-error');
                $(cell_phone).parent().find('.help-block').remove();
            }
        }
        return flag;
    }

    function validateEmail(flag) {
        var email = $("input[name='customer-email']");
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if($(email).val() == ""){
            flag = false;
            if(!$(email).parent().hasClass('has-error')) {
                $(email).parent().addClass('has-error');
                $(email).parent().append('<span class="help-block">El correo es obligatorio</span>');
            }else{
                $(email).parent().find('.help-block').remove();
                $(email).parent().append('<span class="help-block">El correo es obligatorio</span>');
            }
        }else{
            if(re.test($(email).val())){
                if($(email).parent().hasClass('has-error')) {
                    $(email).parent().removeClass('has-error');
                    $(email).parent().find('.help-block').remove();
                }
            }else{
                if(!$(email).parent().hasClass('has-error')) {
                    $(email).parent().addClass('has-error');
                    $(email).parent().append('<span class="help-block">Ingrese un correo válido</span>');
                    flag = false;
                }else{
                    $(email).parent().find('.help-block').remove();
                    $(email).parent().append('<span class="help-block">Ingrese un correo válido</span>');
                }
            }
        } 
        return flag;
    }

    function validateNotes(flag){
        var fecha = $('input[name="date-hour"]');
        var title = $("input[name='note-title']");
        var noteDetails = $("textarea[name='note-details']");
        if($(fecha).val() == "") {
            flag = false;
            if(!$(fecha).parent().hasClass('has-error')){
                $(fecha).parent().addClass('has-error');
                $(fecha).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(fecha).parent().hasClass('has-error')){
                $(fecha).parent().removeClass('has-error');
                $(fecha).parent().find('.help-block').remove();
            }
        }
        if($(title).val() == "") {
            flag = false;
            if(!$(title).parent().hasClass('has-error')) {
                $(title).parent().addClass('has-error');
                $(title).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(title).parent().hasClass('has-error')){
                $(title).parent().removeClass('has-error');
                $(title).parent().find('.help-block').remove();
            }
        }
        if($(noteDetails).val() == "") {
            flag = false;
            if(!$(noteDetails).parent().hasClass('has-error')) {
                $(noteDetails).parent().addClass('has-error');
                $(noteDetails).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(noteDetails).parent().hasClass('has-error')){
                $(noteDetails).parent().removeClass('has-error');
                $(noteDetails).parent().find('.help-block').remove();
            }
        }
        if(!flag){
            new PNotify({
                title: 'Notas',
                text: 'Llene todos los campos de las notas',
                type :'warning'
            });
        }
        return flag;
    }
//fin validaciones

    function inicializeLibraries() {

        $(".select2").select2();
            setTimeout(function(){
                $('.text-red.visible').slideUp(8000, function(){
                    $('.text-red.visible').removeClass('visible');
                });

            }, 5000);

            $('.date-picker').datepicker({
                'format': 'yyyy-mm-dd',
            });
            $('.date-time-picker').datetimepicker({
                format:'YYYY-MM-DD hh:mm:00 a',
                locale: 'es'
            });

            jQuery(function($) {
                $('[data-numeric]').payment('restrictNumeric');
                $('.cc-number').payment('formatCardNumber');
                $('.cc-exp').payment('formatCardExpiry');
                $('.cc-cvc').payment('formatCardCVC');

                $.fn.toggleInputError = function(erred) {
                    this.parent('.form-group').toggleClass('has-error', erred);
                    return this;
                };
            });
    }

    function inicializeInputs() {
        $(document).on('keyup', 'input[name="customer-name"]', function(event){validateName(true);});
        $(document).on('keyup', 'input[name="cell-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="home-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="office-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="customer-email"]', function(event){validateEmail(true);});
    }

       
	</script>
@endsection
@section('after_styles')
<link rel="stylesheet" href="{{ asset("plugins/dropzone/dropzone.css") }}">
<style>
	.form-check-input:hover{
		cursor:pointer;
	}
	.text-red{
		display:none;
	}
    label{
        text-align:center;
        padding: 0 0 0 0;
        white-space: nowrap;
    }
    .select2-container {
        width:100% !important;
    }
    i{
        display:inline-block;
    }
    .no-padding{
        padding:0;
    }
    .text-white, .text-white:active, .text-white:focus{
        color:#fff;
    }
</style>
@endsection
