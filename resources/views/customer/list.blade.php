@extends('layout-2')
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>

    <div class="page-title">
        <h1>
           Lista de Clientes/prospectos
        </h1>
    </div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
      <div class="widget-container fluid-height clearfix">
				<!-- Filtro avanzado -->
				@if(Auth::user()->hasAccessApp('customer_advanced_filter'))
					@include("partials.advanced-filter")
				@endif
				<div class="widget-content ">
					<table style="width:100%" class="table table-bordered table-striped display dataTable">
						<thead>
							<tr>
								<th></th>
								<th class="sorting">LLEGADA</th>
								<th class="sorting">NOMBRE</th>
								<th class="sorting">EMAIL</th>
								<th class="sorting">TELEFONOS</th>
								<th class="sorting">TIPO DE Q´S</th>
								<th class="sorting">FOLIO DE RESERVA</th>
								<th class="sorting">PLAN</th>
								<th class="sorting">EJECUTIVO</th>
								<th class="sorting">CATEGORIA</th>
								<th>Operación</th>
							</tr>
						</thead>
						<tbody>
							<!--se quita el include'includes.customer.customer-list'-->
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th width="100px" class="sorting">LLEGADA</th>
								<th class="sorting">NOMBRE</th>
								<th class="sorting">EMAIL</th>
								<th class="sorting">TELEFONOS</th>
								<th class="sorting">TIPO DE Q´S</th>
								<th class="sorting">FOLIO DE RESERVA</th>
								<th class="sorting">PLAN</th>
								<th class="sorting">EJECUTIVO</th>
								<th class="sorting">CATEGORIA</th>
								<th>Operación</th>
							</tr>
						</tfoot>
					
					</table>
				</div>
			@include("partials.modal-delete")
			@include("partials.modal-edit")
		</div>
	</div>
	</div>
@endsection
@section('after_scripts')
    @include('partials.script-update')
@endsection
