@extends('layout-2')
@section("title", "Modificar Cliente/prospecto")
@section('content')
<div class="widget-container">
	<h3 class="box-title">Editar cliente</h3>
	<!-- Nav tabs -->
	<div class="row">
		<a class="btn btn-primary btn-lg btn-block" href="{{ route('listCustomer') }}">Ir a la lista de clientes /prospectos</a>
	</div>
	<div class="row"></div>
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item active">
			<a class="nav-link active" id="client_data" data-toggle="tab" href="#clientData" role="tab" aria-controls="clientData" aria-selected="true">DATOS DEL CLIENTE</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="reservation_data" data-toggle="tab" href="#reservationData" role="tab" aria-controls="reservationData" aria-selected="true">DATOS DE RESERVACIÓN</a>
		</li>
	    <li class="nav-item">
	        <a class="nav-link" id="card_data" data-toggle="tab" href="#cardData" role="tab" aria-controls="cardData" aria-selected="true">DATOS DE TARJETA</a>
	    </li>
	    <li class="nav-item">
	        <a class="nav-link" id="membership_data" data-toggle="tab" href="#membershipData" role="tab" aria-controls="membershipData" aria-selected="true">DATOS DE MEMBRESÍA</a>
	    </li>
	    <li class="nav-item">
	        <a class="nav-link" id="client_document" data-toggle="tab" href="#clientDocument" role="tab" aria-controls="clientDocument" aria-selected="true">DOCUMENTOS DEL CLIENTE</a>
	    </li>
	    <li class="nav-item">
	        <a class="nav-link" id="category_client" data-toggle="tab" href="#categoryClient" role="tab" aria-controls="categoryClient" aria-selected="true">CATEGORIA</a>
	    </li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="clientData" role="tabpanel" aria-labelledby="client_data">
			@include('includes.customer-edit.client-data')
		</div>
		<div class="tab-pane" id="reservationData" role="tabpanel" aria-labelledby="reservation_data">
			@include('includes.customer-edit.reservation-data')
		</div>
	    <div class="tab-pane" id="cardData" role="tabpanel" aria-labelledby="card_data">
	        @include('includes.customer-edit.card-data')
	    </div>
	    <div class="tab-pane" id="membershipData" role="tabpanel" aria-labelledby="membership_data">
	        @include('includes.customer-edit.membership-data')
	    </div>
	    <div class="tab-pane" id="clientDocument" role="tabpanel" aria-labelledby="client_document">
	        @include('includes.customer-edit.client-document')
	    </div>
	    <div class="tab-pane" id="categoryClient" role="tabpanel" aria-labelledby="category_client">
	        @include('includes.customer-edit.category')
	    </div>		
	</div>
</div>
<script>
	$(function(){
		var dropzone= "";
		inicializeEdit();
    $("#page-loader").fadeOut(500);
    $(".nav-link").first().click();
    $("#page-loader").fadeOut(500, function(){
         $(".nav-link").first().click();
    });
    if($("input[name='has-note']:checked").val() == "S"){
        var route = $("input[name='has-note']").data("route");
        $("#notas").load(route, function(){
            inicializeLibraries();
        });
    }else{
        document.getElementById("notas").innerHTML="";
    }
	})
</script>
@include('partials.script-update')
<link rel="stylesheet" href="{{ asset("plugins/dropzone/dropzone.css") }}">
<style>
    .form-check-input:hover{
        cursor:pointer;
    }
    .text-red{
        display:none;
    }
    label{
        text-align:center;
        padding: 0 0 0 0;
        white-space: nowrap;
    }
    .select2-container {
        width:100% !important;
    }
    i{
        display:inline-block;
    }
    .no-padding{
        padding:0;
    }
    .text-white, .text-white:active, .text-white:focus{
        color:#fff;
    }
</style>
@endsection