<!DOCTYPE html>
<html>
  <head>
    @php
    $routes = [];
    foreach(\Route::getRoutes() as $route) {
        $routes[$route->getName()] = asset('/').$route->uri;
    }
    @endphp
    <title>
      @yield('title')
    </title>
    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicon') }}/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicon') }}/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon') }}/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/{{ asset('images/favicon') }}apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon') }}/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicon') }}/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon') }}/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon') }}/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon') }}/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicon') }}/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon') }}/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon') }}/favicon-16x16.png">
    <link rel="manifest" href="{{ asset('images/favicon') }}/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/favicon') }}/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--Favicon para dispositivos-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicon') }}/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicon') }}/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon') }}/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon') }}/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon') }}/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicon') }}/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon') }}/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon') }}/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon') }}/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicon') }}/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon') }}/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon') }}/favicon-16x16.png">
    <link rel="manifest" href="{{ asset('images/favicon') }}/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/favicon') }}/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">

    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{--Get route for ajax query and get notification json data for today--}}
    <meta name="notifications-today" content="{{ route('getNotificationsToday') }}">
    <meta id="routeList" content="{{ json_encode($routes) }}">  
    <meta id="user_level" content="{{ Auth::user()->level_user }}"  >

    @yield('before_styles')
    
    <!--estilos css -->
    <link href="{{ asset('css/app.css')}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/fonts.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/glyphicons.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/se7en-font.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/isotope.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{asset('theme/jquery.fancybox.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/fullcalendar.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/wizard.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/morris.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/datepicker.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/timepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/colorpicker.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/bootstrap-switch.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/bootstrap-editable.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('theme/daterange-picker.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/typeahead.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/summernote.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/ladda-themeless.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/social-buttons.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/pygments.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/color/green.css')}}" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="{{ asset('theme/color/orange.css')}}" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="{{ asset('theme/color/magenta.css')}}" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="{{ asset('theme/color/gray.css')}}" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="{{asset('theme/jquery.fileupload-ui.css')}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dropzone.css')}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/bootstrap-datetimepicker.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/responsive.bootstrap4.min.css') }}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/pnotify.custom.min.css')}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/style.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/kareem.css')}}" media="screen" rel="stylesheet" type="text/css" />

    @yield('after_styles')
    <!-- javascripts -->
    @yield('before_scripts')
    <script>
    </script>
    <script type="text/javascript" src="{{ asset('js/all.js') }}?v=1.1" ></script>
        
    @include('admin.inc.alerts')
    <!-- fin javascripts -->
  </head>
  <body class="page-header-fixed bg-1">
    <div class="modal-shiftfix">
        <element id="crm"  >
        </element>
        @include('partials.se7en.navigation')
        <script type="text/javascript"> console.log($("meta#user_level").attr("content"));</script>

        <div class="container-fluid main-content">
            @yield('content')
        </div> 
    </div>
    
    @include('partials.se7en.style')
    @yield('after_scripts')
    <script>
        function disableselect(e) {return false;}
        
        $("input").attr("autocomplete", "off");
    </script>
    <div id="message_flash" data-message-success="{{ session()->get('success') }}" data-message-error="{{ $errors->first() }}"></div> 
  </body>
</html>