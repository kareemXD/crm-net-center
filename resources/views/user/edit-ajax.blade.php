<div class="row">
	<div class="widget-container @if(request()->ajax()) col-md-12  @else  col-md-8 col-md-offset-2 @endif">
		<form id="updateUserForm" action="{{ route("updateUser", $user->login) }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<h3 class="text-center">Modificar a {{ $user->name }}</h3>
			<div class="col-md-10 col-md-offset-1">
				<div class="row @if($errors->has('username')) has-error @endif">
					<label class="control-label" for="username">Nombre de usuario</label>
					<input disabled id="username" name="username" type="text" class="form-control" value="{{ $user->login }}" describedby="help_username" >
				</div>

				<div class="row @if($errors->has("password")) has-error @endif">
					<label for="password" class="control-label">Contraseña</label>
					<input name="password" type="password" class="form-control" id="password" describedby="help_password" value="" >
					@if($errors->has("password"))
					<span id="help_password" class="help-block">
						@foreach($errors->get("password") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="row @if($errors->has("passwordpassword_confirmation")) has-error @endif">
					<label for="password_confirm" class="control-label">Confirmar contraseña</label>
					<input name="password_confirmation" type="password" class="form-control" id="password_confirm" describedby="help_password_confirm" value=""> 
					
					@if($errors->has("password_confirmation"))
					<span id="help_password_confirm" class="help-block">
						@foreach($errors->get("password_confirmation") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="row @if($errors->has('name')) has-error @endif">
					<label for="name" class="control-label">Nombre</label>
					<input name="name" type="text" class="form-control" id="name" value="{{ $user->name }}" describedby="help_name" >
					@if($errors->has("name"))
					<span id="help_name" class="help-block">
						@foreach($errors->get("name") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="row @if($errors->has('email')) has-error @endif">
					<label for="email" class="control-label">Correo</label>
					<input name="email" type="email" class="form-control" id="email" value="{{ $user->email }}" describedby="help_email" >
					@if($errors->has("name"))
					<span id="help_email" class="help-block">
						@foreach($errors->get("email") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>
				<div style="padding-top:5px;" class="row @if($errors->has('active')) has-error @endif">
					<label for="active" class="control-label">Activo</label>
				</div>
				<div class="row @if($errors->has('active')) has-error @endif">
					<div class="custom-control custom-radio custom-control-inline">
					  <input  id="active_N" @if($user->active == "N") checked  @endif type="radio"  name ="active" value="N"  type="radio" class="custom-control-input">
					  <label class="custom-control-label" for="active_N" >NO</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
					  <input  id="active_Y"@if($user->active == "Y") checked  @endif type="radio"  name ="active" value="Y"  type="radio"  class="custom-control-input">
					  <label class="custom-control-label" for="active_Y" >SI</label>
					</div>
				</div>
				<div class="row @if($errors->has("user-group")) has-error @endif">
					<label for="user_group" class="control-label">Grupo</label>
					<select data-old-value="{{ @$user->userRole->group_id }}" class="form-control" name="user-group" id="user_group" describedby="help_user_group">
						<option data-permissions="[]" value="" >No definido</option>
						@foreach($groups as $group)
						<option data-level="{{ $group->level }}" @if(@$user->userRole->group_id == $group->group_id) selected @endif data-permissions="{{ json_encode($group->permissions) }}" value="{{ $group->group_id }}">{{ $group->description }}</option>
						@endforeach
					</select>
					@if($errors->has("user-group"))
						<span id="help_user_group" class="help-block">
							@foreach($errors->get("user-group") as $error)
								<li>{{ $error }}</li>
							@endforeach
						</span>
					@endif
				</div>

				<div class="row @if($errors->has("user-permission")) has-error @endif" style="padding-top:5px;">
						<label class="control-label" for="">Permisos</label>
						@foreach($permissions  as $permission)
							<div class="custom-control custom-checkbox custom-control-inline">
							  <input id="permissions_{{ $permission->id }}" type="checkbox" class="custom-control-input"
							   @if(in_array($permission->id, ( old("permission") != null )? old("permission") : [])) checked @endif type="checkbox" data-id-element="{{ $permission->id }}"  value="{{ $permission->id }}" name="permission[]"
							  >
							  <label  class="custom-control-label" for="permissions_{{ $permission->id }}" >{{ $permission->permission }}</label>
							</div>
						@endforeach
					@if($errors->has("user-permission"))
						<span id="help_user_permission" class="help-block">
							@foreach($errors->get("user-group") as $error)
								<li>{{ $error }}</li>
							@endforeach
						</span>
					@endif
				</div>
				<div style="padding-top:5px;" class="row text-center">
					<button class="btn btn-primary" data-loading-text='<i class="fa fa-refresh fa-spin"></i>Cargando' type="submit">Modificar</button>
				</div>
			</div>

		</form>
	</div>
</div>
<div id="message_success" data-message="{{ session()->has('success')? session()->get('success') : '' }}"></div>
<script type="text/javascript">
	$(function(){
		/* Se inicializa la funcion para cambiar los permisos automaticamente cuando se modifica el grupo al que pertenece */
		changePermissionsUsingGroup();
		if($("#message_success").data("message") != ""){
				new PNotify({
				  title: "Correcto",
				  text: $("#message_success").data("message"),
				  type: "success"
				});
		}
		/*Inicializar elementos*/
		$(document).on("change", "#user_group", function(e){
			if(parseInt($("meta#user_level").attr("content")) >= $(this).find(':selected').data("level") ){
				changePermissionsUsingGroup();
			}else{
				$(this).val($(this).data('oldValue'));
				new PNotify({
					title: "No permitido",
					text: "No puedes colocar un rol de mayor rango al tuyo",
					type: "warning"
				})
			}
		});
		/*Funciones*/
		function changePermissionsUsingGroup() {
			var permissions = $("#user_group > option:selected").data("permissions");
			$("input[data-id-element]").prop("checked", false);
			$.each(permissions, (index, permission) => {
				$("input[data-id-element='"+ permission.id +"']").prop('checked', true);
			});
			$("input[data-id-element]").attr("disabled", "disabled");
		}
		
	});
</script>
