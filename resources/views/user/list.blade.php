@extends('layout-2')
@section("title", "Lista de usuarios")
@section('content')
<div id="page-loader"><span class="preloader-interior"></span></div>

<div class="page-title">
    <h1>
       Lista de Usuarios
    </h1>
</div>
<div class="row">
	<div class="col">
      <div class="widget-container fluid-height clearfix">
      <div class="widget-content padded clearfix">
      	<table id="dataTable" data-route-ajax="" data-order-type="asc" class="table table-bordered table-striped display dataTable">
					<thead>
						<tr>
							<th>USUARIO</th>
							<th>NOMBRE</th>
							<th>EMAIL</th>
							<th>ROL</th>
							<th>ESTATUS</th>
							<th>Operacion</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $index => $user)
						<tr>
							<td>{{ $user->login }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>
								@if(Auth::user()->hasAccessApp('user_update_roles'))
								<select data-old-value="{{ @$user->userRole->group_id }}" class="form-control" name="user-roles" id="user_roles" data-change-role data-route="{{ route('changeRoleUser', $user->login) }}">
									<option value="0">No definido</option>
									@foreach($roles as $role)
									<option data-level="{{ $role->level }}" @if(@$user->userRole->group_id == $role->group_id) selected @endif value="{{ $role->group_id }}">{{ $role->description }}</option>
									@endforeach
								</select>
								@else
								{{ !is_null($user->roles->first())? $user->roles->first()->description : "NO definido" }}
								@endif
							</td>
							<td>
				              <div class="material-switch">
				                <input @if(!Auth::user()->hasAccessApp('user_update')) disabled  @endif data-switch data-route="{{ route('changeStatusUser', $user->login) }}" @if($user->active=="Y") checked @endif id="is_active_{{$user->login}}" name="is_active['{{$user->login}}']" type="checkbox"/>
				                <label for="is_active_{{$user->login}}" class="label-primary"></label>
				              </div>
							</td>
							<td>
								@if(Auth::user()->hasAccessApp("user_delete"))
								<button data-route="{{ route("deleteUser", $user->login) }}" data-toggle="modal" data-target="#modalDeleteUser" class="btn btn-danger"><i class="fa fa-trash" ></i></button>
								@endif
								@if(Auth::user()->hasAccessApp('user_update') && (Auth::user()->level_user >  $user->level_user || Auth::user()->hasAccessApp("all")) )
								<button class="btn btn-success" data-route="{{ route('editUser', $user->login) }}" data-target="#modalEditUser" data-toggle="modal"><i class="fa fa-edit"></i></button>
								@endif

							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>USUARIO</th>
							<th>NOMBRE</th>
							<th>EMAIL</th>
							<th>ROL</th>
							<th>ESTATUS</th>
							<th>Operacion</th>
						</tr>
					</tfoot>
				</table>
      		</div>
		</div>
	</div>
</div>
{{--modal para eliminar a un usuario --}}
<div class="modal fade" id="modalDeleteUser" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel"> 
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalDeleteLabel">¿Seguro de eliminar usuario?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="formDeleteUser" action="" method="post">
					{{ csrf_field() }}
					{!! method_field('delete') !!}
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        	<button type="submit" class="btn btn-danger">Eliminar</button>
				</form>
			</div>
		</div>
	</div>
</div>
{{--fin modal pára elimimnar un usuario--}}
{{--modal para editar la informacion de un usuario--}}
<!-- Modal -->
<div id="modalEditUser" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg widget-container">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>

  </div>
</div>
{{--fin de modal para editar infortmacion de un usuario--}}
@endsection
@section('after_scripts')
<script src="{{ asset('js/users.js') }}" type="text/javascript">

</script>
@endsection