@extends('layout-2')
@section("title", "Registrar usuario")
@section('content')
<div class="row">
	<div class="widget-container col-12 ">
		<form action="" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<h3 class="text-center">Registrar nuevo usuario</h3>
			<div class="col-md-10 offset-md-1">
				<div class="form-group @if($errors->has('username')) has-error @endif">
					<label class="control-label" for="username">Nombre de usuario</label>
					<input id="username" name="username" type="text" class="form-control" value="{{ old('username') }}" describedby="help_username" >
					@if($errors->has('username'))
					<span id="help_username" class="help-block">
						@foreach($errors->get("username") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="form-group @if($errors->has("password")) has-error @endif">
					<label for="password" class="control-label">Contraseña</label>
					<input name="password" type="password" class="form-control" id="password" describedby="help_password" >
					@if($errors->has("password"))
					<span id="help_password" class="help-block">
						@foreach($errors->get("password") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="form-group @if($errors->has("passwordpassword_confirmation")) has-error @endif">
					<label for="password_confirm" class="control-label">Confirmar contraseña</label>
					<input name="password_confirmation" type="password" class="form-control" id="password_confirm" describedby="help_password_confirm" > 
					
					@if($errors->has("password_confirmation"))
					<span id="help_password_confirm" class="help-block">
						@foreach($errors->get("password_confirmation") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="form-group @if($errors->has('name')) has-error @endif">
					<label for="name" class="control-label">Nombre</label>
					<input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}" describedby="help_name" >
					@if($errors->has("name"))
					<span id="help_name" class="help-block">
						@foreach($errors->get("name") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>

				<div class="form-group @if($errors->has('email')) has-error @endif">
					<label for="email" class="control-label">Correo</label>
					<input name="email" type="email" class="form-control" id="email" value="{{ old('email') }}" describedby="help_email" >
					@if($errors->has("name"))
					<span id="help_email" class="help-block">
						@foreach($errors->get("email") as $error)
							<li>{{ $error }}</li>
						@endforeach
					</span>
					@endif
				</div>
				<div style="padding-top:5px;" class="form-group @if($errors->has('active')) has-error @endif">
					<label for="active" class="control-label">Activo</label>
						<div class="form-group @if($errors->has('active')) has-error @endif">
						<div class="custom-control custom-radio custom-control-inline">
							<input id="active_N" @if(old("active") == "N") checked @elseif(old("active") == "N")  @else checked @endif type="radio" class="custom-control-input"  name ="active" value="N" >
							<label class="custom-control-label" for="active_N">NO</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input id="active_Y" type="radio" @if(old("active") == "Y") checked @endif class="custom-control-input"  name ="active" value="Y" >
							<label class="custom-control-label" for="active_Y">SI</label>
						</div>					
					</div>
				</div>
				<div class="form-group @if($errors->has("user-group")) has-error @endif">
					<label for="user_group" class="control-label">Grupo</label>
				</div>
				<div class="form-group @if($errors->has("user-group")) has-error @endif" >
					<select class="form-control" name="user-group" id="user_group" describedby="help_user_group">
						<option value="">Sin asignar</option>
						@foreach($groups as $group)
						<option data-level="{{ $group->level }}" data-permissions="{{ json_encode($group->permissions) }}" value="{{ $group->group_id }}">{{ $group->description }}</option>
						@endforeach
					</select>
					@if($errors->has("user-group"))
						<span id="help_user_group" class="help-block">
							@foreach($errors->get("user-group") as $error)
								<li>{{ $error }}</li>
							@endforeach
						</span>
					@endif
				</div>

				<div class="form-group @if($errors->has("user-permission")) has-error @endif" style="padding-top:5px;">
					<label class="control-label" for="">Permisos</label>
					<div class="form-group">
						@foreach($permissions  as $permission)
							<div class="custom-control custom-checkbox custom-control-inline">
								<input @if(in_array($permission->id, ( old("permission") != null )? old("permission") : [])) checked @endif type="checkbox" data-id-element="{{ $permission->id }}"  value="{{ $permission->id }}" name="permission[]" class="custom-control-input">
								<label  class="custom-control-label"><span>{{ $permission->permission }}</span></label>
							</div>
						@endforeach
					</div>
					@if($errors->has("user-permission"))
						<span id="help_user_permission" class="help-block">
							@foreach($errors->get("user-group") as $error)
								<li>{{ $error }}</li>
							@endforeach
						</span>
					@endif
				</div>
				<div style="padding-top:5px;" class="form-group text-center">
					<button class="btn btn-primary" data-loading-text='<i class="fa fa-refresh fa-spin"></i>Cargando' type="submit">Crear</button>
				</div>
			</div>

		</form>
	</div>
</div>
<div id="message_success" data-message="{{ session()->has('success')? session()->get('success') : '' }}"></div>
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){
		/* Se inicializa la funcion para cambiar los permisos automaticamente cuando se modifica el grupo al que pertenece */
		changePermissionsUsingGroup();
		if($("#message_success").data("message") != ""){
				new PNotify({
				  title: "Correcto",
				  text: $("#message_success").data("message"),
				  type: "success"
				});
		}
		/*Inicializar elementos*/
		$("#user_group").on("change", function(e){
			if(parseInt($("meta#user_level").attr("content")) >= $(this).find(':selected').data("level") ){
				changePermissionsUsingGroup();
			}else{
				$(this).val($(this).data('oldValue'));
				new PNotify({
					title: "No permitido",
					text: "No puedes colocar un rol de mayor rango al tuyo",
					type: "warning"
				})
			}
		});
		$("form").on("submit", function(e){
			var button = $(this).find("button[type='submit']");
			var html = button.html();
			button.html('Enviando...');
			button.attr("disabled", true);

			setTimeout(function(){
				button.html(html);
			}, 3000);
		});
		/*Funciones*/
		function changePermissionsUsingGroup() {
			var permissions = $("#user_group > option:selected").data("permissions");
			$("input[data-id-element]").prop("checked", false);
			$.each(permissions, (index, permission) => {
				$("input[data-id-element='"+ permission.id +"']").prop('checked', true);
			});
			$("input[data-id-element]").attr("disabled", "disabled");
		}
		
	});
</script>
@endsection