<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Arial Narrow";
	panose-1:2 11 6 6 2 2 2 3 2 4;}
@font-face
	{font-family:"Lucida Calligraphy";
	panose-1:3 1 1 1 1 1 1 1 1 1;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
h1
	{mso-style-link:"Título 1 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Tahoma",sans-serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Encabezado Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Texto independiente Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:9.0pt;
	font-family:"Tahoma",sans-serif;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car";
	mso-style-link:Encabezado;}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
span.hps
	{mso-style-name:hps;}
p.Default, li.Default, div.Default
	{mso-style-name:Default;
	margin:0cm;
	margin-bottom:.0001pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Arial Narrow",sans-serif;
	color:black;}
span.Ttulo1Car
	{mso-style-name:"Título 1 Car";
	mso-style-link:"Título 1";
	font-family:"Tahoma",sans-serif;
	font-weight:bold;}
span.TextoindependienteCar
	{mso-style-name:"Texto independiente Car";
	mso-style-link:"Texto independiente";
	font-family:"Tahoma",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	border:double windowtext 3.0pt;
	padding:24.0pt 24.0pt 24.0pt 24.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ES-MX link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal><img width=117 height=119 id="6 Imagen"
src="{{  asset('formats/formato-nq_archivos/image002.jpg') }}" alt=image012.jpg></p>

<p class=MsoNormal><span style='font-size:14.0pt;line-height:115%;font-family:
"Lucida Calligraphy";color:#002060'>{{($customer->nombre_co != "")?"Estimados." : (($customer->prefix_name)=="Sr."? "Estimado." : "Estimada.")}} {{ $customer->prefix_name." ".$customer->nombre }} {{  ($customer->nombre_co != "")? " y ".$customer->prefix_name2." ".$customer->nombre_co : "" }} </span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Cambria",serif;
color:#002060'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:14.0pt;
line-height:115%'>Buganvilias Resort Vacation Club tiene el agrado de confirmar
 la activación de su certificado bajo la siguiente información:</span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:14.0pt;
line-height:115%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:14.0pt'>Número de Pre-reservación: <b><span
style='color:#0070C0'>{{ $customer->folioreserva }}</span></b></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:14.0pt'>Ingreso: <b><span style='color:#0070C0'>Aun
no definido</span></b> </span></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Salida</span><span
lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;color:black'>:</span><b><span
lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;color:#002060'>
</span></b><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;
color:#0070C0'> ------------------------</span></b></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Hospedaje:
</span><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;
color:#0070C0'> {{ $customer->habitacion }} </span></b></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Ocupación:
<u></u></span><b><u><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;
color:#0070C0'>_</span></u></b><b><span lang=ES style='font-size:14.0pt;
font-family:"Calibri",sans-serif;color:#0070C0'>{{ $customer->adultos }}_Adultos __{{ $customer->menores }}__menores (edades: {{ $customer->edadmenores }})</span></b></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Plan
de viaje: </span><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;
color:#0070C0'>{{ $customer->plan=="E"? "Europeo" : "Todo incluido" }}</span></b></p>
@php         
setlocale(LC_TIME,"es_MX");
$fecha_venta = strftime('%d/%B/%G' , strtotime($customer->fecha_venta));
@endphp
<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Fecha
de compra del paquete: </span><b><span lang=ES style='font-size:14.0pt;
font-family:"Calibri",sans-serif;color:#0070C0'>{{ $fecha_venta }}</span></b></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Observaciones:
</span><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif;
color:#0070C0'>{{ $customer->observaciones }}</span></b></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>&nbsp;</span></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>&nbsp;</span></p>

<p class=Default style='text-align:justify'><span lang=ES style='font-size:
14.0pt;font-family:"Calibri",sans-serif'>Para programar sus vacaciones tiene
que ponerse en contacto con su ejecutiva de ventas: <b> {{ $customer->executive->nombre }} </b> al
número al 322 2260404 ext. <b><i>{{ $customer->executive->extension }} </i></b>de 9 a.m. a 3 p.m. de lunes a
viernes. O al correo </span><i><u><span lang=ES style='font-size:14.0pt;
font-family:"Calibri",sans-serif'>{{ $customer->executive->email }}</span></u></i></p>

<p class=Default><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>&nbsp;</span></p>

<p class=Default><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>&nbsp;</span></b></p>

<p class=Default><b><span lang=ES style='font-size:14.0pt;font-family:"Calibri",sans-serif'>Cuenta
con 1 año de vigencia para programas sus vacaciones.</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span
style='font-size:9.0pt;line-height:115%;font-family:"Tahoma",sans-serif'>&nbsp;</span></p>

<p class=MsoBodyText style='text-align:justify'>* Se deberá de presentar esta
carta de confirmación firmada en recepción para un registro exitoso.</p>

<p class=MsoBodyText style='text-align:justify'>*Es posible que algunos 
representantes de otros hoteles  le ofrezcan el servicio, le recomendamos no
atender a esas indicaciones, y solo seguir nuestras instrucciones para evitar
confusiones a su llegada. </p>

<p class=MsoBodyText style='text-align:justify'>* El numero de Pre- reservación
es válido exclusivamente en el departamento de Net Center para verificar
reservación deberá de ponerse en contacto a las ext. 6930 a las 6935.</p>

<p class=MsoBodyText style='text-align:justify'>*El tipo de habitación está
sujeto  a disponibilidad</p>

<p class=MsoNormal>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=166 height=27></td>
 </tr>
 <tr>
  <td></td>
  <td width=273 height=85 bgcolor=white style='vertical-align:top;background:
  white'><span style='position:absolute;z-index:251660288'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td>
    <div style='padding:3.6pt 7.2pt 3.6pt 7.2pt'>
    <div style='border:none;border-bottom:solid windowtext 1.5pt;padding:0cm 0cm 1.0pt 0cm'>
    <p class=MsoNormal style='border:none;padding:0cm'><span lang=ES>&nbsp;</span></p>
    </div>
    <p class=MsoNormal><span lang=ES>                       Firma del cliente</span></p>
    </div>
    </td>
   </tr>
  </table>
  </span>&nbsp;</td>
 </tr>
</table>

<span style='font-size:10.0pt;line-height:115%;font-family:"Tahoma",sans-serif'>&nbsp;</span></p>

</div>

</body>

</html>
