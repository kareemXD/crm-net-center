<script type="text/javascript">
	$(function(){
            inicializeLibraries();
            inicializePriceFormatNull();
			var $table = inizializeDataTable();
            //VALIDAR EL OPERADOR DE ENTRE QUE FECHAS Y QUE FECHAS FILTRAR
            $("#page-loader").fadeOut(500);
            $(".dataTable").attr("style", "width:100%");
            $("select.operator").on("change", function(event){
                $this = $(this);
                if($this.val() != "2") {
                    $this.parent().next().next().slideUp(500);
                }else{
                    $this.parent().next().next().slideDown(500);
                }
            });

            //fin de validar select
            $('#search_form').on('submit', function(e) {
                $table.draw();
                PNotify.removeAll();
                new PNotify({
                    title: "Listo",
                    text: "Filtro aplicado.",
                    type: "info"
                });
                e.preventDefault();
            });
            setInterval(() => {
                $.fn.dataTable.ext.errMode = 'none';
                $table.draw(false);
                $table.ajax.reload(null, false);
                $(".dataTable").attr("style", "width:100%");
                getNotificationsToday();
            }, 10000)
            //agregar tags al select2
            $(".select2-multiple").select2({
                tags: true
            });
			$('#modalDelete').on('shown.bs.modal', function (event) {
				var button = $(event.relatedTarget);
				var id = $(button).data("clientId");
				$("#doDelete").data("id", id);
			});
			$(document).on("click", "#doDelete", function(e){
				var route = $(this).data("route");
				var id = $(this).data("id");
				var token = $(this).data("token");
				$.ajax({
					"type": "post",
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
					"url": route,
					"data": {"id": id, "_token": token },
					"success": function(response){
						new PNotify({
						  title: "Correcto",
						  text: "El cliente/prospecto fué eliminado correctamente.",
						  type: "info"
						});
					}
				})
				var row = $("button[data-client-id='"+id+"']");
				row = row.parent().parent();
				row.remove();
				$('#modalDelete').modal("toggle");
			});
		});

        $(document).on("change","#edit_origen", function(event){
            $route = $('option:selected', this).data('route');
            $("#sub_origen_edit").load($route, function(res){
                inicializeLibraries();
                inicializeInputs();
            });
        });

        $(document).on("change", "#edit_country", function(event){
            $route = $('option:selected', this).data('route');
            $("#edit_state").load($route, function(res){
                inicializeLibraries();
                inicializeInputs();
            })
        });

        $('#filter_head').on("click", function(event){
            $filter_body = $('#filter_body');
            $filter_body.slideToggle(400);
        });
        $("#btn_clean_filter").on("click", function(event){
            cleanAdvancedFilter();
            $table.draw();
            $table.ajax.reload(null, false);
        });

        //cargar vista de modificar cliente 
		$('#modalEdit').on('shown.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var route = $(button).data("route");
            $("#modalEdit").data("route", route);
            $(this).find('.modal-body').prepend('<div id="page-loader"><span class="preloader-interior"></span></div>');
            $("page-loader").fadeIn(500);
			$(this).find('.modal-body').load(route, function(response, status, xhr){
                if(status == "error"){
                    $("#modalEdit").modal("toggle");
                    new PNotify({
                      title: "Error",
                      text: "Problemas al cargar, favor de refrescar la página.",
                      type: "error"
                    });
                    return;
                }
                inicializeEdit();
                 $("#page-loader").fadeOut(500)
                if($("input[name='has-note']:checked").val() == "S"){
                    var route = $("input[name='has-note']").data("route");
                    $("#notas").load(route, function(){
                        inicializeLibraries();
                    });
                }else{
                    document.getElementById("notas").innerHTML="";
                }
            });

		});
        $('#modalEdit').on('hidden.bs.modal', function (event) {
            $(this).find('.modal-body').empty();
        });
		function inicializeEdit(){
            //inicialize functions
            inicializeCheckCategories();
            reiniPriceFormat();
            inicializePriceFormat();
            inicializeLibraries();
            inicializeInputs();

            $(document).on("keyup", ".price-format", function(event){
                var totalCertificate = parseFloat($("#totalCertificate").val().replace("$", "").replace(",", ""));
                var totalHotel = parseFloat($("#totalHotel").val().replace("$", "").replace(",", ""));
                var paying = parseFloat($("#paidOut").val().replace("$", "").replace(",", ""));
                var total = (totalCertificate + totalHotel);
               
                if(paying > total)
                {
                    $("#paidOut").val(0);
                }else{
                        $("#totalPrice").val('$' + total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                        $("#pendingPayment").val('$' + (total-paying).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                }
            });
            $(document).on('click','#confirmation_section', function(e) {
                var i = $(this).find("i").first();
                if(i.hasClass("fa-minus-circle")){
                    i.removeClass("fa-minus-circle");
                    i.addClass("fa-plus-circle")
                }else{
                    i.removeClass("fa-plus-circle");
                    i.addClass("fa-minus-circle")
                }
            });
            //mostrar la seccion de notas 
            $(document).on("change", "input[name='has-note']", function(event){
                if($(this).val() == "S"){
                    var route = $(this).data("route");
                    $("#notas").load(route, function(){
                        inicializeLibraries();
                    });
                }else{
                    document.getElementById("notas").innerHTML="";
                }
            });
            //eventos al enviar un formulario
            $(document).on("submit", "form.edit", function(event){
                event.preventDefault();
                if(validateForm()) {
                    var customer_id;
                    $.each($(".price-format"), function(index, element){
                        var value = parseFloat($(element).val().replace("$", "").replace(",", ""));
                        $(element).val(value);
                    });
                    var dataArray = [];
                    $.each($("form.edit"), function(index, form){
                        var data = $(form).serializeArray();
                        var btn = $(form).find("button[type='submit']");
                        var loadingText = $(btn).data('loadingText');
                        if ($(btn).html() !== loadingText) {
                          $(btn).data('original-text', $(btn).html());
                          $(btn).html(loadingText);
                          $(btn).prop("disabled", true)
                        }
                       //send form ajax
                        dataArray = dataArray.concat(data);
                        setTimeout(function () {
                            $(btn).html($(btn).data('original-text'));
                            $(btn).prop("disabled", false)
                        }, 3000);
                    });
                    var submit = $.ajax({
                        method: $(this).attr("method"),
                        headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $(this).attr("action"),
                        data: dataArray,
                        error: function(response){
                                new PNotify({
                                    title: 'Error',
                                    text: "Ocurrió un error desconocido",
                                    type: "warning"
                                });
                        },
                        success: function(response){

                            if(JSON.parse(response) !== false) {
                                sendDropZone(dropzone);
                                PNotify.removeAll()
                                new PNotify({
                                    title: 'Completado',
                                    text: "Se actualizaron los datos correctamente",
                                    type: "success"
                                });
                            }else{
                                PNotify.removeAll()
                                new PNotify({
                                    title: 'Error',
                                    text: "No se encontró el cliente que intenta modificar",
                                    type: "warning"
                                });
                                return false;   
                            }
                        }
                    });
                    reiniPriceFormat();
                }else{
                    new PNotify({
                          title: 'Alerta',
                          text: "Asegurese de haber llenado correctamente los campos Teléfono, nombre y correo electrónico",
                          type: "warning"
                    });
                }
            });
		}
        //fin del jquery inicialize
        function inicializeCheckCategories() {
            var categoryCheck = $("input[name='category-client']");
            var checked = false;
            $.each(categoryCheck, function(i,v){
                if($(v).attr("checked") == "checked"){
                    checked = true;
                }
            });
            if(checked === false){
                 $("input[name='category-client'][value='S']").first().attr("checked", "checked");
            }
        }

    function inicializePriceFormat() {
        $(".price-format").priceFormat({
            prefix: '$',
            thousandsSeparator: ',',
            allowNegative: false
        });
        $(".price-format").attr("maxlength","11");
    }
    function inicializePriceFormatNull() {
        $(".price-format-null").priceFormat({
            prefix: '$',
            thousandsSeparator: ',',
            allowNegative: false,
            clearOnEmpty: true
        });
        $(".price-format").attr("maxlength","11");
    }
    function reiniPriceFormat() {
        $.each($(".price-format"), function(index, element){
            var value = parseFloat($(element).val());
            $(element).val('$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
    }

//validaciones
    function validateForm() {
        var flag = true;
        flag = validateName(flag)
        flag = validatePhone(flag);
        flag = validateEmail(flag);
        if($("input[name='has-note']:checked").val() == "S"){
            flag = validateNotes(flag);
        }

        return flag;
    }

    function validateName(flag) {
        //validar nombre
        var name = $("input[name='customer-name']");
        if($(name).val() == ""){
            flag= false;
            if(!$(name).parent().hasClass('has-error')) {
                $(name).parent().addClass('has-error');
                $(name).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else{
            if($(name).parent().hasClass('has-error')) {
                $(name).parent().removeClass('has-error');
                $(name).parent().find('.help-block').slideUp();
                $(name).parent().find('.help-block').remove();
            }
        }

        return flag;
    }

    function validatePhone(flag) {
        var cell_phone = $("input[name='cell-phone']");
        var home_phone = $("input[name='home-phone']");
        var office_phone = $("input[name='office-phone']");
        if($(cell_phone).val()=="" && $(home_phone).val()=="" && $(office_phone).val()=="") {
            flag=false;
            if(!$(cell_phone).parent().hasClass('has-error')){
                $(cell_phone).parent().addClass("has-error");
                $(home_phone).parent().addClass("has-error");
                $(office_phone).parent().addClass("has-error");
                $(cell_phone).parent().append('<span class="help-block">Debe ingresar al menos un teléfono</span>');
            }
        }else{
            if($(cell_phone).parent().hasClass('has-error')) {
                $(cell_phone).parent().removeClass('has-error');
                $(home_phone).parent().removeClass('has-error');
                $(office_phone).parent().removeClass('has-error');
                $(cell_phone).parent().find('.help-block').remove();
            }
        }
        return flag;
    }

    function validateEmail(flag) {
        var email = $("input[name='customer-email']");
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if($(email).val() == ""){
            flag = false;
            if(!$(email).parent().hasClass('has-error')) {
                $(email).parent().addClass('has-error');
                $(email).parent().append('<span class="help-block">El correo es obligatorio</span>');
            }else{
                $(email).parent().find('.help-block').remove();
                $(email).parent().append('<span class="help-block">El correo es obligatorio</span>');
            }
        }else{
            if(re.test($(email).val())){
                if($(email).parent().hasClass('has-error')) {
                    $(email).parent().removeClass('has-error');
                    $(email).parent().find('.help-block').remove();
                }
            }else{
                if(!$(email).parent().hasClass('has-error')) {
                    $(email).parent().addClass('has-error');
                    $(email).parent().append('<span class="help-block">Ingrese un correo válido</span>');
                    flag = false;
                }else{
                    $(email).parent().find('.help-block').remove();
                    $(email).parent().append('<span class="help-block">Ingrese un correo válido</span>');
                }
            }
        } 
        return flag;
    }

    function validateNotes(flag){
        var fecha = $('input[name="date-hour"]');
        var title = $("input[name='note-title']");
        var noteDetails = $("textarea[name='note-details']");
        if($(fecha).val() == "") {
            flag = false;
            if(!$(fecha).parent().hasClass('has-error')){
                $(fecha).parent().addClass('has-error');
                $(fecha).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(fecha).parent().hasClass('has-error')){
                $(fecha).parent().removeClass('has-error');
                $(fecha).parent().find('.help-block').remove();
            }
        }
        if($(title).val() == "") {
            flag = false;
            if(!$(title).parent().hasClass('has-error')) {
                $(title).parent().addClass('has-error');
                $(title).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(title).parent().hasClass('has-error')){
                $(title).parent().removeClass('has-error');
                $(title).parent().find('.help-block').remove();
            }
        }
        if($(noteDetails).val() == "") {
            flag = false;
            if(!$(noteDetails).parent().hasClass('has-error')) {
                $(noteDetails).parent().addClass('has-error');
                $(noteDetails).parent().append('<span class="help-block">Campo obligatorio</span>');
            }
        }else {
            if($(noteDetails).parent().hasClass('has-error')){
                $(noteDetails).parent().removeClass('has-error');
                $(noteDetails).parent().find('.help-block').remove();
            }
        }
        if(!flag){
            new PNotify({
                title: 'Recordatorio',
                text: 'Llene todos los campos de las notas',
                type :'warning'
            });
        }
        return flag;
    }
    //fin validaciones

    function inicializeLibraries() {

        $('.modal-body').find('.select2').select2();
            setTimeout(function(){
                $('.text-red.visible').slideUp(8000, function(){
                    $('.text-red.visible').removeClass('visible');
                });

            }, 5000);

            $('.date-picker').datepicker({
                'format': 'yyyy-mm-dd',
            });
            $('.date-time-picker').datetimepicker({
                format:'YYYY-MM-DD hh:mm:00 a',
                locale: 'es'
            });

            jQuery(function($) {
                $('[data-numeric]').payment('restrictNumeric');
                $('.cc-number').payment('formatCardNumber');
                $('.cc-exp').payment('formatCardExpiry');
                $('.cc-cvc').payment('formatCardCVC');

                $.fn.toggleInputError = function(erred) {
                    this.parent('.form-group').toggleClass('has-error', erred);
                    return this;
                };
            });
    }

    function inicializeInputs() {
        $(document).on('keyup', 'input[name="customer-name"]', function(event){validateName(true);});
        $(document).on('keyup', 'input[name="cell-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="home-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="office-phone"]', function(event){validatePhone(true);});
        $(document).on('keyup', 'input[name="customer-email"]', function(event){validateEmail(true);});
    }
    function inizializeDataTable() {
    $table = $(".table").DataTable({
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            ajax: {
                url: '{{ route("api.customer") }}',
                data: function (d) {
                    //ejecutivo
                    d["executive"] = $('select#executive_name').val();
                    d["operator-executive"] = $('select[name="operator-executive"]').val();
                    //checkin
                    d["filter-check-in"] = $('input[name="filter-check-in"]').val();
                    d["check-in-2"] = $('input[name="check-in-2"]').val();
                    d["operator-check-in"] = $('select[name="operator-check-in"]').val();
                    //checkout
                    d["filter-check-out"] = $('input[name="filter-check-out"]').val();
                    d["check-out-2"] = $('input[name="check-out-2"]').val();
                    d["operator-check-out"] = $('select[name="operator-check-out"]').val();
                    //pendiente
                    d["operator-pending-payment"] = $("select[name='operator-pending-payment']").val();
                    d["filter-pending-payment"] = $("input[name='filter-pending-payment']").val();     
                    //pagado                   
                    d["operator-paid-out"] = $("select[name='operator-paid-out']").val();
                    d["filter-paid-out"] = $("input[name='filter-paid-out']").val();
                    //checar si tiene documentos firmados
                    d["filter-has-signed"] = $("input[name='filter-has-signed']:checked").val();
                    //checar el tipo categoria
                    d['operator-category-type'] = $("select[name='operator-category-type']").val();
                    d['category-type'] = $("select[name='category-type']").val();
                    //elegir un orden
                    d['customer-order'] = $("select[name='customer-order']").val();
                    d['operator-customer-order'] = $("select[name='operator-customer-order']").val();
                }
            },
            "columns": [
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'checkin', "width": "115px" },  
                {data: 'nombre'},
                {data: 'emails' ,orderable: false, searchable: false},
                {data: 'telefonos', orderable: false, searchable: false},
                {data: 'tipo_q', "width": "115px"  },
                {data: 'folioreserva'},
                {data: 'plan'},
                {data: 'executiveName', name:"ejecutivos.nombre"},
                {data: 'categoryName', name:'categories.name'},
                {data: 'delete', name:'delete', orderable: false, searchable: false},
            ],
            "pageLength": 25,
            "language":{
                "decimal":        "",
                "emptyTable":     "No hay datos disponibles en esta tabla",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                "infoFiltered":   "(filtrado de _MAX_ total registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No encontrados en la tabla",
                "paginate": {
                    "first":      "Primer",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            }
        });

        return $table;
    }
    //limpiar filtros
    function cleanAdvancedFilter() {
        $('select#executive_name').select2("val", "[]");
        //checkin
        $('input[name="check-in"]').val("");
        $('input[name="check-in-2"]').val("");
        //checkout
        $('input[name="check-out"]').val("");
        $('input[name="check-out-2"]').val("");
        //pendiente
        $("input[name='pending-payment']").val("");     
        //pagado                   
        $("input[name='paid-out']").val("");
        //checar si tiene documentos firmados
        $("input[name='has-signed']:checked").prop("checked", false);
        //checar el tipo categoria
        $("select[name='category-type']").select2("val", "[]");
        //elegir un orden
        $("select[name='customer-order']").val("");
    }
</script>