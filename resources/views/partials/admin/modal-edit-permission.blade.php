<!--editar un permiso -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog  widget-container">
        <div class="modal-content">
            <form id="updateForm" action="#" method="put">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modificar permiso</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="" class="control-label">Código de permiso</label>
                                <input id="permission" class="form-control" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label" ></label>
                                <textarea class="form-control" name="description" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Editar permiso-->
<script type="text/javascript">
    $(function(){
        //vars
        $("#updateForm").on("submit", function(event){
            event.preventDefault();
            $form = $(this);
            $.ajax({
                url: $form.attr("action"),
                method : $form.attr("method"),
                data : $form.serializeArray()
            }).done(function(message){
                message = (typeof(message) == "string")? JSON.parse(message) : {type: "error", title: "Error Desconocido", text: "No se pudo hacer la conexión"};
                new PNotify({
                          title: message.title,
                          text: message.message,
                          type: (message.type).toLowerCase()
                    });
            });
        });
    });
</script>