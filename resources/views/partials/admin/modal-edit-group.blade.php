<!--editar un permiso -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog  widget-container">
        <div class="modal-content">
            <form id="updateForm" action="#" method="put">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modificar Grupo</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="description" class="control-label" >Descripción</label>
                                <input type="text" class="form-control" name="description" >
                            </div>
                            <div class="form-group">
                                <label for="permission" class="control-label" >Permisos</label>
                                <select data-tags="true" multiple name="permission[]" id="permission" class="select2-selection--multiple form-control">
                                    @foreach($permissions as $permission)
                                    <option value="{{ $permission->id }}">{{ $permission->permission }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="edit_level" class="control-label">Nivel (Entre mas alto el número nivel )</label>
                                <input name="edit-level" id="edit_level" type="number" min="0" max="100" class="form-control" placeholder="Ingrese un número">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Editar permiso-->
<script type="text/javascript">
    $(function(){
        $(".select2-selection--multiple").select2({
            tags: "true"
        });
        $("#updateForm").on("submit", function(event){
            event.preventDefault();
            $this = $(this);
            level = $("meta#user_level").attr("content");

            if(!(parseInt(level) > parseInt($("input[name='edit-level']").val()))){
                $("input[name='edit-level']").attr("style='border-color:#dc3545;'");
                new PNotify({
                    title: "ups",
                    text: "No puedes poner un nivel mayor al asignado a tu rol",
                    type:"warning"
                });
                return;
            }else{
                $("input[name='edit-level']").attr("style=''");
            }
            $.ajax({
                type: $this.attr("method"),
                url: $this.attr("action"),
                data : $this.serializeArray(),
                success: function(response) {
                    try {
                    var message = JSON.parse(response);
                    new PNotify({
                          title: message.title,
                          text: message.message,
                          type: (message.type).toLowerCase()
                    });
                    $button.data("description", message.description);
                    $button.data("permissions", message.permissions);
                    $button.data("edit-level", message.level);
                    $button.parent().next().next().html(message.level);
                    $button.parent().next().next().next().html(message.description);

                    } catch(err) {
                        new PNotify({
                            title: "Ups!",
                            text: "hubo un problema al guardar",
                            type: "warning"
                        });
                    }
                },
                error: function(response){
                    new PNotify({
                            title: "Ups!",
                            text: "hubo un problema al guardar",
                            type: "warning"
                        });
                    }
            })
        });
    });

</script>