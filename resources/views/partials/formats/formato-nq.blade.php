<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<title>Forma de Calificación R5</title>
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:"Estrangelo Edessa";
  panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:"Arial Unicode MS";
  panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
  {font-family:"Century Gothic";
  panose-1:2 11 5 2 2 2 2 2 2 4;}
@font-face
  {font-family:Cambria;
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
a:link, span.MsoHyperlink
  {color:blue;
  text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
  {color:#954F72;
  text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
  {mso-style-link:"Texto de globo Car";
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:8.0pt;
  font-family:"Tahoma",sans-serif;}
span.TextodegloboCar
  {mso-style-name:"Texto de globo Car";
  mso-style-link:"Texto de globo";
  font-family:"Tahoma",sans-serif;}
p.Default, li.Default, div.Default
  {mso-style-name:Default;
  margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:12.0pt;
  font-family:"Calibri",sans-serif;
  color:black;}
 /* Page Definitions */
 @page WordSection1
  {size:612.0pt 792.0pt;
  margin:36.0pt 36.0pt 36.0pt 36.0pt;}
div.WordSection1
  {page:WordSection1;}
 /* List Definitions */
 ol
  {margin-bottom:0cm;}
ul
  {margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ES-MX link=blue vlink="#954F72">

<div class=WordSection1>

<p class=MsoNormal>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=291 height=0></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=90 height=73
  src="{{  asset('formats/formato-nq_archivos/image002.jpg') }}"></td>
 </tr>
</table>

<br clear=ALL>
</p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
14.0pt;font-family:"Arial",sans-serif;color:#666699'>           Programa de Invitados
VIP      <b> </b></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
5.0pt;font-family:"Arial",sans-serif;color:#666699'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:red'>Favor
de firmar después de leer en su totalidad el presente documento:                                                                                 </span></b></p>

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="98%"
 style='width:98.78%;border-collapse:collapse;border:none'>
 <tr style='height:25.4pt'>
  <td width="13%" colspan=2 style='width:13.4%;border:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal style='text-align:center'><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>NOMBRE TITULAR:</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{
  $customer->nombre }}</span></p>
  </td>
  <td width="13%" valign=bottom style='width:13.3%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN</span></b><b><span
  lang=EN-GB style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>:</span></b></p>
  </td>
  <td width="21%" colspan=4 valign=bottom style='width:21.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Century Gothic",sans-serif'>{{$customer->ocupacion}}</span></p>
  </td>
  <td width="6%" valign=bottom style='width:6.64%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{$customer->edad}}</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>NOMBRE
  ACOMPAÑANTE :</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{$customer->nombre_co}}</span></p>
  </td>
  <td width="13%" style='width:13.3%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN:</span></b></p>
  </td>
  <td width="21%" colspan=4 style='width:21.2%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{$customer->ocupacion_co}}</span></p>
  </td>
  <td width="6%" style='width:6.64%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD:</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{$customer->edad_co}}</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>ESTADO
  CIVIL:</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.54%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ @$customer->maritalStatus->nombre }}</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.66%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>ADULTOS: ({{$customer->adultos}})</span></b></p>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>MENORES: ({{$customer->menores}})</span></b></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Edades
  de los menores:{{$customer->edadmenores}}</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'> </span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>DIRECCION:  {{ $customer->address }}
  </span></b></p>
  </td>
  <td width="14%" colspan=3 valign=bottom style='width:14.98%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></p>
  </td>
  <td style='border:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt' width="34%"
  colspan=4><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif'>Numero
  Tarjeta de crédito con la que se realizara el pago:             </span></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:red'>{{$customer->document_card}} {{$customer->banco}}</span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Código De
  seguridad</span></b></p>
  <p class=MsoNormal><b><span lang=ES align=center style='text-align:center;font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:red'>{{$customer->codigo}}</span></b></p>
  </td>
  <td width="11%" colspan=2 style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Fecha
  de vencimiento: </span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:red'>
  {{ $customer->expiration }}
</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Monto a
  cargar: <br>  <span style="color:red">$ {{number_format($customer->pagado)}}</span></span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
  style='font-size:8.0pt;font-family:"Estrangelo Edessa",serif;color:red'>&nbsp;</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt'>Cuantas veces
  vacaciona  normalmente al año:</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt'>{{ $customer->many_times }}</span></b></p>
  </td>
  <td width="11%" colspan=2 style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Tiene
  propiedad vacacional</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>No ( {{ ($customer->has_vacation_ownership)? "" : "X" }} )     
  Si  ( {{ ($customer->has_vacation_ownership)? "X" : "" }}  )</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:19.5pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;color:black'>Número de tarjetas de crédito que manejan   
  1 ( {{($customer->num_tarjetas ==1)? "X": "" }} )        2 ( {{($customer->num_tarjetas
  ==2)? "X": "" }}   )      Mas de dos ( {{($customer->num_tarjetas >2)?
  "X": "" }}   )</span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'>Tipos de
  tarjeta de crédito que maneja: Visa( <span style='mso-spacerun:yes'> {{ ($customer->visa == "S")? "x": "" }} </span>)<span
  style='mso-spacerun:yes'>  </span>Master <span class=SpellE>Card</span> ( <span
  style='mso-spacerun:yes'>  {{($customer->mc == "S")? "x": "" }} </span>) American Express( <span
  style='mso-spacerun:yes'> {{($customer->amex == "S")? "x": "" }} </span>)<span style='mso-spacerun:yes'>  </span><span
  style='mso-spacerun:yes'> </span><o:p></o:p></span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:#7030A0'>{{$customer->habitacion}}</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="6%" valign=bottom style='width:6.9%;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  none;padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Correo
  electrónico</span></b></p>
  </td>
  <td width="43%" colspan=4 valign=bottom style='width:43.62%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span style='font-size:
  6.0pt;font-family:"Century Gothic",sans-serif;color:black'>:</span></b><span
  style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;color:black'>
  {{$customer->email_decrypt}}</span></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'>Números telefónicos: Casa                         Oficina          
                 Celular:</span></b></p>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'> {{$customer->tel_casa_decrypt}}         
  {{$customer->tel_oficina_decrypt}}     {{$customer->tel_cel_decrypt}}                  
  </span></b></p>
  </td>
 </tr>
 <tr style='height:20.1pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de Entrada</span></b></p>
  </td>
  <td width="21%" colspan=2 valign=bottom style='width:21.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{
  substr($customer->checkin, 0, 10) }}</span></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de salida</span></b></p>
  </td>
  <td width="13%" colspan=2 valign=bottom style='width:13.7%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{
  substr($customer->checkout, 0, 10) }}</span></p>
  </td>
  <td width="11%" colspan=2 valign=bottom style='width:11.6%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>CIUDAD/PAIS</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>{{$customer->estado}}</span></p>
  </td>
 </tr>
 <tr height=0>
  <td width=48 style='border:none'></td>
  <td width=38 style='border:none'></td>
  <td width=87 style='border:none'></td>
  <td width=67 style='border:none'></td>
  <td width=104 style='border:none'></td>
  <td width=96 style='border:none'></td>
  <td width=3 style='border:none'></td>
  <td width=9 style='border:none'></td>
  <td width=69 style='border:none'></td>
  <td width=50 style='border:none'></td>
  <td width=38 style='border:none'></td>
  <td width=88 style='border:none'></td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Estrangelo Edessa",serif;color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Paquete
Vacacional: </span></b><span lang=ES-TRAD style='font-size:8.0pt;font-family:
"Cambria",serif;color:black'>    {{ ($customer->noches_en_certificado >
0)? $customer->noches_en_certificado." noches" :
(($customer->noches_en_hotel > 0)? $customer->noches_en_hotel."
noches": "") }}                                              </span><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Plan:<span
style='color:red'> </span></span></b><span lang=ES-TRAD style='font-size:8.0pt;
font-family:"Cambria",serif;color:red'> </span><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>{{
($customer->plan =="T")? "Todo incluido":( ($customer->plan
=="E")? "Europeo": "") }}</span><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;color:red'>                                                   <b>  </b></span><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Numero de
personas{{ $customer->adultos + $customer->menores }}&nbsp;<span
style='color:black'>Adultos: {{$customer->adultos}}</span></span></b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>  <b>Menores:
{{$customer->menores}} TOTAL: $ {{number_format($customer->precio_total)
}}</b></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Observaciones: {{$customer->observaciones}}</span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Favor de Leer
todos Términos y Condiciones antes de firmar</span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;color:black'><br>
</span><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>1.
No hay reembolso por la tarifa promocional, sin embargo, dispone de un año
desde la fecha de compra para programar o reprogramar su viaje, puede hacer la
reprogramación, 1 mes de anticipación en temporada alta o 1 semana de
anticipación en temporada baja a su llegada, en caso de no presentarse el día
de su llegada, pierde la vigencia de un año y aplicaría costo de reactivación,
Certificado y habitación sujetos a disponibilidad. Semana santa, pascua,
navidad y año nuevo puede aplicar complemento de reservación. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>2. El check-in
es a las 15:00 hrs. El check out es a las 12:00 hrs. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>3. Si requiere
factura anexar el documento adjunto con los datos completos, por la nueva
disposición de Hacienda y Crédito Público, para factura electrónica se requiere
la información el mismo día de su compra para procesarla al departamento de
contabilidad, siendo este el único día para poder expedir su factura.</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><em><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>&nbsp;</span></em></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 align=left
 style='border-collapse:collapse;border:none;margin-left:4.8pt;margin-right:
 4.8pt'>
 <tr style='height:17.3pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.3pt'>
  <p class=Default><b><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>NOMBRE
  DE CUENTA </span></b></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.3pt'>
  <p class=Default><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>BUGANVILIAS
  RESORT VACATION CLUB S.A. DE C.V. </span></p>
  </td>
 </tr>
 <tr style='height:9.0pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default><b><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'> DE
  CUENTA </span></b></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>032
  0033 4014 </span></p>
  </td>
 </tr>
 <tr style='height:9.0pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default><b><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>CLAVE
  INTERBANCARIA </span></b></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>044
  3750 3200 3340 141 </span></p>
  <p class=Default><span lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'>SCOTIABANK</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='line-height:115%'><b><span lang=ES-TRAD
style='font-size:9.0pt;line-height:115%;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='line-height:115%'><b><span lang=ES-TRAD
style='font-size:9.0pt;line-height:115%;font-family:"Cambria",serif;color:black'>Para
validar la promoción es necesario nos envié los siguientes documentos: </span></b></p>

<p class=MsoNormal><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Formato de Reservación firmado.</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=EN-US style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Boucher Firmado.</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Copia de su identificación por ambos lados.</span></p>

<p class=Default style='margin-left:36.0pt'><span lang=ES>&nbsp;</span></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=ES-TRAD
style='font-size:7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Nombre y Firma:</span><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>     <u>________________________________________</u></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>                                               
</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>                                                                                               
                                                                                                    </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>Fecha:</span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'> {{(strlen($customer->fecha_venta)
>= 10)? substr($customer->fecha_venta, 0, 10): '0000-00-00'}}</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif;color:#0070C0'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Asesor Vacacional: {{
(!is_null(Auth::user()->executive))? Auth::user()->executive->nombre
:"Sin ejecutivo" }}                                  </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>                                
                                                                                        </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>E-mail:  {{
Auth::user()->email }}</span></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>La firma de éste formato
debe ser idéntica a la firma plasmada en la tarjeta de crédito. (En caso de
realizar el pago con tarjeta de crédito) Enviar vía fax, al 01 322 22 6 04 18 o
por correo electrónico.</span></p>

<p class=MsoNormal><b><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif;
color:#2E15E9'>&nbsp;</span></b></p>

</div>

</body>

</html>
