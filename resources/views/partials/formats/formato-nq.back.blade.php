<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List href="formato-nq_archivos/filelist.xml">
<link rel=Edit-Time-Data href="formato-nq_archivos/editdata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Forma de Calificación R5</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Club Vacacional</o:Author>
  <o:LastAuthor>htainf04</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>15</o:TotalTime>
  <o:LastPrinted>2017-09-29T01:33:00Z</o:LastPrinted>
  <o:Created>2017-12-13T18:51:00Z</o:Created>
  <o:LastSaved>2017-12-13T18:51:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>448</o:Words>
  <o:Characters>2465</o:Characters>
  <o:Company>Club Vacacional</o:Company>
  <o:Lines>20</o:Lines>
  <o:Paragraphs>5</o:Paragraphs>
  <o:CharactersWithSpaces>2908</o:CharactersWithSpaces>
  <o:Version>15.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:TargetScreenSize>800x600</o:TargetScreenSize>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="formato-nq_archivos/themedata.thmx">
<link rel=colorSchemeMapping href="formato-nq_archivos/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:PunctuationKerning/>
  <w:DrawingGridHorizontalSpacing>6 pto</w:DrawingGridHorizontalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>2</w:DisplayHorizontalDrawingGridEvery>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>ES-MX</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:UseWord2010TableStyleRules/>
   <w:DontGrowAutofit/>
   <w:DontUseIndentAsNumberingTabStop/>
   <w:FELineBreak11/>
   <w:WW11IndentRules/>
   <w:DontAutofitConstrainedTables/>
   <w:AutofitLikeWW11/>
   <w:HangulWidthLikeWW11/>
   <w:UseNormalStyleForList/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" LatentStyleCount="371">
  <w:LsdException Locked="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="99" SemiHidden="true"
   Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="99" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
@font-face
	{font-family:"\@Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;
	mso-font-charset:128;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-134238209 -371195905 63 0 4129279 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:ES-TRAD;
	mso-fareast-language:ES-TRAD;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-unhide:no;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 212.6pt right 425.2pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:ES-TRAD;
	mso-fareast-language:ES-TRAD;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-unhide:no;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 212.6pt right 425.2pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:ES-TRAD;
	mso-fareast-language:ES-TRAD;}
a:link, span.MsoHyperlink
	{mso-style-unhide:no;
	mso-style-parent:"";
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-unhide:no;
	color:#954F72;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-unhide:no;
	mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:ES-TRAD;
	mso-fareast-language:ES-TRAD;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-parent:"";
	mso-style-link:"Texto de globo";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma",sans-serif;
	mso-ascii-font-family:Tahoma;
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;
	mso-ansi-language:ES-TRAD;
	mso-fareast-language:ES-TRAD;}
p.Default, li.Default, div.Default
	{mso-style-name:Default;
	mso-style-unhide:no;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	mso-layout-grid-align:none;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Calibri;
	color:black;
	mso-ansi-language:ES;
	mso-fareast-language:ES;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("{{ asset('formats/formato-nq_archivos/header.htm') }}") fs;
	mso-footnote-continuation-separator:url("{{ asset('formats/formato-nq_archivos/header.htm') }}") fcs;
	mso-endnote-separator:url("{{ asset('formats/formato-nq_archivos/header.htm') }}") es;
	mso-endnote-continuation-separator:url("{{ asset('formats/formato-nq_archivos/header.htm') }}") ecs;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:36.0pt 36.0pt 36.0pt 36.0pt;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-header:url("{{ asset('formats/formato-nq_archivos/header.htm') }}") h1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:111558743;
	mso-list-type:hybrid;
	mso-list-template-ids:-684811940 201981953 201981955 201981957 201981953 201981955 201981957 201981953 201981955 201981957;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Symbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:"Courier New";}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Wingdings;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Symbol;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:"Courier New";}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Wingdings;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Symbol;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:"Courier New";}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Wingdings;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tabla normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-unhide:no;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
table.MsoTableGrid
	{mso-style-name:"Tabla con cuadrícula";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-unhide:no;
	border:solid windowtext 1.0pt;
	mso-border-alt:solid windowtext .5pt;
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-border-insideh:.5pt solid windowtext;
	mso-border-insidev:.5pt solid windowtext;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2049"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ES-MX link=blue vlink="#954F72" style='tab-interval:35.4pt'>

<div class=WordSection1>

<p class=MsoNormal><!--[if gte vml 1]><o:wrapblock><v:shapetype id="_x0000_t75"
  coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
  filled="f" stroked="f">
  <v:stroke joinstyle="miter"/>
  <v:formulas>
   <v:f eqn="if lineDrawn pixelLineWidth 0"/>
   <v:f eqn="sum @0 1 0"/>
   <v:f eqn="sum 0 0 @1"/>
   <v:f eqn="prod @2 1 2"/>
   <v:f eqn="prod @3 21600 pixelWidth"/>
   <v:f eqn="prod @3 21600 pixelHeight"/>
   <v:f eqn="sum @0 0 1"/>
   <v:f eqn="prod @6 1 2"/>
   <v:f eqn="prod @7 21600 pixelWidth"/>
   <v:f eqn="sum @8 21600 0"/>
   <v:f eqn="prod @7 21600 pixelHeight"/>
   <v:f eqn="sum @10 21600 0"/>
  </v:formulas>
  <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
  <o:lock v:ext="edit" aspectratio="t"/>
 </v:shapetype><v:shape id="_x0000_s1026" type="#_x0000_t75" style='position:absolute;
  margin-left:218.05pt;margin-top:-31.55pt;width:67.3pt;height:55.05pt;
  z-index:251657728'>
  <v:imagedata src="formato-nq_archivos/image001.png" o:title=""/>
  <w:wrap type="topAndBottom"/>
 </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout'>
<center><img width=90 height=73 src="{{  asset('formats/formato-nq_archivos/image002.jpg') }}" v:shapes="_x0000_s1026"></center>
 </span><![endif]><!--[if gte vml 1]></o:wrapblock><![endif]--><br
style='mso-ignore:vglayout' clear=ALL>
<span lang=ES style='font-size:14.0pt;font-family:"Arial",sans-serif;
color:#666699;mso-ansi-language:ES'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
14.0pt;font-family:"Arial",sans-serif;color:#666699;mso-ansi-language:ES'><span
style='mso-spacerun:yes'>           </span>Programa de Invitados VIP<span
style='mso-spacerun:yes'>      </span><b style='mso-bidi-font-weight:normal'><span
style='mso-spacerun:yes'> </span></b><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
5.0pt;mso-bidi-font-size:14.0pt;font-family:"Arial",sans-serif;color:#666699;
mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:red;
mso-bidi-font-family:Arial;'>Favor de proporcionarnos la siguiente
información después de leer los términos y condiciones:<span style='mso-tab-count:
5'>                                                                 </span><o:p></o:p></span></b></p>

<div align="center">

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="98%"
 style='width:98.78%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:
 .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:25.4pt'>
  <td width="13%" colspan=2 style='width:13.4%;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;
  height:25.4pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:EN-GB'>NOMBRE TITULAR:<o:p></o:p></span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p>{{ $customer->nombre }}</o:p></span></p>
  </td>
  <td width="13%" valign=bottom style='width:13.3%;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;
  height:25.4pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES-MX'>OCUPACIÓN</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:Arial;mso-ansi-language:EN-GB'>:<o:p></o:p></span></b></p>
  </td>
  <td width="21%" colspan=4 valign=bottom style='width:21.2%;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
  11.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:ES'><o:p>{{ $customer->ocupacion }}</o:p></span></p>
  </td>
  <td width="6%" valign=bottom style='width:6.64%;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;
  height:25.4pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'>EDAD</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p></o:p></span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p>{{ $customer->edad }}</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;mso-bidi-font-family:Arial;
  mso-ansi-language:ES'>NOMBRE ACOMPAÑANTE :</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p></o:p></span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p>{{ $customer->nombre_co }}</o:p></span></p>
  </td>
  <td width="13%" style='width:13.3%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'>OCUPACIÓN:<o:p></o:p></span></b></p>
  </td>
  <td width="21%" colspan=4 style='width:21.2%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:Arial;mso-ansi-language:ES'><o:p>{{ $customer->ocupacion_co }}</o:p></span></p>
  </td>
  <td width="6%" style='width:6.64%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;
  height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'>EDAD:<o:p></o:p></span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p>{{ $customer->edad_co }}</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'>ESTADO CIVIL:</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p></o:p></span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.54%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  EN-GB'><o:p>{{ (is_null($customer->maritalStatus))? $customer->maritalStatus->nombre : ""  }}</o:p></span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.66%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  EN-GB;white-space: nowrap;'>ADULTOS: ( {{ $customer->adultos }} )<span style='mso-spacerun:yes'>  </span><o:p></o:p></span></b></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  EN-GB;white-space:nowrap;'>MENORES: ( {{ $customer->menores }} )<o:p></o:p></span></b></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'>Edades de los menores:<o:p></o:p></span></b></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'><span style='mso-spacerun:yes'> </span><o:p>{{ $customer->edadmenores }}</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:20.05pt;mso-row-margin-right:34.5%'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'>DIRECCION: <o:p></o:p></span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:14.98%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:Arial;mso-ansi-language:ES'>Numero Tarjeta de crédito
  con la que se realizara el pago:<span style='mso-spacerun:yes'>            
  </span></span><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'><o:p></o:p></span></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;;mso-ansi-language:ES'><span
  style='mso-spacerun:yes'>   </span><span style='mso-spacerun:yes'>  </span><span
  style='mso-spacerun:yes'> </span><u> XXXX XXXX XXXX {{ $customer->four_digits."    ".$customer->banco }}   </u><o:p></o:p></span></b></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;mso-ansi-language:ES'>Código De
  seguridad<o:p></o:p></span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;;mso-ansi-language:ES'>XXX<o:p></o:p></span></b></p>
  </td>
  <td width="11%" colspan=2 style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'>Fecha de vencimiento: <o:p></o:p></span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;;mso-ansi-language:ES'><o:p>{{ $customer->vencimiento1." / ".$customer->vencimiento2 }}</o:p></span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-ansi-language:ES'>Monto a cargar: $</span></b><b style='mso-bidi-font-weight:
  normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Estrangelo Edessa";
  mso-fareast-font-family:"Arial Unicode MS";'><o:p></o:p></span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Estrangelo Edessa";
  mso-fareast-font-family:"Arial Unicode MS";'><o:p>{{ number_format($customer->pagado) }}</o:p></span></b></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;mso-ansi-language:ES'>Cuantas veces vacaciona<span
  style='mso-spacerun:yes'>  </span>normalmente al año<o:p></o:p></span></b></p>
  </td>
  <td width="11%" colspan=2 style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  Arial;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;mso-bidi-font-family:Arial;
  mso-ansi-language:ES'>Tiene propiedad vacacional<o:p></o:p></span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;mso-bidi-font-family:Arial;
  mso-ansi-language:ES'>No (<span style='mso-spacerun:yes'>  </span>)<span
  style='mso-spacerun:yes'>      </span>Si<span style='mso-spacerun:yes'> 
  </span>(<span style='mso-spacerun:yes'>   </span>)<o:p></o:p></span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif;mso-bidi-font-family:Arial;
  mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:19.5pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;color:black;mso-ansi-language:
  ES'>Número de tarjetas de crédito que manejan<span
  style='mso-spacerun:yes'>    </span>1 (<span style='mso-spacerun:yes'> @if($customer->num_tarjetas == 1) x @endif
  </span>)<span style='mso-spacerun:yes'>        </span>2 ( <span
  style='mso-spacerun:yes'> </span><span
  style='mso-spacerun:yes'>@if($customer->num_tarjetas == 2) x @endif </span>)<span style='mso-spacerun:yes'>     
  </span>Mas de dos ( <span style='mso-spacerun:yes'> @if($customer->num_tarjetas > 2) x @endif </span><span
  style='mso-spacerun:yes'>  </span>)<o:p></o:p></span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'>Tipos de
  tarjeta de crédito que maneja: Visa( <span style='mso-spacerun:yes'> @if($customer->visa == "S") x @endif </span>)<span
  style='mso-spacerun:yes'>  </span>Master <span class=SpellE>Card</span> ( <span
  style='mso-spacerun:yes'>  @if($customer->mc == "S") x @endif </span>) American Express( <span
  style='mso-spacerun:yes'> @if($customer->amex == "S") x @endif </span>)<span style='mso-spacerun:yes'>  </span><span
  style='mso-spacerun:yes'> </span><o:p></o:p></span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><b
  style='mso-bidi-font-weight:normal'><span lang=ES style='font-size:8.0pt;
  mso-bidi-font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  "Courier New";color:#7030A0;mso-ansi-language:ES'> {{ $customer->habitacion }} </span></b><b style='mso-bidi-font-weight:normal'><span
  lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:#7030A0;mso-ansi-language:ES'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:20.05pt'>
  <td width="6%" valign=bottom style='width:6.9%;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b
  style='mso-bidi-font-weight:normal'><span style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif;mso-bidi-font-family:"Courier New";color:black;
  mso-ansi-language:ES-MX'>Correo electrónico:<o:p></o:p></span></b></p>
  </td>
  <td width="43%" colspan=4 valign=bottom style='width:43.62%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES-MX'>{{ $customer->email }}</span></b><span
  style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  "Courier New";color:black;mso-ansi-language:ES-MX'> </span><b
  style='mso-bidi-font-weight:normal'><span style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif;mso-bidi-font-family:"Courier New";color:black;
  mso-ansi-language:ES-MX'><o:p></o:p></span></b></p>
  </td>
  <td width="49%" colspan=7 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  "Courier New";color:black;mso-ansi-language:ES-MX'>Números telefónicos:
  Casa<span style='mso-spacerun:yes'>                         
  </span>Oficina<span style='mso-spacerun:yes'>    </span><span
  style='mso-spacerun:yes'>               </span>Celular:<o:p></o:p></span></b></p>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  "Courier New";color:black;mso-ansi-language:ES-MX'><span
  style='mso-spacerun:yes'>    </span><span
  style='mso-spacerun:yes'>                {{ $customer->tel_casa }}                  </span><span
  style='mso-spacerun:yes'>                {{ $customer->tel_oficina }}    </span><span
  style='mso-spacerun:yes'>                         {{ $customer->tel_cel }}                </span><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;mso-yfti-lastrow:yes;height:20.1pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'>Día de
  Entrada<o:p></o:p></span></b></p>
  </td>
  <td width="21%" colspan=2 valign=bottom style='width:21.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:white;padding:0cm 3.5pt 0cm 3.5pt;
  height:20.1pt'> 
  <p class=MsoNormal style='margin-right:-4.05pt'><span  lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'><o:p>{{ substr($customer->checkin, 0, 10) }}</o:p></span></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;
  height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
  normal'><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'>Día de
  salida<o:p></o:p></span></b></p>
  </td>
  <td width="13%" colspan=2 valign=bottom style='width:13.7%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:white;padding:0cm 3.5pt 0cm 3.5pt;
  height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'><o:p>{{ substr($customer->checkout, 0, 10) }}</o:p></span></p>
  </td>
  <td width="11%" colspan=2 valign=bottom style='width:11.6%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;
  height:20.1pt'>
  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;mso-ansi-language:
  ES'>CIUDAD/PAIS</span></b><b style='mso-bidi-font-weight:normal'><span
  lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:ES'><o:p></o:p></span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;background:white;padding:0cm 3.5pt 0cm 3.5pt;
  height:20.1pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt;font-family:"Century Gothic",sans-serif;mso-bidi-font-family:
  "Courier New";color:black;mso-ansi-language:EN-US'><o:p>{{ $customer->estado }}</o:p></span></p>
  </td>
 </tr>
 <![if !supportMisalignedColumns]>
 <tr height=0>
  <td width=53 style='border:none'></td>
  <td width=45 style='border:none'></td>
  <td width=73 style='border:none'></td>
  <td width=74 style='border:none'></td>
  <td width=111 style='border:none'></td>
  <td width=92 style='border:none'></td>
  <td width=2 style='border:none'></td>
  <td width=8 style='border:none'></td>
  <td width=72 style='border:none'></td>
  <td width=64 style='border:none'></td>
  <td width=46 style='border:none'></td>
  <td width=58 style='border:none'></td>
 </tr>
 <![endif]>
</table>

</div>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Estrangelo Edessa";mso-fareast-font-family:
"Arial Unicode MS";color:black'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;
mso-fareast-font-family:"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";
color:black'>Paquete Vacacional:  {{ ($customer->noches_en_certificado > 0)? $customer->noches_en_certificado." noches" : (($customer->noches_en_hotel > 0)? $customer->noches_en_hotel." noches": "") }}</span></b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;mso-fareast-font-family:
"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";color:black'><span
style='mso-spacerun:yes'>                                 </span></span><b
style='mso-bidi-font-weight:normal'><span lang=ES-TRAD style='font-size:8.0pt;
font-family:"Cambria",serif;mso-fareast-font-family:"Arial Unicode MS";
mso-bidi-font-family:"Estrangelo Edessa"'>Plan:       {{ ($customer->plan =="T")? "Todo incluido":( ($customer->plan =="E")? "Europeo": "") }}<span style=''> </span></span></b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;mso-fareast-font-family:
"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";'><span
style='mso-spacerun:yes'>   </span><span
style='mso-spacerun:yes'>          </span></span><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;
mso-fareast-font-family:"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa"'>Número
de personas:<span style='mso-spacerun:yes'>         {{ $customer->adultos + $customer->menores }}          </span><span
style=''><span style='mso-spacerun:yes'> </span></span><span
style='color:black'>Adultos: {{ $customer->adultos }} </span></span></b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;mso-fareast-font-family:
"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";color:black'><span
style='mso-spacerun:yes'>  </span><span
style='mso-spacerun:yes'>              </span><b style='mso-bidi-font-weight:
normal'>Menores: {{ $customer->menores }} TOTAL:   <span style='mso-spacerun:yes'>     </span>$ {{number_format($customer->precio_total) }}<o:p></o:p></b></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;
mso-fareast-font-family:"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";
color:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;
mso-fareast-font-family:"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";
color:black'>Observaciones: <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;
mso-fareast-font-family:"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa";
color:black'><o:p>&nbsp;&nbsp;{{ $customer->observaciones }}</o:p></span></b></p>

<p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b
style='mso-bidi-font-weight:normal'><span lang=ES-TRAD style='font-size:8.0pt;
font-family:"Cambria",serif;mso-fareast-font-family:"Arial Unicode MS";
mso-bidi-font-family:"Estrangelo Edessa"'>Favor de Leer todos Términos y
Condiciones antes de firmar<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;mso-bidi-font-family:
Arial;color:black'><br>
</span><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>1.
No hay reembolso por la tarifa promocional, sin embargo, dispone de un año
desde la fecha de compra para programar o reprogramar su viaje, puede hacer la
reprogramación, 1 mes de anticipación en temporada alta o 1 semana de
anticipación en temporada baja a su llegada, en caso de no presentarse el día
de su llegada, pierde la vigencia de un año y aplicaría costo de reactivación,
Certificado y habitación sujetos a disponibilidad. Semana santa, pascua,
navidad y año nuevo puede aplicar complemento de reservación. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>2. El <span
class=SpellE>check</span>-in es a las 15:00 <span class=SpellE>hrs</span>. El <span
class=SpellE>check</span> <span class=SpellE>out</span> es a las 12:00 <span
class=SpellE>hrs</span>. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>3. Si requiere
factura anexar copia de la cédula fiscal con un máximo de 24 <span
class=SpellE>hrs</span>. después de su compra, para procesarla al departamento
de contabilidad.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><em><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'><o:p>&nbsp;</o:p></span></em></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 align=left
 style='border-collapse:collapse;border:none;mso-table-lspace:7.05pt;
 margin-left:4.8pt;mso-table-rspace:7.05pt;margin-right:4.8pt;mso-table-anchor-vertical:
 paragraph;mso-table-anchor-horizontal:margin;mso-table-left:left;mso-table-top:
 1.25pt;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:17.3pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.3pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><b><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>NOMBRE DE CUENTA </span></b><span
  lang=ES style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:17.3pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>BUGANVILIAS
  RESORT VACATION CLUB S.A. DE C.V. <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:9.0pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><b><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'><span
  style='mso-spacerun:yes'> </span>DE CUENTA </span></b><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>032 0033 4014 <o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:9.0pt'>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><b><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>CLAVE
  INTERBANCARIA </span></b><span lang=ES style='font-size:8.0pt;font-family:
  "Times New Roman",serif'><o:p></o:p></span></p>
  </td>
  <td width=316 valign=top style='width:237.35pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;
  height:9.0pt'>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>044 3750 3200
  3340 141 <o:p></o:p></span></p>
  <p class=Default style='mso-element:frame;mso-element-frame-hspace:7.05pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  margin;mso-element-top:1.25pt;mso-height-rule:exactly'><span lang=ES
  style='font-size:8.0pt;font-family:"Times New Roman",serif'>SCOTIABANK<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif;mso-bidi-font-family:
"Estrangelo Edessa";color:black'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif;mso-bidi-font-family:
"Estrangelo Edessa";color:black'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif;mso-bidi-font-family:
"Estrangelo Edessa";color:black'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:115%'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:9.0pt;line-height:115%;font-family:
"Cambria",serif;mso-bidi-font-family:"Estrangelo Edessa";color:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='line-height:115%'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:9.0pt;line-height:115%;font-family:
"Cambria",serif;mso-bidi-font-family:"Estrangelo Edessa";color:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='line-height:115%'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:9.0pt;line-height:115%;font-family:
"Cambria",serif;mso-bidi-font-family:"Estrangelo Edessa";color:black'>Para
validar la promoción es necesario nos envié los siguientes documentos: <o:p></o:p></span></b></p>

<p class=MsoNormal><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
mso-bidi-font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
lang=ES-TRAD style='font-size:9.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol;color:black'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=ES-TRAD style='font-size:9.0pt;
font-family:"Cambria",serif;mso-bidi-font-family:Arial;color:black'>Formato de
Reservación firmado.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
lang=EN-US style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:black;
mso-ansi-language:EN-US'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-US style='font-size:9.0pt;
font-family:"Cambria",serif;mso-bidi-font-family:Arial;color:black;mso-ansi-language:
EN-US'>Boucher Firmado.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-US style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;
mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
lang=ES style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:black;
mso-ansi-language:ES'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=ES style='font-size:9.0pt;font-family:
"Cambria",serif;mso-bidi-font-family:Arial;color:black;mso-ansi-language:ES'>Copia
de su identificación por ambos lados.</span><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-size:7.0pt;mso-bidi-font-size:8.0pt;
font-family:"Cambria",serif;mso-bidi-font-family:"Courier New";color:black;
mso-ansi-language:ES'><o:p></o:p></span></b></p>

<p class=Default style='margin-left:36.0pt'><span lang=ES><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:7.0pt;mso-bidi-font-size:8.0pt;
font-family:"Cambria",serif;mso-bidi-font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;mso-fareast-font-family:
"Arial Unicode MS";mso-bidi-font-family:"Estrangelo Edessa"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'><span lang=ES-TRAD style='font-size:7.0pt;mso-bidi-font-size:8.0pt;
font-family:"Cambria",serif;mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=ES-TRAD
style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:
Arial;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:
Arial;mso-ansi-language:ES'>Nombre y Firma:</span><span lang=ES
style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;
mso-ansi-language:ES'> <span style='mso-spacerun:yes'>    </span><u>________________________________________</u><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-ansi-language:
ES'><span
style='mso-spacerun:yes'>                                               
</span><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-ansi-language:
ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-ansi-language:
ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-ansi-language:
ES'><span style='mso-tab-count:1'>                        </span><span
style='mso-tab-count:2'>                                               </span><span
style='mso-spacerun:yes'>                        </span><span
style='mso-spacerun:yes'>                                                   </span><span
style='mso-spacerun:yes'>                                             </span><span
style='mso-spacerun:yes'>    </span></span><span lang=ES style='font-size:7.0pt;
mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:Arial;
mso-ansi-language:ES'>Fecha: 
@php
$str = (strlen($customer->fecha_venta) >= 10)? substr($customer->fecha_venta, 0, 10): '0000-00-00';
$date = DateTime::createFromFormat('Y-m-d', $str);
@endphp
{{ $date->format('d/m/Y') }}

</span><span lang=ES style='font-size:7.0pt;
mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-ansi-language:ES'> <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;color:#0070C0;
mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:
Arial;mso-ansi-language:ES'>Asesor Vacacional: <span style='mso-tab-count:1'>    {{ (is_null(Auth::user()->executive))? Auth::user()->executive->nombre :"Sin ejecutivo" }}       </span><span
style='mso-tab-count:2'>                                               </span></span><span
lang=ES style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;
mso-ansi-language:ES'><span style='mso-tab-count:1'>                        </span><span
style='mso-spacerun:yes'>         </span><span style='mso-tab-count:1'>               </span><span
style='mso-spacerun:yes'>                                     </span><span
style='mso-spacerun:yes'> </span><span style='mso-tab-count:2'>                                 </span><span
style='mso-spacerun:yes'>   </span></span><span lang=ES style='font-size:7.0pt;
mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:Arial;
mso-ansi-language:ES'>E-mail:  {{ Auth::user()->email }}<span style='mso-spacerun:yes'> </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;mso-bidi-font-family:"Estrangelo Edessa"'>La
firma de éste formato debe ser idéntica a la firma plasmada en la tarjeta de
crédito. (En caso de realizar el pago con tarjeta de crédito) Enviar vía fax,
al 01 322 22 6 04 18 o por correo electrónico.<b style='mso-bidi-font-weight:
normal'><i style='mso-bidi-font-style:normal'><span style='color:#7030A0'><o:p></o:p></span></i></b></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES-TRAD
style='font-size:7.0pt;mso-bidi-font-size:8.0pt;font-family:"Cambria",serif;
mso-bidi-font-family:Arial;color:#2E15E9'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Century Gothic",sans-serif;
mso-bidi-font-family:Arial;color:#2E15E9;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;mso-bidi-font-size:8.0pt;font-family:"Century Gothic",sans-serif;
mso-bidi-font-family:Arial;color:#2E15E9;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=ES
style='font-size:9.0pt;mso-bidi-font-size:8.0pt;font-family:"Century Gothic",sans-serif;
mso-bidi-font-family:Arial;color:#2E15E9;mso-ansi-language:ES'><o:p>&nbsp;</o:p></span></b></p>

</div>

</body>

</html>
