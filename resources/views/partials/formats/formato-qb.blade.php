<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<title>Forma de Calificación R5</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Estrangelo Edessa";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Century Gothic";
	panose-1:2 11 5 2 2 2 2 2 2 4;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Arial",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Arial",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Arial",serif;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:#954F72;
	text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:36.0pt 36.0pt 36.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ES-MX link=blue vlink="#954F72">

<div class=WordSection1>

<p class=MsoNormal style='margin-left:141.6pt'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=291 height=0></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=90 height=73
  src="{{  asset('formats/formato-nq_archivos/image002.jpg')}}"></td>
 </tr>
</table>

<br clear=ALL>
</p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
14.0pt;font-family:"Arial",sans-serif;color:#666699'>           Programa de Invitados
VIP      <b> </b></span></p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
5.0pt;font-family:"Arial",sans-serif;color:#666699'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:red'>Favor
de firmar después de leer en su totalidad el presente documento:                                                                                 </span></b></p>

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="98%"
 style='width:98.78%;border-collapse:collapse;border:none'>
 <tr style='height:25.4pt'>
  <td width="13%" colspan=2 style='width:13.4%;border:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>NOMBRE COMPLETO:</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES-TRAD style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif' >{{ $customer->nombre }}</span></p>
  </td>
  <td width="13%" valign=bottom style='width:13.3%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN</span></b><b><span
  lang=EN-GB style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>:</span></b></p>
  </td>
  <td width="21%" colspan=3 valign=bottom style='width:21.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=ES-TRAD style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ $customer->ocupacion }}</span></p>
  </td>
  <td width="6%" valign=bottom style='width:6.64%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang='ES-TRAD' style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->edad }}</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>CÓNYUGE</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES-TRAD style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ $customer->nombre_co }}</span></p>
  </td>
  <td width="13%" style='width:13.3%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN:</span></b></p>
  </td>
  <td width="21%" colspan=3 style='width:21.2%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES-TRAD style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ $customer->ocupacion_co }}</span></p>
  </td>
  <td width="6%" style='width:6.64%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD:</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->edad_co }}</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>ESTADO
  CIVIL:</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.54%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ (!is_null($customer->maritalStatus))? $customer->maritalStatus->nombre : "" }}</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.66%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>ADULTOS:( {{ $customer->adultos }} )</span></b></p>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>MENORES:( {{ $customer->menores }} )</span></b></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.4pt 0cm 3.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Edades
  de los menores:</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->edadmenores }}</span></b></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>INGRESO
  MENSUAL COMBINADO:</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'> ({{ ($customer->ingreso_mensual >= 25000 && $customer->ingreso_mensual < 35000 )? "X": "" }})
  $25,000-35,000  (  {{ ($customer->ingreso_mensual >= 35000 && $customer->ingreso_mensual < 45000 )? "X": "" }}  ) $35,000-45,000  ( {{ ($customer->ingreso_mensual >= 45000 && $customer->ingreso_mensual < 55000 )? "X": "" }} ) $45,000-$55,000  ( {{ ($customer->ingreso_mensual > 55000 )? "X": "" }}  ) más de 60
  mil</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>DIRECCION:   {{ $customer->address }}
  </span></b></p>
  </td>
  <td width="34%" colspan=4 valign=top style='width:34.5%;border:none;
  border-bottom:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>PROPIETARIO
  DE VIVIENDA           SI </span></b><u><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif'>__{{ $customer->homeowner? "X" : "" }}__ </span></u><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>           NO
  </span></b><u><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>__{{ $customer->homeowner? "" : "X" }}__</span></u></p>
  </td>
  <td width="14%" colspan=2 valign=bottom style='width:14.98%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class='MsoNormal' style='text-align:center'><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif'>Numero
  Tarjeta de crédito con la que se realizara el pago:             </span></p>
  <p class='MsoNormal' style='text-align:center'><span lang=ES-TRAD style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:red'>{{ $customer->document_card }} {{ $customer->banco }}</span></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Código De
  seguridad</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:red'>{{ $customer->codigo }}</span></b></p>
  </td>
  <td width="11%" style='width:11.6%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Fecha
  de vencimiento: </span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:red'>{{ $customer->vencimiento1." / 20".$customer->vencimiento2 }}</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Monto a
  cargar:   $ {{ number_format($customer->pagado) }} </span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>HA
  TOMADO ALGUNA PRESENTACIÓN DE </span></b><b><span lang=ES-TRAD
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>BUGANVILIAS</span></b><b><span
  lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>
  RESORT VACATION CLUB?</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:7.0pt;font-family:"Century Gothic",sans-serif'>Si  
  ( {{ $customer->take_presentation? "X" : "" }} )   No  ( {{ $customer->take_presentation? "": "X" }} )     Fecha aproximada de última presentación: {{ ($customer->date_presentation > '2000-01-01')? $customer->date_presentation : "" }}</span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt'>Cuantas veces
  vacaciona  normalmente al año</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt'>{{ $customer->many_times }}</span></b></p>
  </td>
  <td width="11%" style='width:11.6%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Tiene
  propiedad vacacional</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>No ( {{ $customer->has_vacation_ownership? "" : "X" }} )     
  Si  ( {{ $customer->has_vacation_ownership? "X" : "" }} )</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:19.5pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;color:black'>Número de tarjetas de crédito que manejan   
  1 ( {{ ($customer->num_tarjetas == 1)? "x" :"" }} )        2 ( {{ ($customer->num_tarjetas == 2)? "x" :"" }}  )      Mas de dos (  {{ ($customer->num_tarjetas >2)? "x" :"" }}  )</span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Tipos
  de tarjeta de crédito que maneja: Visa( {{ ($customer->visa == "S")? "X": "" }} )  Master Card ( {{ ($customer->mc == "S")? "X": "" }} ) American Express( {{ ($customer->amex == "S")? "X": "" }}  )   </span></b></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:#7030A0'>{{ $customer->habitacion }}</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="6%" valign=bottom style='width:6.9%;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  none;padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Correo
  electrónico</span></b></p>
  </td>
  <td width="43%" colspan=4 valign=bottom style='width:43.62%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span style='font-size:
  6.0pt;font-family:"Century Gothic",sans-serif;color:black'>:</span></b><span
  style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;color:black'>
  test@mail.com</span></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'>Números telefónicos: Casa                         
  Oficina                          Celular</span></b></p>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'>                                    3222949223                322292992555      
        32232323232</span></b></p>
  </td>
 </tr>
 <tr style='height:20.1pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de Entrada</span></b></p>
  </td>
  <td width="21%" colspan=2 valign=bottom style='width:21.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{ (strlen($customer->checkin) >= 10)? substr($customer->checkin,0,10) : "0000-00-00" }}</span></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de Salida</span></b></p>
  </td>
  <td width="13%" colspan=2 valign=bottom style='width:13.7%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{ (strlen($customer->checkout) >= 10)? substr($customer->checkout,0,10) : "0000-00-00" }}</span></p>
  </td>
  <td width="11%" valign=bottom style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>CIUDAD/PAIS</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{ $customer->estado }}</span></p>
  </td>
 </tr>
 <tr height=0>
  <td width=726 style='border:none'></td>
  <td width=680 style='border:none'></td>
  <td width=1103 style='border:none'></td>
  <td width=1116 style='border:none'></td>
  <td width=1667 style='border:none'></td>
  <td width=1392 style='border:none'></td>
  <td width=42 style='border:none'></td>
  <td width=1215 style='border:none'></td>
  <td width=964 style='border:none'></td>
  <td width=696 style='border:none'></td>
  <td width=874 style='border:none'></td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Estrangelo Edessa",serif;color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Paquete
Vacacional: </span></b><span lang=ES-TRAD style='font-size:8.0pt;font-family:
"Cambria",serif;color:black'> {{ ($customer->noches_en_certificado > 0)? $customer->noches_en_certificado." noches" : (($customer->noches_en_hotel > 0)? $customer->noches_en_hotel." noches": "") }}  </span><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>Plan:<span
style='color:red'> </span></span></b><span lang=ES-TRAD style='font-size:8.0pt;
font-family:"Cambria",serif;color:red'>      </span><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>{{ ($customer->plan =="T")? "Todo incluido":( ($customer->plan =="E")? "Europeo": "") }} </span><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;color:red'>                                            <b>   </b></span><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Numero de
personas:  {{ $customer->adultos + $customer->menores }}    <span style='color:black'>Adultos: </span></span></b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>  {{ $customer->adultos }}  
<b>Menores:    {{ $customer->menores }}    </b></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'> <b>TOTAL: $ {{number_format($customer->precio_total) }}</b></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Observaciones: {{ $customer->observaciones }}</span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Favor de Leer
todos Términos y Condiciones antes de firmar</span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;color:black'><br>
</span><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>1.
No hay reembolso por la tarifa promocional, sin embargo, dispone de un año
desde la fecha de compra para programar o reprogramar su viaje, puede hacer la
reprogramación con 1 mes de anticipación en temporada alta o 1 semana de
anticipación en temporada baja a la fecha de su llegada, en caso de no
presentarse el día de su llegada pierde la vigencia de un año y aplicaría costo
de reactivación, Certificado y habitación sujetos a disponibilidad. Semana
santa, pascua, navidad y año nuevo puede aplicar complemento de reservación.</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>2. Si es
casado, ambos marido y mujer deben estar presentes para tomar la presentación y
mostrar sus identificaciones válidas con mismo domicilio, en caso de no contar
con ellas, usted tiene más opciones como las siguientes: presentar copia de su
acta de matrimonio o sus anillos de casados, Mujeres solteras solo necesitan su
identificación oficial, unión libre es considerado como matrimonio.</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>3. Promoción
válida para tarjetahabientes de crédito, matrimonios y mujeres solteras,
mayores de 28 años. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>4. Contar con
un ingreso combinado de mínimo $25,000.00 pesos. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>5. Para poder
beneficiarse de las tarifas preferenciales en este programa, usted debe estar
de acuerdo en acompañarnos a un desayuno presentación seguido por un recorrido
personalizado por las instalaciones, estos serán programado para la mañana
siguiente a su llegada y tiene una duración aproximadamente de 120 minutos después
del desayuno. Si deja de asistir se le cobrará la tarifa rack vigente durante
su estancia. Se pueden beneficiar hasta un máximo de 3 certificados por grupo
de familia o amistades. Cuando se viaja con familiares o amistades en las
mismas fechas solo serán válidos los certificados siempre y cuando ingresen
juntos el mismo día a la presentación del club vacacional.</span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif'>6. Mostrar
sus tarjetas de crédito vigente. No se permiten tarjetas de débito, Discover o
Diner's Club. </span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif'>7. El
check-in es a las 15:00 hrs. El check out es a las 12:00 hrs. </span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif'>8. En caso
de que la información que usted proporciona en el presente formulario no sea
verídica con el fin de beneficiarse con la tarifa promocional, se le cobrarán
la tarifa Vigente durante su estancia. </span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif'>9. Si
requiere factura anexar el documento adjunto con los datos completos, por la
nueva disposición de Hacienda y Crédito Público, para factura electrónica se
requiere la información el mismo día de su compra para procesarla al
departamento de contabilidad, siendo este el único día para poder expedir su
factura.</span></p>

<p class=MsoNormal style='line-height:115%'><span lang=ES-TRAD
style='font-size:8.0pt;line-height:115%;font-family:"Cambria",serif'>10. No son
elegibles socios del Buganvilias Resort Vacation Club, agentes de viajes,
empleados de tiempo compartido, o quien ya haya atendido la presentación de
Buganvilias Resort Vacation Club en los últimos 36 meses.</span></p>

<p class=MsoNormal style='line-height:115%'><b><span lang=ES-TRAD
style='font-size:9.0pt;line-height:115%;font-family:"Cambria",serif;color:black'>Para
validar la promoción es necesario nos envié los siguientes documentos: </span></b></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Formato de Reservación firmado</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=EN-US style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Boucher Firmado</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Copia de su identificación por ambos lados</span></p>

<p class=MsoNormal style='margin-left:36.0pt'><b><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>*** </span></b><b><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>FAVOR DE NO AGENDAR
NINGUNA OTRA ACTIVIDAD LA MAÑANA DEL DÍA DE SU PRESENTACIÓN</span></b><b><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'> ***</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>Firmo
la presente de conformidad  aceptando asistir a la presentación de Buganvilias
Resort Vacation Club y asimismo acepto  los Términos y Condiciones arriba
mencionados. Autorizo a Buganvilias Resort Vacation Club a realizar el cargo a
mi tarjeta de crédito en caso de fallar en los términos y condiciones arriba
mencionados y  para la adquisición de este paquete promocional. </span></b></p>

<p class=MsoNormal><b><span lang=ES-TRAD style='font-size:7.0pt;font-family:
"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Nombre y Firma:</span><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>     <u>________________________________________</u>                                                                                    </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>Fecha:</span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>      {{ (strlen($customer->fecha_venta) >= 10)? substr($customer->fecha_venta, 0,10) : "Sin fecha" }}</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Asesor Vacacional: {{ (!is_null(Auth::user()->executive))? Auth::user()->executive->nombre: "Sin ejecutivo" }}</span><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>                                                    
                                                                                                           </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>E-mail: {{ Auth::user()->email }}</span></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>La firma de éste formato
debe ser idéntica a la firma plasmada en la tarjeta de crédito. (En caso de
realizar el pago con tarjeta de crédito) Enviar vía fax, al 01 322 22 6 04 18 o
por correo electrónico.</span></p>
</div>

</body>

</html>
