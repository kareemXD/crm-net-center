<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<title>Forma de Calificación R5</title>
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:"Estrangelo Edessa";
  panose-1:0 0 0 0 0 0 0 0 0 0;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:"Arial Unicode MS";
  panose-1:2 11 6 4 2 2 2 2 2 4;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:"Century Gothic";
  panose-1:2 11 5 2 2 2 2 2 2 4;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:Cambria;
  panose-1:2 4 5 3 5 4 6 3 2 4;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;  src: url({{ storage_path('fonts/font.ttf') }}) format('truetype');
}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
  {margin:0cm;
  margin-bottom:.0001pt;
  font-size:12.0pt;
  font-family:"Times New Roman",serif;}
a:link, span.MsoHyperlink
  {color:blue;
  text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
  {color:#954F72;
  text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
  {mso-style-link:"Texto de globo Car";
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:8.0pt;
  font-family:"Tahoma",sans-serif;}
span.TextodegloboCar
  {mso-style-name:"Texto de globo Car";
  mso-style-link:"Texto de globo";
  font-family:"Tahoma",sans-serif;}
 /* Page Definitions */
 @page WordSection1
  {size:612.0pt 792.0pt;
  margin:36.0pt 36.0pt 36.0pt 36.0pt;}
div.WordSection1
  {page:WordSection1;}
 /* List Definitions */
 ol
  {margin-bottom:0cm;}
ul
  {margin-bottom:0cm;}
-->
</style>

<body lang=ES-MX link=blue vlink="#954F72">

<div class=WordSection1>

<p class=MsoNormal style='margin-left:141.6pt'>

<center><img width=90 height=73 src="{{  asset('formats/formato-nq_archivos/image002.jpg') }}"></center>

<br clear=ALL>
</p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
14.0pt;font-family:"Arial",sans-serif;color:#666699'>           Programa de Invitados
VIP      <b> </b></span></p>

<p class=MsoNormal style='margin-left:141.6pt'><span lang=ES style='font-size:
5.0pt;font-family:"Arial",sans-serif;color:#666699'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:red'>Favor
de proporcionarnos la siguiente información después de leer los términos y
condiciones:                                                                 </span></b></p>

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="98%"
 style='width:98.78%;border-collapse:collapse;border:none'>
 <tr style='height:25.4pt'>
  <td width="13%" colspan=2 style='width:13.4%;border:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>NOMBRE COMPLETO:</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->nombre }}</span></p>
  </td>
  <td width="13%" valign=bottom style='width:13.3%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN</span></b><b><span
  lang=EN-GB style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>:</span></b></p>
  </td>
  <td width="21%" colspan=3 valign=bottom style='width:21.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->ocupacion }}</span></p>
  </td>
  <td width="6%" valign=bottom style='width:6.64%;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.4pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->edad }}</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>CÓNYUGE</span></b></p>
  </td>
  <td width="37%" colspan=3 valign=bottom style='width:37.12%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>{{ $customer->nombre_co }}</span></p>
  </td>
  <td width="13%" style='width:13.3%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>OCUPACIÓN:</span></b></p>
  </td>
  <td width="21%" colspan=3 style='width:21.2%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></p>
  </td>
  <td width="6%" style='width:6.64%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>EDAD:</span></b></p>
  </td>
  <td width="8%" valign=bottom style='width:8.34%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:9.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>ESTADO
  CIVIL:</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.54%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;font-family:
  "Century Gothic",sans-serif'>{{ (!is_null($customer->maritalStatus))? $customer->maritalStatus->nombre: "casado" }}</span></b></p>
  </td>
  <td width="10%" valign=bottom style='width:10.66%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;white-space: nowrap;font-family:
  "Century Gothic",sans-serif'>ADULTOS:  ( {{$customer->adultos}} )</span></b></p>
  <p class=MsoNormal><b><span lang=EN-GB style='font-size:6.0pt;white-space:nowrap;font-family:
  "Century Gothic",sans-serif'>MENORES: (  {{ $customer->menores }} )</span></b></p>
  </td>
  <td width="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Edades
  de los menores:  {{ $customer->edadmenores }}</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>INGRESO
  MENSUAL COMBINADO:</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'> ( @if($customer->ingreso_mensual >= 45000 && $customer->ingreso_mensual < 50000) x @endif
  ) $45,000     ( @if($customer->ingreso_mensual >= 50000 && $customer->ingreso_mensual < 60000) x @endif ) $50,000-60,000  (  @if($customer->ingreso_mensual >= 60000 ) x @endif  ) más de $60,000</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>DIRECCION:  </span></b></p>
  </td>
  <td width="34%" colspan=4 valign=top style='width:34.5%;border:none;
  border-bottom:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>PROPIETARIO
  DE VIVIENDA           SI </span></b><u><span lang=ES style='font-size:6.0pt;
  font-family:"Century Gothic",sans-serif'>___ </span></u><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>           NO
  </span></b><u><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>____</span></u></p>
  </td>
  <td width="14%" colspan=2 valign=bottom style='width:14.98%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif'>Numero
  Tarjeta de crédito con la que se realizara el pago:             </span></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;'><u> XXXX XXXX XXXX {{ $customer->four_digits."    ".$customer->banco }}   </u></span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:red'>&nbsp;</span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Código De
  seguridad</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:red'>****</span></b></p>
  </td>
  <td width="11%" style='width:11.6%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Fecha
  de vencimiento: </span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;'>{{ $customer->vencimiento1." / ".$customer->vencimiento2 }}</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Monto a
  cargar:  {{"$ ".number_format($customer->pagado) }}</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="50%" colspan=5 style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>HA
  TOMADO ALGUNA PRESENTACIÓN DE </span></b><b><span lang=ES-TRAD
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>BUGANVILIAS</span></b><b><span
  lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>
  RESORT VACATION CLUB?</span></b></p>
  <p class=MsoNormal><b><span lang=ES style='font-size:7.0pt;font-family:"Century Gothic",sans-serif'>Si  
  (   )   No  (    )     Fecha aproximada de última presentación:</span></b></p>
  </td>
  <td width="13%" colspan=2 style='width:13.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt'>Cuantas veces
  vacaciona  normalmente al año</span></b></p>
  </td>
  <td width="11%" style='width:11.6%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>            
     </span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.05pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>Tiene
  propiedad vacacional</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>No (  )     
  Si  (   )</span></b></p>
  <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:19.5pt'>
  <td width="50%" colspan=5 valign=bottom style='width:50.52%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;color:black'>Número de tarjetas de crédito que manejan   
  1 ( {{ ($customer->num_tarjetas == 1)? "x" :"" }} )        2 ( {{ ($customer->num_tarjetas == 2)? "x" :"" }} )      Mas de dos (  {{ ($customer->num_tarjetas > 2)? "x" :"" }}  )</span></b></p>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Tipos
  de tarjeta de crédito que maneja: Visa( {{ ($customer->visa == "S")?"X":"" }} )  Master Card ( {{ ($customer->mc == "S")?"X":"" }} ) American
  Express( {{ ($customer->amex == "S")?"X":"" }} )   </span></b></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:19.5pt'>
  <p class=MsoNormal align=center style='margin-left:36.0pt;text-align:center;
  text-indent:-18.0pt'><b><span lang=ES style='font-size:8.0pt;font-family:
  "Century Gothic",sans-serif;'>{{ $customer->habitacion }}<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></b><b><span lang=ES style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;
  color:#7030A0'> </span></b></p>
  </td>
 </tr>
 <tr style='height:20.05pt'>
  <td width="6%" valign=bottom style='width:6.9%;border-top:none;border-left:
  solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
  none;padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Correo
  electrónico</span></b></p>
  </td>
  <td width="43%" colspan=4 valign=bottom style='width:43.62%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span style='font-size:
  6.0pt;font-family:"Century Gothic",sans-serif;color:black'>: {{ $customer->email_decrypt }}</span></b><span
  style='font-size:11.0pt;font-family:"Century Gothic",sans-serif;color:black'>
  </span></p>
  </td>
  <td width="49%" colspan=6 valign=bottom style='width:49.48%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:20.05pt'>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'>Números telefónicos: Casa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Celular&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Oficina</span></b></p>
  <p class=MsoNormal><b><span style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;
  color:black'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $customer->tel_casa_decrypt }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $customer->tel_cel_decrypt }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $customer->tel_oficina_decrypt }}</span></b></p>
  </td>
 </tr>
 <tr style='height:20.1pt'>
  <td width="13%" colspan=2 valign=bottom style='width:13.4%;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de Entrada</span></b></p>
  </td>
  <td width="21%" colspan=2 valign=bottom style='width:21.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{ (strlen($customer->checkin) >= 10)?substr($customer->checkin, 0,10): "" }}</span></p>
  </td>
  <td wid th="15%" valign=bottom style='width:15.92%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES
  style='font-size:6.0pt;font-family:"Century Gothic",sans-serif;color:black'>Día
  de salida</span></b></p>
  </td>
  <td width="13%" colspan=2 valign=bottom style='width:13.7%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal style='margin-right:-4.05pt'><span lang=ES
  style='font-size:9.0pt;font-family:"Cambria",serif;color:black'>{{ (strlen($customer->checkout) >= 10)?substr($customer->checkout, 0,10): "" }}</span></p>
  </td>
  <td width="11%" valign=bottom style='width:11.6%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal><b><span lang=ES style='font-size:6.0pt;font-family:"Century Gothic",sans-serif'>CIUDAD/PAIS</span></b></p>
  </td>
  <td width="24%" colspan=3 valign=bottom style='width:24.18%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:white;padding:0cm 3.5pt 0cm 3.5pt;height:20.1pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:8.0pt;font-family:"Century Gothic",sans-serif;color:black'>{{ $customer->estado }}</span></p>
  </td>
 </tr>
 <tr height=0>
  <td width=53 style='border:none'></td>
  <td width=45 style='border:none'></td>
  <td width=73 style='border:none'></td>
  <td width=74 style='border:none'></td>
  <td width=111 style='border:none'></td>
  <td width=92 style='border:none'></td>
  <td width=2 style='border:none'></td>
  <td width=80 style='border:none'></td>
  <td width=64 style='border:none'></td>
  <td width=46 style='border:none'></td>
  <td width=58 style='border:none'></td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Estrangelo Edessa";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Paquete
Vacacional: </span></b><span lang=ES-TRAD style='font-size:8.0pt;font-family:
"Cambria",serif;color:black'>                 {{ ($customer->noches_en_certificado > 0)? $customer->noches_en_certificado." noches" : (($customer->noches_en_hotel > 0)? $customer->noches_en_hotel." noches": "") }}             </span><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Plan:   <span
style='color:red'> </span></span></b><span lang=ES-TRAD style='font-size:8.0pt;
font-family:"Cambria",serif;'>                     {{ ($customer->plan =="T")? "Todo incluido":( ($customer->plan =="E")? "Europeo": "") }}                             </span><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Número de
personas:<span style=''>                   {{ $customer->adultos + $customer->menores }}               </span><span
style='color:black'>Adultos:   </span>{{ $customer->adultos }}</span></b> <span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>  <b>Menores:</b>   {{ $customer->menores }} 
 <b>TOTAL: &nbsp;$ {{number_format($customer->precio_total) }}</b></span></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-right:-4.05pt'><b><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>Observaciones: </span></b>
<span style='font-size:8.0pt;font-family:"Cambria",serif;color:black'> {{ $customer->observaciones }} </span>
</p>

<p class=MsoNormal align=center style='margin-right:-4.05pt;text-align:center'><b><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>Favor de Leer
todos Términos y Condiciones antes de firmar</span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;color:black'><br>
</span><span lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>1.
No hay reembolso por la tarifa promocional, sin embargo, dispone de un año
desde la fecha de compra para programar o reprogramar su viaje, puede hacer la
reprogramación con 1 mes de anticipación en temporada alta o 1 semana de
anticipación a su llegada en temporada baja, en caso de no presentarse el día
de su llegada pierde la vigencia de un año y aplicaría costo de reactivación,
Certificado y habitación sujetos a disponibilidad. Semana santa, pascua,
navidad y año nuevo puede aplicar complemento de reservación. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>2. Si es
casado, ambos marido y mujer deben estar presentes para tomar la presentación y
mostrar sus identificaciones válidas con mismo domicilio, en caso de no contar
con ellas, usted tiene más opciones como las siguientes: presentar copia de su
acta de matrimonio o sus anillos de casados, Mujeres solteras solo necesitan su
identificación oficial, unión libre es considerado como matrimonio. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>3. Promoción
válida para tarjetahabientes de crédito, matrimonios y mujeres solteras,
mayores de 30 años. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>4. Contar con
un ingreso combinado de mínimo $45,000.00 pesos. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>5. Para poder
beneficiarse de las tarifas preferenciales en este programa, usted debe estar
de acuerdo en acompañarnos a un desayuno presentación seguido por un recorrido
personalizado por las instalaciones, estos serán programado para la mañana
siguiente a su llegada y tiene una duración aproximadamente 120 minutos después
del desayuno. Si deja de asistir se le cobrará la tarifa rack vigente durante
su estancia. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>6. Mostrar 2
tarjetas de crédito vigente. No se permiten tarjetas de crédito adicional,
tarjetas de débito, Discover o Diner's Club. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>7. El check-in
es a las 15:00 hrs. El check out es a las 12:00 hrs. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>8. En caso de que
la información que usted proporciona en el presente formulario no sea verídica
con el fin de beneficiarse con la tarifa promocional, se le cobrarán la tarifa
Vigente durante su estancia, no son válidas promesas o regalos ofrecidos
verbalmente, todo se ofrece con soporte por escrito. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>9. Si requiere
factura anexar copia de la cédula fiscal con un máximo de 24 hrs. después de su
compra, para procesarla al departamento de contabilidad. </span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif'>10. No son
elegibles socios del Buganvilias Resort Vacation Club, agentes de viajes,
empleados de tiempo compartido, o quien ya haya atendido la presentación de
Buganvilias Resort Vacation Club en los últimos 24 meses, ni residentes locales
o de municipios aledaños a Puerto Vallarta.</span></p>

<p class=MsoNormal style='margin-right:-2.85pt;text-align:justify'><span
lang=ES-TRAD style='font-size:8.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></p>

<p class=MsoNormal style='line-height:115%'><b><span lang=ES-TRAD
style='font-size:9.0pt;line-height:115%;font-family:"Cambria",serif;color:black'>Para
validar la promoción es necesario nos envié los siguientes documentos: </span></b></p>

<p class=MsoNormal><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES-TRAD style='font-size:9.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES-TRAD style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Formato de Reservación firmado</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=EN-US style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Boucher Firmado</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-indent:-18.0pt'><span
lang=ES style='font-size:7.0pt;font-family:Symbol;color:black'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=ES style='font-size:9.0pt;font-family:"Cambria",serif;
color:black'>Copia de su identificación por ambos lados</span></p>

<p class=MsoNormal style='margin-left:36.0pt'><b><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>*** </span></b><b><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>FAVOR DE NO AGENDAR
NINGUNA OTRA ACTIVIDAD LA MAÑANA DEL DÍA DE SU PRESENTACIÓN</span></b><b><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'> ***</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>Firmo
la presente de conformidad  aceptando asistir a la presentación de Buganvilias
Resort Vacation Club y asimismo acepto  los Términos y Condiciones arriba
mencionados. Autorizo a Buganvilias Resort Vacation Club a realizar el cargo a
mi tarjeta de crédito por la cantidad arriba mencionado para la adquisición de
este paquete promocional. </span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif;color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
lang=ES-TRAD style='font-size:7.0pt;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span lang=ES-TRAD
style='font-size:7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Nombre y Firma:</span><span lang=ES
style='font-size:7.0pt;font-family:"Cambria",serif'>     <u>________________________________________</u>                                                                                          </span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'>Fecha:</span><span
lang=ES style='font-size:7.0pt;font-family:"Cambria",serif'> {{ (strlen($customer->fecha_venta) >= 10)? substr($customer->fecha_venta, 0, 10) : "0000-00-00" }} </span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif;color:#0070C0'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
7.0pt;font-family:"Cambria",serif'>Asesor Vacacional:   {{ (!is_null(Auth::user()->executive))? Auth::user()->executive->nombre : "Sin ejecutivo"  }}                                                                                                                                          E-mail:  {{ Auth::user()->email }}
</span></p>

<p class=MsoNormal style='margin-right:-2.85pt'><span lang=ES-TRAD
style='font-size:8.0pt;font-family:"Cambria",serif'>La firma de éste formato
debe ser idéntica a la firma plasmada en la tarjeta de crédito. (En caso de
realizar el pago con tarjeta de crédito) Enviar vía fax, al 01 322 22 6 04 18 o
por correo electrónico.</span></p>

</div>

</body>

</html>
