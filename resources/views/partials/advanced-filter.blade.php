<div class="panel panel-default">
    <div class="panel-heading" id="filter_head">
        <h3 class="panel-title text-center">Filtro Avanzado</h3>
    </div>
    <div class="panel-body" style="display:none" id="filter_body">
        <form method="POST" id="search_form"  role="form" action="{{  route('reportStatics') }}"  enctype="multipart/form-data" >
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1 text-nowrap"> 
                <label for="executive">Ejecutivo</label>
            </div>
            <div class="col-md-8 col-sm-8"> 
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-executive" id="operator_executive" class="form-control">
                            <option selected value="=">Igual</option>
                            <option  value="!=">Diferente A</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <select id="executive_name" name="executive" class="select2-multiple form-control" multiple="multiple">
                            @foreach($executives as $executive)
                                @if(!is_null(@$executive->id ))
                                <option value="{{ $executive->id }}">{{ $executive->nombre }}</option>
                                @endif
                            @endforeach
                        </select> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1">
                <label for="check-in">Llegada</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-check-in" id="operator_check_in" class="form-control operator">
                            <option value="2">Entre Fechas</option>
                            <option value=">="> Mayor o igual</option>
                            <option value="=">Igual</option>
                            <option value=">">Mayor</option>
                            <option value="<">Menor</option>
                            <option value="<=">Menor o igual</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control date-picker" name="filter-check-in" id="check_in">   
                    </div>                    
                    <div class="col-md-8 col-sm-8 offset-md-4 offset-sm-4 form-group">
                        <input type="text" placeholder="Fecha 2" class="form-control date-picker" name="check-in-2" id="check_in_2">   
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="check-out">Salida</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-check-out" id="operator_check_out" class="form-control operator">
                            <option value="2">Entre Fechas</option>
                            <option value=">="> Mayor o igual</option>
                            <option value="=">Igual</option>
                            <option value=">">Mayor</option>
                            <option value="<">Menor</option>
                            <option value="<=">Menor o igual</option>
                        </select> 
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control date-picker" name="filter-check-out" id="check_out">
                    </div>
                    <div class="col-md-8 col-sm-8 offset-md-4 offset-sm-4 form-group">
                        <input type="text" class="form-control date-picker" name="check-out-2" id="check_out_2">
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="pending-payment">$pendiente</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-pending-payment" id="operator_payment_paymen" class="form-control operator">
                            <option value=">="> Mayor o igual</option>
                            <option value="=">Igual</option>
                            <option value=">">Mayor</option>
                            <option value="<">Menor</option>
                            <option value="<=">Menor o igual</option>
                        </select>    
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control price-format-null" name="filter-pending-payment" >
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1"> 
                <label for="paid-out">$pagado</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-paid-out" id="operator_paid_out" class="form-control">
                            <option value=">="> Mayor o igual</option>
                            <option value="=">Igual</option>
                            <option value=">">Mayor</option>
                            <option value="<">Menor</option>
                            <option value="<=">Menor o igual</option>
                        </select>    
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <input type="text" class="form-control price-format-null" name="filter-paid-out" >
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-3 col-sm-3 offset-md-1 offset-sm-1"> 
                <label for="paid-out">Datos firmados</label>  
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-3 col-sm-3 text-center">
                        <label for="has_signed_s" class="radio">
                            <input id="has_signed_s" type="radio" value="S" name="filter-has-signed"><span>SI</span>
                        </label>
                    </div>
                    <div class="col-md-3 col-sm-3 text-center">
                        <label for="has_signed_n" class="radio">
                            <input id="has_signed_n" type="radio" value="N" name="filter-has-signed"><span>NO</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-1 col-sm-1 com-md-offset-1 offset-sm-1 text-nowrap">
                <label for="category_type">Tipo Venta</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select name="operator-category-type" id="operator_category_type" class="form-control select2">
                            <option value="=">Igual</option>
                            <option  value="!=">Diferente A</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <select name="category-type" class="select2-multiple form-control" id="category_type" multiple="multiple">
                        @foreach($categories as $category)
                                <option value="{{ $category->code }}">{{ $category->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>   
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-1 offset-md-1 offset-sm-1">
                <label for="customer_order">Orden</label>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <select placeholder="seleccionar orden" class="form-control" name="customer-order" id="customer_order">
                            <option value=""></option>
                            <option value="fecha_alta">Fecha de creación</option>
                            <option value="nombre">Nombre del vendedor</option>
                            <option value="fecha_venta">Fecha de venta</option>
                            <option value="fecha_modificacion">Fecha de Modificación</option>
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <select class="form-control" name="operator-customer-order" id="operator_customer_order">
                            <option value="asc">Ascendiente</option>
                            <option value="desc">Descendiente</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div style="margin-top:10px" class="text-center">
                    <button class="btn btn-primary" type="submit">Filtrar</button>
                    <button id="btn_clean_filter" class="btn btn-secondary">Limpiar </button>
                </div>
            </div>
        </div>
    </div>
    
</form>
    </div>
</div>
