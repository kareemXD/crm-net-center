<!-- Navigation -->
<div class="navbar fixed-top scroll-hide navbar-custom">
<div class="container-fluid top-bar d-block">
  <div class="float-right">
    <ul class="nav navbar-nav float-right">
      <!--notificaciones -->
      <!--<li class="dropdown notifications hidden-xs">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="se7en-flag"></span>
          <div class="sr-only">
            Notas
          </div>
          <p class="counter">
            1
          </p>
        </a>
        <ul class="dropdown-menu">
          <li><a href="#">
            <div class="notifications label label-info">
              Nuevo
            </div>
            <p>
              Nuevo usuario añadido: Kareem Lorenzana
            </p></a>
          </li>
        </ul>
      </li> -->
      <!--dropdown de mensajes-->
      <!--
      <li class="dropdown messages hidden-xs">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="se7en-envelope"></span>
          <div class="sr-only">
            Messages
          </div>
          <p class="counter">
            1
          </p>
        </a>
        <ul class="dropdown-menu messages">
          <li><a href="#">
            <img width="34" height="34" src="http://via.placeholder.com/34x34" />Notificación de un mensaje</a>
          </li>
        </ul>
      </li>-->
      <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <img width="34" height="34" src="http://via.placeholder.com/34x34" />{{ Auth::user()->name }}</a>
        <ul class="dropdown-menu">
          {{--<li class="dropdown-item"><a href="#">
            <i class="fa fa-user"></i>Mi perfil</a>
          </li>
          <div class="dropdown-divider"></div>
          <li class="dropdown-item"><a href="#">
            <i class="fa fa-gear"></i>Configuración</a>
          </li>--}}
          <div class="dropdown-divider"></div>
          <li class="dropdown-item"><a href="{{ route('logout') }}">
            <i class="fa fa-sign-out"></i>Salir</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  <button class="navbar-toggle collapsed" data-toggle="collapse" ><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="logo" href="{{ route('dashboard')}}"></a>
</div>
<div class="container-fluid main-nav clearfix d-block">
  <div class="nav-collapse">
    <ul class="nav d-block">
      <li>
        <a @if(Request::url() == route('home')) class="current" @endif href="{{ route('dashboard')}}"><span aria-hidden="true" class="se7en-home"></span>Dashboard</a>
      </li>
      <li class="dropdown-"><a @if((Request::url() == route('createCustomer')) || (Request::url() == route('listCustomer')) || (Request::url() == route('proyectList'))) class="current"  @endif data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="fa fa-user"></span>Cientes<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('listCustomer') }}">
             <i class="fa fa-list"></i> Lista de clientes</a>
          </li>
          <li>
            <a href="{{ route('createCustomer') }}">
             <i class="fa fa-plus"></i> Grabar Prospecto/Cliente</a>
          </li>
        </ul>
      </li>
      @if(Auth::user()->hasAccessApp('proyectos_view'))
        <li class="dropdown-"><a @if((Request::url() == route('proyectUpload')) || (Request::url() == route('proyectList'))) class="current"  @endif data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="fa fa-id-card-alt"></span>Contáctos<b class="caret"></b></a>
        <ul class="dropdown-menu">
          @if(Auth::user()->hasAccessApp('upload_contact'))
          <li>
            <a href="{{ route('proyectUpload') }}">
             <i class="fa fa-upload"></i> Subir contactoss</a>
          </li>
          @endif
          @if(Auth::user()->hasAccessApp('proyectos'))
          <li>
            <a href="{{ route('proyectList', 'R') }}">
             <i class="fa fa-users"></i> PROYECTOS</a>
          </li>
          @endif
          @if(Auth::user()->hasAccessApp('proyectos'))
          <li>
            <a href="{{ route('proyectList', '21') }}">
             <i class="fa fa-users"></i> NC EXCHANGES</a>
          </li>
          @endif
          @if(Auth::user()->hasAccessApp('leads_rci'))
          <li>
            <a href="{{ route('proyectList', 'Y') }}">
             <i class="fa fa-users"></i> LEADS RCI</a>
          </li>
          @endif

        </ul>
      </li>
      @endif
      @if(Auth::user()->hasAccessApp('survey_show'))
        <li class="dropdown-"><a @if((Request::url() == route('listSurvey')) || (Request::url() == route('createSurvey'))) class="current"  @endif data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="fa fa-poll-h"></span>Survey<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('listSurvey') }}">
             <i class="fa fa-list"></i> Lista de Survey</a>
          </li>
          <li>
            <a href="{{ route('createSurvey') }}">
             <i class="fa fa-plus"></i> Añadir Survey</a>
          </li>

        </ul>
      </li>
      @endif
      @if(Auth::user()->hasAccessApp('report'))
      <li class="dropdown-"><a @if(Request::url() == route('reportCustomer')) class="current"  @endif data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="fa fa-bar-chart"></span>Reportes<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('reportCustomer') }}">
             <i class="fa user-chart"></i> Reportes clientes</a>
          </li>

        </ul>
      </li>
      @endif
      @if(Auth::user()->hasAccessApp('user_view'))
      <!-- secion de administracion -->
      <li class="dropdown-"><a @if(Request::url() == route('admin.permissions') || Request::url() == route('admin.roleList') ) class="current"  @endif data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="fa fa-users"></span>Usuarios y Roles<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('admin.permissions') }}">
             <i class="fa fa-ban"></i> Permisos de usuario</a>
          </li>
          @if(Auth::user()->hasAccessApp("roles"))
          <li>
            <a href="{{ route('admin.roleList') }}">
             <i class="fa fa-address-card"></i> Grupos de usuario</a>
          </li>
          <li>
            <a href="{{ route('admin.createRole') }}">
             <i class="fa fa-plus"></i> Registrar grupo</a>
          </li>
          @endif
          <li>
            <a href="{{ route('listUser') }}">
             <i class="fa fa-list"></i> Usuarios</a>
          </li>
          <li>
            <a href="{{ route('createUser') }}">
             <i class="fa fa-plus"></i> Registrar Nuevo usuario</a>
          </li>
          
        </ul>
      </li>
      <!--fin de seccion de administración -->
      @endif
      @if(Auth::user()->hasAccessApp('user_view'))
      <!--Administracion de la informacion de los ejecutivos-->
      <li class="dropdown">
        <a @if(Request::url() == route('admin.listExecutive'))  class="current" @endif href="#" data-toggle="dropdown">
          <span aria-hidden="dropdown" class="fa fa-address-book"></span>Ejecutivos <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('admin.listExecutive') }}">
             <i class="fa fa-list"></i> Lista</a>
          </li>
        </ul>
      </li>
      @endif
      @if(Auth::user()->hasAccessApp('origen_modify'))
      <!--Administracion de la informacion de los ejecutivos-->
      <li class="dropdown">
        <a @if(Request::url() == route('origenList') || Request::url() == route('subOrigenList'))  class="current" @endif href="#" data-toggle="dropdown">
          <span aria-hidden="dropdown" class="fa fa-file-signature"></span>Origenes <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('origenList') }}">
             <i class="fa fa-list"></i> Administración de Origenes</a>
          </li>
          <li>
            <a href="{{ route('subOrigenList') }}">
             <i class="fa fa-list"></i> Administración de suborigenes</a>
          </li>
        </ul>
      </li>
      @endif
    </ul>
  </div>
</div>
</div>
<!-- End Navigation -->