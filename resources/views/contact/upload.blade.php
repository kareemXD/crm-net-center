@extends('layout-2')
@section('title', 'Contactos - Subir Contactos')
@section('content')
<style>
	.custom-file-label::after{
		content:"Cargar";
	}
</style>
<div id="page-loader"><span class="preloader-interior"></span></div>
<div class="page-title">
    <h1>
       Subir Contáctos.
    </h1>
</div>

<div class="widget-container fluid-height clearfix" style="min-height: 500px;">
	<div class="row">
		<div class="col-8">
			<form id="formUploadContacts" class="form-horizontal needs-validation" method="post" action="{{ route('proyectUpload') }}" enctype="multipart/form-data"  novalidate>
				{{ csrf_field() }}
				<div class="form-group">
					<label for="">Lista de contactos</label>
					<div class="custom-file">
						<input required accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" type="file" class="custom-file-input" name="upload-contact" id="upload_contact">
						<label class="custom-file-label" for="upload_contact" data-text="Seleccione el archivo excel para cargar contactos">Seleccione el archivo excel para cargar contactos</label>
					</div>
					<div class="invalid-feedback">
						Favor de elegir el archivo de los contactos.
					</div>
				</div>
				<button type="submit" data-loading="Modificando<i class='fa fa-cog fa-spin'></i>" class="btn btn-secondary">Cargar <i class="fa fa-upload"></i></button>
			</form>

		</div>
	</div>
</div>
@endsection
@section("after_scripts")

<script >
	$(function(){
		if($("#message_flash").data("messageSuccess") != "" && $("#message_flash").data("messageSuccess") != undefined){
			new PNotify({
				title: "Correcto",
				text:  $("#message_flash").data("messageSuccess"),
				type:"success"
			});
		}

		if($("#message_flash").data("messageError") != "" && $("#message_flash").data("messageError") != undefined){
			new PNotify({
				title: "Ups!",
				text:  $("#message_flash").data("messageError"),
				type:"error"
			});
		}
		$("#upload_contact").on("change", function(event){
			var $filename = $(this).val().split(/(\\|\/)/g).pop();
			$label = $(this).parent().find("label.custom-file-label");
			if($filename == ""){
				$label.text($label.data("text")) 
			}else{
				$label.text($filename);
			}
		});

		$("#formUploadContacts").on("submit", function(event){
			form = $(this)[0];
			if (form.checkValidity() === false) {
	          	event.preventDefault();
	          	event.stopPropagation();
	          	form.classList.add('was-validated');
	          	return;
	        }else{
	        	form.classList.add('was-validated');
	        }

			var $this = $(this);
			var $button = $this.find("button[type='submit']");
			var $data = $this.serializeArray();
			var $action = $this.attr("action");
			var $method = $this.attr("method");
			/*
			$.ajax({
			    url: $action,
			    data: $data,
			    processData: false,
			    dataType: "json",
			    method: "POST",
			    success: function(response) {
			        //Acciones si success
			    },
			    error: function () {
			        //Acciones si error
			    }
			}); */
			$button.data("text", $button.html());
			$button.html($button.data("loading"));
			$button.prop("disabled", true);
			setTimeout(function(){
				$button.prop("disabled", false);
				$button.html($button.data("text"));
			}, 60000);
		});
	});

</script>
@endsection